var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var config = require('./webpack.base.config.js')

var ip = 'localhost'

config.devtool = "#eval-source-map"

// update app version
let fs = require('fs');
let data = fs.readFileSync('build-config.json', 'utf8')
build_config = JSON.parse(data.toString());
build_config.app_version += 0.0001;
fs.writeFileSync('build-config.json', JSON.stringify(build_config), function (err, data) {
    if (err) throw err;
});



config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new BundleTracker({filename: './webpack-stats-local.json'}),
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('development'),
            'BASE_API_URL': JSON.stringify('http://' + ip + ':8000/dashboard/api/'),
            'PUBLIC_URL': JSON.stringify('dashboard'),
            'APP_VERSION': build_config.app_version
        }
    }),
]);

config.module.loaders.push(
    {test: /\.jsx?$/, exclude: /node_modules/, loaders: ['react-hot-loader', 'babel-loader']}
);

config.entry = {
    ConsoleApp: [
        'webpack-dev-server/client?http://' + ip + ':3000',
        'webpack/hot/only-dev-server',
        './web/react/reactjs/ConsoleApp',
    ],
    CustomerApp: [
        'webpack-dev-server/client?http://' + ip + ':3000',
        'webpack/hot/only-dev-server',
        './web/react/reactjs/CustomerApp',
    ],
    // MapApp: [
    //     'webpack-dev-server/client?http://' + ip + ':3000',
    //     'webpack/hot/only-dev-server',
    //     './web/react/reactjs/MapApp',
    // ],
    EnduserApp: [
        'webpack-dev-server/client?http://' + ip + ':3000',
        'webpack/hot/only-dev-server',
        './web/react/reactjs/EnduserApp',
    ]
};

config.output.publicPath = 'http://' + ip + ':3000' + '/assets/bundles/';

module.exports = config;