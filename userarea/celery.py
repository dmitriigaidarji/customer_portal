from __future__ import print_function
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from kombu import Exchange, Queue
from userarea import constants

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'userarea.settings')

app = Celery(str('userarea'), broker=constants.CELERY_BROKER_URL)

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.task_serializer = 'json'

default_exchange = Exchange('default', type='direct')

app.conf.task_queues = (
    Queue('default', default_exchange, routing_key='default'),
)
app.conf.task_default_queue = 'default'
app.conf.task_default_exchange = 'default'
app.conf.task_default_routing_key = 'default'
#
# app.conf.task_routes = {
#     'process_certificates': {'queue': 'certificates', 'routing_key': 'certificates'},
#     'process_failed_certificates': {'queue': 'certificates', 'routing_key': 'certificates'},
#     'write_data': {'queue': 'service_quality', 'routing_key': 'service_quality'},
# }

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()



