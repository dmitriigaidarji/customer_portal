class CoreRouter(object):
    """
    A router to control all database operations
    """

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'radius'\
                or model._meta.app_label == 'provision':
            return model._meta.app_label
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'radius':
            return 'radius'
        elif model._meta.app_label == 'provision':
            return None
        else:
            return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        # if obj1._meta.app_label == 'radius' or \
        #    obj2._meta.app_label == 'radius':
        #    return False
        return True

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == 'default':
            return True
        return False