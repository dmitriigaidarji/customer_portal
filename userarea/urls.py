"""userarea URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from landing.views import Landing, ThanksForRegister, RemoveAccount, Terms
from web.views import UnmatchedRedirect, PasswordResetConfirmView, CheckIfAlive, ApplicationEvents, ReferralConfirm

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard/', include('web.urls')),
    url(r'^api/', include('web.urlsrestapi')),
    url(r'^referral/(?P<token>.+)/$', ReferralConfirm.as_view(), name='referrals'),
    # url(r'^stats/', include('console.urls')),
    url(r'^website-status/$', CheckIfAlive.as_view()),
    url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(),
        name='reset_password_confirm'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^thanks/', ThanksForRegister.as_view(), name='thanks'),
    url(r'^unsubscribe/', RemoveAccount.as_view(), name='unsub'),
    url(r'^terms/', Terms.as_view(), name='terms'),
    url(r'^events/', ApplicationEvents.as_view(), name='events'),
    url(r'^$', Landing.as_view(), name='landing'),
    # url(r'^', UnmatchedRedirect.as_view(), name='default')
]
