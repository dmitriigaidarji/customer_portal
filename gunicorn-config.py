"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count


def max_workers():
    return cpu_count()


bind = 'unix:customer_portal.sock'
max_requests = 1000
workers = 2
errorlog = 'logs/gun-error.log'
accesslog = 'logs/gun-access.log'