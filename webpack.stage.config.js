var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.base.config.js')

config.output.path = require('path').resolve('./djreact/static/bundles/stage/')
config.output.publicPath = '/static/bundles/stage/';

config.plugins = config.plugins.concat([
    new BundleTracker({filename: './webpack-stats-stage.json'}),

    // removes a lot of debugging code in React
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('staging'),
            'BASE_API_URL': JSON.stringify('https://portal.roamvu.com/dashboard/api/'),
            'PUBLIC_URL': JSON.stringify('dashboard')
        }
    }),

    // keeps hashes consistent between compilations
    new webpack.optimize.OccurrenceOrderPlugin(),

    // minifies your code
    new webpack.optimize.UglifyJsPlugin({
        compressor: {
            warnings: false
        }
    })
])

// Add a loader for JSX files
config.module.loaders.push(
    {test: /\.jsx?$/, exclude: /node_modules/, loaders: ['babel-loader']}
);

module.exports = config