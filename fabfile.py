# from fabric.api import local
from fabric.api import *

env.activate = 'source env/bin/activate'

@task
def static():
    local('./manage.py collectstatic --settings=userarea.localsettings --no-input')

@task
def webpack():
    local('rm -rf djreact/static/bundles/stage/*')
    local('rm -rf djreact/static/bundles/prod/*')
    local('node_modules/.bin/webpack --config webpack.stage.config.js --progress --colors')
    local('node_modules/.bin/webpack --config webpack.prod.config.js --progress --colors')
    static()

@task
def webpack_stage():
    local('rm -rf djreact/static/bundles/stage/*')
    local('node_modules/.bin/webpack --config webpack.stage.config.js --progress --colors')
    static()
    
@task
def webpack_prod():
    local('rm -rf djreact/static/bundles/prod/*')
    local('rm -rf static/bundles/prod/*')
    local('node_modules/.bin/webpack --config webpack.prod.config.js --progress --colors')
    static()
