from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from landing.models import Subscriber, Quiz
from django.template import loader


class Landing(View):
    def get(self, request):
        tokenToDelete = request.GET.get('token', None)
        if tokenToDelete:
            try:
                Subscriber.objects.get(id=tokenToDelete).delete()
            except:
                pass
        token = request.session.get('landing_token', '')
        name = ''
        if token != '':
            try:
                user = Subscriber.objects.get(id=token)
                if user.name is not None and user.name != '':
                    name = user.name
                else:
                    name = user.email
            except:
                request.session['landing_token'] = ''

        return render(request, 'landing.html', {'token': token, 'name': name})

    def post(self, request):
        request.session.set_expiry(5000000)
        email = request.POST.get('email', None)
        if email:
            min_age = request.POST.get('min_age', None)
            if min_age:
                min_age = int(min_age)
            max_age = request.POST.get('max_age', None)
            if max_age:
                max_age = int(max_age)
            age = request.POST.get('age', None)
            if age:
                if age == "21":
                    max_age = 21
                elif age == "40":
                    min_age = 21
                    max_age = 40
                elif age == "41":
                    min_age = 41
            timezone = request.POST.get('timezone', None)
            if timezone:
                timezone = int(timezone)
            verified = request.POST.get('verified')
            if verified == 'true':
                verified = True
            else:
                verified = False

            q1 = request.POST.get('q1', None)
            if q1 == 'jp':
                q1 = '0'
            elif q1 == 'hk':
                q1 = '1'
            elif q1 == 'no':
                q1 = '2'

            if Subscriber.objects.filter(email=email).exists():
                sub = Subscriber.objects.get(email=email)
                sub.name = request.POST.get('name', sub.name)
                sub.surname = request.POST.get('surname', sub.surname)
                if min_age is not None:
                    sub.min_age = min_age
                    sub.max_axe = max_age
                sub.gender = request.POST.get('gender', sub.gender)
                sub.facebook_id = request.POST.get('id', sub.facebook_id)
                sub.locale = request.POST.get('locale', sub.locale)
                if timezone is not None:
                    sub.timezone = timezone
                sub.link = request.POST.get('link', sub.link)
                sub.picture = request.POST.get('picture', sub.picture)
                sub.updated_time = request.POST.get('updated_time', sub.updated_time)
                sub.platform = request.POST.get('platform', sub.platform)
                if verified:
                    sub.verified = verified
                if q1:
                    if hasattr(sub, 'quiz'):
                        quiz = sub.quiz
                        quiz.going_to_HK_or_JP_date = q1
                        quiz.save()
                    else:
                        Quiz.objects.create(
                            user=sub,
                            going_to_HK_or_JP_date=q1
                        )
                sub.save()
            else:
                sub = Subscriber.objects.create(email=email,
                                                name=request.POST.get('name', None),
                                                surname=request.POST.get('surname', None),
                                                min_age=min_age,
                                                max_age=max_age,
                                                gender=request.POST.get('gender', None),
                                                facebook_id=request.POST.get('id', None),
                                                locale=request.POST.get('locale', None),
                                                timezone=timezone,
                                                link=request.POST.get('link', None),
                                                picture=request.POST.get('picture', None),
                                                updated_time=request.POST.get('updated_time', None),
                                                platform=request.POST.get('platform', None),
                                                verified=verified
                                                )
                if q1:
                    Quiz.objects.create(
                        user=sub,
                        going_to_HK_or_JP_date=q1
                    )
                sendSubEmail(sub)
            request.session['landing_token'] = str(sub.id)
            return JsonResponse({'message': 'ok', 'token': sub.id})
        else:
            return JsonResponse({'message': 'no'})

    def delete(self, request):
        if 'token=' in request.body:
            split = request.body.split('&')
            for pair in split:
                items = pair.split('=')
                if items[0] == 'token':
                    token = items[1]
                    Subscriber.objects.get(id=token).delete()
                    return JsonResponse({'message': 'ok'})
        return JsonResponse({'message': 'no'})


class Terms(View):
    def get(self, request):
        return render(request, 'landing/terms.html')

class ThanksForRegister(View):
    def get(self, request):
        return render(request, 'thanks.html', {'username': request.GET.get('username', None)})


class RemoveAccount(View):
    def get(self, request):
        tokenToDelete = request.GET.get('token', None)
        if tokenToDelete:
            try:
                Subscriber.objects.get(id=tokenToDelete).delete()
            except:
                pass
        return render(request, 'landing/delete.html')


def sendSubEmail(user):
    c = {
        'name': user.name,
        'cancel_link': 'https://portal.roamvu.com{}?token={}'.format(reverse('unsub'), user.id)
    }
    subject_template_name = 'landing/email/subscriber.txt'
    email_template_name = 'landing/email/subscriber.html'
    subject = loader.render_to_string(subject_template_name, c)
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)
    msg = EmailMultiAlternatives(subject, email, 'support@roamvu.com', [user.email])
    msg.attach_alternative(email, "text/html")
    msg.send()