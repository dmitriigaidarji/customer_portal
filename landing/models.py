from __future__ import unicode_literals

import uuid as uuid
from django.db import models

class Subscriber(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=255, null=True)
    surname = models.CharField(max_length=255, null=True)
    min_age = models.IntegerField(null=True)
    max_age = models.IntegerField(null=True)
    picture = models.URLField(null=True)
    gender = models.CharField(max_length=10, null=True)
    facebook_id = models.CharField(max_length=20, null=True)
    link = models.URLField(null=True)
    locale = models.CharField(max_length=10, null=True)
    timezone = models.IntegerField(null=True)
    updated_time = models.CharField(max_length=30, null=True)
    verified = models.BooleanField(default=False)
    platform = models.CharField(max_length=10, null=True)
    # token = models.CharField(max_length=32, default=set_string)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


    #Beta fields
    status_CHOICES = (
        ('0', 'New'),
        ('1', 'Rejected'),
        ('2', 'Accepted'),
        ('3', 'Offered'),
        ('4', 'Started'),
        ('5', 'Active'),
        ('6', 'Ended')
    )
    status = models.CharField(max_length=15, choices=status_CHOICES, default='0')
    test_flight = models.BooleanField(default=False)
    roamvu_instructions = models.BooleanField(default=False)
    instructions_send_date = models.DateTimeField(null=True)
    expiration_date = models.DateTimeField(null=True)
    followup_message_sent = models.BooleanField(default=False)
    thank_you_message_sent = models.BooleanField(default=False)
    beta_notes = models.TextField(default='')


class Quiz(models.Model):
    user = models.OneToOneField(Subscriber)
    going_to_HK_or_JP_date_CHOICES = (
        ('0', 'JP'),
        ('1', 'HK'),
        ('2', 'No')
    )
    going_to_HK_or_JP_date = models.CharField(max_length=2, choices=going_to_HK_or_JP_date_CHOICES, default='2')

