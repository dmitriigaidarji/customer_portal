var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.base.config.js')

config.output.path = require('path').resolve('./djreact/static/bundles/prod/')
config.output.publicPath = '/static/bundles/prod/'
// update app version
let fs = require('fs');
let data = fs.readFileSync('build-config.json', 'utf8')
build_config = JSON.parse(data.toString());
build_config.app_version += 0.001;
fs.writeFileSync('build-config.json', JSON.stringify(build_config), function (err, data) {
    if (err) throw err;
});


config.plugins = config.plugins.concat([
    new BundleTracker({filename: './webpack-stats-prod.json'}),

    // removes a lot of debugging code in React
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production'),
            'BASE_API_URL': JSON.stringify('https://portal.roamvu.com/dashboard/api/'),
            'PUBLIC_URL': JSON.stringify('dashboard'),
            'APP_VERSION': build_config.app_version
        }
    }),

    // keeps hashes consistent between compilations
    new webpack.optimize.OccurrenceOrderPlugin(),

    // minifies your code
    new webpack.optimize.UglifyJsPlugin({
        compressor: {
            warnings: false
        }
    })
])

// Add a loader for JSX files
config.module.loaders.push(
    {test: /\.jsx?$/, exclude: /node_modules/, loaders: ['babel-loader']}
)

module.exports = config