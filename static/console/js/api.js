function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
    }
});

BASE_API_URL = location.origin;
API_PREFIX = '/stats/console-api'
API_URL = BASE_API_URL + API_PREFIX

LOCAL = true;

function getStats(customer_id, month, callback){
    if (LOCAL){
        callback({
            data :
            '[{"provider_id": "700001", "request": 14, "accept": 14, "uniq_users": 4, "sources": ["110.79.14.115"], "country_code": "1", "open": 4, "cui_resolved": 14, "start": 12, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 27155, "stop": 9, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488412800000}, "provider_country": "US", "deny": 0, "bytes": 33968792, "token": 14, "domains": 1, "_id": {"$oid": "58bdf80a42f9710f5a50856d"}}, {"provider_id": "700001", "request": 24, "accept": 24, "uniq_users": 7, "sources": ["110.79.14.115"], "country_code": "1", "open": 2, "cui_resolved": 23, "start": 23, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 13751, "stop": 21, "update": 0, "cui_unresolved": 1, "anonymous": 0, "date": {"$date": 1488499200000}, "provider_country": "US", "deny": 0, "bytes": 108206551, "token": 23, "domains": 1, "_id": {"$oid": "58bdf80b42f9710f5a508646"}}, {"provider_id": "700001", "request": 8, "accept": 8, "uniq_users": 4, "sources": ["110.79.14.115"], "country_code": "1", "open": 1, "cui_resolved": 8, "start": 7, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 164102, "stop": 8, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488585600000}, "provider_country": "US", "deny": 0, "bytes": 11151010, "token": 8, "domains": 1, "_id": {"$oid": "58bdf80d42f9710f5a5087e6"}}, {"provider_id": "700001", "request": 1, "accept": 1, "uniq_users": 1, "sources": ["110.79.14.115"], "country_code": "1", "open": 0, "cui_resolved": 1, "start": 1, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 599, "stop": 1, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488672000000}, "provider_country": "US", "deny": 0, "bytes": 4782, "token": 1, "domains": 1, "_id": {"$oid": "58bdf80e42f9710f5a508855"}}, {"provider_id": "700001", "request": 10, "accept": 10, "uniq_users": 5, "sources": ["110.79.14.115"], "country_code": "1", "open": 1, "cui_resolved": 10, "start": 10, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 95208, "stop": 9, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488758400000}, "provider_country": "US", "deny": 0, "bytes": 975288109, "token": 10, "domains": 1, "_id": {"$oid": "58bdf80e42f9710f5a508866"}}, {"provider_id": "700001", "request": 12, "accept": 9, "uniq_users": 3, "sources": ["110.79.14.115"], "country_code": "1", "open": 2, "cui_resolved": 9, "start": 9, "reject": 2, "provider_name": "RoamVU Inc.", "seconds": 19051, "stop": 8, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488844800000}, "provider_country": "US", "deny": 1, "bytes": 134536583, "token": 9, "domains": 1, "_id": {"$oid": "58bf55a542f971700f1c702b"}}, {"provider_id": "700001", "request": 17, "accept": 17, "uniq_users": 4, "sources": ["110.79.14.115"], "country_code": "1", "open": 0, "cui_resolved": 17, "start": 17, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 75404, "stop": 19, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1488931200000}, "provider_country": "US", "deny": 0, "bytes": 512535826, "token": 17, "domains": 2, "_id": {"$oid": "58c0ac1442f9712177833071"}}, {"provider_id": "700001", "request": 20, "accept": 20, "uniq_users": 3, "sources": ["110.79.14.115"], "country_code": "1", "open": 0, "cui_resolved": 20, "start": 20, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 25230, "stop": 20, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1489017600000}, "provider_country": "US", "deny": 0, "bytes": 243895195, "token": 20, "domains": 2, "_id": {"$oid": "58c1ee9b42f9715c8525c9d4"}}, {"provider_id": "700001", "request": 35, "accept": 31, "uniq_users": 5, "sources": ["110.79.14.115"], "country_code": "1", "open": 1, "cui_resolved": 31, "start": 31, "reject": 4, "provider_name": "RoamVU Inc.", "seconds": 26029, "stop": 30, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1489104000000}, "provider_country": "US", "deny": 0, "bytes": 18127287, "token": 31, "domains": 2, "_id": {"$oid": "58c73fe442f9716b71ef02f8"}}, {"provider_id": "700001", "request": 5, "accept": 5, "uniq_users": 2, "sources": ["110.79.14.115"], "country_code": "1", "open": 0, "cui_resolved": 5, "start": 5, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 21919, "stop": 5, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1489190400000}, "provider_country": "US", "deny": 0, "bytes": 45308337, "token": 5, "domains": 1, "_id": {"$oid": "58c73fe642f9716b71ef044f"}}, {"provider_id": "700001", "request": 231, "accept": 230, "uniq_users": 5, "sources": ["110.79.14.115"], "country_code": "1", "open": 18, "cui_resolved": 230, "start": 41, "reject": 1, "provider_name": "RoamVU Inc.", "seconds": 33163, "stop": 23, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1489363200000}, "provider_country": "US", "deny": 0, "bytes": 111600172, "token": 230, "domains": 2, "_id": {"$oid": "58c73fe742f9716b71ef04b1"}}, {"provider_id": "700001", "request": 177, "accept": 176, "uniq_users": 5, "sources": ["110.79.14.115"], "country_code": "1", "open": 2, "cui_resolved": 176, "start": 24, "reject": 0, "provider_name": "RoamVU Inc.", "seconds": 31249, "stop": 23, "update": 0, "cui_unresolved": 0, "anonymous": 0, "date": {"$date": 1489449600000}, "provider_country": "US", "deny": 1, "bytes": 66533249, "token": 176, "domains": 2, "_id": {"$oid": "58c895c342f9713e905fa6a4"}}]'
        });
        return;
    }

  $.ajax({
    url : API_URL +  '/stats/{0}/{1}/'.format(customer_id, month),
    type : "GET",
    success : callback
  });
}