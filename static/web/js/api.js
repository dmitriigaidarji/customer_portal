function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
    }
});

BASE_API_URL = location.origin;

function resendCode(code, callback){
  $.ajax({
    url : BASE_API_URL + '/dashboard/resend/',
    type : "POST",
    data : {code : code},
    success : callback
  });
}