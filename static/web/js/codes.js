String.prototype.splice = function(idx, rem, str) {
        return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
    };
function cloneMore(selector, type) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + type + '-TOTAL_FORMS').val();
    newElement.find(':input').each(function() {
        var name = $(this).attr('name').replace('-' + (total-1) + '-','-' + total + '-');
        var id = 'id_' + name;
        console.log(this.nodeName);
        if (this.nodeName == "SELECT") $(this).attr({'name': name, 'id': id});
            else $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
    });
    newElement.find('label').each(function() {
        var newFor = $(this).attr('for').replace('-' + (total-1) + '-','-' + total + '-');
        $(this).attr('for', newFor);
    });
    total++;
    $('#id_' + type + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
}
$(document).ready(function () {
    $('.add-row').click(function() {
                cloneMore('.form-group-container:last', 'form');
        });
        $('#sub').click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            $.each($('.form-group-container'), function(index, item){
                count = 0;
                $.each($(item).find(':input'), function(index2, input){
                    if (input.value.length > 0) count ++;
                });
                if (count < 2 && $('.form-group-container').length > 1) $(item).remove()
            });
            $('#id_form-TOTAL_FORMS').val($('.form-group-container').length);
            $('.form-group-container').each(function (index) {
                $(this).find(':input').each(function() {
                    console.log(this);
                    var stname =  $(this).attr('name');
                    var thenum = stname.replace(/[^0-9]/g, '');
                    var stindex = stname.indexOf(thenum);
                    var name = stname.splice(stindex, thenum.length, index);
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id});
                });
                $(this).find('label').each(function() {
                    var stname =  $(this).attr('for');
                    var thenum = stname.replace(/[^0-9]/g, '');
                    var stindex = stname.indexOf(thenum);
                    var name = stname.splice(stindex, thenum.length, index);
                    $(this).attr('for', name);
                });
            });

            // $.each($('.form-group-container'), function(index, item){
            //     $.each($(item).find(':input'), function(index2, input){
            //         var stname =  $(input).attr('name');
            //         var thenum = stname.replace(/[^0-9]/g, '');
            //         var stindex = stname.indexOf(thenum);
            //         console.log(stname, stindex, thenum, index)
            //         stname = stname.splice(stindex, thenum.length, index);
            //         var name = stname;
            //         var id = 'id_' + name;
            //         $(input).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            //     });
            //     $.each($(item).find('label'), function(index2, input){
            //         var stname =  $(input).attr('for');
            //         var thenum = stname.replace( /^\D+/g, '');
            //         var stindex = stname.indexOf(thenum);
            //         stname = stname.splice(stindex, thenum.length, index);
            //         var name = stname;
            //         $(input).attr('for', name)
            //     });
            // });
            //
            $('#sendCodes').submit()
        })
    $('.resendbtn').on('click',function (event) {
        $this = $(this)
        if (!$this.hasClass('disabled')){
            $this.addClass('disabled');
            code = $this.parents('tr').children()[0].innerHTML;
            resendCode(code, function (response) {
                console.log(response);
            })
        }
    });
});
