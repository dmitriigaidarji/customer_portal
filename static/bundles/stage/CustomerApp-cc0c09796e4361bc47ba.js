webpackJsonp([1],{

/***/ 1605:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(18);

var _radium = __webpack_require__(145);

var _reactRouterDom = __webpack_require__(11);

var _MainController = __webpack_require__(739);

var _MainController2 = _interopRequireDefault(_MainController);

__webpack_require__(202);

var _redux = __webpack_require__(146);

var _reactRedux = __webpack_require__(199);

var _reduxThunk = __webpack_require__(201);

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _reduxPersist = __webpack_require__(200);

var _reducers = __webpack_require__(198);

var reducers = _interopRequireWildcard(_reducers);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import 'bootstrap/dist/css/bootstrap.css';


var finalCreateStore = (0, _redux.compose)((0, _redux.applyMiddleware)(_reduxThunk2.default), (0, _reduxPersist.autoRehydrate)(), window.devToolsExtension ? window.devToolsExtension() : function (f) {
    return f;
})(_redux.createStore);
var reducer = (0, _redux.combineReducers)(reducers);
var store = finalCreateStore(reducer);
// store.dispatch({'type':'INCREASE'});
//     store.dispatch({'type':'INCREASE'});
(0, _reduxPersist.persistStore)(store);
console.log('customer');

var CustomerApp = function (_React$Component) {
    _inherits(CustomerApp, _React$Component);

    function CustomerApp() {
        _classCallCheck(this, CustomerApp);

        return _possibleConstructorReturn(this, (CustomerApp.__proto__ || Object.getPrototypeOf(CustomerApp)).apply(this, arguments));
    }

    _createClass(CustomerApp, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                _reactRedux.Provider,
                { store: store },
                _react2.default.createElement(
                    _radium.StyleRoot,
                    null,
                    _react2.default.createElement(
                        _reactRouterDom.BrowserRouter,
                        null,
                        _react2.default.createElement(_reactRouterDom.Route, { path: "/", name: "main", component: _MainController2.default })
                    )
                )
            );
        }
    }]);

    return CustomerApp;
}(_react2.default.Component);

(0, _reactDom.render)(_react2.default.createElement(CustomerApp, null), document.getElementById('App1'));

/***/ }),

/***/ 739:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__(11);

var _CustomerMonthly = __webpack_require__(881);

var _CustomerMonthly2 = _interopRequireDefault(_CustomerMonthly);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MainController = function (_React$Component) {
    _inherits(MainController, _React$Component);

    function MainController() {
        _classCallCheck(this, MainController);

        var _this2 = _possibleConstructorReturn(this, (MainController.__proto__ || Object.getPrototypeOf(MainController)).call(this));

        _this2.state = {
            providers: undefined,
            customers: undefined
        };
        return _this2;
    }

    _createClass(MainController, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            var _this = this;
            _Api2.default.get("https://portal.roamvu.com/dashboard/api/" + 'providers/').then(function (res) {
                _this.setState({ providers: res.data.results });
            });
            _Api2.default.get("https://portal.roamvu.com/dashboard/api/" + 'customers/').then(function (res) {
                _this.setState({ customers: res.data.results });
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            return _react2.default.createElement(
                "div",
                null,
                _react2.default.createElement(
                    _reactRouterDom.Switch,
                    null,
                    _react2.default.createElement(_reactRouterDom.Route, { path: "/" + "dashboard" + "/customers/" + CONST_CUSTOMER_ID + "/:user_hash/:year/:month", render: function render(props) {
                            if (_this3.state.customers != undefined && _this3.state.providers != undefined) return _react2.default.createElement(_CustomerMonthly2.default, _extends({}, props, { customers: _this3.state.customers,
                                providers: _this3.state.providers }));else return _react2.default.createElement(CustomersNoMatch, _extends({}, props, { customers: _this3.state.customers }));
                        } }),
                    _react2.default.createElement(_reactRouterDom.Route, { render: function render(props) {
                            return _react2.default.createElement(CustomersNoMatch, _extends({}, props, { customers: _this3.state.customers,
                                providers: _this3.state.providers }));
                        } })
                )
            );
        }
    }]);

    return MainController;
}(_react2.default.Component);

exports.default = MainController;
;

var CustomersNoMatch = function (_React$Component2) {
    _inherits(CustomersNoMatch, _React$Component2);

    function CustomersNoMatch() {
        _classCallCheck(this, CustomersNoMatch);

        return _possibleConstructorReturn(this, (CustomersNoMatch.__proto__ || Object.getPrototypeOf(CustomersNoMatch)).apply(this, arguments));
    }

    _createClass(CustomersNoMatch, [{
        key: "render",
        value: function render() {
            var _props = this.props,
                customers = _props.customers,
                providers = _props.providers;

            if (customers != undefined && providers != undefined) {
                var date = new Date();
                var month = date.getMonth() + 1;
                if (month < 10) month = '0' + month;
                var url = "/" + "dashboard" + "/customers/" + CONST_CUSTOMER_ID + "/-/" + date.getFullYear() + "/" + month + "/";
                return _react2.default.createElement(_reactRouterDom.Redirect, { to: url });
            }
            return _react2.default.createElement(
                "div",
                null,
                "Loading customers..."
            );
        }
    }]);

    return CustomersNoMatch;
}(_react2.default.Component);

/***/ }),

/***/ 876:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _rv_logo = __webpack_require__(50);

var _rv_logo2 = _interopRequireDefault(_rv_logo);

__webpack_require__(46);

var _reactDates = __webpack_require__(53);

__webpack_require__(57);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _jquery = __webpack_require__(19);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderDaily = function (_React$Component) {
    _inherits(HeaderDaily, _React$Component);

    function HeaderDaily(props) {
        _classCallCheck(this, HeaderDaily);

        var _this = _possibleConstructorReturn(this, (HeaderDaily.__proto__ || Object.getPrototypeOf(HeaderDaily)).call(this, props));

        _this.toggle = _this.toggle.bind(_this);
        _this.toggleProviderDropdown = _this.toggleProviderDropdown.bind(_this);
        _this.toggleLinksDropdown = _this.toggleLinksDropdown.bind(_this);
        _this.isDayBlocked = _this.isDayBlocked.bind(_this);
        _this.allowedDays = [];
        if (props.active_dates != undefined) props.active_dates.map(function (date) {
            _this.allowedDays.push(date.format('YYYYMMDD'));
        });
        _this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false
        };
        return _this;
    }

    _createClass(HeaderDaily, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                isOpen: !this.state.isOpen
            });
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps) {
            var _this2 = this;

            if (nextProps.active_dates != undefined) {
                this.allowedDays = [];
                nextProps.active_dates.map(function (date) {
                    _this2.allowedDays.push(date.format('YYYYMMDD'));
                });
            }
        }
    }, {
        key: 'isDayBlocked',
        value: function isDayBlocked(date) {
            return !this.allowedDays.includes(date.format('YYYYMMDD'));
        }
    }, {
        key: 'toggleProviderDropdown',
        value: function toggleProviderDropdown() {
            this.setState({
                providerDropdownOpen: !this.state.providerDropdownOpen
            });
        }
    }, {
        key: 'toggleLinksDropdown',
        value: function toggleLinksDropdown() {
            this.setState({
                linksDropdownOpen: !this.state.linksDropdownOpen
            });
        }
    }, {
        key: 'isOutsideRange',
        value: function isOutsideRange(date) {
            return (0, _moment2.default)() < date;
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            (0, _jquery2.default)('.elddi').hover(function (e) {
                (0, _jquery2.default)('.elddi').removeClass('eldd');
            }, function () {
                (0, _jquery2.default)('.elddi').addClass('eldd');
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var props = this.props;
            var customers = props.customers,
                customer = props.customer;

            var backurl = '/' + "dashboard" + '/customers/' + this.props.customer.id + '/-/' + this.props.current_day.format('YYYY') + '/' + this.props.current_day.format('MM') + '/';
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactstrap.Navbar,
                    { color: 'faded', light: true, toggleable: true, fixed: 'top', className: 'consoleNav' },
                    _react2.default.createElement(
                        'div',
                        { style: { marginRight: '5px' } },
                        _react2.default.createElement(
                            _reactRouterDom.Link,
                            { to: backurl },
                            '<Monthly'
                        )
                    ),
                    _react2.default.createElement(_reactstrap.NavbarToggler, { right: true, onClick: this.toggle }),
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { className: 'navbar-brand', to: '/' + "dashboard" + '/' },
                        _react2.default.createElement('img', { src: _rv_logo2.default, width: '30', height: '30',
                            className: 'd-inline-block align-top roamvuNavLogo', alt: '' }),
                        'RoamVU Console'
                    ),
                    _react2.default.createElement(
                        _reactstrap.Collapse,
                        { isOpen: this.state.isOpen, navbar: true },
                        _react2.default.createElement(
                            _reactstrap.Nav,
                            { navbar: true },
                            _react2.default.createElement(_reactDates.SingleDatePicker, {
                                date: this.props.current_day // momentPropTypes.momentObj or null,
                                , onDateChange: this.props.handleDayChange.bind(this) // PropTypes.func.isRequired,
                                , focused: this.state.focused // PropTypes.bool
                                , onFocusChange: function onFocusChange(_ref) {
                                    var focused = _ref.focused;
                                    return _this3.setState({ focused: focused });
                                } // PropTypes.func.isRequired
                                , isOutsideRange: this.isOutsideRange,
                                onPrevMonthClick: props.handleMonthSubstract.bind(this, props.match.params.day + '/'),
                                onNextMonthClick: props.handleMonthAdd.bind(this, props.match.params.day + '/'),
                                numberOfMonths: 1,
                                isDayBlocked: this.isDayBlocked,
                                displayFormat: 'YYYY/MM/DD'
                            })
                        )
                    )
                )
            );
        }
    }]);

    return HeaderDaily;
}(_react2.default.Component);

exports.default = HeaderDaily;

/***/ }),

/***/ 877:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _rv_logo = __webpack_require__(50);

var _rv_logo2 = _interopRequireDefault(_rv_logo);

__webpack_require__(46);

var _reactDates = __webpack_require__(53);

__webpack_require__(57);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _jquery = __webpack_require__(19);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderMonthly = function (_React$Component) {
    _inherits(HeaderMonthly, _React$Component);

    function HeaderMonthly(props) {
        _classCallCheck(this, HeaderMonthly);

        var _this = _possibleConstructorReturn(this, (HeaderMonthly.__proto__ || Object.getPrototypeOf(HeaderMonthly)).call(this, props));

        _this.toggle = _this.toggle.bind(_this);
        _this.toggleProviderDropdown = _this.toggleProviderDropdown.bind(_this);
        _this.toggleLinksDropdown = _this.toggleLinksDropdown.bind(_this);
        _this.allowedDays = [];
        if (props.source_stats != undefined) props.source_stats.map(function (stat) {
            _this.allowedDays.push((0, _moment2.default)(parseInt(stat.date.$date)).format('YYYYMMDD'));
        });
        _this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined
        };
        return _this;
    }

    _createClass(HeaderMonthly, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                isOpen: !this.state.isOpen
            });
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps) {
            var _this2 = this;

            if (nextProps.source_stats != undefined) {
                this.allowedDays = [];
                nextProps.source_stats.map(function (stat) {
                    _this2.allowedDays.push((0, _moment2.default)(parseInt(stat.date.$date)).format('YYYYMMDD'));
                });
            }
        }
    }, {
        key: 'toggleProviderDropdown',
        value: function toggleProviderDropdown() {
            this.setState({
                providerDropdownOpen: !this.state.providerDropdownOpen
            });
        }
    }, {
        key: 'toggleLinksDropdown',
        value: function toggleLinksDropdown() {
            this.setState({
                linksDropdownOpen: !this.state.linksDropdownOpen
            });
        }
    }, {
        key: 'isOutsideRange',
        value: function isOutsideRange(date) {
            return (0, _moment2.default)() < date;
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            (0, _jquery2.default)('.elddi').hover(function (e) {
                (0, _jquery2.default)('.elddi').removeClass('eldd');
            }, function () {
                (0, _jquery2.default)('.elddi').addClass('eldd');
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var props = this.props;
            var customers = props.customers,
                customer = props.customer;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactstrap.Navbar,
                    { color: 'faded', light: true, toggleable: true, fixed: 'top', className: 'consoleNav' },
                    _react2.default.createElement(_reactstrap.NavbarToggler, { right: true, onClick: this.toggle }),
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { className: 'navbar-brand', to: '/' + "dashboard" + '/' },
                        _react2.default.createElement('img', { src: _rv_logo2.default, width: '30', height: '30',
                            className: 'd-inline-block align-top roamvuNavLogo', alt: '' }),
                        _react2.default.createElement(
                            'span',
                            null,
                            'RoamVU Console'
                        )
                    ),
                    _react2.default.createElement(
                        _reactstrap.Collapse,
                        { isOpen: this.state.isOpen, navbar: true },
                        _react2.default.createElement(
                            _reactstrap.Nav,
                            { navbar: true },
                            _react2.default.createElement(_reactDates.DateRangePicker, {
                                startDate: this.props.startDate // momentPropTypes.momentObj or null,
                                , endDate: this.props.endDate,
                                onDatesChange: props.handleDateChange // PropTypes.func.isRequired,
                                , focusedInput: this.state.focusedInput // PropTypes.bool
                                , onFocusChange: function onFocusChange(focusedInput) {
                                    return _this3.setState({ focusedInput: focusedInput });
                                } // PropTypes.func.isRequired,
                                , isOutsideRange: this.isOutsideRange,
                                onPrevMonthClick: props.handleMonthSubstract.bind(this, undefined),
                                onNextMonthClick: props.handleMonthAdd.bind(this, undefined),
                                numberOfMonths: 1,
                                displayFormat: 'YYYY/MM/DD'
                            })
                        )
                    )
                )
            );
        }
    }]);

    return HeaderMonthly;
}(_react2.default.Component);

exports.default = HeaderMonthly;

/***/ }),

/***/ 878:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _rv_logo = __webpack_require__(50);

var _rv_logo2 = _interopRequireDefault(_rv_logo);

__webpack_require__(46);

var _reactDates = __webpack_require__(53);

__webpack_require__(57);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _jquery = __webpack_require__(19);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderDaily = function (_React$Component) {
    _inherits(HeaderDaily, _React$Component);

    function HeaderDaily(props) {
        _classCallCheck(this, HeaderDaily);

        var _this = _possibleConstructorReturn(this, (HeaderDaily.__proto__ || Object.getPrototypeOf(HeaderDaily)).call(this, props));

        _this.toggle = _this.toggle.bind(_this);
        _this.toggleProviderDropdown = _this.toggleProviderDropdown.bind(_this);
        _this.toggleLinksDropdown = _this.toggleLinksDropdown.bind(_this);
        _this.isDayBlocked = _this.isDayBlocked.bind(_this);
        _this.allowedDays = [];
        if (props.active_dates != undefined) props.active_dates.map(function (date) {
            _this.allowedDays.push(date.format('YYYYMMDD'));
        });
        _this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false
        };
        return _this;
    }

    _createClass(HeaderDaily, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                isOpen: !this.state.isOpen
            });
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps) {
            var _this2 = this;

            if (nextProps.active_dates != undefined) {
                this.allowedDays = [];
                nextProps.active_dates.map(function (date) {
                    _this2.allowedDays.push(date.format('YYYYMMDD'));
                });
            }
        }
    }, {
        key: 'isDayBlocked',
        value: function isDayBlocked(date) {
            return !this.allowedDays.includes(date.format('YYYYMMDD'));
        }
    }, {
        key: 'toggleProviderDropdown',
        value: function toggleProviderDropdown() {
            this.setState({
                providerDropdownOpen: !this.state.providerDropdownOpen
            });
        }
    }, {
        key: 'toggleLinksDropdown',
        value: function toggleLinksDropdown() {
            this.setState({
                linksDropdownOpen: !this.state.linksDropdownOpen
            });
        }
    }, {
        key: 'isOutsideRange',
        value: function isOutsideRange(date) {
            return (0, _moment2.default)() < date;
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            (0, _jquery2.default)('.elddi').hover(function (e) {
                (0, _jquery2.default)('.elddi').removeClass('eldd');
            }, function () {
                (0, _jquery2.default)('.elddi').addClass('eldd');
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var props = this.props;
            var customers = props.customers,
                customer = props.customer;

            var backurl = '/' + "dashboard" + '/customers/' + props.customer.id + '/' + props.user.nai + '/' + props.current_day.format('YYYY') + '/' + this.props.current_day.format('MM') + '/';
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactstrap.Navbar,
                    { color: 'faded', light: true, toggleable: true, fixed: 'top', className: 'consoleNav' },
                    _react2.default.createElement(
                        'div',
                        { style: { marginRight: '5px' } },
                        _react2.default.createElement(
                            _reactRouterDom.Link,
                            { to: backurl },
                            '<Monthly'
                        )
                    ),
                    _react2.default.createElement(_reactstrap.NavbarToggler, { right: true, onClick: this.toggle }),
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { className: 'navbar-brand', to: '/' + "dashboard" + '/' },
                        _react2.default.createElement('img', { src: _rv_logo2.default, width: '30', height: '30',
                            className: 'd-inline-block align-top roamvuNavLogo', alt: '' }),
                        'RoamVU Console'
                    ),
                    _react2.default.createElement(
                        _reactstrap.Collapse,
                        { isOpen: this.state.isOpen, navbar: true },
                        _react2.default.createElement(
                            _reactstrap.Nav,
                            { navbar: true },
                            _react2.default.createElement(
                                _reactstrap.NavLink,
                                { href: '#' },
                                this.props.user.nai
                            ),
                            _react2.default.createElement(_reactDates.SingleDatePicker, {
                                date: this.props.current_day // momentPropTypes.momentObj or null,
                                , onDateChange: this.props.handleDayChange.bind(this) // PropTypes.func.isRequired,
                                , focused: this.state.focused // PropTypes.bool
                                , onFocusChange: function onFocusChange(_ref) {
                                    var focused = _ref.focused;
                                    return _this3.setState({ focused: focused });
                                } // PropTypes.func.isRequired
                                , isOutsideRange: this.isOutsideRange,
                                onPrevMonthClick: props.handleMonthSubstract.bind(this, props.match.params.day + '/'),
                                onNextMonthClick: props.handleMonthAdd.bind(this, props.match.params.day + '/'),
                                numberOfMonths: 1,
                                isDayBlocked: this.isDayBlocked,
                                displayFormat: 'YYYY/MM/DD'
                            })
                        )
                    )
                )
            );
        }
    }]);

    return HeaderDaily;
}(_react2.default.Component);

exports.default = HeaderDaily;

/***/ }),

/***/ 879:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _rv_logo = __webpack_require__(50);

var _rv_logo2 = _interopRequireDefault(_rv_logo);

__webpack_require__(46);

var _reactDates = __webpack_require__(53);

__webpack_require__(57);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _jquery = __webpack_require__(19);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderMonthly = function (_React$Component) {
    _inherits(HeaderMonthly, _React$Component);

    function HeaderMonthly(props) {
        _classCallCheck(this, HeaderMonthly);

        var _this = _possibleConstructorReturn(this, (HeaderMonthly.__proto__ || Object.getPrototypeOf(HeaderMonthly)).call(this, props));

        _this.toggle = _this.toggle.bind(_this);
        _this.toggleProviderDropdown = _this.toggleProviderDropdown.bind(_this);
        _this.toggleLinksDropdown = _this.toggleLinksDropdown.bind(_this);
        _this.allowedDays = [];
        if (props.source_stats != undefined) props.source_stats.map(function (stat) {
            _this.allowedDays.push((0, _moment2.default)(parseInt(stat.date.$date)).format('YYYYMMDD'));
        });
        _this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined
        };
        return _this;
    }

    _createClass(HeaderMonthly, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                isOpen: !this.state.isOpen
            });
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps) {
            var _this2 = this;

            if (nextProps.source_stats != undefined) {
                this.allowedDays = [];
                nextProps.source_stats.map(function (stat) {
                    _this2.allowedDays.push((0, _moment2.default)(parseInt(stat.date.$date)).format('YYYYMMDD'));
                });
            }
        }
    }, {
        key: 'toggleProviderDropdown',
        value: function toggleProviderDropdown() {
            this.setState({
                providerDropdownOpen: !this.state.providerDropdownOpen
            });
        }
    }, {
        key: 'toggleLinksDropdown',
        value: function toggleLinksDropdown() {
            this.setState({
                linksDropdownOpen: !this.state.linksDropdownOpen
            });
        }
    }, {
        key: 'isOutsideRange',
        value: function isOutsideRange(date) {
            return (0, _moment2.default)() < date;
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            (0, _jquery2.default)('.elddi').hover(function (e) {
                (0, _jquery2.default)('.elddi').removeClass('eldd');
            }, function () {
                (0, _jquery2.default)('.elddi').addClass('eldd');
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var props = this.props;

            var backurl = '/' + "dashboard" + '/customers/' + this.props.customer.id + '/-/' + this.props.startDate.format('YYYY') + '/' + this.props.startDate.format('MM') + '/';
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactstrap.Navbar,
                    { color: 'faded', light: true, toggleable: true, fixed: 'top', className: 'consoleNav' },
                    _react2.default.createElement(
                        'div',
                        { style: { marginRight: '5px' } },
                        _react2.default.createElement(
                            _reactRouterDom.Link,
                            { to: backurl },
                            '<Customer'
                        )
                    ),
                    _react2.default.createElement(_reactstrap.NavbarToggler, { right: true, onClick: this.toggle }),
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { className: 'navbar-brand', to: '/' + "dashboard" + '/' },
                        _react2.default.createElement('img', { src: _rv_logo2.default, width: '30', height: '30',
                            className: 'd-inline-block align-top roamvuNavLogo', alt: '' }),
                        'RoamVU Console'
                    ),
                    _react2.default.createElement(
                        _reactstrap.Collapse,
                        { isOpen: this.state.isOpen, navbar: true },
                        _react2.default.createElement(
                            _reactstrap.Nav,
                            { navbar: true },
                            _react2.default.createElement(
                                _reactstrap.NavLink,
                                { href: '#' },
                                this.props.user.nai
                            ),
                            _react2.default.createElement(_reactDates.DateRangePicker, {
                                startDate: this.props.startDate // momentPropTypes.momentObj or null,
                                , endDate: this.props.endDate,
                                onDatesChange: props.handleDateChange // PropTypes.func.isRequired,
                                , focusedInput: this.state.focusedInput // PropTypes.bool
                                , onFocusChange: function onFocusChange(focusedInput) {
                                    return _this3.setState({ focusedInput: focusedInput });
                                } // PropTypes.func.isRequired,
                                , isOutsideRange: this.isOutsideRange,
                                onPrevMonthClick: props.handleMonthSubstract.bind(this, undefined),
                                onNextMonthClick: props.handleMonthAdd.bind(this, undefined),
                                numberOfMonths: 1,
                                displayFormat: 'YYYY/MM/DD'
                            })
                        )
                    )
                )
            );
        }
    }]);

    return HeaderMonthly;
}(_react2.default.Component);

exports.default = HeaderMonthly;

/***/ }),

/***/ 880:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _HeaderDaily = __webpack_require__(876);

var _HeaderDaily2 = _interopRequireDefault(_HeaderDaily);

var _Totals = __webpack_require__(70);

var _Totals2 = _interopRequireDefault(_Totals);

var _UsageSummary = __webpack_require__(123);

var _UsageSummary2 = _interopRequireDefault(_UsageSummary);

var _Transactions = __webpack_require__(71);

var _Transactions2 = _interopRequireDefault(_Transactions);

var _Timeline = __webpack_require__(154);

var _Timeline2 = _interopRequireDefault(_Timeline);

var _ConsumptionChart = __webpack_require__(122);

var _ConsumptionChart2 = _interopRequireDefault(_ConsumptionChart);

var _TableData = __webpack_require__(69);

var _TableData2 = _interopRequireDefault(_TableData);

var _Donut = __webpack_require__(62);

var _Donut2 = _interopRequireDefault(_Donut);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var style = {
    piecontainer: {
        paddingTop: '20px'
    }
};

var CustomerDaily = function (_React$Component) {
    _inherits(CustomerDaily, _React$Component);

    function CustomerDaily(props) {
        _classCallCheck(this, CustomerDaily);

        var _this = _possibleConstructorReturn(this, (CustomerDaily.__proto__ || Object.getPrototypeOf(CustomerDaily)).call(this, props));

        _this.state = {
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined,
            total_seconds: undefined,
            active_dates: undefined,
            current_day: (0, _moment2.default)(props.startDate).date(parseInt(props.match.params.day))
        };

        _this.parseStats = _this.parseStats.bind(_this);
        _this.handleDayChange = _this.handleDayChange.bind(_this);
        return _this;
    }

    _createClass(CustomerDaily, [{
        key: 'handleDayChange',
        value: function handleDayChange(date) {
            var _this2 = this;

            this.setState({ current_day: date }, function () {
                return _this2.parseStats(_this2.props.source_stats);
            });
            this.props.history.push('/' + "dashboard" + '/customers/' + this.props.customer.id + '/-/' + date.format('YYYY') + '/' + date.format('MM') + '/' + date.format('DD') + '/');
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps, nextState) {
            if (nextProps.source_stats != undefined && this.props.source_stats == undefined) this.parseStats(nextProps.source_stats);
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.parseStats(this.props.source_stats);
        }
    }, {
        key: 'parseStats',
        value: function parseStats(documents) {
            var providerdata = this.props.providers;
            var current_day = this.state.current_day;

            var startDate = (0, _moment2.default)(current_day).startOf('day');
            var endDate = (0, _moment2.default)(current_day).endOf('day');
            var tprecord = {
                'user_count': 0,
                'connections': { 'count': 0, 'data': {} },
                'activations': { 'count': 0, 'data': {} },
                'devices': {
                    'count': 0,
                    'data': {
                        'iphone': {
                            'count': 0,
                            'os': {},
                            'release': {}
                        },
                        'android': {
                            'count': 0,
                            'release': []
                        }
                    }
                },
                'totals': {
                    'tbytes': 0,
                    'tseconds': 0,
                    'tdata': 0,
                    'avgdata': 0,
                    'ttime': 0,
                    'avgtime': 0
                }
            };

            var stats = [];
            var alltx = [];
            var secs = [];
            var totaldata = 0;
            var totalsecs = 0;
            var activationcount = 0;
            var connectioncount = 0;
            var activeDates = [];

            if (documents.length > 0) {
                for (var i = 0; i < documents.length; i++) {
                    var record = documents[i];
                    var found = false;
                    record['activations_current'] = [];
                    record['user_tx_current'] = [];
                    record['devices_current'] = [];
                    for (var j = 0; j < record['activations'].length; j++) {
                        var aitem = record['activations'][j];
                        aitem.date_obj = _moment2.default.utc(aitem.date.$date);
                        activeDates.push(aitem.date_obj);
                        if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                            ///
                            var actday = aitem.date_obj.format('YYYYMMDD');
                            if (tprecord['activations']['data'][actday] == undefined) tprecord['activations']['data'][actday] = 0;
                            tprecord['activations']['data'][actday] += 1;

                            //device
                            for (var k = 0; k < aitem['sqi'].length; k++) {
                                var sqi = aitem['sqi'][k];
                                var dev_id = sqi.device_id;
                                var dev_found = false;
                                for (var l = 0; l < record['devices_current'].length; l++) {
                                    if (record['devices_current'][l]['device_id'] == dev_id) dev_found = true;
                                }
                                if (!dev_found) {
                                    for (var _l = 0; _l < record['devices'].length; _l++) {
                                        if (record['devices'][_l]['device_id'] == dev_id) {
                                            record['devices_current'].push(record['devices'][_l]);
                                            break;
                                        }
                                    }
                                }
                            }

                            //
                            found = true;
                            record['activations_current'].push(aitem);
                            activationcount += 1;
                            var geoposition = [0, 0];
                            var device = '';
                            var txid = aitem['auth_id'];
                            var nai = record['nai'];
                            var rtype = 'Activation';
                            var location = '100002';
                            var country = 'US';
                            var seconds = '-';
                            var bytes = '-';
                            if (aitem['sqi'].length > 0) {
                                for (var _k = 0; _k < aitem['sqi'].length; _k++) {
                                    var _sqi = aitem['sqi'][_k];
                                    device = _sqi['device_id'];
                                    if (_sqi['lat'] != 0) geoposition = [_sqi['lat'], _sqi['lng']];
                                }
                            }
                            alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes]);
                        }
                    }
                    var userbytes = 0;
                    var userseconds = 0;

                    var _loop = function _loop(_j) {
                        var citem = record['user_tx'][_j];
                        citem.date_obj = _moment2.default.utc(citem.tx_date.$date);
                        activeDates.push(citem.date_obj);
                        if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                            for (var _k2 = 0; _k2 < citem['sqi'].length; _k2++) {
                                var _sqi2 = citem['sqi'][_k2];
                                var _dev_id = _sqi2.device_id;
                                var _dev_found = false;
                                for (var _l2 = 0; _l2 < record['devices_current'].length; _l2++) {
                                    if (record['devices_current'][_l2]['device_id'] == _dev_id) _dev_found = true;
                                }
                                if (!_dev_found) {
                                    for (var _l3 = 0; _l3 < record['devices'].length; _l3++) {
                                        if (record['devices'][_l3]['device_id'] == _dev_id) {
                                            record['devices_current'].push(record['devices'][_l3]);
                                            break;
                                        }
                                    }
                                }
                            }
                            ///
                            found = true;
                            record['user_tx_current'].push(citem);
                            connectioncount += 1;
                            var _geoposition = [0, 0];
                            if (citem['usage']['secs'] != 'asda') {
                                ///
                                var txday = citem.date_obj.format('YYYYMMDD');
                                if (tprecord['connections']['data'][txday] == undefined) tprecord['connections']['data'][txday] = 0;
                                tprecord['connections']['data'][txday] += 1;
                                ///

                                var _txid = citem['tx_id'];
                                var _nai = record['nai'];
                                var _rtype = 'Connection';
                                var _location = citem['provider_id'];
                                var _country = undefined;
                                providerdata.some(function (prov) {
                                    if (prov['id'] == citem['provider_id']) {
                                        _country = prov['iso_code'];
                                        return true;
                                    }
                                });
                                var _device = citem['calling_station'];
                                if (citem['sqi'].length > 0) for (var _k3 = 0; _k3 < citem['sqi'].length; _k3++) {
                                    var _sqi3 = citem['sqi'][_k3];
                                    if (_sqi3['lat'] != 0) _geoposition = [_sqi3['lat'], _sqi3['lng']];
                                }
                                var _seconds = citem['usage']['secs'];
                                var _bytes = 0;
                                _bytes += citem['usage']['bytes_in'];
                                _bytes += citem['usage']['bytes_out'];
                                _bytes += citem['usage']['giga_in'] * 2147483648;
                                _bytes += citem['usage']['giga_out'] * 2147483648;
                                userbytes += _bytes;
                                totaldata += _bytes;
                                userseconds += _seconds;
                                totalsecs += _seconds;
                                alltx.push([citem.date_obj, _txid, _nai, _rtype, _location, _country, _geoposition, _device, _seconds, _bytes]);
                                secs.push({ timestamp: citem.tx_date.$date, usage: { secs: _seconds, bytes: _bytes }, cui: _nai });
                            }
                        }
                    };

                    for (var _j = 0; _j < record['user_tx'].length; _j++) {
                        _loop(_j);
                    }
                    record['usage_current'] = { total_bytes: userbytes, total_seconds: userseconds };
                    if (found) stats.push(record);
                }
            }

            tprecord['connections']['count'] = connectioncount;
            tprecord['activations']['count'] = activationcount;
            tprecord['totals']['tbytes'] = totaldata;
            tprecord['totals']['tseconds'] = totalsecs;

            for (var _i = 0; _i < stats.length; _i++) {
                var item = stats[_i];
                for (var _j2 = 0; _j2 < item['devices_current'].length; _j2++) {
                    var dev = item['devices_current'][_j2];
                    tprecord['devices']['count'] += 1;
                    if (dev['platform']['device_model'] == 'iPhone') {
                        tprecord['devices']['data']['iphone']['count'] += 1;
                        if (!tprecord['devices']['data']['iphone']['os'].hasOwnProperty(dev['platform']['ios_version'])) tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] = 1;else tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] += 1;
                        if (!tprecord['devices']['data']['iphone']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['iphone']['release'][dev['version']] = 1;else tprecord['devices']['data']['iphone']['release'][dev['version']] += 1;
                    } else if (dev['platform']['device_model'] == 'Android') {
                        tprecord['devices']['data']['android']['count'] += 1;
                        if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['os_version'])) tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] = 1;else tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] += 1;
                        if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['android']['release'][dev['version']] = 1;else tprecord['devices']['data']['android']['release'][dev['version']] += 1;
                    }
                }
            }
            tprecord['user_count'] = stats.length;
            tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
            tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
            if (tprecord['user_count'] > 0) {
                tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count']);
                tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count']);
            }
            this.setState({
                current_stats: stats,
                total_stats: tprecord,
                transactions: alltx,
                total_seconds: secs,
                active_dates: activeDates
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var props = this.props;
            var customer = props.customer;
            var _state = this.state,
                current_day = _state.current_day,
                total_stats = _state.total_stats,
                active_dates = _state.active_dates,
                current_stats = _state.current_stats,
                transactions = _state.transactions,
                total_seconds = _state.total_seconds;


            var releases = [];
            if (total_stats != undefined) {
                releases = [];
                var release = total_stats.devices.data.iphone.release;
                for (var key in release) {
                    if (release.hasOwnProperty(key)) {
                        releases.push({ name: key, count: release[key] });
                    }
                }
            }

            var osversions = [];
            if (total_stats != undefined) {
                osversions = [];
                var os = total_stats.devices.data.iphone.os;
                for (var _key in os) {
                    if (os.hasOwnProperty(_key)) {
                        osversions.push({ name: _key, count: os[_key] });
                    }
                }
            }
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_HeaderDaily2.default, _extends({}, props, {
                    active_dates: active_dates,
                    current_day: this.state.current_day,
                    handleDayChange: this.handleDayChange,
                    handleMonthSubstract: this.props.handleMonthSubstract,
                    handleMonthAdd: this.props.handleMonthAdd
                })),
                _react2.default.createElement(
                    _reactstrap.Container,
                    { fluid: true, className: 'main_container' },
                    _react2.default.createElement(_Totals2.default, { stats: total_stats }),
                    _react2.default.createElement(
                        _reactstrap.Row,
                        null,
                        _react2.default.createElement(
                            _reactstrap.Col,
                            { sm: '12' },
                            _react2.default.createElement(
                                'div',
                                { style: style.piecontainer },
                                releases.length > 0 && _react2.default.createElement(_Donut2.default, { data: releases, title: 'RoamVU Releases' }),
                                osversions.length > 0 && _react2.default.createElement(_Donut2.default, { data: osversions, title: 'iOS Versions' }),
                                _react2.default.createElement(_TableData2.default, { data: total_stats })
                            )
                        )
                    ),
                    _react2.default.createElement(_Timeline2.default, { daily: total_seconds }),
                    _react2.default.createElement(_ConsumptionChart2.default, { stats: current_stats }),
                    _react2.default.createElement(_UsageSummary2.default, {
                        stats: current_stats,
                        month: current_day.format('MMMM'),
                        date: current_day,
                        customer_id: customer.id
                    }),
                    _react2.default.createElement(_Transactions2.default, {
                        stats: transactions,
                        month: current_day.format('MMMM'),
                        date: current_day,
                        customer_id: customer.id,
                        showCSV: true,
                        hideProvider: true
                    })
                )
            );
        }
    }]);

    return CustomerDaily;
}(_react2.default.Component);

exports.default = CustomerDaily;

/***/ }),

/***/ 881:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _HeaderMonthly = __webpack_require__(877);

var _HeaderMonthly2 = _interopRequireDefault(_HeaderMonthly);

var _Totals = __webpack_require__(70);

var _Totals2 = _interopRequireDefault(_Totals);

var _UsageSummary = __webpack_require__(123);

var _UsageSummary2 = _interopRequireDefault(_UsageSummary);

var _Transactions = __webpack_require__(71);

var _Transactions2 = _interopRequireDefault(_Transactions);

var _ConnectionsChart = __webpack_require__(382);

var _ConnectionsChart2 = _interopRequireDefault(_ConnectionsChart);

var _ConsumptionChart = __webpack_require__(122);

var _ConsumptionChart2 = _interopRequireDefault(_ConsumptionChart);

var _TableData = __webpack_require__(69);

var _TableData2 = _interopRequireDefault(_TableData);

var _Donut = __webpack_require__(62);

var _Donut2 = _interopRequireDefault(_Donut);

var _UserMonthly = __webpack_require__(883);

var _UserMonthly2 = _interopRequireDefault(_UserMonthly);

var _CustomerDaily = __webpack_require__(880);

var _CustomerDaily2 = _interopRequireDefault(_CustomerDaily);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var style = {
    piecontainer: {
        paddingTop: '20px'
    }
};

var CustomerMonthly = function (_React$Component) {
    _inherits(CustomerMonthly, _React$Component);

    function CustomerMonthly(props) {
        _classCallCheck(this, CustomerMonthly);

        var _this2 = _possibleConstructorReturn(this, (CustomerMonthly.__proto__ || Object.getPrototypeOf(CustomerMonthly)).call(this, props));

        var customer_id = parseInt(CONST_CUSTOMER_ID);
        var customer = undefined;
        props.customers.map(function (lcustomer) {
            if (lcustomer.id == customer_id) customer = lcustomer;
        });

        var month = parseInt(props.match.params.month);
        if (month < 9) month = '0' + month;else month = '' + month;

        _this2.state = {
            customer: customer,
            source_stats: undefined,
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined,
            startDate: _moment2.default.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').startOf('month'),
            endDate: _moment2.default.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').endOf('month')

        };
        _this2.firstLoad = true;

        _this2.getNewStats = _this2.getNewStats.bind(_this2);
        _this2.parseStats = _this2.parseStats.bind(_this2);
        _this2.handleDateChange = _this2.handleDateChange.bind(_this2);
        _this2.handleMonthSubstract = _this2.handleMonthSubstract.bind(_this2);
        _this2.handleMonthAdd = _this2.handleMonthAdd.bind(_this2);
        return _this2;
    }

    _createClass(CustomerMonthly, [{
        key: "handleDateChange",
        value: function handleDateChange(_ref) {
            var _this3 = this;

            var startDate = _ref.startDate,
                endDate = _ref.endDate;

            this.setState({
                startDate: startDate.startOf('day'),
                endDate: endDate.endOf('day')
            }, function () {
                _this3.parseStats(_this3.state.source_stats);
            });
        }
    }, {
        key: "handleMonthSubstract",
        value: function handleMonthSubstract() {
            var path_suffix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

            var oldDate = this.state.startDate.format('YYYYMM');
            var newStart = this.state.startDate.subtract(1, 'months').startOf('month').startOf('day');
            var newEnd = (0, _moment2.default)(newStart).endOf('month').endOf('day');
            this.setState({
                startDate: newStart,
                endDate: newEnd
            });
            var params = this.props.match.params;

            this.props.history.push("/" + "dashboard" + "/customers/" + this.state.customer.id + "/" + params.user_hash + "/" + newStart.format('YYYY') + "/" + newStart.format('MM') + "/" + path_suffix);
            if (oldDate != newStart.format('YYYYMM')) {
                this.setState({ source_stats: undefined, total_stats: undefined, transactions: undefined });
                this.getNewStats();
            }
        }
    }, {
        key: "handleMonthAdd",
        value: function handleMonthAdd() {
            var path_suffix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

            var oldDate = this.state.startDate.format('YYYYMM');
            var newStart = this.state.startDate.add(1, 'months').startOf('month').startOf('day');
            var newEnd = (0, _moment2.default)(newStart).endOf('month').endOf('day');
            this.setState({
                startDate: newStart,
                endDate: newEnd
            });
            var params = this.props.match.params;

            this.props.history.push("/" + "dashboard" + "/customers/" + this.state.customer.id + "/" + params.user_hash + "/" + newStart.format('YYYY') + "/" + newStart.format('MM') + "/" + path_suffix);
            if (oldDate != newStart.format('YYYYMM')) {
                this.setState({ source_stats: undefined, total_stats: undefined, transactions: undefined });
                this.getNewStats();
            }
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            this.getNewStats();
        }
    }, {
        key: "getNewStats",
        value: function getNewStats(callback, customer_id) {
            var id = customer_id != undefined ? customer_id : this.state.customer.id;
            var _this = this;
            _Api2.default.get("https://portal.roamvu.com/dashboard/api/" + 'customer/{0}/{1}/'.format(id, this.state.startDate.format('YYYYMM'))).then(function (response) {
                _this.firstLoad = false;
                _this.parseStats(JSON.parse(response.data.results));
                if (callback != undefined) callback(response);
            });
        }
    }, {
        key: "parseStats",
        value: function parseStats(documents) {
            var providerdata = this.props.providers;
            var _state = this.state,
                startDate = _state.startDate,
                endDate = _state.endDate;

            var tprecord = {
                'user_count': 0,
                'connections': { 'count': 0, 'data': {} },
                'activations': { 'count': 0, 'data': {} },
                'devices': {
                    'count': 0,
                    'data': {
                        'iphone': {
                            'count': 0,
                            'os': {},
                            'release': {}
                        },
                        'android': {
                            'count': 0,
                            'release': []
                        }
                    }
                },
                'totals': {
                    'tbytes': 0,
                    'tseconds': 0,
                    'tdata': 0,
                    'avgdata': 0,
                    'ttime': 0,
                    'avgtime': 0
                }
            };

            var stats = [];
            var alltx = [];
            var totaldata = 0;
            var totalsecs = 0;
            var activationcount = 0;
            var connectioncount = 0;
            if (documents.length > 0) {
                for (var i = 0; i < documents.length; i++) {
                    var record = documents[i];
                    var found = false;
                    record['activations_current'] = [];
                    record['user_tx_current'] = [];
                    record['devices_current'] = [];
                    for (var j = 0; j < record['activations'].length; j++) {
                        var aitem = record['activations'][j];
                        aitem.date_obj = _moment2.default.utc(aitem.date.$date);
                        if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                            ///
                            var actday = aitem.date_obj.format('YYYYMMDD');
                            if (tprecord['activations']['data'][actday] == undefined) tprecord['activations']['data'][actday] = 0;
                            tprecord['activations']['data'][actday] += 1;

                            //device
                            for (var k = 0; k < aitem['sqi'].length; k++) {
                                var sqi = aitem['sqi'][k];
                                var dev_id = sqi.device_id;
                                var dev_found = false;
                                for (var l = 0; l < record['devices_current'].length; l++) {
                                    if (record['devices_current'][l]['device_id'] == dev_id) dev_found = true;
                                }
                                if (!dev_found) {
                                    for (var _l = 0; _l < record['devices'].length; _l++) {
                                        if (record['devices'][_l]['device_id'] == dev_id) {
                                            record['devices_current'].push(record['devices'][_l]);
                                            break;
                                        }
                                    }
                                }
                            }

                            //
                            found = true;
                            record['activations_current'].push(aitem);
                            activationcount += 1;
                            var geoposition = [0, 0];
                            var device = '';
                            var txid = aitem['auth_id'];
                            var nai = record['nai'];
                            var rtype = 'Activation';
                            var location = '100002';
                            var country = 'US';
                            var seconds = '-';
                            var bytes = '-';
                            if (aitem['sqi'].length > 0) {
                                for (var _k = 0; _k < aitem['sqi'].length; _k++) {
                                    var _sqi = aitem['sqi'][_k];
                                    device = _sqi['device_id'];
                                    if (_sqi['lat'] != 0) geoposition = [_sqi['lat'], _sqi['lng']];
                                }
                            }
                            alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes]);
                        }
                    }
                    var userbytes = 0;
                    var userseconds = 0;

                    var _loop = function _loop(_j) {
                        var citem = record['user_tx'][_j];
                        citem.date_obj = _moment2.default.utc(citem.tx_date.$date);
                        if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                            for (var _k2 = 0; _k2 < citem['sqi'].length; _k2++) {
                                var _sqi2 = citem['sqi'][_k2];
                                var _dev_id = _sqi2.device_id;
                                var _dev_found = false;
                                for (var _l2 = 0; _l2 < record['devices_current'].length; _l2++) {
                                    if (record['devices_current'][_l2]['device_id'] == _dev_id) _dev_found = true;
                                }
                                if (!_dev_found) {
                                    for (var _l3 = 0; _l3 < record['devices'].length; _l3++) {
                                        if (record['devices'][_l3]['device_id'] == _dev_id) {
                                            record['devices_current'].push(record['devices'][_l3]);
                                            break;
                                        }
                                    }
                                }
                            }
                            ///
                            found = true;
                            record['user_tx_current'].push(citem);
                            connectioncount += 1;
                            var _geoposition = [0, 0];
                            if (citem['usage']['secs'] != 'asda') {
                                ///
                                var txday = citem.date_obj.format('YYYYMMDD');
                                if (tprecord['connections']['data'][txday] == undefined) tprecord['connections']['data'][txday] = 0;
                                tprecord['connections']['data'][txday] += 1;
                                ///
                                var _txid = citem['tx_id'];
                                var _nai = record['nai'];
                                var _rtype = 'Connection';
                                var _location = citem['provider_id'];
                                var _country = undefined;
                                providerdata.some(function (prov) {
                                    if (prov['id'] == citem['provider_id']) {
                                        _country = prov['iso_code'];
                                        return true;
                                    }
                                });
                                var _device = citem['calling_station'];
                                if (citem['sqi'].length > 0) for (var _k3 = 0; _k3 < citem['sqi'].length; _k3++) {
                                    var _sqi3 = citem['sqi'][_k3];
                                    if (_sqi3['lat'] != 0) _geoposition = [_sqi3['lat'], _sqi3['lng']];
                                }
                                var _seconds = citem['usage']['secs'];
                                var _bytes = 0;
                                _bytes += citem['usage']['bytes_in'];
                                _bytes += citem['usage']['bytes_out'];
                                _bytes += citem['usage']['giga_in'] * 2147483648;
                                _bytes += citem['usage']['giga_out'] * 2147483648;
                                userbytes += _bytes;
                                totaldata += _bytes;
                                userseconds += _seconds;
                                totalsecs += _seconds;
                                alltx.push([citem.date_obj, _txid, _nai, _rtype, _location, _country, _geoposition, _device, _seconds, _bytes]);
                            }
                        }
                    };

                    for (var _j = 0; _j < record['user_tx'].length; _j++) {
                        _loop(_j);
                    }
                    record['usage_current'] = { total_bytes: userbytes, total_seconds: userseconds };
                    if (found) stats.push(record);
                }
            }

            tprecord['connections']['count'] = connectioncount;
            tprecord['activations']['count'] = activationcount;
            tprecord['totals']['tbytes'] = totaldata;
            tprecord['totals']['tseconds'] = totalsecs;

            for (var _i = 0; _i < stats.length; _i++) {
                var item = stats[_i];
                for (var _j2 = 0; _j2 < item['devices_current'].length; _j2++) {
                    var dev = item['devices_current'][_j2];
                    tprecord['devices']['count'] += 1;
                    if (dev['platform']['device_model'] == 'iPhone') {
                        tprecord['devices']['data']['iphone']['count'] += 1;
                        if (!tprecord['devices']['data']['iphone']['os'].hasOwnProperty(dev['platform']['ios_version'])) tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] = 1;else tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] += 1;
                        if (!tprecord['devices']['data']['iphone']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['iphone']['release'][dev['version']] = 1;else tprecord['devices']['data']['iphone']['release'][dev['version']] += 1;
                    } else if (dev['platform']['device_model'] == 'Android') {
                        tprecord['devices']['data']['android']['count'] += 1;
                        if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['os_version'])) tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] = 1;else tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] += 1;
                        if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['android']['release'][dev['version']] = 1;else tprecord['devices']['data']['android']['release'][dev['version']] += 1;
                    }
                }
            }
            tprecord['user_count'] = stats.length;
            tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
            tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
            if (tprecord['user_count'] > 0) {
                tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count']);
                tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count']);
            }
            this.setState({
                source_stats: documents,
                current_stats: stats,
                total_stats: tprecord,
                transactions: alltx
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            if (this.firstLoad) {
                return _react2.default.createElement(
                    "div",
                    null,
                    "Loading monthly stats.."
                );
            }
            var props = this.props;
            var _state2 = this.state,
                customer = _state2.customer,
                startDate = _state2.startDate,
                endDate = _state2.endDate,
                source_stats = _state2.source_stats,
                total_stats = _state2.total_stats,
                current_stats = _state2.current_stats,
                transactions = _state2.transactions;


            var releases = [];
            if (total_stats != undefined) {
                releases = [];
                var release = total_stats.devices.data.iphone.release;
                for (var key in release) {
                    if (release.hasOwnProperty(key)) {
                        releases.push({ name: key, count: release[key] });
                    }
                }
            }

            var osversions = [];
            if (total_stats != undefined) {
                osversions = [];
                var os = total_stats.devices.data.iphone.os;
                for (var _key in os) {
                    if (os.hasOwnProperty(_key)) {
                        osversions.push({ name: _key, count: os[_key] });
                    }
                }
            }
            var user_hash = props.match.params.user_hash;

            if (user_hash != '-') return _react2.default.createElement(_UserMonthly2.default, { providers: props.providers,
                customers: props.customers,
                customer: customer,
                user_hash: user_hash,
                startDate: startDate,
                endDate: endDate,
                history: props.history,
                match: props.match,
                handleDateChange: this.handleDateChange,
                handleMonthSubstract: this.handleMonthSubstract,
                handleMonthAdd: this.handleMonthAdd
            });
            return _react2.default.createElement(
                _reactRouterDom.Switch,
                null,
                _react2.default.createElement(_reactRouterDom.Route, { path: "" + props.match.url + (props.match.url.slice(-1) == '/' ? ':day' : '/:day'), render: function render(componentProps) {
                        return _react2.default.createElement(
                            "div",
                            null,
                            _react2.default.createElement(_CustomerDaily2.default, _extends({}, componentProps, {
                                customer: customer,
                                customers: props.customers,
                                startDate: startDate,
                                endDate: endDate,
                                source_stats: source_stats,
                                handleMonthSubstract: _this4.handleMonthSubstract,
                                handleMonthAdd: _this4.handleMonthAdd,
                                providers: props.providers
                            }))
                        );
                    } }),
                _react2.default.createElement(_reactRouterDom.Route, { render: function render(componentProps) {
                        return _react2.default.createElement(
                            "div",
                            null,
                            _react2.default.createElement(_HeaderMonthly2.default, _extends({}, props, {
                                customer: customer,
                                startDate: startDate,
                                endDate: endDate,
                                handleDateChange: _this4.handleDateChange,
                                handleMonthSubstract: _this4.handleMonthSubstract,
                                handleMonthAdd: _this4.handleMonthAdd
                            })),
                            _react2.default.createElement(
                                _reactstrap.Container,
                                { fluid: true, className: "main_container" },
                                _react2.default.createElement(_Totals2.default, { stats: total_stats }),
                                _react2.default.createElement(
                                    _reactstrap.Row,
                                    null,
                                    _react2.default.createElement(
                                        _reactstrap.Col,
                                        { sm: "12" },
                                        _react2.default.createElement(
                                            "div",
                                            { style: style.piecontainer },
                                            releases.length > 0 && _react2.default.createElement(_Donut2.default, { data: releases, title: "RoamVU Releases" }),
                                            osversions.length > 0 && _react2.default.createElement(_Donut2.default, { data: osversions, title: "iOS Versions" }),
                                            _react2.default.createElement(_TableData2.default, { data: total_stats })
                                        )
                                    )
                                ),
                                _react2.default.createElement(_ConnectionsChart2.default, { stats: total_stats, match: props.match, history: props.history }),
                                _react2.default.createElement(_ConsumptionChart2.default, { stats: current_stats }),
                                _react2.default.createElement(_UsageSummary2.default, {
                                    stats: current_stats,
                                    month: _this4.state.startDate.format('MMMM'),
                                    date: _this4.state.startDate,
                                    customer_id: _this4.state.customer.id
                                }),
                                _react2.default.createElement(_Transactions2.default, {
                                    stats: transactions,
                                    month: _this4.state.startDate.format('MMMM'),
                                    date: _this4.state.startDate,
                                    customer_id: _this4.state.customer.id,
                                    showCSV: true,
                                    hideProvider: true
                                })
                            )
                        );
                    } })
            );
        }
    }]);

    return CustomerMonthly;
}(_react2.default.Component);

exports.default = CustomerMonthly;

/***/ }),

/***/ 882:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactstrap = __webpack_require__(7);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _HeaderDaily = __webpack_require__(878);

var _HeaderDaily2 = _interopRequireDefault(_HeaderDaily);

var _Totals = __webpack_require__(70);

var _Totals2 = _interopRequireDefault(_Totals);

var _Transactions = __webpack_require__(71);

var _Transactions2 = _interopRequireDefault(_Transactions);

var _TableData = __webpack_require__(69);

var _TableData2 = _interopRequireDefault(_TableData);

var _Donut = __webpack_require__(62);

var _Donut2 = _interopRequireDefault(_Donut);

var _Timeline = __webpack_require__(154);

var _Timeline2 = _interopRequireDefault(_Timeline);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var style = {
    piecontainer: {
        paddingTop: '20px'
    }
};

var UserMonthly = function (_React$Component) {
    _inherits(UserMonthly, _React$Component);

    function UserMonthly(props) {
        _classCallCheck(this, UserMonthly);

        var _this = _possibleConstructorReturn(this, (UserMonthly.__proto__ || Object.getPrototypeOf(UserMonthly)).call(this, props));

        _this.state = {
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined,
            total_seconds: undefined,
            active_dates: undefined,
            current_day: (0, _moment2.default)(props.startDate).date(parseInt(props.match.params.day))
        };
        _this.parseStats = _this.parseStats.bind(_this);
        _this.handleDayChange = _this.handleDayChange.bind(_this);

        return _this;
    }

    _createClass(UserMonthly, [{
        key: 'handleDayChange',
        value: function handleDayChange(date) {
            var _this2 = this;

            var props = this.props;

            this.setState({ current_day: date }, function () {
                return _this2.parseStats(_this2.props.source_stats);
            });
            props.history.push('/' + "dashboard" + '/customers/' + props.customer.id + '/' + props.user.nai + '/' + date.format('YYYY') + '/' + date.format('MM') + '/' + date.format('DD') + '/');
        }
    }, {
        key: 'componentWillUpdate',
        value: function componentWillUpdate(nextProps, nextState) {
            if (nextProps.source_stats != undefined && this.props.source_stats == undefined) this.parseStats(nextProps.source_stats);
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.parseStats(this.props.source_stats);
        }
    }, {
        key: 'parseStats',
        value: function parseStats(documents) {
            var providerdata = this.props.providers;
            var current_day = this.state.current_day;

            var startDate = (0, _moment2.default)(current_day).startOf('day');
            var endDate = (0, _moment2.default)(current_day).endOf('day');
            var tprecord = {
                'user_count': 0,
                'connections': { 'count': 0, 'data': {} },
                'activations': { 'count': 0, 'data': {} },
                'duration': {
                    'data': {}
                },
                'usage': {
                    'data': {}
                },
                'devices': {
                    'count': 0,
                    'data': {
                        'iphone': {
                            'count': 0,
                            'os': {},
                            'release': {}
                        },
                        'android': {
                            'count': 0,
                            'release': []
                        }
                    }
                },
                'totals': {
                    'tbytes': 0,
                    'tseconds': 0,
                    'tdata': 0,
                    'avgdata': 0,
                    'ttime': 0,
                    'avgtime': 0
                }
            };

            var stats = [];
            var alltx = [];
            var secs = [];
            var totaldata = 0;
            var totalsecs = 0;
            var activationcount = 0;
            var connectioncount = 0;
            var activeDates = [];
            if (documents.length > 0) {
                for (var i = 0; i < documents.length; i++) {
                    var record = documents[i];
                    var found = false;
                    record['activations_current'] = [];
                    record['user_tx_current'] = [];
                    record['devices_current'] = [];
                    for (var j = 0; j < record['activations'].length; j++) {
                        var aitem = record['activations'][j];
                        aitem.date_obj = _moment2.default.utc(aitem.date.$date);
                        activeDates.push(aitem.date_obj);
                        if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                            ///
                            var actday = aitem.date_obj.format('YYYYMMDD');
                            if (tprecord['activations']['data'][actday] == undefined) tprecord['activations']['data'][actday] = 0;
                            tprecord['activations']['data'][actday] += 1;

                            //device
                            for (var k = 0; k < aitem['sqi'].length; k++) {
                                var sqi = aitem['sqi'][k];
                                var dev_id = sqi.device_id;
                                var dev_found = false;
                                for (var l = 0; l < record['devices_current'].length; l++) {
                                    if (record['devices_current'][l]['device_id'] == dev_id) dev_found = true;
                                }
                                if (!dev_found) {
                                    for (var _l = 0; _l < record['devices'].length; _l++) {
                                        if (record['devices'][_l]['device_id'] == dev_id) {
                                            record['devices_current'].push(record['devices'][_l]);
                                            break;
                                        }
                                    }
                                }
                            }

                            //
                            found = true;
                            record['activations_current'].push(aitem);
                            activationcount += 1;
                            var geoposition = [0, 0];
                            var device = '';
                            var txid = aitem['auth_id'];
                            var nai = record['nai'];
                            var rtype = 'Activation';
                            var location = '100002';
                            var country = 'US';
                            var seconds = '-';
                            var bytes = '-';
                            if (aitem['sqi'].length > 0) {
                                for (var _k = 0; _k < aitem['sqi'].length; _k++) {
                                    var _sqi = aitem['sqi'][_k];
                                    device = _sqi['device_id'];
                                    if (_sqi['lat'] != 0) geoposition = [_sqi['lat'], _sqi['lng']];
                                }
                            }
                            alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes]);
                        }
                    }
                    var userbytes = 0;
                    var userseconds = 0;

                    var _loop = function _loop(_j) {
                        var citem = record['user_tx'][_j];
                        citem.date_obj = _moment2.default.utc(citem.tx_date.$date);
                        activeDates.push(citem.date_obj);
                        if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                            ///
                            for (var _k2 = 0; _k2 < citem['sqi'].length; _k2++) {
                                var _sqi2 = citem['sqi'][_k2];
                                var _dev_id = _sqi2.device_id;
                                var _dev_found = false;
                                for (var _l2 = 0; _l2 < record['devices_current'].length; _l2++) {
                                    if (record['devices_current'][_l2]['device_id'] == _dev_id) _dev_found = true;
                                }
                                if (!_dev_found) {
                                    for (var _l3 = 0; _l3 < record['devices'].length; _l3++) {
                                        if (record['devices'][_l3]['device_id'] == _dev_id) {
                                            record['devices_current'].push(record['devices'][_l3]);
                                            break;
                                        }
                                    }
                                }
                            }
                            ///
                            found = true;
                            record['user_tx_current'].push(citem);
                            connectioncount += 1;
                            var _geoposition = [0, 0];
                            if (citem['usage']['secs'] > 0) {
                                ///Connections
                                var txday = citem.date_obj.format('YYYYMMDD');
                                if (tprecord['connections']['data'][txday] == undefined) tprecord['connections']['data'][txday] = 0;
                                tprecord['connections']['data'][txday] += 1;
                                var _txid = citem['tx_id'];
                                var _nai = record['nai'];
                                var _rtype = 'Connection';
                                var _location = citem['provider_id'];
                                var _country = undefined;
                                providerdata.some(function (prov) {
                                    if (prov['id'] == citem['provider_id']) {
                                        _country = prov['iso_code'];
                                        return true;
                                    }
                                });
                                var _device = citem['calling_station'];
                                if (citem['sqi'].length > 0) for (var _k3 = 0; _k3 < citem['sqi'].length; _k3++) {
                                    var _sqi3 = citem['sqi'][_k3];
                                    if (_sqi3['lat'] != 0) _geoposition = [_sqi3['lat'], _sqi3['lng']];
                                }
                                var _seconds = citem['usage']['secs'];
                                var _bytes = 0;
                                _bytes += citem['usage']['bytes_in'];
                                _bytes += citem['usage']['bytes_out'];
                                _bytes += citem['usage']['giga_in'] * 2147483648;
                                _bytes += citem['usage']['giga_out'] * 2147483648;
                                userbytes += _bytes;
                                totaldata += _bytes;
                                userseconds += _seconds;
                                totalsecs += _seconds;
                                alltx.push([citem.date_obj, _txid, _nai, _rtype, _location, _country, _geoposition, _device, _seconds, _bytes]);
                                secs.push({ timestamp: citem.tx_date.$date, usage: { secs: _seconds, bytes: _bytes }, cui: _nai });
                                //Duration
                                if (tprecord['duration']['data'][txday] == undefined) tprecord['duration']['data'][txday] = 0;
                                tprecord['duration']['data'][txday] += _seconds;

                                //usage
                                if (tprecord['usage']['data'][txday] == undefined) tprecord['usage']['data'][txday] = 0;
                                tprecord['usage']['data'][txday] += _bytes;
                            }
                        }
                    };

                    for (var _j = 0; _j < record['user_tx'].length; _j++) {
                        _loop(_j);
                    }
                    record['usage_current'] = { total_bytes: userbytes, total_seconds: userseconds };
                    if (found) stats.push(record);
                }
            }

            tprecord['connections']['count'] = connectioncount;
            tprecord['activations']['count'] = activationcount;
            tprecord['totals']['tbytes'] = totaldata;
            tprecord['totals']['tseconds'] = totalsecs;

            for (var _i = 0; _i < stats.length; _i++) {
                var item = stats[_i];
                for (var _j2 = 0; _j2 < item['devices_current'].length; _j2++) {
                    var dev = item['devices_current'][_j2];
                    tprecord['devices']['count'] += 1;
                    if (dev['platform']['device_model'] == 'iPhone') {
                        tprecord['devices']['data']['iphone']['count'] += 1;
                        if (!tprecord['devices']['data']['iphone']['os'].hasOwnProperty(dev['platform']['ios_version'])) tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] = 1;else tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] += 1;
                        if (!tprecord['devices']['data']['iphone']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['iphone']['release'][dev['version']] = 1;else tprecord['devices']['data']['iphone']['release'][dev['version']] += 1;
                    } else if (dev['platform']['device_model'] == 'Android') {
                        tprecord['devices']['data']['android']['count'] += 1;
                        if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['os_version'])) tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] = 1;else tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] += 1;
                        if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['android']['release'][dev['version']] = 1;else tprecord['devices']['data']['android']['release'][dev['version']] += 1;
                    }
                }
            }
            tprecord['user_count'] = stats.length;
            tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
            tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
            if (tprecord['user_count'] > 0) {
                tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count']);
                tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count']);
            }
            this.setState({
                current_stats: stats,
                total_stats: tprecord,
                transactions: alltx,
                total_seconds: secs,
                active_dates: activeDates
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var props = this.props,
                state = this.state;
            var current_day = state.current_day,
                active_dates = state.active_dates,
                total_stats = state.total_stats,
                total_seconds = state.total_seconds,
                current_stats = state.current_stats,
                transactions = state.transactions;
            var user = props.user,
                customer = props.customer;

            var releases = undefined;
            if (total_stats != undefined) {
                releases = [];
                var release = total_stats.devices.data.iphone.release;
                for (var key in release) {
                    if (release.hasOwnProperty(key)) {
                        releases.push({ name: key, count: release[key] });
                    }
                }
            }

            var osversions = undefined;
            if (total_stats != undefined) {
                osversions = [];
                var os = total_stats.devices.data.iphone.os;
                for (var _key in os) {
                    if (os.hasOwnProperty(_key)) {
                        osversions.push({ name: _key, count: os[_key] });
                    }
                }
            }
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_HeaderDaily2.default, _extends({}, props, {
                    active_dates: active_dates,
                    current_day: current_day,
                    handleDayChange: this.handleDayChange
                })),
                _react2.default.createElement(
                    _reactstrap.Container,
                    { fluid: true, className: 'main_container' },
                    _react2.default.createElement(_Totals2.default, { stats: total_stats }),
                    _react2.default.createElement(
                        _reactstrap.Row,
                        null,
                        _react2.default.createElement(
                            _reactstrap.Col,
                            { sm: '12' },
                            _react2.default.createElement(
                                'div',
                                { style: style.piecontainer },
                                _react2.default.createElement(_Donut2.default, { data: releases, title: 'RoamVU Releases' }),
                                _react2.default.createElement(_Donut2.default, { data: osversions, title: 'iOS Versions' }),
                                _react2.default.createElement(_TableData2.default, { data: total_stats })
                            )
                        )
                    ),
                    _react2.default.createElement(_Timeline2.default, { daily: total_seconds }),
                    _react2.default.createElement(_Transactions2.default, {
                        stats: transactions,
                        month: current_day.format('MMMM'),
                        date: current_day,
                        user: user,
                        customer_id: customer.id,
                        hideProvider: true
                    })
                )
            );
        }
    }]);

    return UserMonthly;
}(_react2.default.Component);

exports.default = UserMonthly;

/***/ }),

/***/ 883:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Api = __webpack_require__(12);

var _Api2 = _interopRequireDefault(_Api);

var _reactstrap = __webpack_require__(7);

var _reactRouterDom = __webpack_require__(11);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _HeaderMonthly = __webpack_require__(879);

var _HeaderMonthly2 = _interopRequireDefault(_HeaderMonthly);

var _Totals = __webpack_require__(70);

var _Totals2 = _interopRequireDefault(_Totals);

var _Transactions = __webpack_require__(71);

var _Transactions2 = _interopRequireDefault(_Transactions);

var _UsageDuration = __webpack_require__(232);

var _UsageDuration2 = _interopRequireDefault(_UsageDuration);

var _TableData = __webpack_require__(69);

var _TableData2 = _interopRequireDefault(_TableData);

var _Donut = __webpack_require__(62);

var _Donut2 = _interopRequireDefault(_Donut);

var _UserDaily = __webpack_require__(882);

var _UserDaily2 = _interopRequireDefault(_UserDaily);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var style = {
    piecontainer: {
        paddingTop: '20px'
    }
};

var UserMonthly = function (_React$Component) {
    _inherits(UserMonthly, _React$Component);

    function UserMonthly(props) {
        _classCallCheck(this, UserMonthly);

        var _this2 = _possibleConstructorReturn(this, (UserMonthly.__proto__ || Object.getPrototypeOf(UserMonthly)).call(this, props));

        _this2.state = {
            user: undefined,
            source_stats: undefined,
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined,
            startDate: props.startDate,
            endDate: props.endDate
            // startDate: moment("{0}{1}".format(props.match.params.year, month), 'YYYYMM').startOf('month'),
            // endDate: moment("{0}{1}".format(props.match.params.year, month), 'YYYYMM').endOf('month')
        };
        _this2.firstLoad = true;
        _this2.getData = _this2.getData.bind(_this2);
        _this2.parseStats = _this2.parseStats.bind(_this2);
        _this2.handleDateChange = _this2.handleDateChange.bind(_this2);
        _this2.handleMonthSubstract = _this2.handleMonthSubstract.bind(_this2);
        _this2.handleMonthAdd = _this2.handleMonthAdd.bind(_this2);
        return _this2;
    }

    _createClass(UserMonthly, [{
        key: "handleDateChange",
        value: function handleDateChange(_ref) {
            var _this3 = this;

            var startDate = _ref.startDate,
                endDate = _ref.endDate;

            this.setState({
                startDate: startDate.startOf('day'),
                endDate: endDate.endOf('day')
            }, function () {
                _this3.parseStats(_this3.state.source_stats);
            });
        }
    }, {
        key: "handleMonthSubstract",
        value: function handleMonthSubstract() {
            var path_suffix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

            var oldDate = this.state.startDate.format('YYYYMM');
            var newStart = this.state.startDate.subtract(1, 'months').startOf('month').startOf('day');
            var newEnd = (0, _moment2.default)(newStart).endOf('month').endOf('day');
            this.setState({
                startDate: newStart,
                endDate: newEnd
            });
            // this.props.history.push(`/${process.env.PUBLIC_URL}/customers/${this.props.customer.id}/${this.props.user_hash}/${newStart.format('YYYY')}/${newStart.format('MM')}/` + path_suffix);
            if (oldDate != newStart.format('YYYYMM')) {
                this.setState({ source_stats: undefined, total_stats: undefined, transactions: undefined });
                this.getData();
            }
        }
    }, {
        key: "handleMonthAdd",
        value: function handleMonthAdd() {
            var path_suffix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

            var oldDate = this.state.startDate.format('YYYYMM');
            var newStart = this.state.startDate.add(1, 'months').startOf('month').startOf('day');
            var newEnd = (0, _moment2.default)(newStart).endOf('month').endOf('day');
            this.setState({
                startDate: newStart,
                endDate: newEnd
            });
            // this.props.history.push(`/${process.env.PUBLIC_URL}/customers/${this.props.customer.id}/${this.props.user_hash}/${newStart.format('YYYY')}/${newStart.format('MM')}/` + path_suffix);
            if (oldDate != newStart.format('YYYYMM')) {
                this.setState({ source_stats: undefined, total_stats: undefined, transactions: undefined });
                this.getData();
            }
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            this.getData();
        }
    }, {
        key: "getData",
        value: function getData() {
            var _this = this;
            _Api2.default.get("https://portal.roamvu.com/dashboard/api/" + 'users/' + this.props.user_hash + '/' + this.state.startDate.format('YYYYMM') + '/').then(function (res) {
                if (res.data.results != undefined) {
                    _this.firstLoad = false;
                    _this.parseStats(JSON.parse(res.data.results));
                }
            });
        }
    }, {
        key: "parseStats",
        value: function parseStats(documents) {
            var providerdata = this.props.providers;
            var _state = this.state,
                startDate = _state.startDate,
                endDate = _state.endDate;

            var tprecord = {
                'user_count': 0,
                'connections': { 'count': 0, 'data': {} },
                'activations': { 'count': 0, 'data': {} },
                'duration': {
                    'data': {}
                },
                'usage': {
                    'data': {}
                },
                'devices': {
                    'count': 0,
                    'data': {
                        'iphone': {
                            'count': 0,
                            'os': {},
                            'release': {}
                        },
                        'android': {
                            'count': 0,
                            'release': []
                        }
                    }
                },
                'totals': {
                    'tbytes': 0,
                    'tseconds': 0,
                    'tdata': 0,
                    'avgdata': 0,
                    'ttime': 0,
                    'avgtime': 0
                }
            };

            var stats = [];
            var alltx = [];
            var totaldata = 0;
            var totalsecs = 0;
            var activationcount = 0;
            var connectioncount = 0;
            if (documents.length > 0) {
                for (var i = 0; i < documents.length; i++) {
                    var record = documents[i];
                    var found = false;
                    record['activations_current'] = [];
                    record['user_tx_current'] = [];
                    record['devices_current'] = [];
                    for (var j = 0; j < record['activations'].length; j++) {
                        var aitem = record['activations'][j];
                        aitem.date_obj = _moment2.default.utc(aitem.date.$date);
                        if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                            ///
                            var actday = aitem.date_obj.format('YYYYMMDD');
                            if (tprecord['activations']['data'][actday] == undefined) tprecord['activations']['data'][actday] = 0;
                            tprecord['activations']['data'][actday] += 1;

                            //device
                            for (var k = 0; k < aitem['sqi'].length; k++) {
                                var sqi = aitem['sqi'][k];
                                var dev_id = sqi.device_id;
                                var dev_found = false;
                                for (var l = 0; l < record['devices_current'].length; l++) {
                                    if (record['devices_current'][l]['device_id'] == dev_id) dev_found = true;
                                }
                                if (!dev_found) {
                                    for (var _l = 0; _l < record['devices'].length; _l++) {
                                        if (record['devices'][_l]['device_id'] == dev_id) {
                                            record['devices_current'].push(record['devices'][_l]);
                                            break;
                                        }
                                    }
                                }
                            }

                            //
                            found = true;
                            record['activations_current'].push(aitem);
                            activationcount += 1;
                            var geoposition = [0, 0];
                            var device = '';
                            var txid = aitem['auth_id'];
                            var nai = record['nai'];
                            var rtype = 'Activation';
                            var location = '100002';
                            var country = 'US';
                            var seconds = '-';
                            var bytes = '-';
                            if (aitem['sqi'].length > 0) {
                                for (var _k = 0; _k < aitem['sqi'].length; _k++) {
                                    var _sqi = aitem['sqi'][_k];
                                    device = _sqi['device_id'];
                                    if (_sqi['lat'] != 0) geoposition = [_sqi['lat'], _sqi['lng']];
                                }
                            }
                            alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes]);
                        }
                    }
                    var userbytes = 0;
                    var userseconds = 0;

                    var _loop = function _loop(_j) {
                        var citem = record['user_tx'][_j];
                        citem.date_obj = _moment2.default.utc(citem.tx_date.$date);
                        if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {

                            ///
                            for (var _k2 = 0; _k2 < citem['sqi'].length; _k2++) {
                                var _sqi2 = citem['sqi'][_k2];
                                var _dev_id = _sqi2.device_id;
                                var _dev_found = false;
                                for (var _l2 = 0; _l2 < record['devices_current'].length; _l2++) {
                                    if (record['devices_current'][_l2]['device_id'] == _dev_id) _dev_found = true;
                                }
                                if (!_dev_found) {
                                    for (var _l3 = 0; _l3 < record['devices'].length; _l3++) {
                                        if (record['devices'][_l3]['device_id'] == _dev_id) {
                                            record['devices_current'].push(record['devices'][_l3]);
                                            break;
                                        }
                                    }
                                }
                            }
                            ///
                            found = true;
                            record['user_tx_current'].push(citem);
                            connectioncount += 1;
                            var _geoposition = [0, 0];
                            if (citem['usage']['secs'] > 0) {
                                ///Connections
                                var txday = citem.date_obj.format('YYYYMMDD');
                                if (tprecord['connections']['data'][txday] == undefined) tprecord['connections']['data'][txday] = 0;
                                tprecord['connections']['data'][txday] += 1;
                                var _txid = citem['tx_id'];
                                var _nai = record['nai'];
                                var _rtype = 'Connection';
                                var _location = citem['provider_id'];
                                var _country = undefined;
                                providerdata.some(function (prov) {
                                    if (prov['id'] == citem['provider_id']) {
                                        _country = prov['iso_code'];
                                        return true;
                                    }
                                });
                                var _device = citem['calling_station'];
                                if (citem['sqi'].length > 0) for (var _k3 = 0; _k3 < citem['sqi'].length; _k3++) {
                                    var _sqi3 = citem['sqi'][_k3];
                                    if (_sqi3['lat'] != 0) _geoposition = [_sqi3['lat'], _sqi3['lng']];
                                }
                                var _seconds = citem['usage']['secs'];
                                var _bytes = 0;
                                _bytes += citem['usage']['bytes_in'];
                                _bytes += citem['usage']['bytes_out'];
                                _bytes += citem['usage']['giga_in'] * 2147483648;
                                _bytes += citem['usage']['giga_out'] * 2147483648;
                                userbytes += _bytes;
                                totaldata += _bytes;
                                userseconds += _seconds;
                                totalsecs += _seconds;
                                alltx.push([citem.date_obj, _txid, _nai, _rtype, _location, _country, _geoposition, _device, _seconds, _bytes]);

                                //Duration
                                if (tprecord['duration']['data'][txday] == undefined) tprecord['duration']['data'][txday] = 0;
                                tprecord['duration']['data'][txday] += _seconds;

                                //usage
                                if (tprecord['usage']['data'][txday] == undefined) tprecord['usage']['data'][txday] = 0;
                                tprecord['usage']['data'][txday] += _bytes;
                            }
                        }
                    };

                    for (var _j = 0; _j < record['user_tx'].length; _j++) {
                        _loop(_j);
                    }
                    record['usage_current'] = { total_bytes: userbytes, total_seconds: userseconds };
                    if (found) stats.push(record);
                }
            }

            tprecord['connections']['count'] = connectioncount;
            tprecord['activations']['count'] = activationcount;
            tprecord['totals']['tbytes'] = totaldata;
            tprecord['totals']['tseconds'] = totalsecs;

            for (var _i = 0; _i < stats.length; _i++) {
                var item = stats[_i];
                for (var _j2 = 0; _j2 < item['devices_current'].length; _j2++) {
                    var dev = item['devices_current'][_j2];
                    tprecord['devices']['count'] += 1;
                    if (dev['platform']['device_model'] == 'iPhone') {
                        tprecord['devices']['data']['iphone']['count'] += 1;
                        if (!tprecord['devices']['data']['iphone']['os'].hasOwnProperty(dev['platform']['ios_version'])) tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] = 1;else tprecord['devices']['data']['iphone']['os'][dev['platform']['ios_version']] += 1;
                        if (!tprecord['devices']['data']['iphone']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['iphone']['release'][dev['version']] = 1;else tprecord['devices']['data']['iphone']['release'][dev['version']] += 1;
                    } else if (dev['platform']['device_model'] == 'Android') {
                        tprecord['devices']['data']['android']['count'] += 1;
                        if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['os_version'])) tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] = 1;else tprecord['devices']['data']['android']['os'][dev['platform']['os_version']] += 1;
                        if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version'])) tprecord['devices']['data']['android']['release'][dev['version']] = 1;else tprecord['devices']['data']['android']['release'][dev['version']] += 1;
                    }
                }
            }
            tprecord['user_count'] = stats.length;
            tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
            tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
            if (tprecord['user_count'] > 0) {
                tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count']);
                tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count']);
            }
            this.setState({
                source_stats: documents,
                current_stats: stats,
                total_stats: tprecord,
                transactions: alltx,
                user: {
                    nai: documents[0].nai,
                    hash_id: documents[0].hash_id,
                    customer_id: documents[0].customer_id
                }
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            var props = this.props,
                state = this.state;

            if (this.firstLoad) {
                return _react2.default.createElement(
                    "div",
                    null,
                    "Loading user stats.."
                );
            }
            var user = state.user,
                startDate = state.startDate,
                endDate = state.endDate,
                source_stats = state.source_stats,
                total_stats = state.total_stats,
                current_stats = state.current_stats,
                transactions = state.transactions;
            var customer = props.customer,
                customers = props.customers,
                providers = props.providers,
                history = props.history,
                match = props.match;

            var releases = undefined;
            if (total_stats != undefined) {
                releases = [];
                var release = total_stats.devices.data.iphone.release;
                for (var key in release) {
                    if (release.hasOwnProperty(key)) {
                        releases.push({ name: key, count: release[key] });
                    }
                }
            }

            var osversions = undefined;
            if (total_stats != undefined) {
                osversions = [];
                var os = total_stats.devices.data.iphone.os;
                for (var _key in os) {
                    if (os.hasOwnProperty(_key)) {
                        osversions.push({ name: _key, count: os[_key] });
                    }
                }
            }
            return _react2.default.createElement(
                _reactRouterDom.Switch,
                null,
                _react2.default.createElement(_reactRouterDom.Route, { path: "" + props.match.url + (props.match.url.slice(-1) == '/' ? ':day' : '/:day'), render: function render(componentProps) {
                        return _react2.default.createElement(
                            "div",
                            null,
                            _react2.default.createElement(_UserDaily2.default, _extends({}, componentProps, {
                                customer: customer,
                                customers: customers,
                                user: user,
                                providers: providers,
                                startDate: startDate,
                                endDate: endDate,
                                source_stats: source_stats,
                                handleMonthSubstract: _this4.handleMonthSubstract,
                                handleMonthAdd: _this4.handleMonthAdd
                            }))
                        );
                    } }),
                _react2.default.createElement(_reactRouterDom.Route, { render: function render(componentProps) {
                        return _react2.default.createElement(
                            "div",
                            null,
                            _react2.default.createElement(_HeaderMonthly2.default, _extends({}, props, {
                                user: user,
                                startDate: startDate,
                                endDate: endDate,
                                handleDateChange: _this4.handleDateChange,
                                handleMonthSubstract: _this4.handleMonthSubstract,
                                handleMonthAdd: _this4.handleMonthAdd
                            })),
                            _react2.default.createElement(
                                _reactstrap.Container,
                                { fluid: true, className: "main_container" },
                                _react2.default.createElement(_Totals2.default, { stats: total_stats }),
                                _react2.default.createElement(
                                    _reactstrap.Row,
                                    null,
                                    _react2.default.createElement(
                                        _reactstrap.Col,
                                        { sm: "12" },
                                        _react2.default.createElement(
                                            "div",
                                            { style: style.piecontainer },
                                            _react2.default.createElement(_Donut2.default, { data: releases, title: "RoamVU Releases" }),
                                            _react2.default.createElement(_Donut2.default, { data: osversions, title: "iOS Versions" }),
                                            _react2.default.createElement(_TableData2.default, { data: total_stats })
                                        )
                                    )
                                ),
                                _react2.default.createElement(_UsageDuration2.default, { stats: total_stats, match: props.match, history: props.history }),
                                _react2.default.createElement(_Transactions2.default, {
                                    stats: transactions,
                                    month: _this4.state.startDate.format('MMMM'),
                                    date: _this4.state.startDate,
                                    user: user,
                                    customer_id: props.customer.id,
                                    hideProvider: true
                                })
                            )
                        );
                    } })
            );
        }
    }]);

    return UserMonthly;
}(_react2.default.Component);

exports.default = UserMonthly;

/***/ })

},[1605]);