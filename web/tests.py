from django.test import TestCase

# Create your tests here.
from web.functions import send_end_user_registration_email
from web.models import User


class EmailTests(TestCase):
    email = 'dmitriigaidarji@gmail.com'

    def test_end_user_register(self):
        send_end_user_registration_email(User.objects.get(email=self.email))
        self.assertTrue(True)