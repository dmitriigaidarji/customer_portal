from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from web.functions import send_end_user_registration_email
from web.models import User

message_key = 'detail'

class ResetPassword(APIView):
    def post(self, request):
        uid = request.data.get('uid', None)
        if uid is not None:
            if User.objects.filter(username=uid, groups__name='End User').exists():
                user = User.objects.get(username=uid, groups__name='End User')
                send_end_user_registration_email(user, bcc=None)
                return Response({
                    message_key: 'Password has been reset.',
                    'email': user.email
                })
            elif User.objects.filter(email=uid, groups__name='End User').exists():
                user = User.objects.get(email=uid, groups__name='End User')
                send_end_user_registration_email(user, bcc=None)
                return Response({
                    message_key: 'Password has been reset.',
                    'email': user.email
                })
            else:
                return Response({message_key: 'User not found'}, status=status.HTTP_404_NOT_FOUND)
        return Response({message_key: 'UID was not provided'}, status=status.HTTP_400_BAD_REQUEST)