#!/usr/bin/python2
import os, time, bson
from pymongo import MongoClient as Connection
from pymongo import ASCENDING
from pymongo import MongoClient as Connection
from pymongo.errors import ConnectionFailure
from pymongo import GEO2D
from bson.son import SON
from userarea import constants
transdb = Connection(constants.TRANSACTION_DB_HOST, constants.TRANSACTION_DB_PORT)

#x = 5.66
#y = 139.73
def find_nearest(x,y,limit=5):
    '''Finds nearest points within 1 degree (69 miles) and returns the top 5 (close or not)'''
    db = transdb['transdb']
    locations = db.locations
    nearest = []
    query = {'location': SON([('$near', [x,y]), ('$maxDistance', 1)])}
    for record in locations.find(query).limit(limit):
        nearest.append(record)
    return nearest

#x=22.285326
#y=114.157219
# Good for finding the closes hotspot to connection
def find_within(x,y,offset=0.00125):
    '''Finds all points within a specified offset boundary for SW/NE or returns none'''
    db = transdb['transdb']
    locations = db.locations
    within = []
    ne = [x+offset, y+offset]
    sw = [x-offset, y-offset]
    query = {'location': {'$within': {'$box': [sw,ne]}}}
    for record in locations.find(query).sort('location_id'):
        within.append(record)
    return within

#x=35.6843405
#y=139.6883869
# Good for finding all locations in a larger Region or Country
# Must be limited or it will exceed memory
def find_circle(x,y,radius=1,limit=1000):
    '''Finds all points within a specified radius of a centerpoint'''
    db = transdb['transdb']
    locations = db.locations
    circle = []
    query = {'location': {'$within': {'$center': [[x,y], radius]}}}
    for record in locations.find(query).limit(limit):
        circle.append(record)
    return circle

#>>> import find_pois as fp
#>>> locations = fp.find_geonear(22.285326,114.157219)
#>>> len(locations)
#100
#>>> from pprint import pprint
#>>> pprint(locations[:3])
def find_geonear(x,y):
    '''Finds closest 100 locations by nearest.  Take top slice.'''
    db = transdb['transdb']
    nearest = []
    locations = db.command(SON([('geoNear', 'locations'), ('near', [x,y])]))
    for record in locations['results']:
        nearest.append(record)
    return nearest


def find_all():
    db = transdb['transdb']
    locations = db.locations
    results = []
    for record in locations.find({'status': 'Active'}):
        results.append({
            'l': record['location'],
            'a': record['english_address'],
            'n': record['venue_name'],
            'c': record['country_name']
        })
    return results

def find_rect(x1,y1,x2,y2):
    '''Finds all points within a specified offset boundary for SW/NE or returns none'''
    db = transdb['transdb']
    locations = db.locations
    within = []
    sw = [x1, y1]
    ne = [x2, y2]
    query = {'location': {'$within': {'$box': [sw,ne]}}}
    for record in locations.find(query).sort('location_id'):
        within.append(record)
    return within
