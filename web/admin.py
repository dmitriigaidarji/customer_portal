from django.contrib import admin


# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from web.models import User, Customer, Domain

from django import forms

class CustomerForm(UserChangeForm):
    new_company = forms.CharField(max_length=40, label='Create new company', required=False)

    def clean_new_company(self):
        company = self.cleaned_data.get('company')
        new_company = self.cleaned_data.get('new_company')
        if company is None and new_company == u'':
            raise forms.ValidationError(u"You haven't chosen a company for the customer. Want to create a new one?")
        else:
            return new_company

    # def save(self, commit=True):
    #     new_company = self.cleaned_data.get('new_company', None)
    #     if new_company and not Company.objects.filter(name=new_company).exists():
    #         Company.objects.create(name=new_company)
    #     Customer.objects
    #     return super(CustomerForm, self).save(commit=commit)

    class Meta(UserChangeForm.Meta):
        fields = '__all__'
        model = User

class CustomerAdmin(UserAdmin):
    list_display = ('email', 'company')
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('company',)}),
    )
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('company',)}),
    )
    pass


admin.site.register(Customer)
admin.site.register(Domain)
# admin.site.register(User, CustomerAdmin)
admin.site.register(User)