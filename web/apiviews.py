import calendar
import datetime
import hashlib

from django.core import serializers
from django.db import transaction
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views import View

import tdbQuery as tdb
from bson import json_util
import json
import os
from django.utils import timezone

from landing.models import Subscriber
from models import User, Order, ActivationCode, \
    Transaction as OrderTransaction, Customer, DomainRequest, Referral
from django.core.cache import cache
import logging
import localconstants
import csv
from email.utils import parseaddr

from web import config
from web.find_pois import find_within, find_all, find_rect
from django.forms.models import model_to_dict
import time
from django.contrib.auth.models import Group, Permission
import re

from web.functions import send_end_user_registration_email, send_custom_end_user_registration_email, send_referral_email
from web.views import get_user_permissions, delete_user_from_rad, send_codes_for_order
from web.tasks import issue_activation_code
email_pattern = re.compile(
    '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
logger = logging.getLogger('web')


def json_default(value):
    if isinstance(value, datetime.date):
        return value.isoformat()
    elif isinstance(value, datetime.datetime):
        return value.isoformat()
    else:
        return value.__dict__


# Data Conversion
def convert_secs(seconds):
    if seconds > 0:
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        time = '%d:%02d:%02d' % (h, m, s)
    else:
        time = '00:00:00'
    return time


def convert_bytes(bytes):
    if bytes > 1073741824:
        data = "{0:.2f}".format(float(bytes / 1073741824)) + ' Gb'
    elif bytes > 1048576:
        data = "{0:.2f}".format(float(bytes / 1048576)) + ' Mb'
    elif bytes > 1024:
        data = "{0:.2f}".format(float(bytes / 1024)) + ' Kb'
    else:
        data = str(bytes) + ' b'
    return data


def hashed_id(str):
    hash_object = hashlib.sha256(str)
    hex_dig = hash_object.hexdigest()
    return hex_dig


def get_hotspots(all_data):
    hotspots = []
    offset = 0.0015

    class Rect:
        sw = 0
        nw = 0
        se = 0
        ne = 0

        def __init__(self, x, y):
            self.sw = (x - offset, y - offset)
            self.nw = (x - offset, y + offset)
            self.ne = (x + offset, y + offset)
            self.se = (x + offset, y - offset)

        def point_in_rect(self, p):
            if self.sw[0] <= p[0] <= self.ne[0] and self.sw[1] <= p[1] <= self.ne[1]:
                return True
            return False

        def overlap(self, rect):
            if self.point_in_rect(rect.sw):
                return True
            if self.point_in_rect(rect.se):
                return True
            if self.point_in_rect(rect.nw):
                return True
            if self.point_in_rect(rect.ne):
                return True
            return False

        def combine_all(self, rects):
            for rect in rects:
                self.sw = (min(self.sw[0], rect.sw[0]), min(self.sw[1], rect.sw[1]))
                self.nw = (min(self.nw[0], rect.nw[0]), max(self.nw[1], rect.nw[1]))
                self.ne = (max(self.ne[0], rect.ne[0]), max(self.ne[1], rect.ne[1]))
                self.se = (max(self.se[0], rect.se[0]), min(self.se[1], rect.se[1]))

    rects = []
    for i in xrange(0, len(all_data)):
        item = all_data[i]
        lat = float(item['lat'])
        lng = float(item['lng'])
        if lat != 0 and lng != 0:
            newrect = Rect(lat, lng)

            # check if rect overlaps with other rects
            overlapped = []
            overlappedindexes = []
            for j in xrange(0, len(rects)):
                rect = rects[j]
                if rect.overlap(newrect):
                    overlapped.append(rect)
                    overlappedindexes.append(j)
            if len(overlapped) > 0:
                newrect.combine_all(overlapped)
                for index in sorted(overlappedindexes, reverse=True):
                    del rects[index]

            rects.append(newrect)

    for rect in rects:
        hotspots.extend(json.loads(json.dumps(
            find_rect(
                rect.sw[0],
                rect.sw[1],
                rect.ne[0],
                rect.ne[1])
            , default=json_util.default)
        ))
    return hotspots


class Main(View):
    def get(self, request, customer_id, month):
        if request.user.is_superuser:
            return render(request, 'console/main.html',
                          # {'data': json.dumps(pstats.documents, default=json_util.default)}
                          )
        return HttpResponse('error', status=403)


class Stats(View):
    def get(self, request, customer_id, start_date, end_date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse({'results':
                                     localconstants.providers_monthly
                                 })
        s_y = int(start_date[:4])
        s_m = int(start_date[4:6])
        s_d = int(start_date[6:8])
        e_y = int(end_date[:4])
        e_m = int(end_date[4:6])
        e_d = int(end_date[6:8])
        if s_y != e_y:
            return JsonResponse({'message': 'Years should be equal'}, status=400)
        results = cache.get('console_stats_{}_{}_{}'.format(customer_id, start_date, end_date), None)
        if results is None:
            start_day = datetime.datetime(s_y, s_m, s_d)
            end_day = datetime.datetime(e_y, e_m, e_d, 23, 59)
            query = tdb.tdbQuery()
            query.pstats(start_day, end_day, customer_id)
            pstats = tdb.tdbConnect(query)
            pstats.transdb()
            pstats.execute()
            now = datetime.datetime.now()
            data = json.dumps(pstats.documents, default=json_util.default)
            if now.month == s_m:
                cache.set('console_stats_{}_{}_{}'.format(customer_id, start_date, end_date), data,
                          timeout=86400)  # 1 day
            else:
                cache.set('console_stats_{}_{}_{}'.format(customer_id, start_date, end_date), data,
                          timeout=2592000)  # 1 month
            return JsonResponse({'results': data})
        else:
            return JsonResponse({'results': results})


class UserMonthly(View):
    def get(self, request, hash, start_date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.user2)  # json.loads(localconstants.user)})
        results = cache.get('console_users_{}_{}'.format(hash, start_date), None)
        if results is None:
            try:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m%d')
                charge_period = (charge_date.year, charge_date.month)
            except:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m')
                charge_period = (charge_date.year, charge_date.month)
            query = tdb.tdbQuery()
            # Check if nai or hash
            if '@' in parseaddr(hash)[1]:
                query.attr_user(charge_period, hash)
            else:
                query.attr_hash(charge_period, hash)
            atrecords = tdb.tdbConnect(query)
            atrecords.transdb()
            atrecords.execute()
            if len(atrecords.documents) == 0:
                return JsonResponse({'results': []})
            else:
                data = json.loads(json.dumps(atrecords.documents, default=json_util.default))
                if charge_date.month == datetime.datetime.now().month:
                    cache.set('console_users_{}_{}'.format(hash, start_date), data, timeout=86400)
                else:
                    cache.set('console_users_{}_{}'.format(hash, start_date), data, timeout=2592000)
                return JsonResponse({'results': data})
        else:
            return JsonResponse({'results': results})


class CustomerMonth(View):
    def get(self, request, customer_id, start_date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.monthly1)
        results = cache.get('console_customers_{}_{}'.format(customer_id, start_date), None)
        if results is None:
            try:
                customer_id = int(customer_id)
            except:
                pass
            try:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m%d')
                charge_period = (charge_date.year, charge_date.month)
            except:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m')
                charge_period = (charge_date.year, charge_date.month)
            # initialize query object
            query = tdb.tdbQuery()
            # montly attribution document
            query.attr_company(charge_period, customer_id)
            atrecords = tdb.tdbConnect(query)
            atrecords.transdb()
            atrecords.execute()

            # calc hotspots
            # coordinates = [];
            # for item in atrecords.documents:
            #     for activation in item['activations']:
            #         for sqi in activation['sqi']:
            #             coordinates.append({
            #                 'lat': sqi['lat'],
            #                 'lng': sqi['lng']
            #             })
            #     for connection in item['user_tx']:
            #         for sqi in connection['sqi']:
            #             coordinates.append({
            #                 'lat': sqi['lat'],
            #                 'lng': sqi['lng']
            #             })

            data = {
                'results': json.loads(json.dumps(atrecords.documents, default=json_util.default)),
                'hotspots': []  # get_hotspots(coordinates)
            }
            if charge_date.month == datetime.datetime.now().month:
                cache.set('console_customers_{}_{}'.format(customer_id, start_date), data, timeout=86400)
            else:
                cache.set('console_customers_{}_{}'.format(customer_id, start_date), data, timeout=2592000)
            return JsonResponse(data)
        else:
            return JsonResponse(results)


class CustomerUsageCSV(View):
    def get(self, request, customer_id, start_date):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}-{}-usage.csv"'.format(customer_id, start_date)

        charge_period = (0, 0)
        try:
            customer_id = int(customer_id)
        except:
            pass
        try:
            charge_date = datetime.datetime.strptime(start_date, '%Y%m%d')
            charge_period = (charge_date.year, charge_date.month)
        except:
            try:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m')
                charge_period = (charge_date.year, charge_date.month)
            except:
                pass
        # initialize query object
        query = tdb.tdbQuery()
        # montly attribution document
        query.attr_company(charge_period, customer_id)
        atrecords = tdb.tdbConnect(query)
        atrecords.transdb()
        atrecords.execute()

        writer = csv.writer(response)
        writer.writerow(['id', 'User NAI/ActivationID', 'User HashID', 'Devices', 'Sign-In',
                         'Connect', 'Total Data', 'Total Time', 'Consumes'])
        if len(atrecords.documents) > 0:
            for i in xrange(0, len(atrecords.documents)):
                item = atrecords.documents[i]
                if item['usage']['total_seconds'] > 3600:
                    interval = 'Hr'
                    avg_rate = convert_bytes(
                        item['usage']['total_bytes'] / (item['usage']['total_seconds'] / float(3600)))
                elif item['usage']['total_seconds'] > 60:
                    interval = 'Min'
                    avg_rate = convert_bytes(
                        item['usage']['total_bytes'] / (item['usage']['total_seconds'] / float(60)))
                elif item['usage']['total_seconds'] > 0:
                    interval = 'Sec'
                    avg_rate = convert_bytes(item['usage']['total_bytes'] / item['usage']['total_seconds'])
                else:
                    interval = 'Sec'
                    avg_rate = '0 B'

                writer.writerow([
                    i + 1, item['nai'], item['hash_id'].split('@')[0],
                    len(item['devices']), len(item['activations']), len(item['user_tx']),
                    convert_bytes(item['usage']['total_bytes']),
                    convert_secs(item['usage']['total_seconds']),
                    "{}/{}".format(avg_rate, interval)
                ])
        return response


class CustomerTransactionsCSV(View):
    def get(self, request, customer_id, start_date):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}-{}-transactions.csv"'.format(customer_id,
                                                                                                 start_date)

        charge_period = (0, 0)
        try:
            customer_id = int(customer_id)
        except:
            pass
        try:
            charge_date = datetime.datetime.strptime(start_date, '%Y%m%d')
            charge_period = (charge_date.year, charge_date.month)
        except:
            try:
                charge_date = datetime.datetime.strptime(start_date, '%Y%m')
                charge_period = (charge_date.year, charge_date.month)
            except:
                pass
        # initialize query object
        query = tdb.tdbQuery()
        # montly attribution document
        query.attr_company(charge_period, customer_id)
        atrecords = tdb.tdbConnect(query)
        atrecords.transdb()
        atrecords.execute()
        alltx = []
        if len(atrecords.documents) > 0:
            activationcount = 0
            connectioncount = 0
            providerdata = get_providers()
            for record in atrecords.documents:
                for aitem in record['activations']:
                    activationcount += 1
                    geoposition = (0, 0)
                    device = ''
                    txid = aitem['auth_id']
                    date = aitem['date']
                    nai = record['nai']
                    rtype = 'Activation'
                    location = '100002'
                    country = 'US'
                    if len(aitem['sqi']) > 0:
                        geopostion = (0, 0)
                        for sqi in aitem['sqi']:
                            device = sqi['device_id']
                            if sqi['lat'] != 0:
                                geoposition = (sqi['lat'], sqi['lng'])
                    seconds = '-'
                    bytes = '-'
                    line = (date, txid, nai, rtype, location, country, geoposition, device, seconds, bytes)
                    alltx.append(line)
                for citem in record['user_tx']:
                    connectioncount += 1
                    geoposition = (0, 0)
                    # exclude sessions under X seconds
                    if citem['usage']['secs'] > 0:
                        txid = citem['tx_id']
                        date = citem['tx_date']
                        nai = record['nai']
                        rtype = 'Connection'
                        location = citem['provider_id']
                        country = None
                        for prov in providerdata:
                            if prov['id'] == citem['provider_id']:
                                country = prov['iso_code']
                                break
                        # country = providerdata[citem['provider_id']]['iso_code']
                        device = citem['calling_station']
                        if len(citem['sqi']) > 0:
                            for sqi in citem['sqi']:
                                if sqi['lat'] != 0:
                                    geoposition = (sqi['lat'], sqi['lng'])
                        seconds = citem['usage']['secs']
                        bytes = 0
                        bytes += citem['usage']['bytes_in']
                        bytes += citem['usage']['bytes_out']
                        bytes += citem['usage']['giga_in'] * 2147483648
                        bytes += citem['usage']['giga_out'] * 2147483648
                        line = (date, txid, nai, rtype, location, country, geoposition, device, seconds, bytes)
                        alltx.append(line)

        writer = csv.writer(response)
        writer.writerow(['Date', 'Transaction ID', 'User Identity', 'Tx Type', 'Provider',
                         'Country', 'Geoposition', 'Device', 'Seconds', 'Bytes'])
        if len(alltx) > 0:
            for i in xrange(0, len(alltx)):
                item = alltx[i]
                writer.writerow([
                    item[0].strftime('%Y-%m-%d %H:%M:%S'),
                    item[1],
                    item[2],
                    item[3],
                    item[4],
                    item[5],
                    "{},{}".format(item[6][0], item[6][1]),
                    item[7],
                    item[8],
                    item[9]
                ])
        return response


class StatsMonth(View):
    def get(self, request, customer_id, start_date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            if customer_id == '2':
                return JsonResponse({'results': []})
            if int(start_date[4:6]) == 4:
                return JsonResponse({'results':
                                         json.loads(localconstants.providers_monthly_2)
                                     })
            return JsonResponse({'results':
                                     json.loads(localconstants.providers_monthly)
                                 })
        s_y = int(start_date[:4])
        s_m = int(start_date[4:6])
        results = cache.get('console_stats_{}_{}'.format(customer_id, start_date), None)
        if results is None:
            _, num_days = calendar.monthrange(s_y, s_m)
            start_day = datetime.datetime(s_y, s_m, 1, 0, 0)
            end_day = datetime.datetime(s_y, s_m, num_days, 23, 59)
            query = tdb.tdbQuery()
            query.pstats(start_day, end_day, customer_id)
            pstats = tdb.tdbConnect(query)
            pstats.transdb()
            pstats.execute()
            now = datetime.datetime.now()
            data = json.loads(json.dumps(pstats.documents, default=json_util.default))
            if now.month == s_m:
                cache.set('console_stats_{}_{}'.format(customer_id, start_date), data, timeout=86400)  # 1 day
            else:
                cache.set('console_stats_{}_{}'.format(customer_id, start_date), data, timeout=2592000)  # 1 month
            logger.info('Uncached results of console_stats_{}_{}'.format(customer_id, start_date))
            return JsonResponse({'results': data})
        else:
            logger.info('Cached results of console_stats_{}_{}'.format(customer_id, start_date))
            return JsonResponse({'results': results})


class DailyTransactions(View):
    def get(self, request, customer_id, start_date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.pdaily3)
        results = cache.get('console_daily_{}_{}'.format(customer_id, start_date), None)
        if results is None:
            startday = datetime.datetime.strptime(start_date, '%Y%m%d')
            endday = startday + datetime.timedelta(days=1)

            query = tdb.tdbQuery()
            query.tx_closed(startday, endday, customer_id)

            txrecords = tdb.tdbConnect(query)
            txrecords.transdb()
            txrecords.execute()
            docs = txrecords.documents
            for doc in docs:
                doc['avp'] = None
                doc['auth'] = None
            data = json.loads(json.dumps(docs, default=json_util.default))
            cache.set('console_daily_{}_{}'.format(customer_id, start_date), data, timeout=2592000)
            logger.info('Uncached results of console_daily_{}_{}'.format(customer_id, start_date))
            return JsonResponse({'results': data})
        else:
            logger.info('Cached results of console_daily_{}_{}'.format(customer_id, start_date))
            return JsonResponse({'results': results})


class ConsoleApp(View):
    def get(self, request):
        if request.user.is_superuser:
            return render(request, 'console/react.html')
        return redirect('login')
        # return HttpResponse('Access denied. Please login to admin account first!', status=403)


        # API


class Test(View):
    def get(self, request):
        return JsonResponse({'message': 'success'})

    def post(self, request):
        return JsonResponse({'message': 'post success'})


def get_providers():
    if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
        return [
            {
                'id': 1,
                'name': 'name1',
                'country': 'US',
                'iso_code': 'US'
            },
            {
                'id': 2,
                'name': 'name2',
                'country': 'RU',
                'iso_code': 'US'
            }
        ]
    results = cache.get('console_providers', None)
    if results is None:
        customers = Customer.objects.filter(
            service__in=['Two-way', 'Provider'],
            on_portal=True,
            status__in=['Production', 'In-Test']
        )
        results = []
        for customer in customers:
            results.append({
                'id': customer.customer_id,
                'name': customer.customer_name,
                'country': customer.country.country_name,
                'iso_code': customer.country.iso_code
            })
        cache.set('console_providers', results, timeout=86400)  # 1 day cache
        logger.info('Uncached results of console_providers')
        return results
    else:
        logger.info('Cached results of console_providers')
        return results


class Providers(View):
    def get(self, request):
        providers = get_providers()
        return JsonResponse({'results': providers})


class Customers(View):
    def get(self, request):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse({'results': [
                {
                    'id': 1,
                    'name': 'name1',
                    'country': 'US'
                },
                {
                    'id': 2,
                    'name': 'name2',
                    'country': 'RU'
                }
            ]})
        results = cache.get('console_customers', None)
        if results is None:
            customers = Customer.objects.filter(
                service__in=['Two-way', 'Roamer'],
                on_portal=True,
                status__in=['Production', 'In-Test']
            )
            results = []
            for customer in customers:
                results.append({
                    'id': customer.customer_id,
                    'name': customer.customer_name,
                    'country': customer.country.country_name
                })
            cache.set('console_customers', results, timeout=86400)  # 1 day cache
            return JsonResponse({'results': results})
        else:
            return JsonResponse({'results': results})


class Transaction(View):
    def get(self, request, transaction_id):
        query = tdb.tdbQuery()
        query.tx_query({'transaction_id': transaction_id})
        txrecords = tdb.tdbConnect(query)
        txrecords.transdb()
        txrecords.execute()
        if len(txrecords.documents) == 0:
            return JsonResponse({})
        else:
            txrecord = txrecords.documents[0]
            return JsonResponse({'result': json.loads(json.dumps(txrecord, default=json_util.default))})


class Poi_all(View):
    def get(self, request):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse({'results':localconstants.hotspots})
        results = cache.get('console_all_pois', None)
        if results is None:
            results = find_all()
            cache.set('console_all_pois', results, timeout=2592000)
        return JsonResponse({'results': results})


class ClearCache(View):
    def post(self, request):
        cache.delete_pattern('console*')
        # cache.delete_pattern('console_providers')
        # cache.delete_pattern('console_stats_*')
        # cache.delete_pattern('console_daily_*')
        # cache.delete_pattern('console_customer*')
        # cache.delete_pattern('console_users*')
        return JsonResponse({'message': 'success'})


## SQI
class SQIUser(View):
    def get(self, request, user, date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.sqiuserdaily)
        if '@' in parseaddr(user)[1]:
            user = hashed_id(user)
        path = '{}{}/{}.json'.format(config.SQI_LOCATION, date, user)
        hotspots = []
        if not os.path.isfile(path):
            return JsonResponse({'results': [], 'hotspots': hotspots})
        with open(path) as data_file:
            data = json.load(data_file)
            for item in data:
                top = bottom = left = right = None
                lat = float(item['lat'])
                lng = float(item['lng'])
                if lat != 0 and lng != 0:
                    if top is None:
                        top = lat
                    elif top < lat:
                        top = lat

                    if bottom is None:
                        bottom = lat
                    elif bottom > lat:
                        bottom = lat

                    if left is None:
                        left = lng
                    elif left > lng:
                        left = lng

                    if right is None:
                        right = lng
                    elif right < lng:
                        right = lng

                if all([top, bottom, left, right]):
                    offset = 0.00125
                    hotspots = json.loads(json.dumps(
                        find_rect(
                            bottom - offset,
                            left - offset,
                            top + offset,
                            right + offset)
                        , default=json_util.default))

            return JsonResponse({'results': data, 'hotspots': hotspots})


class SQIDaily(View):
    def get(self, request, date):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.sqidaily)
        source_path = '{}{}/'.format(config.SQI_LOCATION, date)
        walkers = os.walk(source_path)
        all_data = []
        try:
            walker = walkers.next()
        except:
            return JsonResponse({'results': []})
        for filename in walker[2]:
            if '.json' in filename:
                with open(os.path.join(walker[0], filename)) as data_file:
                    all_data.extend(json.load(data_file))
        return JsonResponse({'results': all_data, 'hotspots': get_hotspots(all_data)})


class Poi(View):
    def get(self, request):
        lat = float(request.GET.get('lat'))
        lng = float(request.GET.get('lng'))
        locations = find_within(lat, lng)
        return JsonResponse({'results': json.loads(json.dumps(locations, default=json_util.default))})

    def post(self, request):
        if os.environ['DJANGO_SETTINGS_MODULE'] == 'userarea.localsettings':
            return JsonResponse(localconstants.hotspots)
        data = json.loads(request.body)
        return JsonResponse({'hotspots': get_hotspots(data['coordinates'])})


# Subscribers and Testers
class Testers(View):
    def update_status(self, testers):
        with transaction.atomic():
            now = timezone.now()
            customer = Customer.objects.get(customer_id=100005)
            for tester in testers:
                if User.objects.filter(email=tester.email, customer=customer).exists():
                    user = User.objects.filter(email=tester.email, customer=customer).first()
                    if user.activated != None:
                        exp_date = (user.activated + datetime.timedelta(30))
                        if now > exp_date:
                            tester.status = 'Ended'
                        else:
                            tester.status = 'Active'
                    elif user.is_active:
                        tester.status = 'Started'
                    else:
                        tester.status = 'Offered'
                    tester.save()

    def update_status_if_necessary(self, testers):
        if not cache.get('testers_status_update', False):
            self.update_status(testers)
            cache.set('testers_status_update', True,
                      timeout=7200)
            return True
        return False

    def get(self, request):
        testers = Subscriber.objects.filter(quiz__going_to_HK_or_JP_date__in=('0', '1'))
        others = Subscriber.objects.exclude(quiz__going_to_HK_or_JP_date__in=('0', '1'))

        # if self.update_status_if_necessary(testers):
        #     testers = Subscriber.objects.filter(quiz__going_to_HK_or_JP_date__in=('0', '1'))

        testersdata = []
        for index in xrange(len(testers) - 1, 0, -1):
            tester = testers[index]
            choice = tester.quiz.get_going_to_HK_or_JP_date_display()
            d = model_to_dict(tester)
            d['destination'] = choice
            d['created'] = int(time.mktime(tester.created.timetuple()))
            try:
                user = User.objects.get(email=tester.email)
                d['instructions_send_date'] = int(time.mktime(user.created.timetuple()))
                if user.activated != None:
                    d['expiration_date'] = int(time.mktime((user.activated + datetime.timedelta(30)).timetuple()))
            except:
                pass
                # if tester.instructions_send_date != None:
                #     d['instructions_send_date'] = int(time.mktime(tester.instructions_send_date.timetuple()))
                # if tester.expiration_date != None:
                #     d['expiration_date'] = int(time.mktime(tester.expiration_date.timetuple()))
            d['status'] = tester.get_status_display()
            testersdata.append(d)

        subscribersdata = []
        for d in json.loads(serializers.serialize('json', others)):
            d['fields']['pk'] = d['pk']
            subscribersdata.append(d['fields'])
        return JsonResponse(
            {
                'testers': testersdata,
                'subscribers': subscribersdata
            })

    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        field = data['field']
        value = data['value']
        if email != None and field != None and value != None:
            # return JsonResponse({'m' : field}, status=403)
            if field in ['status',
                         'test_flight',
                         'roamvu_instructions',
                         'instructions_send_date',
                         'expiration_date',
                         'followup_message_sent',
                         'thank_you_message_sent',
                         'beta_notes']:
                sub = Subscriber.objects.get(email=email)
                if field in ['instructions_send_date',
                             'expiration_date']:
                    if value == '':
                        setattr(sub, field, None)
                    else:
                        setattr(sub, field, datetime.datetime.fromtimestamp(value))
                else:
                    setattr(sub, field, value)
                sub.save()
                return JsonResponse({'message': 'success'})
        return JsonResponse({'message': 'wrong data'}, status=400)


def serialize_user(user):
    group = user.groups.first()
    activated = user.activated
    if activated is not None:
        activated = int(time.mktime(user.activated.timetuple()))
    if group is not None:
        return {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'role': group.name,
            'password_set': user.is_active,
            'created': int(time.mktime(user.created.timetuple())),
            'activated': activated
        }
    return None

def get_orders(customer):
        orders_arr = []
        batch_orders_arr = []
        orders = Order.objects.filter(customer=customer)
        for order in orders:
            order_dict = {'value': order.value, 'is_paid': order.is_paid, 'transactions': []}
            batch = False
            for trans in order.transaction_set.all():
                if trans.phone is None and trans.email is None:
                    batch = True
                activated = trans.activation_code.activated
                if activated is not None:
                    activated = int(time.mktime(trans.activation_code.activated.timetuple()))
                order_dict['transactions'].append({
                    'email': trans.email,
                    'phone': trans.phone,
                    'code': trans.activation_code.code,
                    'pin': trans.activation_code.pin,
                    'entitlement': trans.activation_code.entitlement,
                    'date': trans.order_date.strftime('%m/%d/%Y'),
                    'price': float(trans.price),
                    'status': trans.delivery_status,
                    'created': int(time.mktime(trans.activation_code.issue_date.timetuple())),
                    'activated': activated
                })
            if batch:
                batch_orders_arr.append(order_dict)
            else:
                orders_arr.append(order_dict)
        return orders_arr, batch_orders_arr
## Codes View
class Codes(View):
    def post(self, request):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user):
            data = json.loads(request.body)
            email = data['email']
            entitlement = int(data['entitlement'])
            if entitlement in (7, 14, 30):
                with transaction.atomic():
                    order_value = 0
                    order = Order.objects.create(
                        orderer=request.user,
                        customer=request.user.customer,
                        value=order_value,
                        is_paid=True
                    )
                    act_code = ActivationCode.objects.select_for_update().filter(
                        entitlement=entitlement,
                        issue_date=None
                    ).first()
                    OrderTransaction.objects.create(
                        order=order,
                        activation_code=act_code,
                        phone=None,
                        email=email,
                        price=0
                    )
                    act_code.issue_date = datetime.date.today()
                    act_code.save()
                    send_codes_for_order(order.id)
            else:
                return JsonResponse({'message': 'wrong codetype'}, status=400)
            return JsonResponse({'orders': get_orders(request.user.customer)})


class BatchCode(View):
    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}-batchcodes.csv"'.format(request.user.customer.customer_id)
        writer = csv.writer(response)
        writer.writerow(['code', 'pin', 'entitlement'])

        orders = Order.objects.filter(customer=request.user.customer)
        for order in orders:
            for trans in order.transaction_set.all():
                if trans.phone is None and trans.email is None:
                    writer.writerow([
                        trans.activation_code.code,
                        trans.activation_code.pin,
                        trans.activation_code.entitlement
                        ])
        return response

    def post(self, request):
        data = json.loads(request.body)
        amount = int(data['amount'])
        entitlement = int(data['entitlement'])
        if entitlement in (7, 14, 30):
            with transaction.atomic():
                order_value = 0
                order = Order.objects.create(
                    orderer=request.user,
                    customer=request.user.customer,
                    value=order_value,
                    is_paid=True
                )
                for i in xrange(0, amount):
                    act_code = ActivationCode.objects.select_for_update().filter(
                        entitlement=entitlement,
                        issue_date=None
                    ).first()
                    OrderTransaction.objects.create(
                        order=order,
                        activation_code=act_code,
                        price=0
                    )
                    act_code.issue_date = datetime.date.today()
                    act_code.save()
                    issue_activation_code.delay(act_code.id)
        return JsonResponse({'batch_orders': get_orders(request.user.customer)[1]})


## Users View
class Users(View):
    def get(self, request):
        #Users
        users = request.user.customer.user_set.all()
        domains = request.user.customer.domain_set.all()
        requested_domains = request.user.customer.domainrequest_set.exclude(status=DomainRequest.STATUS_ACTIVE)
        results = []
        for user in users:
            user_json = serialize_user(user)
            if user_json is not None:
                results.append(user_json)
        #Codes
        orders_arr = []
        batch_orders_arr = []
        if Order.objects.filter(customer=request.user.customer).exists():
            orders_arr, batch_orders_arr = get_orders(request.user.customer)
        return JsonResponse({'users': results,
                             'max_users': request.user.customer.max_users,
                             'max_codes': request.user.customer.max_codes,
                             'domains': [domain.name for domain in domains],
                             'requested_domains': [{
                                 'name': domain.name,
                                 'status': domain.get_status_display()
                             } for domain in requested_domains],
                             'groups': [group.name for group in Group.objects.exclude(name='Customer Administrator')],
                             'orders': orders_arr,
                             'batch_orders': batch_orders_arr
                             })

    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        username = data['username']
        domain = data['domain']
        role = data['role']
        if request.user.customer.domain_set.filter(name=domain).exists() and \
                Group.objects.exclude(name='Customer Administrator').filter(name=role).exists() and \
                        email_pattern.match(email) is not None and email_pattern.match(
            '{}@test.com'.format(username)) is not None:
            # check if already exists
            if User.objects.filter(username="{}@{}".format(username, domain)).exists():
                return JsonResponse({'message': 'User with such username already exists'}, status=400)

            # create user
            user = User.objects.create(
                email=email,
                username="{}@{}".format(username, domain)
            )
            user.customer = request.user.customer
            user.save()
            user.groups.add(Group.objects.get(name=role))
            user.save()

            # send register email
            send_end_user_registration_email(user)

            # return new data
            users = request.user.customer.user_set.all()
            results = []
            for user in users:
                user_json = serialize_user(user)
                if user_json is not None:
                    results.append(user_json)
            return JsonResponse({
                'users': results
            })
        return JsonResponse({'message': 'Wrong input data'}, status=400)


class UserDelete(View):
    def post(self, request, username):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user) \
                and None != username != request.user.username:
            user = User.objects.get(username=username, customer=request.user.customer)
            if not user.groups.filter(name='Customer Administrator').exists():
                if user.groups.filter(name='End User').exists() and os.environ[
                    'DJANGO_SETTINGS_MODULE'] != 'userarea.localsettings':
                    delete_user_from_rad(user)
                user.delete()
                # return new data
                users = request.user.customer.user_set.all()
                results = []
                for user in users:
                    user_json = serialize_user(user)
                    if user_json is not None:
                        results.append(user_json)
                return JsonResponse({
                    'users': results
                })
            return JsonResponse({'message': 'can\'t delete this user'}, status=401)
        return JsonResponse({'message': 'error'}, status=400)


class UserReset(View):
    def post(self, request, username):
        if Permission.objects.get(codename='create_user') in get_user_permissions(
                request.user) and None != username != request.user.username:
            user = User.objects.get(username=username, customer=request.user.customer)
            send_end_user_registration_email(user, bcc=None)
            return JsonResponse({'message': 'success'})
        return JsonResponse({'message': 'error'}, status=400)

class RequestDomainEdit(View):
    def post(self, request):
        data = json.loads(request.body)
        request_id = data['request_id']
        value = data['value']
        domain_request = DomainRequest.objects.get(pk=request_id)
        domain_request.status = value
        domain_request.save()
        return JsonResponse({'requested_domains': [{
            'request_id': domain.id,
            'name': domain.name,
            'status': domain.get_status_display(),
            'customer_id': domain.customer.customer_id,
            'user': domain.user.username,
            'created': int(time.mktime(domain.created.timetuple()))
        } for domain in DomainRequest.objects.all()]})

class RequestDomain(View):
    def get(self, request):
        return JsonResponse({'requested_domains': [{
            'request_id': domain.id,
            'name': domain.name,
            'status': domain.get_status_display(),
            'customer_id': domain.customer.customer_id,
            'user': domain.user.username,
            'created': int(time.mktime(domain.created.timetuple()))
        } for domain in DomainRequest.objects.all()]})

    def post(self, request):
        data = json.loads(request.body)
        name = data['name']
        DomainRequest.objects.create(name=name, user=request.user, customer=request.user.customer)
        return JsonResponse({'message': 'success',
                             'requested_domains': [{
                                 'name': domain.name,
                                 'status': domain.get_status_display()
                             } for domain in DomainRequest.objects.filter(customer=request.user.customer)
                            .exclude(status=DomainRequest.STATUS_ACTIVE)]
                             })

class CustomEmailTemplate(View):
    def post(self,request):
        data = json.loads(request.body)
        email = data['email']
        text = data['text']
        send_custom_end_user_registration_email(email, text)
        return JsonResponse({'message': 'good'})

class BatchUsersUpload(View):
    def post(self, request):
        my_uploaded_file = request.FILES['file'].read()
        import csv
        from StringIO import StringIO
        buffer = StringIO(my_uploaded_file)
        spamreader = csv.DictReader(buffer)
        role = Group.objects.get(name='End User')
        counter = 0
        for row in spamreader:
            username = row['username']
            domain = row['domain']
            password = row['password']
            email = row['email']
            if request.user.customer.domain_set.filter(name=domain).exists() and \
                            email_pattern.match(email) is not None and email_pattern.match(
                '{}@{}'.format(username, domain)) is not None:
                counter += 1
                # check if already exists
                if User.objects.filter(username="{}@{}".format(username, domain)).exists():
                    return JsonResponse({'message': 'User with such username already exists'}, status=201)

                # create user
                user = User.objects.create(
                    email=email,
                    username="{}@{}".format(username, domain)
                )
                user.customer = request.user.customer
                if password is not None:
                    user.set_password(password)
                user.save()
                user.groups.add(Group.objects.get(name=role))
                user.save()

                # send register email
                send_end_user_registration_email(user)
        return JsonResponse({'message': '{} users were added'.format(counter)})


class Referrals(View):
    def get(self, request):
        referrals = request.user.referral_set.all()
        results = []
        for item in referrals:
            results.append({
                'id': item.pk,
                'email': item.email,
                'name': item.name,
                'created': int(time.mktime(item.created.timetuple())),
                'status': item.get_status_display()
            })
        return JsonResponse({'results': results})

    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        name = data['name']
        if email is not None and name is not None:
            referral = Referral.objects.create(
                parent = request.user.customer,
                orderer = request.user,
                name = name,
                email = email
            )
            # process referral
            send_referral_email(referral.pk)
            return self.get(request)
        return JsonResponse({'message':'Wrong input data'}, status=201)


class ReferralRequests(View):
    def get(self, request):
        items = Referral.objects.filter(status__in=(Referral.STATUS_SUBMITTED, Referral.STATUS_REJECTED, Referral.STATUS_COMPETED))
        results = []
        for item in items:
            submit_date = None
            if item.submit_date is not None:
                submit_date = int(time.mktime(item.submit_date.timetuple()))
            response_date = None
            if item.response_date is not None:
                response_date = int(time.mktime(item.response_date.timetuple()))
            results.append({
                'id': item.pk,
                'email': item.email,
                'name': item.name,
                'created': int(time.mktime(item.created.timetuple())),
                'submit_date': submit_date,
                'company_name': item.business_name,
                'response_date': item.response_date,
                'status': item.get_status_display()
            })
        return JsonResponse({'results': results})

    def post(self, request):
        data = json.loads(request.body)
        referral_id = data['referral_id']
        value = data['value']
        if referral_id is not None:
            ref = Referral.objects.get(pk=referral_id)
            ref.status = value
            ref.save()
        return self.get(request)

class ReferralsReview(View):
    def post(self, request):
        data = json.loads(request.body)
        referral_id = data['referral_id']
        if referral_id is not None:
            ref = Referral.objects.get(pk=referral_id)
            ref.status = Referral.STATUS_SUBMITTED
            ref.save()
        referrals = request.user.referral_set.all()
        results = []
        for item in referrals:
            results.append({
                'id': item.pk,
                'email': item.email,
                'name': item.name,
                'created': int(time.mktime(item.created.timetuple())),
                'status': item.get_status_display()
            })
        return JsonResponse({'results': results})