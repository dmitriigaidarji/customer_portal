from django.conf.urls import url
import rest_api

urlpatterns = [
    url(r'^users/reset/$', rest_api.ResetPassword.as_view()),
]
