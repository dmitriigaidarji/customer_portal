import React from "react"
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import CustomerMonthly from "../../containers/CustomerMonthly"
import {connect} from 'react-redux'
import {fetchProvidersIfNeeded} from '../../actions/providers'
import {fetchCustomersIfNeeded} from '../../actions/customers'
import WebsiteVersion from '../../../reactjs/components/WebsiteVersion'
import UserMonthly from "../../containers/UserMonthly"
import ReferralsContainer from '../../containers/ReferralsContainer'
class MainController extends React.Component {
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(fetchProvidersIfNeeded());
        dispatch(fetchCustomersIfNeeded());
    }

    render() {
        let {providers, customers, access_level} = this.props;
        if (providers == undefined || customers == undefined || access_level == undefined)
            return <div>Loading...</div>
        return (
            <div>
                <Switch>
                    <Route path={`/${process.env.PUBLIC_URL}/customers/${CONST_CUSTOMER_ID}/:year/:month`}
                           component={CustomerMonthly}/>
                    <Route path={`/${process.env.PUBLIC_URL}/users/:user_hash/:year/:month`}
                           component={UserMonthly}/>
                    {CONST_CAN_HAVE_REFERRALS === true &&
                    <Route path={`/${process.env.PUBLIC_URL}/referrals`} component={ReferralsContainer}/>
                    }
                    <Route render={(props) => <CustomersNoMatch {...props} customers={customers}
                                                                providers={providers}/>}/>
                </Switch>
                <WebsiteVersion/>
            </div>
        )
    }
}

class CustomersNoMatch extends React.Component {
    render() {
        let {customers, providers} = this.props;
        let date = new Date();
        let month = date.getMonth() + 1
        if (month < 10) month = '0' + month
        let url = `/${process.env.PUBLIC_URL}/customers/${CONST_CUSTOMER_ID}/${date.getFullYear()}/${month}/`;
        return (
            <Redirect to={url}/>
        )
        return (
            <div>Loading customers...</div>
        )
    }
}


const mapStateToProps = (state) => ({
    providers: state.providers.items,
    customers: state.customers.items,
    access_level: state.selects.access_level
});

export default connect(
    mapStateToProps
)(MainController)
