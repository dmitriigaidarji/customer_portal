import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import 'airbnb-js-shims/target/es2015.js'
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import $ from 'jquery'
import {connect} from 'react-redux'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'
import {selectStartEndDate} from '../../actions/selects'
import Home from '../../../reactjs/components/header/Home'
class HeaderMonthly extends React.Component {
    constructor() {
        super()
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);

        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date
    }

    render() {
        let {props} = this;
        let {customers, customer, startDate, endDate, dispatch, history} = props;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight:'10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <DateRangePicker
                                startDate={startDate} // momentPropTypes.momentObj or null,
                                endDate={endDate}
                                onDatesChange={({startDate, endDate}) => {
                                    dispatch(selectStartEndDate(
                                        startDate.startOf('day'),
                                        endDate.endOf('day')
                                    ));
                                }}
                                focusedInput={this.state.focusedInput} // PropTypes.bool
                                onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                numberOfMonths={1}
                                displayFormat="YYYY/MM/DD"
                            />
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    customer: state.selects.customer,
    customers: state.customers.items
});
export default connect(mapStateToProps)(HeaderMonthly)
