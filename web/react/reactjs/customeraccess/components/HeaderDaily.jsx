import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import 'airbnb-js-shims/target/es2015.js'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import $ from 'jquery'
import {connect} from 'react-redux'
import {selectCurrentDay, selectStartEndDate} from '../../actions/selects'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'
import Home from '../../../reactjs/components/header/Home'

class HeaderDaily extends React.Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);
        this.isDayBlocked = this.isDayBlocked.bind(this);
        this.allowedDays = [];
        if (props.active_dates != undefined)
            props.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentWillUpdate(nextProps) {
        if (nextProps.active_dates != undefined) {
            this.allowedDays = [];
            nextProps.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        }
    }

    isDayBlocked(date) {
        return !this.allowedDays.includes(date.format('YYYYMMDD'));
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date
    }

    render() {
        let {props} = this;
        let {customers, customer, current_day, startDate, endDate, dispatch, history} = props;
        let backurl = `/${process.env.PUBLIC_URL}/customers/${customer.id}/${current_day.format('YYYY')}/${current_day.format('MM')}/`;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <div style={{marginRight: '10px'}}><Link to={backurl}>&lt;Monthly</Link></div>
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight:'10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <SingleDatePicker
                                date={current_day} // momentPropTypes.momentObj or null,
                                onDateChange={(date) => {
                                    history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}/`);
                                    dispatch(selectCurrentDay(date))
                                }} focused={this.state.focused} // PropTypes.bool
                                onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                numberOfMonths={1}
                                isDayBlocked={this.isDayBlocked}
                                displayFormat="YYYY/MM/DD"
                            />
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

export default connect(

)(HeaderDaily)
