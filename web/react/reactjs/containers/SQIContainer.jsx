import React from "react"
import moment from 'moment'
import {
    Route
} from 'react-router-dom'
import {
    Container, Row, Col, Button
} from 'reactstrap';
import {connect} from 'react-redux'
import {selectCurrentDay} from '../actions/selects'
import {fetchSqiDailyIfNeeded} from '../actions/sqiDaily'
import Header from '../components/sqi/HeaderDaily'
import SqiTable from '../components/sqi/SqiTable'
import SQIMap from '../components/sqi/SQIMap'
import Radium from 'radium'
let styles = {
    block: {
        marginTop: '15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '110%'
    },
    filters: {
        fontSize: '90%',
        fontWeight: 400,
        marginLeft: '15px'
    },
    mapcontainer: {
        position: 'fixed',
        top: '112px',
        left: 0
    },
    tablecontainer: {
        marginLeft: '500px',
    },
    allcontainer: {
        whiteSpace: 'nowrap',
        marginTop: '112px'
    }
};
@Radium
class SQIContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            show_map: true
        };
        this.toggleMap = this.toggleMap.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentDidMount() {
        let {props} = this;
        let {dispatch, stats} = props;

        let currentDay = moment.utc(props.match.params.date, 'YYYYMMDD')
        dispatch(selectCurrentDay(currentDay));
        dispatch(fetchSqiDailyIfNeeded(currentDay));
        window.addEventListener("resize", this.updateDimensions);
    }

    toggleMap() {
        this.setState({show_map: !this.state.show_map})
    }

    componentWillUpdate(nextProps) {
        if (!nextProps.current_day.isSame(this.props.current_day)) {
            this.setState({show_map: false})
        } else if (nextProps.stats != undefined && this.props.stats != undefined && nextProps.stats.length !== this.props.stats.length) {
            this.setState({show_map: false})
        }
    }

    componentDidUpdate() {
        if (this.state.show_map == false) {
            if (this.updateMap != undefined)
                window.clearTimeout(this.updateMap);
            this.updateMap = window.setTimeout(() => {
                this.setState({show_map: true})
            }, 200)
        }
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    render() {
        let {current_day, history} = this.props;
        if (current_day == undefined)
            return (<Container fluid={true} className="main_container">
                Loading container...
            </Container>)
        let w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight,
            mapwidth = 450,
            mapheight = Math.max(y - 180, 400),
            margin = 10,
            tablewidth = x - mapwidth - margin * 2;
        let maxWidth = 1050;
        if (tablewidth > maxWidth) {
            margin = tablewidth - maxWidth;
            tablewidth = maxWidth;
            let newmapwidth = mapwidth + margin;
            mapwidth = Math.min(700, newmapwidth)
            margin = (x - mapwidth - tablewidth) / 2;
        }
        return (
            <div>
                <Header
                    current_day={current_day}
                    history={history}
                />
                {x > 900 ?
                    <div style={styles.allcontainer}>
                        <div style={[styles.mapcontainer, {width: mapwidth + 'px'}]}>
                            <div style={styles.block}>
                            <span style={styles.title}>SQI Map
                            </span>
                            </div>
                            {this.state.show_map &&
                            <SQIMap mapheight={mapheight}/>
                            }
                        </div>
                        <div style={[styles.tablecontainer, {
                            width: tablewidth + 'px',
                            marginLeft: mapwidth + margin + 'px'
                        }]}>
                            <SqiTable />
                        </div>
                    </div>
                    :
                    <div style={{marginTop: '112px'}}>
                        <SqiTable />
                        <div style={styles.block}>
                            <span style={styles.title}>SQI Map
                            </span>
                        </div>
                        {this.state.show_map &&
                        <div style={{padding: '0 30px'}}>
                            <SQIMap mapheight={mapheight}/>
                        </div>
                        }
                    </div>
                }

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    let current_day = state.selects.current_day;
    let stats;
    let hotspots;
    let filteredStats = [];
    if (current_day != undefined) {
        current_day = moment.utc(current_day)
        stats = state.sqiDaily[current_day.format('YYYYMMDD')];
        if (stats != undefined) {
            hotspots = stats.hotspots;
            if (stats.items != undefined && state.sqiSorted.indexes.length > 0) {
                filteredStats = [];
                if (state.sqiSorted.indexes.length == stats.items.length)
                    filteredStats = stats.items;
                else state.sqiSorted.indexes.map((value) => {
                    filteredStats.push(stats.items[value]);
                });
            }
            stats = stats.items;
        }
    }
    return {
        current_day: current_day,
        stats: filteredStats,
        source_stats: stats,
        hotspots: hotspots
    }
};
export default connect(
    mapStateToProps
)(SQIContainer)
