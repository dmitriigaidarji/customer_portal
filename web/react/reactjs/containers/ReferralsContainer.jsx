import React from "react"
import '../static/css/referrals.css'
import {
    Container, Row, Col, Button, ListGroup, ListGroupItem
} from 'reactstrap';
import SubmitForm from '../components/referrals/SubmitForm'
import Information from '../components/referrals/Information'
import ReferralsList from '../components/referrals/ReferralsList'
import {getReferrals} from '../actions/referrals'
import {connect} from 'react-redux'
class ReferralsContainer extends React.Component {
    componentDidMount(){
        let {dispatch}  = this.props;
        dispatch(getReferrals())
    }
    render() {
        return (
            <Container fluid={true} className="referrals_main_container">
                <Row>
                    <legend>Referrals</legend>
                    <Col md="4" xs="12">
                        <div className="referrals_container">
                            <Information/>
                        </div>
                        <div className="referrals_container">
                            <SubmitForm/>
                        </div>
                    </Col>
                    <Col md="8" xs="12">
                        <div className="referrals_container">
                            <ReferralsList/>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default connect()(ReferralsContainer)