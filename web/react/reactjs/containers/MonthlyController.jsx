import React from "react"
import Api from "../Api"
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import DailyController from "./DailyContainer"
import ConsoleAppContainer from "./ConsoleAppContainer"
import moment from 'moment'
import {connect} from 'react-redux'
import {fetchProviderMonthlyIfNeeded} from '../actions/providerMontly'
import {selectStartEndDate, selectProvider} from '../actions/selects'
class MonthlyController extends React.Component {
    componentDidMount() {
        let {props} = this;
        let {dispatch} = props;

        let provider_id = parseInt(props.match.params.provider_id);
        let selectedProvider = undefined;
        props.providers.map((provider) => {
            if (provider.id == provider_id)
                selectedProvider = provider;
        });

        if (selectedProvider == undefined)
            selectedProvider = props.providers[0];

        dispatch(selectProvider(
            selectedProvider
        ));

        let month = parseInt(props.match.params.month);
        if (month < 9) month = '0' + month;
        else month = '' + month;

        let startDate = moment.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').startOf('month');
        dispatch(selectStartEndDate(
            startDate,
            moment.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').endOf('month')
        ));

        dispatch(fetchProviderMonthlyIfNeeded(startDate, selectedProvider));
    }

    render() {
        if (this.props.provider == undefined)
            return <div>Loading...</div>
        return (
            <div>
                <Switch>
                    <Route path={`/${process.env.PUBLIC_URL}/providers/:provider_id/:year/:month/:day`}
                           component={DailyController}/>
                    <Route path={`/${process.env.PUBLIC_URL}/providers/:provider_id/:year/:month`}
                           component={ConsoleAppContainer}/>
                </Switch>
            </div>
        )
    }
}
//
const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    provider: state.selects.provider,
    providers: state.providers.items
        // source_stats: state.providerMonthly['' + state.selectedProvider + state.startDate]
});
//
export default connect(mapStateToProps)(MonthlyController)

