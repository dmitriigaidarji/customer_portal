import React from "react"
import {withGoogleMap, GoogleMap, Marker, InfoWindow} from "react-google-maps";
import MarkerClusterer from "react-google-maps/lib/addons/MarkerClusterer";
import _ from "lodash";
import {connect} from 'react-redux'
import {fetchCoverageMapIfNeeded} from '../actions/coveragemap'
let styles = {
    markerTitle : {
        fontWeight: 'bold',
        marginRight: '5px'   
    }
}
const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={props.zoom}
        defaultCenter={{lat: 35, lng: 140}}
        onClick={props.onMapClick}
        onZoomChanged={props.onZoomChanged}
        onDragEnd={props.onDragEnd}
        onCenterChanged={props.onCenterChanged}
    >
        <MarkerClusterer
            averageCenter
            enableRetinaIcons
            gridSize={60}
            minimumClusterSize={5}
            maxZoom={18}
        >
            {props.markers.map(marker => (
                <Marker
                    {...marker}
                    onClick={() => props.onMarkerClick(marker)}
                >
                    {marker.showInfo && (
                        <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
                            <div>{marker.infoContent}</div>
                        </InfoWindow>
                    )}
                </Marker>
            ))}
        </MarkerClusterer>
    </GoogleMap>
));
const style = {
    container: {
        marginTop: '70px'
    },
    map: {
        padding: '0 20px',
    }
};

class CoverageMapContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            zoom: 11,
            markers: [],
            reloadMap: false,
            countries: [],
            country: ''
        }
        this.handleMapMounted = this.handleMapMounted.bind(this);
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.onCenterChanged = this.onCenterChanged.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
        this.calcHotspots = this.calcHotspots.bind(this)
        this.handleCountryChange = this.handleCountryChange.bind(this)
    }
    handleMarkerClick(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleMarkerClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(fetchCoverageMapIfNeeded());
    }

    handleMapMounted(map) {
        this._connmap = map;
        if (map!= undefined && this.props.stats != undefined)
            this.setState(this.calcHotspots(this.props.stats, this.state.country))
        console.log('map mounted', map)
    }

    componentWillUpdate(nextProps, nextState) {
        let old_stats = this.props.stats, {stats} = nextProps;
        if (stats != undefined && old_stats == undefined || stats.length !== old_stats.length){
            console.log('updating')
            setTimeout(() =>
                this.setState(this.calcHotspots(this.props.stats, nextState.country)), 1500)
        }
    }
    componentDidUpdate(){
        let {reloadMap, country} = this.state;
        if (reloadMap){
            let data = this.calcHotspots(this.props.stats, country)
            data['reloadMap'] = false
            this.setState(data)
        }
    }

    calcHotspots(hotspots, country='') {
        console.log(hotspots)
        let hotspotmarkers = [], countries = [];
        if (this._connmap != undefined && hotspots != undefined && hotspots.length > 0) {
            const bounds = new google.maps.LatLngBounds();
            hotspots.map((item, index) => {
                let lat = parseFloat(item.l.x);
                let lng = parseFloat(item.l.y);
                if (lat != 0 && lng != 0) {
                    if (!countries.includes(item.c))
                        countries.push(item.c)
                    if (item.c == country){    
                        let key = 'h' + index;
                        bounds.extend(new google.maps.LatLng(lat, lng))
                        hotspotmarkers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            showInfo: false,
                            infoContent: (<div>
                                {/*<div><span style={styles.markerTitle}>ID:</span><span*/}
                                    {/*className="marekValue">{item._id.$oid}</span></div>*/}
                                {/*<div><span style={styles.markerTitle}>Country:</span><span*/}
                                    {/*className="marekValue">{item.c}</span></div>*/}
                                {/*<div><span style={styles.markerTitle}>City District:</span><span*/}
                                    {/*className="marekValue">{item.city_district}</span></div>*/}
                                {/*<div><span style={styles.markerTitle}>Region:</span><span*/}
                                    {/*className="marekValue">{item.pref_state_region}</span></div>*/}
                                <div><span style={styles.markerTitle}>Venue:</span><span
                                    className="marekValue">{item.n}</span></div>
                                <div><span style={styles.markerTitle}>Address:</span><span
                                    className="marekValue">{item.a}</span></div>
                                {/*<div><span style={styles.markerTitle}>ZIP:</span><span*/}
                                    {/*className="marekValue">{item.zipcode}</span></div>*/}
                                <div><span style={styles.markerTitle}>Latitude:</span><span
                                    className="marekValue">{lat}</span></div>
                                <div><span style={styles.markerTitle}>Longitude:</span><span
                                    className="marekValue">{lng}</span></div>
                            </div>),
                            key: key,
                            defaultAnimation: null,
                            title: item.venue_name
                        });
                    }
                    this._connmap.fitBounds(bounds);
                }
            });
        }else if (countries.length == 0){
            hotspots.map((item, index) => {
                let lat = parseFloat(item.l.x);
                let lng = parseFloat(item.l.y);
                if (lat != 0 && lng != 0) {
                    if (!countries.includes(item.c))
                        countries.push(item.c)
                }
            })
        }
        console.log(countries)
        return {'markers':hotspotmarkers, 'countries':countries}
    }

    handleDrag() {
        // let _this = this;
        // let {stats} = this.props;
        // this.bounds = this._connmap.getBounds();
        // window.boundss = this.bounds;
        // if (stats != undefined) {
        //     if (this.updateDrag != undefined)
        //         window.clearTimeout(this.updateDrag);
        //     this.updateDrag = window.setTimeout(() => {
        //         _this.setState({
        //             reloadMap: true,
        //         })
        //     }, 1000)
        // }
    }
    onCenterChanged(){
        // let _this = this;
        // let {stats} = this.props;
        // this.bounds = this._connmap.getBounds();
        // window.boundss = this.bounds;
        // if (stats != undefined) {
        //     if (this.updateDrag != undefined)
        //         window.clearTimeout(this.updateDrag);
        //     this.updateDrag = window.setTimeout(() => {
        //         _this.setState({
        //             reloadMap: true,
        //         })
        //     }, 1500)
        // }
    }
    handleZoomChange() {
        // let _this = this;
        // let {stats} = this.props;
        // this.zoom = this._connmap.getZoom();
        // if (stats != undefined) {
        //     if (this.zoomHotspotsShow != undefined)
        //         window.clearTimeout(this.zoomHotspotsShow);
        //     this.zoomHotspotsShow = window.setTimeout(() =>
        //         this.setState({reloadMap: true}), 1500);
        // }
    }
    handleCountryChange(event) {
        let data = this.calcHotspots(this.props.stats, event.target.value)
        data['country'] = event.target.value
        data['reloadMap'] = true
        this.setState(data);
    }

    render() {
        let {markers, reloadMap, countries, country} = this.state;
        console.log(markers.length)
        let loadingdiv = <div></div>
        if (countries.length == 0) {
            loadingdiv = (
                <div>
                    <div style={style.container}>
                        <h5>Loading hotspots...</h5>
                    </div>
                </div>
            )
        }
        let zoom = 14;
        if (this.zoom != undefined)
            zoom = this.zoom
        return (
            <div>
                <div style={style.container}>
                    {loadingdiv}
                    <select className='custom-select' style={{marginBottom:'10px'}} value={country} onChange={this.handleCountryChange}>
                        <option value='' key='aas'>Select value..</option>
                        {countries.map((item)=><option value={item} key={item}>{item}</option>)}
                      </select>
                    {reloadMap === false &&
                    <div style={style.map}>
                        <GettingStartedGoogleMap
                            containerElement={
                                <div style={{height: `85vh`}}/>
                            }
                            mapElement={
                                <div style={{height: `100%`}}/>
                            }
                            onMapLoad={this.handleMapMounted}
                            onMapClick={_.noop}
                            markers={markers}
                            onMarkerRightClick={_.noop}
                            onDragEnd={this.handleDrag}
                            onZoomChanged={this.handleZoomChange}
                            onCenterChanged={this.onCenterChanged}
                            onMarkerClick={this.handleMarkerClick}
                            onMarkerClose={this.handleMarkerClose}
                            zoom={zoom}
                        />
                    </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({stats: state.coveragemap.hotspots});

export default connect(
    mapStateToProps
)(CoverageMapContainer)