import React from "react"
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import Dashboard from './Dashboard'
import MonthlyController from "./MonthlyController"
import CustomerMonthly from "./CustomerMonthly"
import SQIContainer from './SQIContainer'
import {connect} from 'react-redux'
import {fetchProvidersIfNeeded} from '../actions/providers'
import {fetchCustomersIfNeeded} from '../actions/customers'
import moment from 'moment'
import WebsiteVersion from '../components/WebsiteVersion'
import DomainRequests from './DomainRequests'
import UserMonthly from './UserMonthly'
import ReferralsContainer from './ReferralsContainer'
import ReferralRequests from './ReferralRequests'
class MainController extends React.Component {
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(fetchProvidersIfNeeded());
        dispatch(fetchCustomersIfNeeded());
    }

    render() {
        if (this.props.providers == undefined || this.props.customers == undefined)
            return <div>Loading...</div>
        return (
            <div>
                <Switch>
                    <Route path={`/${process.env.PUBLIC_URL}/providers/:provider_id/:year/:month`}
                           component={MonthlyController}/>
                    <Route path={`/${process.env.PUBLIC_URL}/customers/:customer_id/:year/:month`}
                           component={CustomerMonthly}/>
                    <Route path={`/${process.env.PUBLIC_URL}/customers`}
                           render={(props) => <CustomersNoMatch {...props} customers={this.props.customers}
                                                                providers={this.props.providers}/>}/>
                    <Route path={`/${process.env.PUBLIC_URL}/providers`}
                           render={(props) => <NoMatch {...props} providers={this.props.providers}/>}/>
                    <Route path={`/${process.env.PUBLIC_URL}/users/:user_hash/:year/:month`}
                           component={UserMonthly}/>
                    <Route path={`/${process.env.PUBLIC_URL}/sqi/:date/`} component={SQIContainer} />
                    <Route path={`/${process.env.PUBLIC_URL}/sqi`}
                           render={(props) => <NoMatch {...props} providers={this.props.providers}/>}/>
                    <Route path={`/${process.env.PUBLIC_URL}/domains`} component={DomainRequests}/>
                    {CONST_CAN_HAVE_REFERRALS === true &&
                    <Route path={`/${process.env.PUBLIC_URL}/referrals`} component={ReferralsContainer}/>
                    }
                    <Route path={`/${process.env.PUBLIC_URL}/referral-requests`} component={ReferralRequests}/>
                    <Route render={(props) => <Dashboard {...props} customers={this.props.customers}
                                                         providers={this.props.providers}/>}
                    />
                </Switch>
                <WebsiteVersion />
            </div>
        )
    }
}
class SqiNoMatch extends React.Component {
    render(){
        let date = moment();
        return <Redirect to={`/${process.env.PUBLIC_URL}/sqi/${date.format('YYYYMMDD')}/`}/>
    }
}
class CustomersNoMatch extends React.Component {
    render() {
        let {customers, providers} = this.props;
        let customer;
        if (customers != undefined && customers.length > 0) {
            customer = customers[0];
            for (let i = 0; i < customers.length; i++) {
                if (customers[i].id == 700001) {
                    customer = customers[i];
                    break
                }
            }
        }
        if (customers != undefined && providers != undefined) {
            let date = new Date();
            let month = date.getMonth() + 1
            if (month < 10) month = '0' + month
            let url = `/${process.env.PUBLIC_URL}/customers/${customer.id}/${date.getFullYear()}/${month}/`;
            return (
                <Redirect to={url}/>
            )
        }
        return (
            <div>Loading customers...</div>
        )
    }
}

class NoMatch extends React.Component {
    render() {
        let {providers} = this.props;
        let provider;
        if (providers != undefined && providers.length > 0) {
            provider = providers[0];
            for (let i = 0; i < providers.length; i++) {
                if (providers[i].id == 700001) {
                    provider = providers[i];
                    break
                }
            }
            let date = new Date();
            let url = `/${process.env.PUBLIC_URL}/providers/${provider.id}/${date.getFullYear()}/${date.getMonth() + 1}/`;
            return (
                <Redirect to={url}/>
            )
        }
        return (
            <div>Loading providers...</div>
        )
    }
}

const mapStateToProps = (state) => ({
    providers: state.providers.items,
    customers: state.customers.items
});

export default connect(
    mapStateToProps
)(MainController)

