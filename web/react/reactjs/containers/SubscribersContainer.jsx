import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {connect} from 'react-redux'
import {getUsers, editUser} from '../actions/subscribers'
import moment from 'moment'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';


import Radium from 'radium'
import '../static/css/subscribers.css'
import Masculin from '../static/media/masculine.png'
import Femenin from '../static/media/venus.png'
import Fb from '../static/media/facebook.png'
import JP from '../static/media/japan.png'
import HK from '../static/media/hong-kong.png'

const styles = {
    container: {
        margin: '0 auto'
    },
    inline: {
        display: 'inlineBlock'
    }
};

class DateCell extends React.Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        let {props} = this;
        let value = '';
        if (date != undefined)
            value = date.valueOf() / 1000;
        props.dispatch(editUser(props.email, props.field, value));
    }

    render() {
        let {value} = this.props;
        let date = '';
        if (value != undefined)
            date = moment.utc(value * 1000);
        return <DatePicker
            selected={date}
            onChange={this.handleChange}
        />
    }

}

class SelectCell extends React.Component {
    constructor() {
        super();
        this.onValueChange = this.onValueChange.bind(this)
    }

    onValueChange(event) {
        let {props} = this;
        props.dispatch(editUser(props.email, props.field, event.target.value));
    }

    render() {
        return (<div className="tablemargintop">
            <select className="form-control" value={this.props.value} onChange={this.onValueChange}>
                <option value="New">New</option>
                <option value="Accepted">Accepted</option>
                <option value="Rejected">Rejected</option>
                <option value="Offered">Offered</option>
                <option value="Started">Started</option>
                <option value="Active">Active</option>
                <option value="Ended">Ended</option>
            </select>
        </div>)
    }
}


@Radium
class SubscribersContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            sorting: [{id: 'date', desc: true}],
            showFilters: false
        };
        this.toggleFilters = this.toggleFilters.bind(this);
    }

    toggleFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(getUsers());
    }

    render() {
        let {testers, subscribers, dispatch} = this.props;

        let columns = [
            {
                id: 'date',
                Header: 'Created',
                accessor: d => moment(d.created * 1000).format('YYYY/MM/DD HH:mm:ss'),
                Cell: d => {
                    let values = d.value.split(' ');
                    return <div>
                        <div>{values[0]}</div>
                        <div>{values[1]}</div>
                    </div>
                },
                width: 100
            },
            {
                id: 'status',
                Header: 'Status',
                accessor: d => d.status,
                Cell: d => <SelectCell value={d.value}
                                       email={d.original.email}
                                       field='status'
                                       dispatch={dispatch}/>,
                width: 100
            },
            {
                id: 'profile',
                Header: 'Profile',
                accessor: d => d.name,
                Cell: d => {
                    let item = d.original;
                    return <div className="infoCell">
                        <div>
                            <div className="name">{item.name} {item.surname}</div>
                            {item.picture &&
                            <img src={item.picture} className="picture"/>
                            }
                            <div className="nameContainer inline">
                                <div className="age">
                                    {item.gender == 'male' ?
                                        <img src={Masculin} className="gender"/>
                                        :
                                        <img src={Femenin} className="gender"/>
                                    }
                                    Age: {item.min_age == undefined ? 'null' : item.min_age}
                                    - {item.max_age == undefined ? 'null' : item.max_age}
                                </div>
                                <div className="foot">
                                    {item.timezone &&
                                    <span className="tz">GMT
                                        {item.timezone >= 0 ?
                                            <span>+{item.timezone}</span>
                                            :
                                            <span>{item.timezone}</span>
                                        }
                                </span>
                                    }
                                    {item.link &&
                                    <span><a href={item.link} target="_blank"><img src={Fb} className="fb"/></a></span>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                },
                width: 200
            },
            {
                id: 'device',
                Header: 'Device',
                accessor: d => d.platform,
                Cell: d => {
                    let node = <div></div>;
                    if (d.value == 'iOS')
                        node = <div style={{textAlign: 'center'}}><span className="device ios">iOS</span></div>
                    else if (d.value == 'Android')
                        node = <div style={{textAlign: 'center'}}><span className="device android">Android</span></div>
                    return node;
                },
                width: 70
            },
            {
                id: 'destination',
                Header: 'Destination',
                accessor: d => d.destination,
                Cell: d => {
                    let node = <div></div>;
                    if (d.value == 'JP')
                        node = <div style={{textAlign: 'center'}}><img src={JP} className="countryFlag"/> - JP</div>
                    else if (d.value == 'HK')
                        node = <div style={{textAlign: 'center'}}><img src={HK} className="countryFlag"/> - HK</div>
                    return node;
                },
                width: 70
            },
            {
                id: 'email',
                Header: 'Email',
                accessor: d => d.email,
                Cell: d => <a href={`mailto:${d.value}`}>{d.value}</a>,
                width: 150
            },
            {
                id: 'testFlight',
                Header: 'TestFlight',
                accessor: d => d.test_flight,
                Cell: d => <input onChange={(event) => {
                    let value = event.target.checked;
                    dispatch(editUser(d.original.email, 'test_flight', value))
                }} type="checkbox" name="testfligh" className="checkbutton" checked={d.value}/>,
                width: 80
            },
            {
                id: 'roamVV',
                Header: <div>
                    <div>RoamVU</div>
                    <div>Instructions Sent</div>
                </div>,
                accessor: d => d.instructions_send_date,
                Cell: d => <input type="checkbox" name="roamvu"
                                  className="checkbutton" checked={d.value != undefined} disabled={true}/>,
                width: 80
            },
            {
                id: 'start',
                Header: <div>
                    <div>Date of</div>
                    <div>instructions sent</div>
                </div>,
                accessor: d => d.instructions_send_date,
                Cell: d => d.value != undefined ?
                    <div style={{marginTop:'20px', textAlign:'center'}}>{moment(d.value * 1000).format('lll')}</div>
                    : '',
                //   <DateCell value={d.value}
                //                  email={d.original.email}
                //                field='instructions_send_date'
                //              dispatch={dispatch}
                ///>,
                width: 150
            },
            {
                id: 'end',
                Header: <div>
                    <div>Account</div>
                    <div>expiration date</div>
                </div>,
                accessor: d => d.expiration_date,
                Cell: d => d.value != undefined ?
                    <div style={{marginTop:'20px', textAlign:'center'}}>{moment(d.value * 1000).format('ll')}</div>
                    : '',
                //    <DateCell value={d.value}
                  //                   email={d.original.email}
                    //                 field='expiration_date'
                      //               dispatch={dispatch}
                ///>,
                width: 150
            },
            {
                id: 'followUp',
                Header: <div>
                    <div>Followup message in</div>
                    <div>the middle of user experience</div>
                </div>,
                accessor: d => d.followup_message_sent,
                Cell: d => <input onChange={(event) => {
                    let value = event.target.checked;
                    dispatch(editUser(d.original.email, 'followup_message_sent', value))
                }} type="checkbox" name="followup" className="checkbutton" checked={d.value}/>,
                width: 80
            },
            {
                id: 'final',
                Header: <div>
                    <div>Final "Thank you</div>
                    <div>for participation" message</div>
                </div>,
                accessor: d => d.thank_you_message_sent,
                Cell: d => <input onChange={(event) => {
                    let value = event.target.checked;
                    dispatch(editUser(d.original.email, 'thank_you_message_sent', value))
                }} type="checkbox" name="final" className="checkbutton" checked={d.value}/>,
                width: 80
            }
        ];
        let pageSize = 10;
        let showPagination = true;
        if (testers.length == 0)
            return <div>
                <div className="tableContainer">
                    Loading data..
                </div>
            </div>
        return (
            <div>
                <div className="tableContainer">
                    <div><input onClick={this.toggleFilters} style={{marginBottom: '10px'}}
                                type="button" className="btn btn-secondary" value='Toggle Filters'/></div>
                    <ReactTable
                        className='-striped -highlight'
                        data={testers}
                        showPagination={showPagination}
                        columns={columns}
                        defaultPageSize={pageSize}
                        filterable={this.state.showFilters}
                        defaultFilterMethod={(filter, row, column) => {
                            const id = filter.pivotId || filter.id
                            return row[id] !== undefined ?
                                String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                                true
                        }}
                        defaultSorted={[{
                            id: 'date',
                            desc: true
                        }]}
                        SubComponent={(row, a, b, c) => {
                            return (
                                <div style={{padding: '20px'}}>
                                    <em>Add any note for the user:</em>
                                    <br />
                                    <textarea className="notearea" defaultValue={row.original.beta_notes}/>
                                    <input type="submit" onClick={(event) => {
                                        dispatch(editUser(row.original.email, 'beta_notes', event.target.previousElementSibling.value));
                                    }}/>
                                </div>
                            )
                        }}
                        getTrProps={(state, rowInfo, column) => {
                            let colour;
                            if (rowInfo != undefined) {
                                let {status} = rowInfo.row;
                                if (status == 'Rejected')
                                    colour = 'rgb(255, 232, 232)';
                                else if (status == 'Accepted')
                                    colour = 'rgba(190, 255, 219, 0.57)';
                                else if (status == 'New')
                                    colour = 'rgb(235, 246, 255)';
                                if (colour != undefined)
                                    return {
                                        style: {
                                            background: colour
                                        }
                                    };
                            }
                            return {};
                        }}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    testers: state.subscribers.testers,
    subscribers: state.subscribers.subscribers
});

export default connect(
    mapStateToProps
)(SubscribersContainer)
