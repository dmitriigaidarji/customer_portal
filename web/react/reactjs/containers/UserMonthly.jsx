import React from "react"
import {
    Container, Row, Col
} from 'reactstrap';
import {
    Route,
    Switch
} from 'react-router-dom'
import moment from 'moment'
import HeaderMonthly from '../components/user/HeaderMonthly'
import Totals from '../components/customers/Totals'
import Transactions from '../components/customers/Transactions'
import UsageDuration from '../components/user/UsageDuration'
import Donut from '../components/Donut'
import UserDaily from './UserDaily'
import {connect} from 'react-redux'
import {selectUser, selectStartEndDate} from '../actions/selects'
import {fetchUserMonthlyIfNeeded} from '../actions/userMonthly'
import DeviceTiles from '../components/DeviceTiles'
import UsageMap from '../components/user/UsageMap'
import UserDevices from '../components/customers/UserDevices'

let style = {
    piecontainer: {
        paddingTop: '20px'
    }
};
class UserMonthly extends React.Component {
    constructor() {
        super();
        this.state = {
            user: undefined,
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined
        };
        this.parseStats = this.parseStats.bind(this);
    }


    componentDidMount() {
        let {dispatch, source_stats, match} = this.props;
        let {user_hash} = match.params;
        let userObj = {
            nai: this.props.user_hash,
            hash_id: ''
        };
        dispatch(selectUser(userObj));
        let month = parseInt(match.params.month);
        if (month < 9) month = '0' + month;
        else month = '' + month;

        let startDate = moment.utc("{0}{1}".format(match.params.year, month), 'YYYYMM').startOf('month');
        dispatch(selectStartEndDate(
            startDate,
            moment.utc("{0}{1}".format(match.params.year, month), 'YYYYMM').endOf('month')
        ));
        console.log(user_hash)
        dispatch(fetchUserMonthlyIfNeeded(startDate, user_hash));
    }

    componentDidUpdate(prevProps, prevState) {
        let prev_start = prevProps.startDate, prev_end = prevProps.endDate,
            prev_stats = prevProps.source_stats, prev_hash = prevProps.match.params.user_hash;
        let {startDate, endDate, source_stats} = this.props;
        let {user_hash} = this.props.match.params
        if (
            source_stats != undefined && startDate != undefined &&
            (this.state.current_stats == undefined ||
            !prev_start.isSame(startDate) || !prev_end.isSame(endDate) ||
            prev_stats == undefined || user_hash != prev_hash)
        ) {
            this.parseStats(source_stats)
        }
    }

    parseStats(documents) {
        let providerdata = this.props.providers;
        let {startDate, endDate, dispatch} = this.props;
        let tprecord = {
            'user_count': 0,
            'connections': {'count': 0, 'data': {}},
            'activations': {'count': 0, 'data': {}},
            'duration': {
                'data': {}
            },
            'usage': {
                'data': {}
            },
            'devices': {
                'count': 0,
                'data': {
                    'ios': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': {}
                    },
                    'android': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': []
                    }
                }
            },
            'totals': {
                'tbytes': 0,
                'tseconds': 0,
                'tdata': 0,
                'avgdata': 0,
                'ttime': 0,
                'avgtime': 0
            }
        };


        let stats = [];
        let alltx = [];
        let totaldata = 0;
        let totalsecs = 0;
        let activationcount = 0;
        let connectioncount = 0;
        let userObj;
        if (documents.length > 0) {
            for (let i = 0; i < documents.length; i++) {
                let record = documents[i];
                if (userObj == undefined && record.nai != undefined) {
                    userObj = {
                        nai: record.nai,
                        hash_id: record.hash_id
                    };
                }
                let found = false;
                record['activations_current'] = [];
                record['user_tx_current'] = [];
                record['devices_current'] = [];
                for (let j = 0; j < record['activations'].length; j++) {
                    let aitem = record['activations'][j];
                    aitem.date_obj = moment.utc(aitem.date.$date);
                    if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                        ///
                        let actday = aitem.date_obj.format('YYYYMMDD');
                        if (tprecord['activations']['data'][actday] == undefined)
                            tprecord['activations']['data'][actday] = 0
                        tprecord['activations']['data'][actday] += 1

                        //device
                        for (let k = 0; k < aitem['sqi'].length; k++) {
                            let sqi = aitem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }


                        //
                        found = true;
                        record['activations_current'].push(aitem);
                        activationcount += 1
                        let geoposition = [0, 0]
                        let device = ''
                        let txid = aitem['auth_id']
                        let nai = record['nai']
                        let rtype = 'Activation'
                        let location = '100002'
                        let country = 'US'
                        let seconds = '-'
                        let bytes = '-'
                        if (aitem['sqi'].length > 0) {
                            for (let k = 0; k < aitem['sqi'].length; k++) {
                                let sqi = aitem['sqi'][k];
                                device = sqi['device_id'];
                                if (sqi['lat'] != 0)
                                    geoposition = [sqi['lat'], sqi['lng']]
                            }
                        }
                        alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])
                    }
                }
                let userbytes = 0;
                let userseconds = 0;
                for (let j = 0; j < record['user_tx'].length; j++) {
                    let citem = record['user_tx'][j];
                    citem.date_obj = moment.utc(citem.tx_date.$date);
                    if (citem.usage.secs > 10 && startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                        ///
                        for (let k = 0; k < citem['sqi'].length; k++) {
                            let sqi = citem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }
                        ///
                        found = true;
                        record['user_tx_current'].push(citem)
                        connectioncount += 1
                        let geoposition = [0, 0]
                        if (citem['usage']['secs'] > 0) {
                            ///Connections
                            let txday = citem.date_obj.format('YYYYMMDD');
                            if (tprecord['connections']['data'][txday] == undefined)
                                tprecord['connections']['data'][txday] = 0;
                            tprecord['connections']['data'][txday] += 1;
                            let txid = citem['tx_id']
                            let nai = record['nai']
                            let rtype = 'Connection'
                            let location = citem['provider_id']
                            let country = undefined
                            providerdata.some((prov) => {
                                if (prov['id'] == citem['provider_id']) {
                                    country = prov['iso_code'];
                                    return true;
                                }
                            })
                            let device = citem['calling_station']
                            if (citem['sqi'].length > 0)
                                for (let k = 0; k < citem['sqi'].length; k++) {
                                    let sqi = citem['sqi'][k];
                                    if (sqi['lat'] != 0)
                                        geoposition = [sqi['lat'], sqi['lng']]
                                }
                            let seconds = citem['usage']['secs']
                            let bytes = 0
                            bytes += citem['usage']['bytes_in']
                            bytes += citem['usage']['bytes_out']
                            bytes += citem['usage']['giga_in'] * 2147483648
                            bytes += citem['usage']['giga_out'] * 2147483648
                            userbytes += bytes;
                            totaldata += bytes;
                            userseconds += seconds;
                            totalsecs += seconds;
                            if (seconds >= 10 && bytes > 0)
                                alltx.push([citem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])

                            //Duration
                            if (tprecord['duration']['data'][txday] == undefined)
                                tprecord['duration']['data'][txday] = 0;
                            tprecord['duration']['data'][txday] += seconds;

                            //usage
                            if (tprecord['usage']['data'][txday] == undefined)
                                tprecord['usage']['data'][txday] = 0;
                            tprecord['usage']['data'][txday] += bytes;
                        }
                    }
                }
                record['usage_current'] = {total_bytes: userbytes, total_seconds: userseconds};
                if (found) stats.push(record)
            }
        }

        tprecord['connections']['count'] = connectioncount;
        tprecord['activations']['count'] = activationcount;
        tprecord['totals']['tbytes'] = totaldata;
        tprecord['totals']['tseconds'] = totalsecs

        for (let i = 0; i < stats.length; i++) {
            let item = stats[i];
            for (let j = 0; j < item['devices_current'].length; j++) {
                let dev = item['devices_current'][j];
                tprecord['devices']['count'] += 1
                if (dev['platform']['ios_version'] != undefined) {
                    tprecord['devices']['data']['ios']['count'] += 1
                    if (!tprecord['devices']['data']['ios']['os'].hasOwnProperty(dev['platform']['ios_version']))
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] += 1;
                    if (!tprecord['devices']['data']['ios']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['ios']['release'][dev['version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['release'][dev['version']] += 1;
                    if (!tprecord['devices']['data']['ios']['device'].hasOwnProperty(dev['platform']['device_model']))
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] = 1;
                    else
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] += 1;
                }
                else if (dev['platform']['device_model'] == 'Android') {
                    tprecord['devices']['data']['android']['count'] += 1
                    if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['android_version']))
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] = 1
                    else
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] += 1
                    if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['android']['release'][dev['version']] = 1
                    else
                        tprecord['devices']['data']['android']['release'][dev['version']] += 1
                    if (!tprecord['devices']['data']['android']['device'].hasOwnProperty(dev['platform']['phone']))
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] = 1;
                    else
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] += 1;
                }
            }
        }
        tprecord['user_count'] = stats.length;
        tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
        tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
        if (tprecord['user_count'] > 0) {
            tprecord['totals']['avgdata'] = tprecord['connections']['count'] === 0 ? present_data(0) : present_data(tprecord['totals']['tbytes'] / tprecord['connections']['count']);
            tprecord['totals']['avgtime'] = tprecord['connections']['count'] === 0 ? present_clock(0) : present_clock(tprecord['totals']['tseconds'] / tprecord['connections']['count'])
        }
        if (userObj == undefined) {
            userObj = {
                nai: this.props.match.params.user_hash,
                hash_id: ''
            }
        }
        this.setState({
            current_stats: stats,
            total_stats: tprecord,
            transactions: alltx,
            user: userObj
        });
        dispatch(selectUser(userObj));
    }

    render() {
        let {props, state} = this;
        let {user, total_stats, current_stats, transactions} = state;
        let {customers, providers, history, match, source_stats, startDate, endDate, access_level} = props;
        if (startDate === undefined)
            return <Container fluid={true} className="main_container">Loading...</Container>
        let iosreleases = [],
            androidreleases = [],
            iosversions = [],
            androidversions = [];
        if (source_stats != undefined) {
            if (total_stats != undefined) {
                let data = total_stats.devices.data.ios.release
                for (let key in data)
                    if (data.hasOwnProperty(key)) {
                        iosreleases.push({name: key, count: data[key]})
                    }
                data = total_stats.devices.data.android.release
                for (let key in data)
                    if (data.hasOwnProperty(key)) {
                        androidreleases.push({name: key, count: data[key]})
                    }
                data = total_stats.devices.data.ios.os
                for (let key in data)
                    if (data.hasOwnProperty(key)) {
                        iosversions.push({name: key, count: data[key]})
                    }
                data = total_stats.devices.data.android.os
                for (let key in data)
                    if (data.hasOwnProperty(key)) {
                        androidversions.push({name: key, count: data[key]})
                    }
            }
        }

        let iosuserdevices = [],
            androiduserdevices = [];
        if (current_stats != undefined)
            current_stats.map((item) => {
                if (item.devices_current != undefined)
                    item.devices_current.map((device) => {
                        let found = false;
                        if (device.platform.device_model == 'Android') {
                            androiduserdevices.map((old_device) => {
                                if (old_device.device_id == device.device_id) {
                                    found = true;
                                }
                            })
                            if (!found) {
                                androiduserdevices.push({
                                    device_id: device.device_id,
                                    os: 'Android',
                                    os_ver: device.platform.android_version,
                                    model: device.platform.phone,
                                    app_ver: device.version,
                                    wifi_mac: device.wifi_mac
                                })
                            }
                        } else if (device.platform.ios_version != undefined) {
                            iosuserdevices.map((old_device) => {
                                if (old_device.device_id == device.device_id) {
                                    found = true;
                                }
                            })
                            if (!found) {
                                iosuserdevices.push({
                                    device_id: device.device_id,
                                    os: 'iOS',
                                    os_ver: device.platform.ios_version,
                                    model: device.platform.device_model,
                                    app_ver: device.version,
                                    wifi_mac: device.wifi_mac
                                })
                            }
                        }
                    })
            });
        return (
            <Switch>
                <Route path={`${props.match.url}${props.match.url.slice(-1) == '/' ? ':day' : '/:day'}`}
                       render={(componentProps) =>
                           <div>
                               {user &&
                               <UserDaily
                                   {...componentProps}
                                   customers={customers}
                                   user={user}
                                   providers={providers}
                                   startDate={startDate}
                                   endDate={endDate}
                                   source_stats={source_stats}
                               />
                               }
                           </div>
                       }/>
                <Route render={(componentProps) =>
                    <div>
                        <HeaderMonthly
                            {...props}
                            startDate={startDate}
                            endDate={endDate}
                        />
                        {source_stats != undefined ?
                            <Container fluid={true} className="main_container" style={{marginTop: '210px'}}>
                                <Totals stats={total_stats}
                                        userLevel={true}
                                />
                                {total_stats != undefined &&
                                <div style={{marginBottom: '10px'}}>
                                    {
                                        total_stats.connections.count + total_stats.activations.count == 0 ?
                                            total_stats.activations.count :
                                            "{0} ({1}%)".format(total_stats.activations.count, (total_stats.activations.count / (total_stats.connections.count + total_stats.activations.count) * 100).toFixed(0))
                                    } Activations
                                    <span style={{marginLeft: '100px'}}>{total_stats.connections.count} Connections</span>
                                </div>
                                }
                                <UsageDuration stats={total_stats} match={props.match} history={props.history}/>
                                {transactions != undefined &&
                                <UsageMap stats={transactions} date={startDate} access_level={access_level}/>
                                }
                                <UserDevices stats={total_stats}
                                             iosreleases={iosreleases}
                                             iosversions={iosversions}
                                             androidreleases={androidreleases}
                                             androidversions={androidversions}
                                             iosuserdevices={iosuserdevices}
                                             androiduserdevices={androiduserdevices}
                                             userLevel={true}
                                />
                                <Transactions
                                    stats={transactions}
                                    month={startDate.format('MMMM')}
                                    date={startDate}
                                    user={user}
                                />
                            </Container>
                            :
                            <Container fluid={true} className="main_container" style={{marginTop: '120px'}}>
                                No data was found for this period
                            </Container>
                        }
                    </div>
                }/>
            </Switch>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let stats = state.userMonthly[
    '' + ownProps.match.params.user_hash + '-' + moment.utc(state.selects.start_date).format('YYYYMM')
        ];
    stats = stats == undefined ? undefined : stats.items;
    if (state.selects.start_date != undefined)
        return ({
            providers: state.providers.items,
            customers: state.customers.items,
            source_stats: stats,
            startDate: moment.utc(state.selects.start_date),
            endDate: moment.utc(state.selects.end_date),
            access_level: state.selects.access_level
        });
    else return ({
        providers: state.providers.items,
        customers: state.customers.items,
        source_stats: stats,
        access_level: state.selects.access_level
    });
};


export default connect(mapStateToProps)(UserMonthly)
