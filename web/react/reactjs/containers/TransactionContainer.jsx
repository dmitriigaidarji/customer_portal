import React from "react"
import Api from '../Api'

export default class TransactionContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            transaction: undefined
        };
        this.getData = this.getData.bind(this)
    }

    shouldComponentUpdate(props) {
        return this.props.transaction_id != undefined;
    }

    componentDidMount() {
        if (this.props.transaction_id != undefined) {
            this.getData(this.props.transaction_id)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.transaction_id != this.props.transaction_id)
            this.getData(this.props.transaction_id)
    }

    getData(transaction_id) {
        let _this = this;
        Api.get(process.env.BASE_API_URL + 'transaction/' + transaction_id + '/')
            .then((response) => {
                console.log(response);
                _this.setState({transaction: response.data.result})
            })
    }

    render() {
        let {props, state} = this;
        let {transaction} = state;
        if (transaction == undefined)
            return (<div>Loading transaction data..</div>)
        let txrecord = transaction;

        let tr1, tr2, tr3;
        if (txrecord['start'] != undefined && txrecord['start']['avp'] != undefined) {
            tr1 = <tr>
                <td>Session Start:</td>
                <td>{txrecord['start']['avp'][0]},</td>
            </tr>;
            tr2 = txrecord['start']['avp'].slice(1, -1).map((item, index) =>
                <tr key={index}>
                    <td></td>
                    <td>{item},</td>
                </tr>
            )
            tr3 = <tr>
                <td><p></p></td>
                <td>{txrecord['start']['avp'][-1]}<p></p></td>
            </tr>
        }
        let tr4, tr5, tr6;
        if (txrecord['auth'] != undefined && txrecord['auth']['result'] != undefined
            && txrecord['auth']['result']['avp'] != undefined && txrecord['auth']['result'] != {}) {
            tr4 = <tr>
                <td>Authorization:</td>
                <td>{txrecord['auth']['result']['avp'][0]},</td>
            </tr>;
            tr5 = txrecord['auth']['result']['avp'].slice(1, -1).map((item, index) =>
                <tr key={index}>
                    <td></td>
                    <td>{item},</td>
                </tr>);
            tr6 = <tr>
                <td><p></p></td>
                <td>{txrecord['auth']['result']['avp'][txrecord['auth']['result']['avp'].length - 1]}<p></p></td>
            </tr>
        }

        let tr7, tr8, tr9;
        if (txrecord['auth'] != undefined && txrecord['auth']['avp'] != undefined) {
            tr7 = <tr>
                <td>Access-Request:</td>
                <td>{txrecord['auth']['avp'][0]},</td>
            </tr>
            tr8 = txrecord['auth']['avp'].slice(1, -1).map((item, index) => <tr key={index}>
                <td></td>
                <td>{item},</td>
            </tr>)
            tr9 = <tr>
                <td><p></p></td>
                <td>{txrecord['auth']['avp'][txrecord['auth']['avp'].length - 1]}<p></p></td>
            </tr>
        }

        return (
            <div>
                <table id="transactionTable">
                    <thead>
                    <tr>
                        <th style={{minWidth: '120px'}}>Transaction ID:</th>
                        <th>{txrecord['transaction_id']}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Transaction Date:</td>
                        <td>{new Date(txrecord['transaction_date'].$date).toISOString().replace('T', ' ').substring(0, 19) + ' GMT'}</td>
                    </tr>
                    <tr>
                        <td>Auth Node:<p></p></td>
                        <td>{txrecord['site']}<p></p></td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>{txrecord['status']}</td>
                    </tr>
                    <tr>
                        <td>Token:</td>
                        <td>{txrecord['is_token'] ? 'True' : 'False'}</td>
                    </tr>
                    <tr>
                        <td>Anonymous:</td>
                        <td>{txrecord['is_anonymous'] ? 'True' : 'False'}</td>
                    </tr>
                    <tr>
                        <td>Resolved:<p></p></td>
                        <td>{txrecord['resolved'] ? 'True' : 'False'}<p></p></td>
                    </tr>
                    <tr>
                        <td>RADIUS User-name:</td>
                        <td>{txrecord['username']}</td>
                    </tr>
                    <tr>
                        <td>Authentication Realm:</td>
                        <td>{txrecord['realm']}</td>
                    </tr>
                    <tr>
                        <td>Auth Customer:</td>
                        <td>{txrecord['customer_id']}</td>
                    </tr>
                    <tr>
                        <td>Chargeable User ID:</td>
                        <td>{txrecord['cui']}</td>
                    </tr>
                    <tr>
                        <td>Chargeable Customer:<p></p></td>
                        <td>{txrecord['charge_customer']}<p></p></td>
                    </tr>
                    <tr>
                        <td>Transaction Type:</td>
                        <td>{txrecord['type']}</td>
                    </tr>
                    <tr>
                        <td>Termination Cause:</td>
                        <td>{txrecord['term_cause']}</td>
                    </tr>
                    <tr>
                        <td>Provider ID:</td>
                        <td>{txrecord['provider_id']}</td>
                    </tr>
                    <tr>
                        <td>Sources:</td>
                        <td>{txrecord['source'].join()}</td>
                    </tr>
                    <tr>
                        <td>Provider Name:</td>
                        <td>{txrecord['provider_name']}</td>
                    </tr>
                    <tr>
                        <td>Provider Country:</td>
                        <td>{txrecord['provider_country']}</td>
                    </tr>
                    <tr>
                        <td>NAS IP Address:</td>
                        <td>{txrecord['nas_ip']}</td>
                    </tr>
                    <tr>
                        <td>NAS Identifier:</td>
                        <td>{txrecord['nas_identifier']}</td>
                    </tr>
                    <tr>
                        <td>Locale:</td>
                        <td>Undetermined</td>
                    </tr>
                    <tr>
                        <td>Session ID:</td>
                        <td>{txrecord['sessionid']}</td>
                    </tr>
                    <tr>
                        <td>Port Type:</td>
                        <td>{txrecord['port_type']}</td>
                    </tr>
                    <tr>
                        <td>Calling Station ID:</td>
                        <td>{txrecord['calling_sid']}</td>
                    </tr>
                    <tr>
                        <td>Called Station ID:<p></p></td>
                        <td>{txrecord['called_sid']}<p></p></td>
                    </tr>
                    <tr>
                        <td>Acct-Session-Time:</td>
                        <td>{txrecord['usage']['secs']}</td>
                    </tr>
                    <tr>
                        <td>Acct-Output-Octets:</td>
                        <td>{txrecord['usage']['bytes_out']}</td>
                    </tr>
                    <tr>
                        <td>Acct-Input-Octets:</td>
                        <td>{txrecord['usage']['bytes_in']}</td>
                    </tr>
                    <tr>
                        <td>Acct-Output-Gigawords:</td>
                        <td>{txrecord['usage']['giga_out']}</td>
                    </tr>
                    <tr>
                        <td>Acct-Input-Gigawords:<p></p></td>
                        <td>{txrecord['usage']['giga_in']}<p></p></td>
                    </tr>
                    <tr>
                        <td>Accounting AVP:</td>
                        <td>{txrecord['avp'][0]},</td>
                    </tr>
                    {
                        txrecord['avp'].slice(1, -1).map((item, index) => <tr key={index}>
                            <td></td>
                            <td>{item},</td>
                        </tr>)
                    }
                    <tr>
                        <td><p></p></td>
                        <td>{txrecord['avp'][txrecord['avp'].length - 1]}<p></p></td>
                    </tr>
                    {tr1}
                    {tr2}
                    {tr3}
                    {tr4}
                    {tr5}
                    {tr6}
                    {tr7}
                    {tr8}
                    {tr9}
                    </tbody>
                </table>
            </div>
        )
    }
}