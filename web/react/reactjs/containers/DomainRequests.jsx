import React from "react"
import {connect} from 'react-redux'
import {getDomains, editDomainRequest} from '../actions/domainRequests'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import moment from 'moment'
import Header from '../components/domainRequests/Header'
class DomainRequests extends React.Component {
    constructor() {
        super();
        this.state = {
            showFilters: false
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }
    componentDidMount(){
        let {dispatch} = this.props;
        dispatch(getDomains())
    }
    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }
    render() {
        let {domainRequests, dispatch} = this.props;
        if (domainRequests == undefined || domainRequests.length ==0)
            return <div style={{marginTop: '110px', padding: '10px 20px'}}>No domain requests</div>
        let columns = [{
                id: 'date',
                Header: 'Created',
                accessor: d => d.created,
                Cell: d => moment(d.value * 1000).format('lll'),
                width: 140
            },
            {
                id: 'status',
                Header: 'Status',
                accessor: d => d.status,
                Cell: d => d.value === 'Active' ? d.value :
                    <SelectCell value={d.value}
                                request_id={d.original.request_id}
                                dispatch={dispatch}/>,
                width: 100
            },
            {
                Header: 'Domain',
                accessor: 'name'
            },
            {
                Header: 'Customer',
                accessor: 'customer_id'
            },
            {
                Header: 'User',
                accessor: 'user'
            }
        ]
        return (
            <div style={{marginTop: '110px',minHeight: '400px', padding: '10px 20px'}}>
                <Header/>
                <legend>Domain Requests<a onClick={this.toggleTableFilters}
                                     href="javascript:void(0);"><i className={`fa fa-filter`}
                                                                   aria-hidden="true"></i></a></legend>
                <div style={{textAlign:'center', maxWidth: '900px', margin:'0 auto', fontSize:'110%'}}>
                <ReactTable
                    className='-striped -highlight'
                    data={domainRequests}
                    columns={columns}
                    filterable={this.state.showFilters}
                    defaultPageSize={20}
                    showPagination={true}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] != undefined ?
                            String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                            true
                    }}
                    defaultSorted={[{
                        id: 'date',
                        desc: true
                    }]}
                />
                </div>
            </div>
        )
    }
}

class SelectCell extends React.Component {
    constructor() {
        super();
        this.onValueChange = this.onValueChange.bind(this)
    }

    onValueChange(event) {
        let {props} = this;
        props.dispatch(editDomainRequest(props.request_id, event.target.value));
    }

    render() {
        return (<div className="tablemargintop">
            <select className="form-control" value={this.props.value} onChange={this.onValueChange}
                style={{padding: '2px 0 0 5px', height:'22px'}}>
                <option value="Pending">Pending</option>
                <option value="Accepted">Accepted</option>
                <option value="Denied">Denied</option>
            </select>
        </div>)
    }
}

const mapStateToProps = (state) => ({
    domainRequests: state.domainRequests.items
});

export default connect(
    mapStateToProps
)(DomainRequests)

