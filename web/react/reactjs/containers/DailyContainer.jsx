import React from "react"
import moment from 'moment'
import HeaderDaily from "../components/HeaderDaily"
import MainStats from '../components/MainStats'
import DailyTotals from '../components/daily/DailyTotals'
import GaugeContainer from './GaugeContainer'
import Timeline from '../components/Timeline'
import UserTotals from '../components/daily/UserTotals'
import Sessions from '../components/daily/Sessions'
import Api from "../Api"
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import {
    Redirect
} from 'react-router-dom'
import {connect} from 'react-redux'
import {fetchProviderDailyIfNeeded} from '../actions/providerDaily'
import {selectCurrentDay} from '../actions/selects'

class DailyContainer extends React.Component {
    handleDayChange(date) {
        let _this = this;
        this.setState({current_day: date, daily_stats: undefined});
        this.getDaily((res) => {
            _this.setState({daily_stats: JSON.parse(res.data.results)});
        }, date);
        this.props.history.push(`/${process.env.PUBLIC_URL}/providers/${this.props.match.params.customer_id}/${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}/`);
    }

    componentDidMount() {
        let {props} = this;
        let {dispatch} = props;
        let current_day = moment(props.startDate).date(parseInt(props.match.params.day))
        dispatch(selectCurrentDay(
            current_day
        ));
        dispatch(fetchProviderDailyIfNeeded(current_day, props.provider))
    }

    componentWillUpdate(nextProps, nextState) {
        let {current_day, provider, dispatch, history, source_stats} = this.props;
        if (current_day != undefined) {
            if (current_day.format('YYYYMMDD') != nextProps.current_day.format('YYYYMMDD')) {
                history.push(`/${process.env.PUBLIC_URL}/providers/${nextProps.provider.id}/${nextProps.current_day.format('YYYY')}/${nextProps.current_day.format('MM')}/${nextProps.current_day.format('DD')}/`);
                dispatch(fetchProviderDailyIfNeeded(nextProps.current_day, nextProps.provider));
            } else if (provider.id != nextProps.provider.id) {
                history.push(`/${process.env.PUBLIC_URL}/providers/${nextProps.provider.id}/${nextProps.current_day.format('YYYY')}/${nextProps.current_day.format('MM')}/${nextProps.current_day.format('DD')}/`);
                dispatch(fetchProviderDailyIfNeeded(nextProps.current_day, nextProps.provider));
            } else if (source_stats == undefined && nextProps.source_stats != undefined)
                dispatch(fetchProviderDailyIfNeeded(nextProps.current_day, nextProps.provider));
        }
    }


    componentDidUpdate(prevPorps) {
        // let old_source_stats = prevPorps.source_stats;
        // let {current_day, source_stats, startDate, dispatch} = this.props;
        //
        // if (
        //     current_day.format('YYYYMM') != startDate.format('YYYYMM') ||
        //     (current_day.isSame(startDate.startOf('month').endOf('day')) && source_stats != undefined && old_source_stats == undefined)
        // ) {
        //     if (source_stats != undefined) {
        //         let date = moment(parseInt(source_stats[0].date.$date));
        //         if (date.format('YYYYMM') == startDate.format('YYYYMM')) {
        //             dispatch(selectCurrentDay(
        //                 date
        //             ));
        //             //this.setState({current_day: date});
        //             //this.props.history.push(`/${process.env.PUBLIC_URL}/providers/${this.props.match.params.customer_id}/${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}/`);
        //         }
        //         else dispatch(selectCurrentDay(
        //             startDate.startOf('month').endOf('day')
        //         ));//this.setState({current_day: startDate.startOf('month').endOf('day')})
        //     } else dispatch(selectCurrentDay(
        //         startDate.startOf('month').endOf('day')
        //     ));//this.setState({current_day: startDate.startOf('month').endOf('day')})
        // }
    }

    render() {
        let {props} = this;
        let {source_stats, current_day, daily_stats, history} = props;
        let day_stat;
        if (source_stats)
            for (let i = 0; i < source_stats.length; i++) {
                let stat = source_stats[i];
                if (moment.utc(parseInt(stat.date.$date)).date() == parseInt(current_day.format('D'))) {
                    day_stat = stat;
                    break;
                }
            }
        return (
            <div>
                <HeaderDaily {...props}/>
                <Container fluid={true} className="main_container" style={{marginTop: '180px'}}>
                    {day_stat &&
                    <div>
                        <MainStats stats={day_stat}/>
                        <GaugeContainer total_stats={day_stat}/>
                        <DailyTotals stats={day_stat}/>
                    </div>
                    }
                    {daily_stats &&
                    <div>
                        <Timeline daily={daily_stats} date={current_day} history={history}/>
                        <UserTotals stats={daily_stats} date={current_day}/>
                        <Sessions stats={daily_stats} date={current_day}/>
                    </div>
                    }

                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let source_stats = state.providerMonthly[
    '' + state.selects.provider.id + '-' + moment.utc(state.selects.start_date).format('YYYYMM')
        ];
    source_stats = source_stats == undefined ? undefined : source_stats.items;
    let daily_stats = state.providerDaily[
    '' + state.selects.provider.id + '-' + moment.utc(state.selects.current_day).format('YYYYMMDD')
        ];
    daily_stats = daily_stats == undefined ? undefined : daily_stats.items;
    return ({
        startDate: moment.utc(state.selects.start_date),
        current_day: moment.utc(state.selects.current_day),
        provider: state.selects.provider,
        source_stats: source_stats,
        daily_stats: daily_stats
    });
};


export default connect(mapStateToProps)(DailyContainer)

