import React from "react"
import {connect} from 'react-redux'
import {getUsers} from '../actions/accounts'
import 'react-datepicker/dist/react-datepicker.css';
import UserList from '../components/accounts/UserList'
import AdminList from '../components/accounts/AdminList'
import CreateUser from '../components/accounts/CreateUser'
import Stats from '../components/accounts/Stats'
import Domains from '../components/accounts/Domains'
import EmailTemplate from '../components/accounts/EmailTemplate'
import BatchUpload from '../components/accounts/BatchUpload'
import CreateCode from '../components/accounts/CreateCode'
import CodesList from '../components/accounts/CodesList'
import BatchDownloadCodes from '../components/accounts/BatchDownloadCodes'
import {
    Container, Row, Col, Button, ListGroup, ListGroupItem
} from 'reactstrap';
const styles = {
    tablecontainer: {
        padding: '5px',
        display: 'inline-block',
        verticalAlign: 'top',
    },
    createusercontainer: {
        display: 'inline-block',
        verticalAlign: 'top',
    },
    inline: {
        display: 'inlineBlock'
    }
};


class MainContainer extends React.Component {
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(getUsers());
    }
    render() {
        return (<Container className="accounts_container">
            <Stats/>
            <AdminContainer/>
            <UsersContainer/>
        </Container>)
    }
}

class AdminContainer extends React.Component{
    constructor(){
        super()
        this.modes = ['admin_list', 'admin_create', 'domains']
        this.state = {
            mode: this.modes[0]
        };
        this.setMode = this.setMode.bind(this)
    }
    setMode(modeIndex) {
        this.setState({mode: this.modes[modeIndex]})
    }

    render() {
        let {mode} = this.state;
        let content = undefined;
        switch (mode) {
            case this.modes[2]:
                content = <Domains />
                break;
            case this.modes[0]:
                content = <AdminList setMode={this.setMode}/>
                break;
            case this.modes[1]:
                content = <CreateUser mode="admin"/>
                break;
        }
        return (
            <Row className="group_container">
                <Col md="3" xs="12">
                    <AdminMenu mode={mode} modes={this.modes} setMode={this.setMode}/>
                </Col>
                <Col md="9" xs="12">
                    {content}
                </Col>
            </Row>
        )
    }
}

class UsersContainer extends React.Component{
    constructor(){
        super()
        this.modes = ['user_list', 'create_user',
            'email_templates', 'batch_upload',
            'codes_list', 'order_code', 'batch_codes'
        ];
        this.state = {
            mode: this.modes[0]
        };
        this.setMode = this.setMode.bind(this)
    }
    setMode(modeIndex) {
        this.setState({mode: this.modes[modeIndex]})
    }

    render() {
        let {mode} = this.state;
        let content = undefined;
        switch (mode) {
            case this.modes[0]:
                content = <UserList setMode={this.setMode}/>
                break;
            case this.modes[1]:
                content = <CreateUser/>
                break;
            case this.modes[2]:
                content = <EmailTemplate/>
                break;
            case this.modes[3]:
                content = <BatchUpload/>
                break;
            case this.modes[4]:
                content = <CodesList setMode={this.setMode}/>
                break;
            case this.modes[5]:
                content = <CreateCode />
                break;
            case this.modes[6]:
                content = <BatchDownloadCodes />
                break;
        }
        return (
            <Row className="group_container">
                <Col md="3" xs="12">
                    <UsersMenu mode={mode} modes={this.modes} setMode={this.setMode}/>
                </Col>
                <Col md="9" xs="12">
                    {content}
                </Col>
            </Row>
        )
    }
}

class AdminMenu extends React.Component {
    render() {
        let {mode, modes, setMode} = this.props;
        return (<div>
            <ListGroup>
                <div style={{margin: '5px'}}>Administrators</div>
                <ListGroupItem active={mode === modes[0]} tag="button" action onClick={() => setMode(0)}>
                    Admin Roles</ListGroupItem>
                <ListGroupItem active={mode === modes[1]} tag="button" action onClick={() => setMode(1)}>
                    Create Role</ListGroupItem>
                <ListGroupItem active={mode === modes[2]} tag="button" action onClick={() => setMode(2)}>
                    Manage Domains</ListGroupItem>
            </ListGroup>
        </div>)
    }
}
class UsersMenu extends React.Component {
    render() {
        let {mode, modes, setMode} = this.props;
        return (<div>
            <ListGroup>
                <div style={{margin: '10px 0 5px'}}>Users</div>
                <ListGroupItem active={mode === modes[0]} tag="button" action onClick={() => setMode(0)}>
                    User List</ListGroupItem>
                <ListGroupItem active={mode === modes[1]} tag="button" action onClick={() => setMode(1)}>
                    Create User</ListGroupItem>
                <ListGroupItem active={mode === modes[2]} tag="button" action onClick={() => setMode(2)}>
                    Email Templates</ListGroupItem>
                <ListGroupItem active={mode === modes[3]} tag="button" action onClick={() => setMode(3)}>
                    Batch Upload</ListGroupItem>
                <div style={{margin: '10px 0 5px'}}>Codes</div>
                <ListGroupItem active={mode === modes[4]} tag="button" action onClick={() => setMode(4)}>
                    Code List</ListGroupItem>
                <ListGroupItem active={mode === modes[5]} tag="button" action onClick={() => setMode(5)}>
                    Order Code</ListGroupItem>
                <ListGroupItem active={mode === modes[6]} tag="button" action onClick={() => setMode(6)}>
                    Batch Download</ListGroupItem>
            </ListGroup>
        </div>)
    }
}


export default connect()(MainContainer)
