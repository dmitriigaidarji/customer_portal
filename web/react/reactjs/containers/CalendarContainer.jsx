import React from "react"
import {DateRange} from 'react-date-range';
import myDateRanges from "../components/myDateRanges"
import {Button} from 'reactstrap';
import moment from 'moment'



import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

export default class CalendarContainer extends React.Component {
    constructor(){
        super()
        this.state = {
            focusedInput : 'startDate',
        }
    }
    isOutsideRange(date){
        return moment() < date
    }
    render() {
        let {calendarDisplay, handleChange, confirm, startDate, endDate, substractMonth, addMonth} = this.props;
        return (
            // <div className={calendarDisplay ? "calendarVisible" : "calendarHidden"}>
            //     <DateRange
            //         startDate={ now => {return now.startOf('month')}}
            //         endDate={ now => {return now}}
            //         calendars={1}
            //         ranges={ myDateRanges }
            //         onInit={ handleChange.bind(this) }
            //         onChange={ handleChange.bind(this) }
            //     />
            //     <Button outline color="primary" className="calendarOK" onClick={confirm}>OK</Button>{' '}
            //</div>
        <DateRangePicker
          startDate={startDate} // momentPropTypes.momentObj or null,
          endDate={endDate} // momentPropTypes.momentObj or null,
          onDatesChange={handleChange.bind(this)} // PropTypes.func.isRequired,
          focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
          onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
          numberOfMonths={1}
          isOutsideRange={this.isOutsideRange}
          onPrevMonthClick={substractMonth}
          onNextMonthClick={addMonth}
        />
        )
    }
}