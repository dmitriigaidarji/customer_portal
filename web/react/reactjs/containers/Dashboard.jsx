import React from "react"
import Radium from 'radium';
import {
    Row, Col, Container
} from 'reactstrap';
import moment from 'moment'

let style = {
    block: {
        height: '100px',
        cursor: 'pointer',
        whiteSpace: 'nowrap'
    },
    bg1: {
        // backgroundColor: 'rgb(243, 255, 211)'
    },
    bg2: {
        // backgroundColor: 'rgb(230, 222, 255)'
    },
    bg3: {
        // backgroundColor: '#89ffd9'
    },
    title: {
        lineHeight: '100px',
        fontSize: '200%',
        fontWeight: 500,
        width: 300,
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '50px'
    },
    list: {
        display: 'inline-block',
        margin: '20px 0',
        verticalAlign: 'middle',
        padding: '0 20px',
    },
    li: {
        cursor: 'pointer',
        ':hover': {
            background: '#e7ede5'
        }
    }
}
@Radium
export default class Dashboard extends React.Component {
    redirect() {

    }

    render() {
        let {providers, customers, history} = this.props;
        let date = moment();
        return (
            <Container fluid={true} className="main_container" style={{marginTop: '120px'}}>
                <div style={[style.block, style.bg1]}>
                    <div style={style.title} onClick={() => {
                        history.push(`/${process.env.PUBLIC_URL}/customers/`)
                    }}>
                        Customers
                    </div>
                    { customers != undefined &&
                    <div style={style.list}>
                        <select defaultValue='0' className="form-control" style={{width: '300px'}}
                                onChange={(event) => {
                            if (event.target.value != '0')
                                history.push(`/${process.env.PUBLIC_URL}/customers/${event.target.value}/${date.format('YYYY')}/${date.format('MM')}/`);
                        }}>
                            <option value="0" disabled>Choose a customer...</option>
                            {customers.map((customer) =>
                                <option key={customer.id} value={customer.id}>
                                    {customer.id }: {customer.name }, { customer.country }
                                </option>
                            )}
                        </select>
                    </div>
                    }
                </div>
                <div style={[style.block, style.bg2]}>
                    <div style={style.title} onClick={() => {
                        history.push(`/${process.env.PUBLIC_URL}/providers/`)
                    }}>
                        Providers
                    </div>
                    { providers != undefined &&
                    <div style={style.list}>
                        <select defaultValue='0' className="form-control" style={{width: '300px'}}
                                onChange={(event) => {
                            if (event.target.value != '0')
                                history.push(`/${process.env.PUBLIC_URL}/providers/${event.target.value}/${date.format('YYYY')}/${date.format('MM')}/`)
                        }}>
                            <option value="0" disabled>Choose a provider...</option>
                            {providers.map((provider) =>
                                <option key={'p' + provider.id} value={provider.id}>
                                    {provider.id }: {provider.name }, { provider.country }
                                </option>
                            )}
                        </select>
                    </div>
                    }
                </div>
                <div style={[style.block, style.bg2]} onClick={() => {
                        history.push(`/${process.env.PUBLIC_URL}/sqi/${date.format('YYYYMMDD')}/`)
                    }}>
                    <div style={style.title}>
                        SQI Data
                    </div>
                </div>
                <div style={[style.block]} onClick={() => {
                        history.push(`/${process.env.PUBLIC_URL}/domains/`)
                    }}>
                    <div style={style.title}>
                        Domain Requests
                    </div>
                </div>
                <div style={[style.block]} onClick={() => {
                        history.push(`/${process.env.PUBLIC_URL}/referral-requests/`)
                    }}>
                    <div style={style.title}>
                        Referral Requests
                    </div>
                </div>
            </Container>
        )
    }
}