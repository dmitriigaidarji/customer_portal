import React from "react"
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import Header from "../components/Header"
import MainStats from "../components/MainStats"
import TotalStats from "../components/TotalStats"
import GaugeContainer from "./GaugeContainer"
import ConnectionsBarChartContainer from "../containers/ConnectionsBarChartContainer"
import moment from 'moment'
import {connect} from 'react-redux'

import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect
} from 'react-router-dom'

class ConsoleAppContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stats: undefined,
            total_stats: undefined,
        };
        this.updateStats = this.updateStats.bind(this)
    }

    componentDidMount() {
        this.updateStats();
    }

    componentDidUpdate(prevProps, prevState) {
        let prev_start = prevProps.startDate, prev_end = prevProps.endDate,
            prev_stats = prevProps.source_stats, prev_provider = prevProps.provider;
        let {startDate, endDate, source_stats, provider} = this.props;
        if (
            source_stats != undefined && (
            !prev_start.isSame(startDate) || !prev_end.isSame(endDate) ||
            prev_stats == undefined || prev_provider.id != provider.id)
        ) {
            this.updateStats()
        }
    }

    updateStats() {
        let {source_stats, startDate, endDate} = this.props;
        let neededstats = [];
        if (source_stats != undefined)
            for (let i = 0; i < source_stats.length; i++) {
                let stat = source_stats[i];
                let statDate = moment.utc(stat.date.$date);
                if (startDate.isSameOrBefore(statDate) && statDate.isSameOrBefore(endDate)) {
                    neededstats.push(stat)
                }
            }
        this.setState({stats: neededstats, total_stats: getTotalStats(neededstats)})
    }

    render() {
        let {props} = this;
        return (
            <div>
                <Header {...props}/>
                <Container fluid={true} className="main_container" style={{marginTop: '160px'}}>
                    {props.source_stats ?
                        <div>
                            <MainStats stats={this.state.total_stats}/>
                            <GaugeContainer total_stats={this.state.total_stats}/>
                            <TotalStats stats={this.state.total_stats}/>
                            <ConnectionsBarChartContainer
                                stats={this.state.stats}
                                {...props}
                            />
                        </div>
                        :
                        <div>
                            Loading monthly stats..
                        </div>
                    }
                </Container>
            </div>
        )
    }
}

function getTotalStats(data) {
    let total_stats = {
        accept: 0,
        bytes: 0,
        request: 0,
        reject: 0,
        open: 0,
        stop: 0,
        start: 0,
        deny: 0,
        update: 0,
        anonymous: 0,
        token: 0,
        cui_resolved: 0,
        cui_unresolved: 0,
        uniq_users: 0,
        seconds: 0,
        avg_users: 0,
        avg_bytes: 0
    };
    data.forEach(function (d) {
        for (let property in total_stats) {
            if (total_stats.hasOwnProperty(property)) {
                total_stats[property] += d[property]
            }
        }
    });
    if (data.length > 0) {
        total_stats.avg_users = total_stats.uniq_users / data.length;
        total_stats.avg_bytes = total_stats.bytes / data.length;
    }
    return total_stats
}


const mapStateToProps = (state) => {
    let source_stats = state.providerMonthly[
    '' + state.selects.provider.id + '-' + moment.utc(state.selects.start_date).format('YYYYMM')
        ];
    source_stats = source_stats == undefined ? undefined : source_stats.items;
    return ({
        startDate: moment.utc(state.selects.start_date),
        endDate: moment.utc(state.selects.end_date),
        provider: state.selects.provider,
        source_stats: source_stats
    });
}
export default connect(mapStateToProps)(ConsoleAppContainer)