import React from "react"
import Gauge from "../components/Gauge"
import {
    Row, Col
} from 'reactstrap';
import PieChart from '../components/PieChart'
import Donut from '../components/Donut'
export default class GaugeContainer extends React.Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.total_stats != undefined;
    }

    render() {
        if (this.props.total_stats == undefined) {
            return (
                <div></div>
            )
        }
        let {total_stats} = this.props
        let closeRateContent = [
            {name: 'Stops', count: total_stats.stop, caption: total_stats.stop + ' stops', color: '#1f77b4'},
            {name: 'Starts', count: total_stats.start, caption: total_stats.start + ' starts', color: '#7fe47f'},
            {name: 'Updates', count: total_stats.update, caption: total_stats.update + ' updates', color: '#ff7f0e'},
        ];
        let authRespRate = [
            {name: 'Accepts', count: total_stats.accept, caption: total_stats.accept + ' accepts', color: '#7fe47f'},
            {name: 'Rejects', count: total_stats.reject, caption: total_stats.reject + ' rejects', color: '#1f77b4'},
            {name: 'Denies', count: total_stats.deny, caption: total_stats.deny + ' denies', color: '#ff7f0e'}
        ];
        let sesStartRate = [
            {name: 'Accepts', count: total_stats.accept, caption: total_stats.accept + ' accepts', color: '#7fe47f'},
            {name: 'Starts', count: total_stats.start, caption: total_stats.start + ' starts', color: '#1f77b4'},
        ];
        let cuiResRate = [
            {name: 'Resolved', count: total_stats.cui_resolved, caption: total_stats.cui_resolved + ' resolved', color: '#7fe47f'},
            {name: 'Unresolved', count: total_stats.cui_unresolved, caption: total_stats.cui_unresolved + ' unresolved', color: '#1f77b4'},
        ];
        return (
            <Row>
                <Col md="3" lg="3" xs="6" className="gaugeCol">
                    <Donut data={authRespRate} title="Auth. Response Rate"
                           value={total_stats.request == 0 ? 'No requests' : (total_stats.accept * 100 / total_stats.request).toFixed(0) + '%'}
                           sort="value"
                    />
                    {/*<Gauge*/}
                    {/*value={this.props.total_stats.request == 0 ? 0 : this.props.total_stats.accept / this.props.total_stats.request * 100}*/}
                    {/*title="Authentication Response Rate"*/}
                    {/*/>*/}
                </Col>
                <Col md="3" lg="3" xs="6" className="gaugeCol">
                    <Donut data={sesStartRate} title="Session Start Rate"
                           value={total_stats.start == 0 ? 'No sessions' : (total_stats.start * 100 / total_stats.accept).toFixed(0) + '%'}
                           sort="value"
                    />
                    {/*<Gauge*/}
                    {/*value={this.props.total_stats.start == 0 ? 0 : this.props.total_stats.start / this.props.total_stats.accept * 100}*/}
                    {/*title="Session Start Rate"*/}
                    {/*/>*/}
                </Col>
                <Col md="3" lg="3" xs="6" className="gaugeCol">
                    <Donut data={closeRateContent} title="Session Close Rate"
                           value={total_stats.start == 0 ? 'No sessions' : (total_stats.stop * 100 / total_stats.start).toFixed(0) + '%'}
                           sort="value"
                    />
                </Col>
                <Col md="3" lg="3" xs="6" className="gaugeCol">
                    <Donut data={cuiResRate} title="CUI Resolve Rate"
                           value={total_stats.cui_resolved == 0 ? 'No cuis' : (total_stats.cui_resolved * 100 / (total_stats.cui_unresolved + total_stats.cui_resolved)).toFixed(0) + '%'}
                           sort="value"
                    />
                {/*<Gauge*/}
                {/*value={this.props.total_stats.cui_resolved == 0 ? 0 : this.props.total_stats.cui_resolved / ( this.props.total_stats.cui_unresolved + this.props.total_stats.cui_resolved) * 100}*/}
                {/*title="CUI Resolve Rate"*/}
                {/*/>*/}
                </Col>
            </Row>
        )
    }
}