import React from "react"
import {
    Container, Row, Col
} from 'reactstrap';
import moment from 'moment'
import CustomerAccessHeaderDaily from '../customeraccess/components/HeaderDaily'
import HeaderDaily from '../components/customers/HeaderDaily'
import Totals from '../components/customers/Totals'
import UsageSummary from '../components/customers/UsageSummary'
import Transactions from '../components/customers/Transactions'
import Timeline from '../components/customers/Timeline'
import ConsumptionChart from '../components/customers/ConsumptionChart'
import Donut from '../components/Donut'
import {connect} from 'react-redux'
import {selectCurrentDay} from '../actions/selects'
import UsageMap from '../components/user/UsageMap'
import UserDevices from '../components/customers/UserDevices'
class CustomerDaily extends React.Component {
    constructor() {
        super();
        this.state = {
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined,
            total_seconds: undefined,
            active_dates: undefined,
        };

        this.parseStats = this.parseStats.bind(this);
    }

    componentDidUpdate(prevPorps) {
        let old_source_stats = prevPorps.source_stats, prev_startDate = prevPorps.startDate,
            prev_customer = prevPorps.customer;
        let {current_day, source_stats, startDate, customer} = this.props;
        if (source_stats != undefined &&
            (old_source_stats == undefined ||
            !current_day.isSame(prevPorps.current_day) ||
            !prev_startDate.isSame(startDate) ||
            prev_customer.id != customer.id)
        )
            this.parseStats(source_stats)
    }

    componentDidMount() {
        let {props} = this;
        let {dispatch, source_stats, current_day} = props;
        dispatch(selectCurrentDay(
            moment(props.startDate).date(parseInt(props.match.params.day))
        ));
        if (source_stats != undefined && current_day != undefined)
            this.parseStats(source_stats);
    }

    parseStats(documents) {
        let providerdata = this.props.providers;
        let {current_day} = this.props;
        let startDate = moment(current_day).startOf('day');
        let endDate = moment(current_day).endOf('day');
        let tprecord = {
            'user_count': 0,
            'connections': {'count': 0, 'data': {}},
            'activations': {'count': 0, 'data': {}},
            'devices': {
                'count': 0,
                'data': {
                    'ios': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': {}
                    },
                    'android': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': []
                    }
                }
            },
            'totals': {
                'tbytes': 0,
                'tseconds': 0,
                'tdata': 0,
                'avgdata': 0,
                'ttime': 0,
                'avgtime': 0
            }
        };


        let stats = [];
        let alltx = [];
        let secs = [];
        let totaldata = 0;
        let totalsecs = 0;
        let activationcount = 0;
        let connectioncount = 0;
        let activeDates = [];

        if (documents.length > 0) {
            for (let i = 0; i < documents.length; i++) {
                let record = JSON.parse(JSON.stringify(documents[i]));
                let found = false;
                record['activations_current'] = [];
                record['user_tx_current'] = [];
                record['devices_current'] = []
                for (let j = 0; j < record['activations'].length; j++) {
                    let aitem = record['activations'][j];
                    aitem.date_obj = moment.utc(aitem.date.$date);
                    activeDates.push(aitem.date_obj)
                    if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                        ///
                        let actday = aitem.date_obj.format('YYYYMMDD');
                        if (tprecord['activations']['data'][actday] == undefined)
                            tprecord['activations']['data'][actday] = 0
                        tprecord['activations']['data'][actday] += 1

                        //device
                        for (let k = 0; k < aitem['sqi'].length; k++) {
                            let sqi = aitem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }


                        //
                        found = true;
                        record['activations_current'].push(aitem);
                        activationcount += 1
                        let geoposition = [0, 0]
                        let device = ''
                        let txid = aitem['auth_id']
                        let nai = record['nai']
                        let rtype = 'Activation'
                        let location = '100002'
                        let country = 'US'
                        let seconds = '-'
                        let bytes = '-'
                        if (aitem['sqi'].length > 0) {
                            for (let k = 0; k < aitem['sqi'].length; k++) {
                                let sqi = aitem['sqi'][k];
                                device = sqi['device_id'];
                                if (sqi['lat'] != 0)
                                    geoposition = [sqi['lat'], sqi['lng']]
                            }
                        }
                        alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])
                    }
                }
                let userbytes = 0;
                let userseconds = 0;
                for (let j = 0; j < record['user_tx'].length; j++) {
                    let citem = record['user_tx'][j];
                    citem.date_obj = moment.utc(citem.tx_date.$date);
                    activeDates.push(citem.date_obj)
                    if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                        for (let k = 0; k < citem['sqi'].length; k++) {
                            let sqi = citem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }
                        ///
                        found = true;
                        record['user_tx_current'].push(citem)
                        connectioncount += 1
                        let geoposition = [0, 0]
                        if (citem['usage']['secs'] != 'asda') {
                            ///
                            let txday = citem.date_obj.format('YYYYMMDD');
                            if (tprecord['connections']['data'][txday] == undefined)
                                tprecord['connections']['data'][txday] = 0;
                            tprecord['connections']['data'][txday] += 1;
                            ///

                            let txid = citem['tx_id']
                            let nai = record['nai']
                            let rtype = 'Connection'
                            let location = citem['provider_id']
                            let country = undefined
                            providerdata.some((prov) => {
                                if (prov['id'] == citem['provider_id']) {
                                    country = prov['iso_code'];
                                    return true;
                                }
                            })
                            let device = citem['calling_station']
                            if (citem['sqi'].length > 0)
                                for (let k = 0; k < citem['sqi'].length; k++) {
                                    let sqi = citem['sqi'][k];
                                    if (sqi['lat'] != 0)
                                        geoposition = [sqi['lat'], sqi['lng']]
                                }
                            let seconds = citem['usage']['secs']
                            let bytes = 0
                            bytes += citem['usage']['bytes_in']
                            bytes += citem['usage']['bytes_out']
                            bytes += citem['usage']['giga_in'] * 2147483648
                            bytes += citem['usage']['giga_out'] * 2147483648
                            userbytes += bytes;
                            totaldata += bytes;
                            userseconds += seconds;
                            totalsecs += seconds;
                            if (seconds >= 10 && bytes > 0) {
                                alltx.push([citem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])
                                secs.push({
                                    timestamp: citem.tx_date.$date,
                                    usage: {secs: seconds, bytes: bytes},
                                    cui: nai,
                                    tx_id: txid
                                })
                            }
                        }
                    }
                }
                record['usage_current'] = {total_bytes: userbytes, total_seconds: userseconds};
                if (found) stats.push(record)
            }
        }

        tprecord['connections']['count'] = connectioncount;
        tprecord['activations']['count'] = activationcount;
        tprecord['totals']['tbytes'] = totaldata;
        tprecord['totals']['tseconds'] = totalsecs

        for (let i = 0; i < stats.length; i++) {
            let item = stats[i];
            for (let j = 0; j < item['devices_current'].length; j++) {
                let dev = item['devices_current'][j];
                tprecord['devices']['count'] += 1
                if (dev['platform']['ios_version'] != undefined) {
                    tprecord['devices']['data']['ios']['count'] += 1
                    if (!tprecord['devices']['data']['ios']['os'].hasOwnProperty(dev['platform']['ios_version']))
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] += 1;
                    if (!tprecord['devices']['data']['ios']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['ios']['release'][dev['version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['release'][dev['version']] += 1;
                    if (!tprecord['devices']['data']['ios']['device'].hasOwnProperty(dev['platform']['device_model']))
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] = 1;
                    else
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] += 1;
                }
                else if (dev['platform']['device_model'] == 'Android') {
                    tprecord['devices']['data']['android']['count'] += 1
                    if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['android_version']))
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] = 1
                    else
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] += 1
                    if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['android']['release'][dev['version']] = 1
                    else
                        tprecord['devices']['data']['android']['release'][dev['version']] += 1
                    if (!tprecord['devices']['data']['android']['device'].hasOwnProperty(dev['platform']['phone']))
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] = 1;
                    else
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] += 1;
                }
            }
        }
        tprecord['user_count'] = stats.length;
        tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
        tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
        if (tprecord['user_count'] > 0) {
            tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count'])
            tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count'])
        }
        this.setState({
            current_stats: stats,
            total_stats: tprecord,
            transactions: alltx,
            total_seconds: secs,
            active_dates: activeDates
        });
    }

    render() {
        let {props} = this;
        let {customer, current_day, access_level, history} = props;
        if (current_day == undefined)
            return <div>Loading...</div>
        let {total_stats, active_dates, current_stats, transactions, total_seconds} = this.state;

        let iosreleases = [],
            androidreleases = [],
            iosversions = [],
            androidversions = [];
        if (total_stats != undefined) {
            let data = total_stats.devices.data.ios.release
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    iosreleases.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.android.release
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    androidreleases.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.ios.os
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    iosversions.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.android.os
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    androidversions.push({name: key, count: data[key]})
                }
        }
        return (
            <div>
                {access_level === 'CUSTOMER' ?
                    <CustomerAccessHeaderDaily {...props}
                                               active_dates={active_dates}/>
                    :
                    <HeaderDaily {...props}
                                 active_dates={active_dates}
                    />
                }
                <Container fluid={true} className="main_container" style={{marginTop: '210px'}}>
                    <Totals stats={total_stats} />
                    {total_stats != undefined &&
                    <div style={{marginBottom: '10px'}}>
                        {
                            total_stats.connections.count + total_stats.activations.count == 0 ?
                                total_stats.activations.count :
                                "{0} ({1}%)".format(total_stats.activations.count, (total_stats.activations.count / (total_stats.connections.count + total_stats.activations.count) * 100).toFixed(0))
                        } Activations
                        <span style={{marginLeft: '100px'}}>{total_stats.connections.count} Connections</span>
                    </div>
                    }
                    <Timeline daily={total_seconds}/>
                    {/*<UsageDaily stats={total_seconds} date={current_day}/>*/}
                    <ConsumptionChart stats={current_stats} full_width={true} startDate={current_day}
                                      history={history}/>
                    {transactions != undefined &&
                    <UsageMap stats={transactions} date={current_day} access_level={access_level}/>
                    }
                    <UserDevices stats={total_stats}
                                 iosreleases={iosreleases}
                                 iosversions={iosversions}
                                 androidreleases={androidreleases}
                                 androidversions={androidversions}
                    />
                    <UsageSummary
                        stats={current_stats}
                        date={current_day}
                        customer_id={customer.id}
                    />
                    <Transactions
                        stats={transactions}
                        date={current_day}
                        customer_id={customer.id}
                        showCSV={true}
                    />
                </Container>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    current_day: moment.utc(state.selects.current_day),
    access_level: state.selects.access_level
});

export default connect(
    mapStateToProps
)(CustomerDaily)
