import React from "react"
import {connect} from 'react-redux'
import {getReferralRequests, editReferralRequests} from '../actions/referralRequests'
import Header from '../components/referralRequests/Header'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import moment from 'moment'

class SelectCell extends React.Component {
    constructor() {
        super();
        this.onValueChange = this.onValueChange.bind(this)
    }

    onValueChange(event) {
        let {props} = this;
        props.dispatch(editReferralRequests(props.referral_id, event.target.value));
    }

    render() {
        return (<div className="tablemargintop">
            <select className="form-control" value={this.props.value} onChange={this.onValueChange}>
                <option value="Submitted">Submitted</option>
                <option value="Completed">Completed</option>
                <option value="Rejected">Rejected</option>
            </select>
        </div>)
    }
}
class ReferralRequests extends React.Component {
    constructor() {
        super();
        this.state = {
            showFilters: false
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }
    componentDidMount(){
        let {dispatch} = this.props;
        dispatch(getReferralRequests())
    }
    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }
    render() {
        let {requests, dispatch} = this.props;
        console.log(requests)
        let columns = [{
                id: 'date',
                Header: 'Date',
                accessor: d => moment(d.created*1000).format('lll'),
                width: 150
            },
            {
                id: 'email',
                Header: 'Email',
                accessor: 'email'
            },
            {
                id: 'name',
                Header: 'Name',
                accessor: 'name',
                width: 100
            },
            {
                id: 'response',
                Header: 'Response Date',
                accessor: d => moment(d.response_date*1000).format('lll'),
                width: 150
            },
            {
                id: 'note',
                Header: 'Note',
                Cell:<div></div>,
                width: 100
            },
            {
                id: 'action',
                Header: 'Action',
                Cell: d => <SelectCell value={d.original.status}
                                       referral_id={d.original.id}
                                       dispatch={dispatch}
                    />,
                width: 100
            }
        ];
        let tablediv = <div>Loading Referral Requests...</div>
        if (requests != undefined)
            if (requests.length == 0)
                tablediv = <div>There are no referral requests yet</div>
            else tablediv = <div style={{maxWidth: '800px', margin:'0 auto'}}>
                <ReactTable
                    className='-striped -highlight'
                    data={requests}
                    columns={columns}
                    filterable={this.state.showFilters}
                    defaultPageSize={20}
                    showPagination={true}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] != undefined ?
                            String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                            true
                    }}
                    defaultSorted={[{
                        id: 'date',
                        desc: true
                    }]}
                />
            </div>
        return (
            <div style={{marginTop: '110px', padding: '10px 20px', minHeight: '400px'}}>
                <Header/>
                <legend>Referral Requests</legend>
                {tablediv}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    requests: state.referralRequests.items
});

export default connect(
    mapStateToProps
)(ReferralRequests)

