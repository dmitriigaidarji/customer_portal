import React from "react"
import Api from '../Api';
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import moment from 'moment'
export default class ConnectionsBarChartContainer extends React.Component {
    constructor(props) {
        super(props)
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {stats} = this.props;
        if (stats != undefined && stats.length > 0)
            this.updateChart(this.props.stats);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {stats} = this.props;
        if (stats != undefined && stats.length > 0)
            this.updateChart(this.props.stats);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;

    }

    updateChart(data) {
        let _this = this;
        if (data != undefined) {
            let svg = d3.select("#packets");
            svg.attr('width', Math.min(1500, $("#barChartParent").width()));
            svg.selectAll("*").remove();
            let margin = {top: 20, right: 20, bottom: 30, left: 40},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom,
                legendWidth = 19,
                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            let x0 = d3.scaleBand()
                .rangeRound([0, width - legendWidth * 2])
                .paddingInner(0.1);

            let x1 = d3.scaleBand()
                .padding(0.05);
            let x = d3.scaleTime()
                .rangeRound([0, width]);
            let y = d3.scaleLinear()
                .rangeRound([height, 0]);

            let z = d3.scaleOrdinal()
                .range(["#F5EE9D", "#9CC586", "#D09188", "#713339", "#5259DE", "#7BAEBC", "#d0743c"]);

            let innerColumns = {
                "column1": ["request"],
                "column2": ["accept", "reject", "deny"],
                "column3": ["open", "stop"]
            };

            // Define 'div' for tooltips
            let div = d3.select("body")
                .append("div")  // declare the tooltip div
                .attr("class", "tooltip")
                .style("opacity", 0);

            let columnHeaders = ['request', 'accept', 'reject', 'deny', 'stop', 'open'];
            z.domain(columnHeaders);
            data.forEach(function (d) {
                let yColumn = [];
                d.columnDetails = columnHeaders.map(function (name) {
                    for (let ic in innerColumns) {
                        if ($.inArray(name, innerColumns[ic]) >= 0) {
                            if (!yColumn[ic]) {
                                yColumn[ic] = 0;
                            }
                            let yBegin = yColumn[ic];
                            yColumn[ic] += +d[name];
                            return {name: name, column: ic, yBegin: yBegin, yEnd: +d[name] + yBegin,};
                        }
                    }
                });
                d.total = d3.max(d.columnDetails, function (d) {
                    return d.yEnd;
                });
                d.date_obj = moment.utc(d.date.$date);
                d.date_str = d.date_obj.format('DD');
            });
            function getDaysInMonth(date) {
                date = date.startOf('month');
                let month = date.month();
                let days = [];
                while (date.month() === month) {
                    days.push(moment(date));
                    date.add(1, 'days')
                }
                return days;
            }
            let monthdates = getDaysInMonth(moment.utc(data[0].date.$date));
            x0.domain(monthdates);
            // x0.domain(data.map(function (d) {
            //     return d.date_obj;
            // }));
            x1.domain(d3.keys(innerColumns)).rangeRound([0, x0.bandwidth()]);

            y.domain([0, d3.max(data, function (d) {
                return d.total;
            })]).nice();
            z.domain(columnHeaders);
            let line = d3.line()
                .x(function (d) {
                    return x0(d.date_obj);
                })
                .y(function (d) {
                    return y(d.start);
                });

            let project_stackedbar = g.selectAll("rect")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform", function (d) {
                    return "translate(" + x0(d.date_obj) + ",0)";
                })
                .on("click", function (d) {
                    _this.props.history.push(`${_this.props.match.url}${d.date_str}/`);
                });

            project_stackedbar.selectAll("rect")
                .data(function (d) {
                    return d.columnDetails;
                })
                .enter().append("rect")
                .attr("class", "hvr-back-pulse")
                .attr("width", x1.bandwidth())
                .attr("x", function (d) {
                    return x1(d.column);
                })
                .attr("y", function (d) {
                    return y(d.yEnd);
                })
                .attr("height", function (d) {
                    return y(d.yBegin) - y(d.yEnd);
                })
                .style("fill", function (d) {
                    return z(d.name);
                })
                .on("mouseover", function () {
                    tooltip.style("display", null);
                })
                .on("mouseout", function () {
                    tooltip.style("display", "none");
                })
                .on("mousemove", function (d) {
                    let coordinates = d3.mouse(this);
                    let pt = svg.node().createSVGPoint();
                    pt.x = coordinates[0];
                    pt.y = coordinates[1];
                    pt = pt.matrixTransform(this.getCTM());
                    let xPosition = pt.x - 70;
                    let yPosition = pt.y - 50;
                    tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                    tooltip.select("text").text("{0}: {1}".format(d.name.capitalizeFirstLetter(), d.yEnd - d.yBegin));
                });

            // Text
            // g.selectAll("text")
            //     .data(data)
            //     .enter().append("text")
            //     .attr("font-size", 12)
            //     .text(function (d) {
            //         return d.request;
            //     })
            //     .attr("x", function(d){
            //         return x0(d.date_str) + x1.bandwidth()
            //     })
            //     .attr("y", function(d){
            //         let max = 0;
            //         for (let i = 0; i < d.columnDetails.length; i++){
            //             let detail = d.columnDetails[i]
            //             if (max < detail.yEnd){
            //                 max = detail.yEnd
            //             }
            //         }
            //         return y(max);
            //     });


            g.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x0).ticks(d3.utcDay).tickFormat(d3.utcFormat("%d")));


            g.append("g")
                .attr("class", "axis")
                .call(d3.axisLeft(y).ticks(null, "s"))
                .append("text")
                .attr("x", -30)
                .attr("transform", "rotate(-90)")
                .attr("y", y(y.ticks().pop()) + 10)
                .attr("dy", "0.32em")
                .attr("fill", "#000")
                .attr("font-weight", "bold")
                .attr("text-anchor", "start")
                .text("Amount");

            // Line chart

            g.append("path")
                .datum(data)
                .attr("transform", "translate(" + x1.bandwidth() * 2.7 + ", 0)")
                .attr("fill", "none")
                .attr("stroke", "steelblue")
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round")
                .attr("stroke-width", 1.5)
                .attr("d", line);


            // Line scatter plot

            g.selectAll("dot")
                .data(data)
                .enter().append("circle")
                .attr("transform", "translate(" + x1.bandwidth() * 2.7 + ", 0)")
                .attr("stroke", "steelblue")
                .attr("fill", "white")
                .attr("r", 3)
                .attr("cx", function (d) {
                    return x0(d.date_obj);
                })
                .attr("cy", function (d) {
                    return y(d.start);
                })
                // Tooltip stuff after this
                .on("mouseover", function (d) {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                    div.transition()
                        .duration(200)
                        .style("opacity", .9);
                    div.text(d.start)
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                })
                .on("mouseout", function () {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });


            let legend = g.append("g")
                .attr("font-size", 10)
                .attr("text-anchor", "end")
                .selectAll("g")
                .data(columnHeaders.slice().reverse())
                .enter().append("g")
                .attr("transform", function (d, i) {
                    return "translate(0," + i * 20 + ")";
                });

            legend.append("rect")
                .attr("x", width - legendWidth)
                .attr("width", legendWidth)
                .attr("height", legendWidth)
                .attr("fill", z);

            legend.append("text")
                .attr("x", width - 24)
                .attr("y", 9.5)
                .attr("dy", "0.32em")
                .text(function (d) {
                    return d.capitalizeFirstLetter();
                });

            // Prep the tooltip bits, initial display is hidden
            let tooltip = g.append("g")
                .attr("class", "chart-tooltip")
                .style("display", "none");

            tooltip.append("rect")
                .attr("width", 60)
                .attr("height", 20)
                .attr("fill", "white")
                .style("opacity", 0.8);

            tooltip.append("text")
                .attr("x", 30)
                .attr("dy", "1.2em")
                .style("text-anchor", "middle")
                .attr("font-size", "12px")
                .attr("font-weight", "bold");
        }
    }

    render() {
        let {stats} = this.props;
        let shouldHide = stats == undefined || this.props.stats.length == 0;
        return (
            <Row className={ shouldHide ? 'hiddenblock' : '' }>
                <Col md="12" id="barChartParent">
                    <h5>RADIUS Packet Requests and Responses</h5>
                    <svg id="packets" width="1000" height="300"></svg>
                </Col>
            </Row>
        )
    }
}
