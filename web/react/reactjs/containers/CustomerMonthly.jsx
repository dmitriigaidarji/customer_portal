import React from "react"
import {
    Container, Row, Col
} from 'reactstrap';
import {
    Route,
    Switch
} from 'react-router-dom'
import moment from 'moment'
import CustomerAccessHeaderMonthly from '../customeraccess/components/HeaderMonthly'
import HeaderMonthly from '../components/customers/HeaderMonthly'
import Totals from '../components/customers/Totals'
import UsageSummary from '../components/customers/UsageSummary'
import Transactions from '../components/customers/Transactions'
import ConnectionsChart from '../components/customers/ConnectionsChart'
import ConsumptionChart from '../components/customers/ConsumptionChart'
import Donut from '../components/Donut'
import UserMonthly from './UserMonthly'
import CustomerDaily from './CustomerDaily'
import {connect} from 'react-redux'
import {selectStartEndDate, selectCustomer} from '../actions/selects'
import {fetchCustomerMonthlyIfNeeded} from '../actions/customerMontly'
import {fetchHotspotsIfNeeded} from '../actions/hotspots'
import UsageMap from '../components/user/UsageMap'
import UserDevices from '../components/customers/UserDevices'
import MonthlyChartsContainer from '../components/customers/MonthlyChartsContainer'
let style = {
    piecontainer: {
        paddingTop: '20px'
    }
};
class CustomerMonthly extends React.Component {
    constructor() {
        super()
        this.state = {
            current_stats: undefined,
            total_stats: undefined,
            transactions: undefined
        };
        this.parseStats = this.parseStats.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        let prev_start = prevProps.startDate, prev_end = prevProps.endDate,
            prev_stats = prevProps.source_stats, prev_customer = prevProps.customer;
        let {startDate, endDate, source_stats, customer, dispatch} = this.props;
        console.log('monthly updated')
        if (
            source_stats != undefined && startDate != undefined && (
            this.state.current_stats == undefined || !prev_start.isSame(startDate) || !prev_end.isSame(endDate) ||
            prev_stats == undefined || prev_customer.id != customer.id)
        ) {
            console.log(startDate, endDate)
            this.parseStats()
        }
    }

    componentDidMount() {
        let {props} = this;
        let {dispatch, customers, match, access_level} = props;
        let customer_id;
        if (access_level === 'CUSTOMER')
            customer_id = parseInt(CONST_CUSTOMER_ID);
        else customer_id = parseInt(match.params.customer_id);
        let customer = undefined;
        customers.map((lcustomer) => {
            if (lcustomer.id == customer_id)
                customer = lcustomer;
        });
        dispatch(selectCustomer(
            customer
        ));

        let month = parseInt(match.params.month);
        if (month < 9) month = '0' + month;
        else month = '' + month;

        let startDate = moment.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').startOf('month');
        dispatch(selectStartEndDate(
            startDate,
            moment.utc("{0}{1}".format(props.match.params.year, month), 'YYYYMM').endOf('month')
        ));

        dispatch(fetchCustomerMonthlyIfNeeded(startDate, customer));
    }

    parseStats() {
        console.log('parsing monthly');
        let documents = this.props.source_stats;
        let {startDate, endDate, providers, customer, dispatch} = this.props;
        let tprecord = {
            'user_count': 0,
            'connections': {'count': 0, 'data': {}},
            'activations': {'count': 0, 'data': {}},
            'duration': {
                'data': {}
            },
            'usage': {
                'data': {}
            },
            'devices': {
                'count': 0,
                'data': {
                    'ios': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': {}
                    },
                    'android': {
                        'count': 0,
                        'os': {},
                        'device': {},
                        'release': []
                    }
                }
            },
            'totals': {
                'tbytes': 0,
                'tseconds': 0,
                'tdata': 0,
                'avgdata': 0,
                'ttime': 0,
                'avgtime': 0
            }
        };


        let stats = [];
        let alltx = [];
        let geos = [];
        let totaldata = 0;
        let totalsecs = 0;
        let activationcount = 0;
        let connectioncount = 0;
        if (documents.length > 0) {
            for (let i = 0; i < documents.length; i++) {
                let record = documents[i];
                let found = false;
                record['activations_current'] = [];
                record['user_tx_current'] = [];
                record['devices_current'] = [];
                for (let j = 0; j < record['activations'].length; j++) {
                    let aitem = record['activations'][j];
                    aitem.date_obj = moment.utc(aitem.date.$date);
                    if (startDate.isSameOrBefore(aitem.date_obj) && aitem.date_obj.isSameOrBefore(endDate)) {
                        ///
                        let actday = aitem.date_obj.format('YYYYMMDD');
                        if (tprecord['activations']['data'][actday] == undefined)
                            tprecord['activations']['data'][actday] = 0
                        tprecord['activations']['data'][actday] += 1

                        //device
                        for (let k = 0; k < aitem['sqi'].length; k++) {
                            let sqi = aitem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }


                        //
                        found = true;
                        record['activations_current'].push(aitem);
                        activationcount += 1
                        let geoposition = [0, 0]
                        let device = ''
                        let txid = aitem['auth_id']
                        let nai = record['nai']
                        let rtype = 'Activation'
                        let location = '100002'
                        let country = 'US'
                        let seconds = '-'
                        let bytes = '-'
                        if (aitem['sqi'].length > 0) {
                            for (let k = 0; k < aitem['sqi'].length; k++) {
                                let sqi = aitem['sqi'][k];
                                device = sqi['device_id'];
                                if (sqi['lat'] != 0) {
                                    geoposition = [sqi['lat'], sqi['lng']]
                                    geos.push({'lat': sqi['lat'], 'lng': sqi['lng']})
                                }
                            }
                        }
                        alltx.push([aitem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])
                    }
                }
                let userbytes = 0;
                let userseconds = 0;
                for (let j = 0; j < record['user_tx'].length; j++) {
                    let citem = record['user_tx'][j];
                    citem.date_obj = moment.utc(citem.tx_date.$date);
                    if (startDate.isSameOrBefore(citem.date_obj) && citem.date_obj.isSameOrBefore(endDate)) {
                        for (let k = 0; k < citem['sqi'].length; k++) {
                            let sqi = citem['sqi'][k]
                            let dev_id = sqi.device_id;
                            let dev_found = false;
                            for (let l = 0; l < record['devices_current'].length; l++) {
                                if (record['devices_current'][l]['device_id'] == dev_id)
                                    dev_found = true;
                            }
                            if (!dev_found) {
                                for (let l = 0; l < record['devices'].length; l++) {
                                    if (record['devices'][l]['device_id'] == dev_id) {
                                        record['devices_current'].push(record['devices'][l]);
                                        break;
                                    }
                                }
                            }
                        }
                        ///
                        found = true;
                        record['user_tx_current'].push(citem)
                        connectioncount += 1
                        let geoposition = [0, 0]
                        if (citem['usage']['secs'] > 0) {
                            ///
                            let txday = citem.date_obj.format('YYYYMMDD');
                            if (tprecord['connections']['data'][txday] == undefined)
                                tprecord['connections']['data'][txday] = 0;
                            tprecord['connections']['data'][txday] += 1;
                            let txid = citem['tx_id']
                            let nai = record['nai']
                            let rtype = 'Connection'
                            let location = citem['provider_id']
                            let country = undefined;
                            providers.some((prov) => {
                                if (prov['id'] == citem['provider_id']) {
                                    country = prov['iso_code'];
                                    return true;
                                }
                            })
                            let device = citem['calling_station']
                            if (citem['sqi'].length > 0)
                                for (let k = 0; k < citem['sqi'].length; k++) {
                                    let sqi = citem['sqi'][k];
                                    if (sqi['lat'] != 0) {
                                        geoposition = [sqi['lat'], sqi['lng']]
                                        geos.push({'lat': sqi['lat'], 'lng': sqi['lng']})
                                    }
                                }
                            let seconds = citem['usage']['secs']
                            let bytes = 0
                            bytes += citem['usage']['bytes_in']
                            bytes += citem['usage']['bytes_out']
                            bytes += citem['usage']['giga_in'] * 2147483648
                            bytes += citem['usage']['giga_out'] * 2147483648
                            userbytes += bytes;
                            totaldata += bytes;
                            userseconds += seconds;
                            totalsecs += seconds;
                            if (seconds >= 10 && bytes > 0)
                                alltx.push([citem.date_obj, txid, nai, rtype, location, country, geoposition, device, seconds, bytes])

                            //Duration
                            if (tprecord['duration']['data'][txday] == undefined)
                                tprecord['duration']['data'][txday] = 0;
                            tprecord['duration']['data'][txday] += seconds;

                            //usage
                            if (tprecord['usage']['data'][txday] == undefined)
                                tprecord['usage']['data'][txday] = 0;
                            tprecord['usage']['data'][txday] += bytes;
                        }
                    }
                }
                record['usage_current'] = {total_bytes: userbytes, total_seconds: userseconds};
                if (found) stats.push(record)
            }
        }

        tprecord['connections']['count'] = connectioncount;
        tprecord['activations']['count'] = activationcount;
        tprecord['totals']['tbytes'] = totaldata;
        tprecord['totals']['tseconds'] = totalsecs

        for (let i = 0; i < stats.length; i++) {
            let item = stats[i];
            for (let j = 0; j < item['devices_current'].length; j++) {
                let dev = item['devices_current'][j];
                tprecord['devices']['count'] += 1
                if (dev['platform']['ios_version'] != undefined) {
                    tprecord['devices']['data']['ios']['count'] += 1
                    if (!tprecord['devices']['data']['ios']['os'].hasOwnProperty(dev['platform']['ios_version']))
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['os'][dev['platform']['ios_version']] += 1;
                    if (!tprecord['devices']['data']['ios']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['ios']['release'][dev['version']] = 1;
                    else
                        tprecord['devices']['data']['ios']['release'][dev['version']] += 1;
                    if (!tprecord['devices']['data']['ios']['device'].hasOwnProperty(dev['platform']['device_model']))
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] = 1;
                    else
                        tprecord['devices']['data']['ios']['device'][dev['platform']['device_model']] += 1;
                }
                else if (dev['platform']['device_model'] == 'Android') {
                    tprecord['devices']['data']['android']['count'] += 1
                    if (!tprecord['devices']['data']['android']['os'].hasOwnProperty(dev['platform']['android_version']))
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] = 1
                    else
                        tprecord['devices']['data']['android']['os'][dev['platform']['android_version']] += 1

                    if (!tprecord['devices']['data']['android']['release'].hasOwnProperty(dev['version']))
                        tprecord['devices']['data']['android']['release'][dev['version']] = 1
                    else
                        tprecord['devices']['data']['android']['release'][dev['version']] += 1

                    if (!tprecord['devices']['data']['android']['device'].hasOwnProperty(dev['platform']['phone']))
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] = 1;
                    else
                        tprecord['devices']['data']['android']['device'][dev['platform']['phone']] += 1;
                }
            }
        }
        tprecord['user_count'] = stats.length;
        tprecord['totals']['tdata'] = present_data(tprecord['totals']['tbytes']);
        tprecord['totals']['ttime'] = present_clock(tprecord['totals']['tseconds']);
        if (tprecord['user_count'] > 0) {
            tprecord['totals']['avgdata'] = present_data(tprecord['totals']['tbytes'] / tprecord['user_count'])
            tprecord['totals']['avgtime'] = present_clock(tprecord['totals']['tseconds'] / tprecord['user_count'])
        }
        console.log("UPDATING STATS")
        this.setState({
            current_stats: stats,
            total_stats: tprecord,
            transactions: alltx
        });
        if (geos.length > 0) {
            if (this.getHotspotsCustomerMonthly != undefined)
                window.clearTimeout(this.getHotspotsCustomerMonthly);
            this.getHotspotsCustomerMonthly = window.setTimeout(() =>
                dispatch(fetchHotspotsIfNeeded(startDate, customer, geos)), 1000)
        }
    }

    componentWillUnmount() {
        window.clearTimeout(this.getHotspotsCustomerMonthly);
    }

    render() {
        let {props} = this;
        let {total_stats, current_stats, transactions} = this.state;
        let {customer, startDate, endDate, source_stats, access_level, hotspots} = props;
        if (customer == undefined) {
            return <div>Loading Customer Monthly...</div>
        }
        let iosreleases = [],
            androidreleases = [],
            iosversions = [],
            androidversions = [];
        if (total_stats != undefined) {
            let data = total_stats.devices.data.ios.release
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    iosreleases.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.android.release
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    androidreleases.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.ios.os
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    iosversions.push({name: key, count: data[key]})
                }
            data = total_stats.devices.data.android.os
            for (let key in data)
                if (data.hasOwnProperty(key)) {
                    androidversions.push({name: key, count: data[key]})
                }
        }
        let mapNode;
        if (transactions != undefined && hotspots != undefined && startDate!=undefined)
            mapNode = <UsageMap stats={transactions} hotspots={hotspots} date={startDate} access_level={access_level}/>
        return (
            <Switch>
                <Route path={`${props.match.url}${props.match.url.slice(-1) == '/' ? ':day' : '/:day'}`}
                       render={(componentProps) =>
                           <div>
                               <CustomerDaily
                                   {...componentProps}
                                   customer={customer}
                                   customers={props.customers}
                                   handleCustomer={this.handleCustomer}
                                   startDate={startDate}
                                   endDate={endDate}
                                   source_stats={source_stats}
                                   providers={props.providers}
                               />
                           </div>
                       }/>
                <Route render={(componentProps) =>
                    <div>
                        {access_level === 'CUSTOMER' ?
                            <CustomerAccessHeaderMonthly {...props}/>
                            :
                            <HeaderMonthly
                                {...props}
                            />
                        }
                        <Container fluid={true} className="main_container" style={{marginTop: '210px'}}>
                            <Totals stats={total_stats} />
                            <MonthlyChartsContainer  stats={total_stats} startDate={startDate}
                                                     current_stats={current_stats}
                                              match={props.match} history={props.history}/>
                            {mapNode}
                            <UserDevices stats={total_stats}
                                    iosreleases={iosreleases}
                                    iosversions={iosversions}
                                    androidreleases={androidreleases}
                                    androidversions={androidversions}
                            />
                            <UsageSummary
                                stats={current_stats}
                                month={startDate.format('MMMM')}
                                date={startDate}
                                customer_id={customer.id}
                            />
                            <Transactions
                                stats={transactions}
                                month={startDate.format('MMMM')}
                                date={startDate}
                                customer_id={customer.id}
                                showCSV={true}
                            />
                        </Container>
                    </div>
                }/>
            </Switch>
        )
    }
}

const mapStateToProps = (state) => {
    if (state.selects.customer != undefined) {
        let source_stats = state.customerMonthly[
        '' + state.selects.customer.id + '-' + moment.utc(state.selects.start_date).format('YYYYMM')
            ];
        source_stats = source_stats == undefined ? undefined : source_stats.items;
        let hotspots_stats = state.hotspots[
        '' + state.selects.customer.id + '-' + moment.utc(state.selects.start_date).format('YYYYMM')
            ];
        let hotspots = hotspots_stats == undefined ? undefined : hotspots_stats.hotspots;
        return ({
            providers: state.providers.items,
            customers: state.customers.items,
            source_stats: source_stats,
            hotspots: hotspots,
            startDate: moment.utc(state.selects.start_date),
            endDate: moment.utc(state.selects.end_date),
            customer: state.selects.customer,
            access_level: state.selects.access_level
        });
    } else return {
        providers: state.providers.items,
        customers: state.customers.items,
        access_level: state.selects.access_level
    }
};

export default connect(
    mapStateToProps
)(CustomerMonthly)

