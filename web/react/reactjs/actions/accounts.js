/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_ACCOUNTS_START = 'FETCH_ACCOUNTS_START';
export const FETCH_ACCOUNTS_SUCCESS = 'FETCH_ACCOUNTS_SUCCESS';
export const FETCH_ACCOUNTS_FAIL = 'FETCH_ACCOUNTS_FAIL';
////
export const CREATE_ACCOUNTS_START = 'CREATE_ACCOUNTS_START';
export const CREATE_ACCOUNTS_SUCCESS = 'CREATE_ACCOUNTS_SUCCESS';
export const CREATE_ACCOUNTS_FAIL = 'CREATE_ACCOUNTS_FAIL';

export const DELETE_ACCOUNTS_START = 'DELETE_ACCOUNTS_START'
export const DELETE_ACCOUNTS_SUCCESS = 'DELETE_ACCOUNTS_SUCCESS'
export const DELETE_ACCOUNTS_FAIL = 'DELETE_ACCOUNTS_FAIL'

export const RESET_PASS_ACCOUNTS_START = 'RESET_PASS_ACCOUNTS_START'
export const RESET_PASS_ACCOUNTS_SUCCESS = 'RESET_PASS_ACCOUNTS_SUCCESS'
export const RESET_PASS_ACCOUNTS_FAIL = 'RESET_PASS_ACCOUNTS_FAIL'

export const CREATE_DOMAINS_START = 'CREATE_DOMAINS_START';
export const CREATE_DOMAINS_SUCCESS = 'CREATE_DOMAINS_SUCCESS';
export const CREATE_DOMAINS_FAIL = 'CREATE_DOMAINS_FAIL';

export const ORDER_CODES_START = 'ORDER_CODES_START';
export const ORDER_CODES_SUCCESS = 'ORDER_CODES_SUCCESS';
export const ORDER_CODES_FAIL = 'ORDER_CODES_FAIL';

export const BATCH_CODES_START = 'BATCH_CODES_START';
export const BATCH_CODES_SUCCESS = 'BATCH_CODES_SUCCESS';
export const BATCH_CODES_FAIL = 'BATCH_CODES_FAIL';

export const CREATE_ACCOUNTS_ALERT_CLEAR = 'CREATE_ACCOUNTS_ALERT_CLEAR';

export const getUsers = () => dispatch => {
    dispatch({type: FETCH_ACCOUNTS_START});
    Api.get(process.env.BASE_API_URL + 'users/').then(res => {
        if (res.status == 200)
            dispatch({
                type: FETCH_ACCOUNTS_SUCCESS,
                users: res.data.users,
                domains: res.data.domains,
                requested_domains: res.data.requested_domains,
                groups: res.data.groups,
                max_users: res.data.max_users,
                max_codes: res.data.max_codes,
                orders: res.data.orders,
                batch_orders: res.data.batch_orders,
                receivedAt: Date.now()
            });
        else dispatch({type: FETCH_ACCOUNTS_FAIL})
    }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_ACCOUNTS_FAIL})
    });
};

export const orderCode = (entitlement, email) => dispatch => {
    dispatch({type: ORDER_CODES_START});
    Api.post(process.env.BASE_API_URL + 'codes/', {
        email: email,
        entitlement: entitlement
    }).then(res => {
        if (res.status === 200)
            dispatch({
                type: ORDER_CODES_SUCCESS,
                orders: res.data.orders,
                receivedAt: Date.now()
            });
        else dispatch({type: ORDER_CODES_FAIL, message: res.data.message})
    }).catch(function (error) {
        console.log(Object.assign({}, error));
        dispatch({type: ORDER_CODES_FAIL, message: error.response.data.message})
    });
};

export const batchCodes = (entitlement, amount) => dispatch => {
    dispatch({type: BATCH_CODES_START});
    Api.post(process.env.BASE_API_URL + 'codes/batch/', {
        entitlement: entitlement,
        amount: amount
    }).then(res => {
        if (res.status === 200)
            dispatch({
                type: BATCH_CODES_SUCCESS,
                batch_orders: res.data.batch_orders,
                receivedAt: Date.now()
            });
        else dispatch({type: BATCH_CODES_FAIL, message: res.data.message})
    })
};

export const createDomain = (name) => dispatch => {
    dispatch({type: CREATE_DOMAINS_START});
    Api.post(process.env.BASE_API_URL + 'domains/', {
        name: name
    }).then(res => {
        if (res.status == 200)
            dispatch({
                type: CREATE_DOMAINS_SUCCESS,
                requested_domains: res.data.requested_domains,
                receivedAt: Date.now()
            });
        else dispatch({type: CREATE_DOMAINS_FAIL, message: res.data.message})
    }).catch(function (error) {
        console.log(Object.assign({}, error));
        dispatch({type: CREATE_DOMAINS_FAIL, message: error.response.data.message})
    });
};
export const createUser = (email, username, domain, role) => dispatch => {
    dispatch({type: CREATE_ACCOUNTS_START});
    Api.post(process.env.BASE_API_URL + 'users/', {
        email: email,
        username: username,
        domain: domain,
        role: role
    }).then(res => {
        if (res.status == 200)
            dispatch({
                type: CREATE_ACCOUNTS_SUCCESS,
                users: res.data.users,
                receivedAt: Date.now()
            });
        else dispatch({type: CREATE_ACCOUNTS_FAIL, message: res.data.message})
    }).catch(function (error) {
        console.log(Object.assign({}, error));
        dispatch({type: CREATE_ACCOUNTS_FAIL, message: error.response.data.message})
    });
};

export const deleteUser = (username) => dispatch => {
    dispatch({type: DELETE_ACCOUNTS_START});
    Api.post(process.env.BASE_API_URL + 'users_delete/' + username + '/').then(res => {
        if (res.status == 200)
            dispatch({
                type: DELETE_ACCOUNTS_SUCCESS,
                users: res.data.users,
                receivedAt: Date.now()
            });
        else dispatch({type: DELETE_ACCOUNTS_FAIL, message: res.data.message})
    }).catch(function (error) {
        console.log(Object.assign({}, error));
        dispatch({type: DELETE_ACCOUNTS_FAIL, message: error.response.data.message})
    });
};

export const resetPasswordUser = (username) => dispatch => {
    dispatch({type: RESET_PASS_ACCOUNTS_START});
    Api.post(process.env.BASE_API_URL + 'users_resetpassword/' + username + '/').then(res => {
        if (res.status == 200)
            dispatch({
                type: RESET_PASS_ACCOUNTS_SUCCESS,
                receivedAt: Date.now()
            });
        else dispatch({type: RESET_PASS_ACCOUNTS_FAIL, message: res.data.message})
    }).catch(function (error) {
        console.log(Object.assign({}, error));
        dispatch({type: RESET_PASS_ACCOUNTS_FAIL, message: error.response.data.message})
    });
};

export const clearCreateUserAlert = () => dispatch => {
    dispatch({type: CREATE_ACCOUNTS_ALERT_CLEAR})
}