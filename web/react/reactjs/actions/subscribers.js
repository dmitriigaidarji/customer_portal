/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_SUBSCRIBERS_START = 'FETCH_SUBSCRIBERS_START';
export const FETCH_SUBSCRIBERS_SUCCESS = 'FETCH_SUBSCRIBERS_SUCCESS';
export const FETCH_SUBSCRIBERS_FAIL = 'FETCH_SUBSCRIBERS_FAIL';
////
export const EDIT_SUBSCRIBERS_START = 'EDIT_SUBSCRIBERS_START';
export const EDIT_SUBSCRIBERS_SUCCESS = 'EDIT_SUBSCRIBERS_SUCCESS';
export const EDIT_SUBSCRIBERS_FAIL = 'EDIT_SUBSCRIBERS_FAIL';

export const getUsers = () => dispatch => {
    dispatch({type: FETCH_SUBSCRIBERS_START});
    Api.get(process.env.BASE_API_URL + 'testers/').then(res => {
        if (res.status == 200)
            dispatch({
                type: FETCH_SUBSCRIBERS_SUCCESS,
                testers: res.data.testers,
                subscribers: res.data.subscribers,
                receivedAt: Date.now()
            });
        else dispatch({type: FETCH_SUBSCRIBERS_FAIL})
    }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_SUBSCRIBERS_FAIL})
    });
};


export const editUser = (email, field, value) => dispatch => {
    dispatch({type: EDIT_SUBSCRIBERS_START});
    Api.post(process.env.BASE_API_URL + 'testers/', {
        email: email,
        field: field,
        value: value
    }).then(res => {
        if (res.status == 200)
            dispatch({
                type: EDIT_SUBSCRIBERS_SUCCESS,
                email: email,
                field: field,
                value: value
            });
        else dispatch({type: EDIT_SUBSCRIBERS_FAIL})
    }).catch(function (error) {
        console.log(error);
        dispatch({type: EDIT_SUBSCRIBERS_FAIL})
    });
};
