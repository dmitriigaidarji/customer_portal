/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_HOTSPOTS_START = 'FETCH_HOTSPOTS_START';
export const FETCH_HOTSPOTS_SUCCESS = 'FETCH_HOTSPOTS_SUCCESS';
export const FETCH_HOTSPOTS_FAIL = 'FETCH_HOTSPOTS_FAIL';
const delta = 1000 * 3600 * 8;
export const getHotspots = (date, customer, coordinates) => dispatch => {
    let date_str = date.format('YYYYMM');
    dispatch({type: FETCH_HOTSPOTS_START, index: "" + customer.id + "-" + date_str});
    Api.post(process.env.BASE_API_URL + 'poi/', {coordinates:coordinates})
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_HOTSPOTS_SUCCESS,
                    hotspots: res.data.hotspots,
                    receivedAt: Date.now(),
                    index: "" + customer.id + "-" + date_str
                });
            else dispatch({type: FETCH_HOTSPOTS_FAIL, index: "" + customer.id + "-" + date_str})
        })
};

function shouldFetch(state, date, customer) {
    const data = state.hotspots['' + customer.id + '-' + date.format('YYYYMM')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchHotspotsIfNeeded(date, customer, coordinates) {
    return (dispatch, getState) => {
        let state = getState();
        customer = customer == undefined ? state.selects.customer : customer;
        if (shouldFetch(state, date, customer)) {
            return dispatch(getHotspots(date, customer, coordinates))
        }
    }
}
