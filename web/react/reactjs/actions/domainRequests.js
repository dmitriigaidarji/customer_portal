/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_DOMAINS_START = 'FETCH_DOMAINS_START';
export const FETCH_DOMAINS_SUCCESS = 'FETCH_DOMAINS_SUCCESS';
export const FETCH_DOMAINS_FAIL = 'FETCH_DOMAINS_FAIL';

export const EDIT_DOMAINS_START = 'EDIT_DOMAINS_START';
export const EDIT_DOMAINS_SUCCESS = 'EDIT_DOMAINS_SUCCESS';
export const EDIT_DOMAINS_FAIL = 'EDIT_DOMAINS_FAIL';
export const getDomains = () => dispatch => {
    dispatch({type: FETCH_DOMAINS_START});
    Api.get(process.env.BASE_API_URL + 'domains/').then(res => {
        if (res.status == 200 && res.data !== null && typeof res.data === 'object')
            dispatch({type: FETCH_DOMAINS_SUCCESS, payload: res.data.requested_domains, receivedAt: Date.now()})
        else dispatch({type: FETCH_DOMAINS_FAIL})
    })
};


export const editDomainRequest = (request_id, value) => dispatch => {
    dispatch({type: EDIT_DOMAINS_START});
    Api.post(process.env.BASE_API_URL + 'domains/edit/',{
        request_id: request_id,
        value: value
    }).then(res => {
        if (res.status == 200 && res.data !== null && typeof res.data === 'object')
            dispatch({type: EDIT_DOMAINS_SUCCESS, payload: res.data.requested_domains, receivedAt: Date.now()})
        else dispatch({type: EDIT_DOMAINS_FAIL})
    })
};