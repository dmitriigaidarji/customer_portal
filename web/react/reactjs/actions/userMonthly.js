/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_USER_MONTHLY_START = 'FETCH_USER_MONTHLY_START';
export const FETCH_USER_MONTHLY_SUCCESS = 'FETCH_USER_MONTHLY_SUCCESS';
export const FETCH_USER_MONTHLY_FAIL = 'FETCH_USER_MONTHLY_FAIL';
const delta = 1000 * 3600 * 8;
export const getUserMonthly = (date, username) => dispatch => {
    let date_str = date.format('YYYYMM');
    dispatch({type: FETCH_USER_MONTHLY_START, index: "" + username + "-" + date_str});
    Api.get(process.env.BASE_API_URL + 'users/{0}/{1}/'.format(username, date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_USER_MONTHLY_SUCCESS,
                    payload: res.data.results,
                    receivedAt: Date.now(),
                    index: "" + username + "-" + date_str
                });
            else dispatch({type: FETCH_USER_MONTHLY_FAIL, index: "" + username + "-" + date_str})
        })
    //     .catch(function (error) {
    //     console.log(error);
    //     dispatch({type: FETCH_USER_MONTHLY_FAIL, index: "" + username + "-" + date_str})
    // });
};

function shouldFetch(state, date, username) {
    const data = state.userMonthly['' + username + '-' + date.format('YYYYMM')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchUserMonthlyIfNeeded(date, user) {
    return (dispatch, getState) => {
        let state = getState();
        user = user == undefined ? state.selects.user.nai : user;
        if (shouldFetch(state, date, user)) {
            return dispatch(getUserMonthly(date, user))
        }
    }
}
