/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_PROVIDER_MONTHLY_START = 'FETCH_PROVIDER_MONTHLY_START';
export const FETCH_PROVIDER_MONTHLY_SUCCESS = 'FETCH_PROVIDER_MONTHLY_SUCCESS';
export const FETCH_PROVIDER_MONTHLY_FAIL = 'FETCH_PROVIDER_MONTHLY_FAIL';
const delta = 1000 * 3600 * 8;
export const getProviderMonthly = (date, provider) => dispatch => {
    let date_str = date.format('YYYYMM');
    dispatch({type: FETCH_PROVIDER_MONTHLY_START, index: "" + provider.id + "-" + date_str});
    Api.get(process.env.BASE_API_URL + 'stats/{0}/{1}/'.format(provider.id, date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_PROVIDER_MONTHLY_SUCCESS,
                    payload: res.data.results,
                    receivedAt: Date.now(),
                    index: "" + provider.id + "-" + date_str
                });
            else dispatch({type: FETCH_PROVIDER_MONTHLY_FAIL, index: "" + provider.id + "-" + date_str})
        }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_PROVIDER_MONTHLY_FAIL, index: "" + provider.id + "-" + date_str})
    });
};

function shouldFetch(state, date, provider) {
    const data = state.providerMonthly['' + provider.id + '-' + date.format('YYYYMM')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchProviderMonthlyIfNeeded(date, provider) {
    return (dispatch, getState) => {
        let state = getState();
        provider = provider == undefined ? state.selects.provider : provider;
        if (shouldFetch(state, date, provider)) {
            return dispatch(getProviderMonthly(date, provider))
        }
    }
}
