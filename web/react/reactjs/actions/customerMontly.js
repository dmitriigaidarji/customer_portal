/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_CUSTOMER_MONTHLY_START = 'FETCH_CUSTOMER_MONTHLY_START';
export const FETCH_CUSTOMER_MONTHLY_SUCCESS = 'FETCH_CUSTOMER_MONTHLY_SUCCESS';
export const FETCH_CUSTOMER_MONTHLY_FAIL = 'FETCH_CUSTOMER_MONTHLY_FAIL';
const delta = 1000 * 3600 * 8;
export const getCustomerMonthly = (date, customer) => dispatch => {
    let date_str = date.format('YYYYMM');
    dispatch({type: FETCH_CUSTOMER_MONTHLY_START, index: "" + customer.id + "-" + date_str});
    Api.get(process.env.BASE_API_URL + 'customer/{0}/{1}/'.format(customer.id, date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_CUSTOMER_MONTHLY_SUCCESS,
                    payload: res.data.results,
                    receivedAt: Date.now(),
                    index: "" + customer.id + "-" + date_str
                });
            else dispatch({type: FETCH_CUSTOMER_MONTHLY_FAIL, index: "" + customer.id + "-" + date_str})
        })
    // .catch(function (error) {
    // console.log(error);
    // dispatch({type: FETCH_CUSTOMER_MONTHLY_FAIL, index: "" + customer.id + "-" + date_str})
    // });
};

function shouldFetch(state, date, customer) {
    const data = state.customerMonthly['' + customer.id + '-' + date.format('YYYYMM')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchCustomerMonthlyIfNeeded(date, customer) {
    return (dispatch, getState) => {
        let state = getState();
        if (customer == undefined)
            customer = state.selects.customer;
        if (customer != undefined && shouldFetch(state, date, customer)) {
            return dispatch(getCustomerMonthly(date, customer))
        }
    }
}
