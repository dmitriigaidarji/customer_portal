/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_SQI_DAILY_START = 'FETCH_SQI_DAILY_START';
export const FETCH_SQI_DAILY_SUCCESS = 'FETCH_SQI_DAILY_SUCCESS';
export const FETCH_SQI_DAILY_FAIL = 'FETCH_SQI_DAILY_FAIL';
const delta = 1000 * 3600 * 8;
export const getSqiDaily = (date) => dispatch => {
    let date_str = date.format('YYYYMMDD');
    dispatch({type: FETCH_SQI_DAILY_START, index: date_str});
    Api.get(process.env.BASE_API_URL + 'sqi/{0}/'.format(date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_SQI_DAILY_SUCCESS,
                    payload: res.data.results,
                    hotspots: res.data.hotspots,
                    receivedAt: Date.now(),
                    index: date_str
                });
            else dispatch({type: FETCH_SQI_DAILY_FAIL, index: date_str})
        }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_SQI_DAILY_FAIL, index: date_str})
    });
};

function shouldFetch(state, date) {
    const data = state.sqiDaily[date.format('YYYYMMDD')];
    if (!data)
        return true;
    else if (data.isFetching)
        return false;
    else if (data.items == undefined || data.items.length == 0)
        return true;
    else
        return data.lastUpdated == undefined ? true : Date.now() - data.lastUpdated > delta;

}

export function fetchSqiDailyIfNeeded(date) {
    return (dispatch, getState) => {
        if (shouldFetch(getState(), date)) {
            return dispatch(getSqiDaily(date))
        }
    }
}
