/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_REFERRALS_START = 'FETCH_REFERRALS_START';
export const FETCH_REFERRALS_SUCCESS = 'FETCH_REFERRALS_SUCCESS';
export const FETCH_REFERRALS_FAIL = 'FETCH_REFERRALS_FAIL';
////
export const ADD_REFERRALS_START = 'ADD_REFERRALS_START';
export const ADD_REFERRALS_SUCCESS = 'ADD_REFERRALS_SUCCESS';
export const ADD_REFERRALS_FAIL = 'ADD_REFERRALS_FAIL';

export const SUBMIT_REFERRALS_START = 'SUBMIT_REFERRALS_START';
export const SUBMIT_REFERRALS_SUCCESS = 'SUBMIT_REFERRALS_SUCCESS';
export const SUBMIT_REFERRALS_FAIL = 'SUBMIT_REFERRALS_FAIL';

export const getReferrals = () => dispatch => {
    dispatch({type: FETCH_REFERRALS_START});
    Api.get(process.env.BASE_API_URL + 'referrals/').then(res => {
        if (res.status == 200)
            dispatch({
                type: FETCH_REFERRALS_SUCCESS,
                referrals: res.data.results,
                receivedAt: Date.now()
            });
        else dispatch({type: FETCH_REFERRALS_FAIL})
    })
};


export const addReferral = (email, name) => dispatch => {
    dispatch({type: ADD_REFERRALS_START});
    Api.post(process.env.BASE_API_URL + 'referrals/', {
        email: email,
        name: name
    }).then(res => {
        if (res.status == 200)
            dispatch({
                type: ADD_REFERRALS_SUCCESS,
                referrals: res.data.results,
                receivedAt: Date.now()
            });
        else dispatch({type: ADD_REFERRALS_FAIL})
    })
};

export const submitToReview = (referral_id) => dispatch => {
    dispatch({type: SUBMIT_REFERRALS_START});
    Api.post(process.env.BASE_API_URL + 'referrals/review/', {
        id: referral_id
    }).then(res => {
        if (res.status == 200)
            dispatch({
                type: SUBMIT_REFERRALS_SUCCESS,
                referrals: res.data.results,
                receivedAt: Date.now()
            });
        else dispatch({type: SUBMIT_REFERRALS_FAIL})
    })
};