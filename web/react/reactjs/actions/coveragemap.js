/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_COVERAGEMAP_START = 'FETCH_COVERAGEMAP_START';
export const FETCH_COVERAGEMAP_SUCCESS = 'FETCH_COVERAGEMAP_SUCCESS';
export const FETCH_COVERAGEMAP_FAIL = 'FETCH_COVERAGEMAP_FAIL';
const delta = 1000 * 60 * 24;
export const getCoverageMap = () => dispatch => {
    dispatch({type: FETCH_COVERAGEMAP_START});
    Api.get(process.env.BASE_API_URL + 'poi/all/').then(res => {
        if (res.status == 200 && res.data.results != undefined)
            dispatch({type: FETCH_COVERAGEMAP_SUCCESS, payload: res.data.results, receivedAt: Date.now()})
        else dispatch({type: FETCH_COVERAGEMAP_FAIL})
    })
};

function shouldFetchCoverageMap(state) {
    const {hotspots} = state.coveragemap;
    if (hotspots == undefined || hotspots.length == 0) {
        return true
    } else if (hotspots.isFetching) {
        return false
    } else {
        return Date.now() - hotspots.lastUpdated > delta
    }
}

export function fetchCoverageMapIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchCoverageMap(getState())) {
            return dispatch(getCoverageMap())
        }
    }
}
