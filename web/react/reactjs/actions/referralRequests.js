/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_REFERRAL_REQUESTS_START = 'FETCH_REFERRAL_REQUESTS_START';
export const FETCH_REFERRAL_REQUESTS_SUCCESS = 'FETCH_REFERRAL_REQUESTS_SUCCESS';
export const FETCH_REFERRAL_REQUESTS_FAIL = 'FETCH_REFERRAL_REQUESTS_FAIL';

export const EDIT_REFERRAL_REQUESTS_START = 'EDIT_REFERRAL_REQUESTS_START';
export const EDIT_REFERRAL_REQUESTS_SUCCESS = 'EDIT_REFERRAL_REQUESTS_SUCCESS';
export const EDIT_REFERRAL_REQUESTS_FAIL = 'EDIT_REFERRAL_REQUESTS_FAIL';
export const getReferralRequests = () => dispatch => {
    dispatch({type: FETCH_REFERRAL_REQUESTS_START});
    Api.get(process.env.BASE_API_URL + 'referral-requests/').then(res => {
        if (res.status == 200 && res.data !== null && typeof res.data === 'object')
            dispatch({type: FETCH_REFERRAL_REQUESTS_SUCCESS, payload: res.data.results, receivedAt: Date.now()})
        else dispatch({type: FETCH_REFERRAL_REQUESTS_FAIL})
    })
};


export const editReferralRequests = (referral_id, value) => dispatch => {
    dispatch({type: EDIT_REFERRAL_REQUESTS_START});
    Api.post(process.env.BASE_API_URL + 'referral-requests/',{
        referral_id: referral_id,
        value: value
    }).then(res => {
        if (res.status == 200 && res.data !== null && typeof res.data === 'object')
            dispatch({type: EDIT_REFERRAL_REQUESTS_SUCCESS, payload: res.data.results, receivedAt: Date.now()})
        else dispatch({type: EDIT_REFERRAL_REQUESTS_FAIL})
    })
};