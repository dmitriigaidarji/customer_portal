/**
 * Created by dmitriigaidarji on 5/19/17.
 */
export const SELECT_SQI_SORTED = 'SELECT_SQI_SORTED';
export const RESET_SQI_SORTED = 'RESET_SQI_SORTED';
export const SELECT_SQI_COORD = 'SELECT_SQI_COORD';

export const selectSortedData = (array) => dispatch => {
    dispatch({type: SELECT_SQI_SORTED, payload: array});
};

export const resetSortedData = () => dispatch => {
    dispatch({type: RESET_SQI_SORTED});
};


export const panToCoordinate = (coordinate) => dispatch => {
    dispatch({type: SELECT_SQI_COORD, coordinate: coordinate})
}