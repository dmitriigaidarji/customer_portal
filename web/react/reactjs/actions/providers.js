/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_PROVIDERS_START = 'FETCH_PROVIDERS_START';
export const FETCH_PROVIDERS_SUCCESS = 'FETCH_PROVIDERS_SUCCESS';
export const FETCH_PROVIDERS_FAIL = 'FETCH_PROVIDERS_FAIL';
const delta = 1000 * 3600 * 24;
export const getProviders = () => dispatch => {
    dispatch({type: FETCH_PROVIDERS_START});
    Api.get(process.env.BASE_API_URL + 'providers/').then(res => {
        if (res.status == 200 && res.data !== null && typeof res.data === 'object')
            dispatch({type: FETCH_PROVIDERS_SUCCESS, payload: res.data.results, receivedAt: Date.now()})
        else dispatch({type: FETCH_PROVIDERS_FAIL})
    })
    //     .catch(function (error) {
    //     console.log(Object.assign({}, error));
    //     dispatch({type: FETCH_PROVIDERS_FAIL})
    // });
};

function shouldFetchProviders(state) {
    const providers = state.providers;
    if (providers.items == undefined || providers.items.length == 0) {
        return true
    } else if (providers.isFetching) {
        return false
    } else {
        return providers.app_version != undefined ?
            providers.app_version != process.env.APP_VERSION ? true
            : Date.now() - providers.lastUpdated > delta
            : true
    }
}

export function fetchProvidersIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchProviders(getState())) {
            return dispatch(getProviders())
        }
    }
}
