/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_PROVIDER_DAILY_START = 'FETCH_PROVIDER_DAILY_START';
export const FETCH_PROVIDER_DAILY_SUCCESS = 'FETCH_PROVIDER_DAILY_SUCCESS';
export const FETCH_PROVIDER_DAILY_FAIL = 'FETCH_PROVIDER_DAILY_FAIL';
const delta = 1000 * 3600 * 8;
export const getProviderDaily = (date, provider) => dispatch => {
    let date_str = date.format('YYYYMMDD');
    dispatch({type: FETCH_PROVIDER_DAILY_START, index: "" + provider.id + "-" + date_str});
    Api.get(process.env.BASE_API_URL + 'daily/{0}/{1}/'.format(provider.id, date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_PROVIDER_DAILY_SUCCESS,
                    payload: res.data.results,
                    receivedAt: Date.now(),
                    index: "" + provider.id + "-" + date_str
                });
            else dispatch({type: FETCH_PROVIDER_DAILY_FAIL, index: "" + provider.id + "-" + date_str})
        }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_PROVIDER_DAILY_FAIL, index: "" + provider.id + "-" + date_str})
    });
};

function shouldFetch(state, date, provider) {
    const data = state.providerDaily['' + provider.id + '-' + date.format('YYYYMMDD')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchProviderDailyIfNeeded(date, provider) {
    return (dispatch, getState) => {
        let state = getState();
        provider = provider == undefined ? state.selects.provider : provider;
        if (shouldFetch(state, date, provider)) {
            return dispatch(getProviderDaily(date, provider))
        }
    }
}
