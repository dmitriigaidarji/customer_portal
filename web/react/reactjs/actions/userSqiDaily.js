/**
 * Created by dmitriigaidarji on 5/20/17.
 */
import Api from '../Api'
export const FETCH_USER_SQI_DAILY_START = 'FETCH_USER_SQI_DAILY_START';
export const FETCH_USER_SQI_DAILY_SUCCESS = 'FETCH_USER_SQI_DAILY_SUCCESS';
export const FETCH_USER_SQI_DAILY_FAIL = 'FETCH_USER_SQI_DAILY_FAIL';
const delta = 1000 * 3600 * 8;
export const getUserSqiDaily = (date, user) => dispatch => {
    let date_str = date.format('YYYYMMDD');
    dispatch({type: FETCH_USER_SQI_DAILY_START, index: "" + user.nai + "-" + date_str});
    Api.get(process.env.BASE_API_URL + 'sqi/{0}/{1}/'.format(user.hash_id, date_str))
        .then(res => {
            if (res.status == 200 && res.data !== null && typeof res.data === 'object')
                dispatch({
                    type: FETCH_USER_SQI_DAILY_SUCCESS,
                    sqi: res.data.results,
                    hotspots: res.data.hotspots,
                    receivedAt: Date.now(),
                    index: "" + user.nai + "-" + date_str
                });
            else dispatch({type: FETCH_USER_SQI_DAILY_FAIL, index: "" + user.nai + "-" + date_str})
        }).catch(function (error) {
        console.log(error);
        dispatch({type: FETCH_USER_SQI_DAILY_FAIL, index: "" + user.nai + "-" + date_str})
    });
};

function shouldFetch(state, date, user) {
    const data = state.userSqiDaily['' + user.nai + '-' + date.format('YYYYMMDD')];
    if (!data) {
        return true
    } else if (data.isFetching) {
        return false
    } else {
        return data.app_version != undefined ?
            data.app_version != process.env.APP_VERSION ? true
            : data.lastUpdated == undefined ? true : Date.now() - data.lastUpdated > delta
            : true
    }
}

export function fetchUserSqiDailyIfNeeded(date, user) {
    return (dispatch, getState) => {
        let state = getState();
        user = user == undefined ? state.selects.user : user;
        if (shouldFetch(state, date, user)) {
            return dispatch(getUserSqiDaily(date, user))
        }
    }
}
