/**
 * Created by dmitriigaidarji on 5/19/17.
 */
import Api from '../Api'
export const FETCH_CUSTOMERS_START = 'FETCH_CUSTOMERS_START';
export const FETCH_CUSTOMERS_SUCCESS = 'FETCH_CUSTOMERS_SUCCESS';
export const FETCH_CUSTOMERS_FAIL = 'FETCH_CUSTOMERS_FAIL';
const delta = 1000 * 3600 * 24;
export const getCustomers = () => dispatch => {
    dispatch({type: FETCH_CUSTOMERS_START});
    Api.get(process.env.BASE_API_URL + 'customers/').then(res => {
        if (res.status === 200 && res.data !== null && typeof res.data === 'object') {
            dispatch({type: FETCH_CUSTOMERS_SUCCESS, payload: res.data.results, receivedAt: Date.now()})
        }
        else dispatch({type: FETCH_CUSTOMERS_FAIL})
    })
    //     .catch(function (error) {
    //     console.log(Object.assign({}, error));
    //     dispatch({type: FETCH_CUSTOMERS_FAIL})
    // });
};

function shouldFetchCustomers(state) {
    const customers = state.customers;
    if (customers.items == undefined || customers.items.length == 0) {
        return true
    } else if (customers.isFetching) {
        return false
    } else {
        return customers.app_version != undefined ?
            customers.app_version != process.env.APP_VERSION ? true
            : Date.now() - customers.lastUpdated > delta
            : true
    }
}

export function fetchCustomersIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchCustomers(getState())) {
            return dispatch(getCustomers())
        }
    }
}
