import React from "react"
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import UserMonthly from "../../containers/UserMonthly"
import {connect} from 'react-redux'
import {fetchProvidersIfNeeded} from '../../actions/providers'
import {fetchCustomersIfNeeded} from '../../actions/customers'
import WebsiteVersion from '../../../reactjs/components/WebsiteVersion'

class MainController extends React.Component {
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(fetchProvidersIfNeeded());
        dispatch(fetchCustomersIfNeeded());
    }

    render() {
        if (this.props.customers == undefined && this.props.providers == undefined)
            return <div>Loading...</div>
        return (
            <div>
                <Switch>
                    <Route path={`/${process.env.PUBLIC_URL}/:year/:month`}
                           render={(componentProps) => <UserMonthly {...componentProps} user_hash={CONST_USERNAME}/>}
                    />
                    <Route component={CustomersNoMatch}/>
                </Switch>
                <WebsiteVersion />
            </div>
        )
    }
}
;

class CustomersNoMatch extends React.Component {
    render() {
        let date = new Date();
        let month = date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        let url = `/${process.env.PUBLIC_URL}/${date.getFullYear()}/${month}/`;
        return (
            <Redirect to={url}/>
        )
    }
}

const mapStateToProps = (state) => ({
    providers: state.providers.items,
    customers: state.customers.items
});

export default connect(
    mapStateToProps
)(MainController)

