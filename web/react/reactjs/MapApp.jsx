import React from "react"
import {render} from "react-dom"
import {
    createStore,
    compose,
    applyMiddleware,
    combineReducers,
} from "redux"
import {Provider} from "react-redux"
import thunk from "redux-thunk"
import {persistStore, autoRehydrate} from 'redux-persist'
import localForage from 'localforage'
import {composeWithDevTools} from 'redux-devtools-extension'
import MapContainer from "./mapapp/containers/MapContainer"
import * as reducers from "./reducers"

let reducer = combineReducers(reducers);
const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk)),
    autoRehydrate()
);
class MapApp extends React.Component {
    constructor() {
        super();
        this.state = {rehydrated: false}
    }

    componentWillMount() {
        persistStore(store, {
            storage: localForage,
            blacklist: ['sqiDaily', 'userSqiDaily', 'coveragemap', 'sqiSorted', 'selects', 'hotspots', 'domainRequests']
        }, () => {
            this.setState({rehydrated: true})
        })
    }

    render() {
        if (!this.state.rehydrated) {
            return <div>Loading...</div>
        }
        return (
            <Provider store={store}>
                <MapContainer/>
            </Provider>
        )
    }
}
render(<MapApp/>, document.getElementById('App1'));