import React from "react"
import {render} from "react-dom"
import {StyleRoot} from 'radium';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch
} from 'react-router-dom'
import MainController from "./containers/MainController"
import AccountsController from './containers/AccountsContainer'
import SubscribersController from './containers/SubscribersContainer'
import MainHeader from './components/MainHeader'
import CoverageMapContainer from './containers/CoverageMapContainer'
import {
    createStore,
    compose,
    applyMiddleware,
    combineReducers,
} from "redux"
import {Provider} from "react-redux"
import thunk from "redux-thunk"
import {persistStore, autoRehydrate} from 'redux-persist'
import localForage from 'localforage'
import {composeWithDevTools} from 'redux-devtools-extension'
import * as reducers from "./reducers"
import './static/css/console.css';
import './static/css/accounts.css'

// import 'bootstrap/dist/css/bootstrap.css';

// let finalCreateStore = compose(
//     applyMiddleware(thunk),
//     autoRehydrate(),
//     window.devToolsExtension ? window.devToolsExtension() : f => f
// )(createStore)
let reducer = combineReducers(reducers);
// let store = finalCreateStore(reducer)
// const store = compose(autoRehydrate())(createStore)(reducer)
const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk)),
    autoRehydrate()
);

class ConsoleApp extends React.Component {
    constructor() {
        super();
        this.state = {rehydrated: false}
    }

    componentWillMount() {
        persistStore(store, {
            storage: localForage,
            blacklist: ['sqiDaily', 'userSqiDaily', 'coveragemap', 'referralRequests',
                'sqiSorted', 'selects', 'hotspots', 'domainRequests', 'referrals']
        }, () => {
            this.setState({rehydrated: true})
        })
    }

    render() {
        if (!this.state.rehydrated) {
            return <div>Loading...</div>
        }
        return (
            <Provider store={store}>
                <StyleRoot>
                    <Router>
                        <div>
                            <MainHeader level="ADMIN"/>
                            <Switch>
                                <Route path={`/${process.env.PUBLIC_URL}/accounts`} name="accounts"
                                       component={AccountsController}/>
                                <Route path={`/${process.env.PUBLIC_URL}/subscribers`} name="subscribers"
                                       component={SubscribersController}/>
                                <Route path={`/${process.env.PUBLIC_URL}/map`} name="map"
                                       component={CoverageMapContainer}/>
                                <Route path='/' name="main" component={MainController}/>
                            </Switch>
                        </div>
                    </Router>
                </StyleRoot>
            </Provider>
        )
    }
}
render(<ConsoleApp/>, document.getElementById('App1'))
