import * as apiActions from "../actions/referrals"

const initialState = {
};


export default function users(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_REFERRALS_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_REFERRALS_SUCCESS:
            return {
                ...state,
                items: action.referrals,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
        case apiActions.ADD_REFERRALS_START:
            return state;
        case apiActions.ADD_REFERRALS_SUCCESS:
            return {
                ...state,
                items: action.referrals,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
        case apiActions.SUBMIT_REFERRALS_SUCCESS:
            return {
                ...state,
                items: action.referrals,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
        default:
            return state
    }
}