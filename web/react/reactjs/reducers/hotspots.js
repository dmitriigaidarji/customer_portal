import * as apiActions from "../actions/hotspots"

const initialState = {};

export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_HOTSPOTS_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_HOTSPOTS_SUCCESS:
            return {
                ...state,
                [action.index]: {
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    hotspots: action.hotspots,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_HOTSPOTS_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}