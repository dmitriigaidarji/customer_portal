import * as apiActions from "../actions/sqiSorted"

const initialState = {
    indexes: [],
    selectedCoordinate: undefined
};


export default function selects(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.SELECT_SQI_SORTED:
            return {...state, indexes: action.payload};
        case apiActions.RESET_SQI_SORTED:
            return {...state, indexes: []};
        case apiActions.SELECT_SQI_COORD:
            return {...state, selectedCoordinate: action.coordinate};
        default:
            return state
    }
}