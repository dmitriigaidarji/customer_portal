import * as apiActions from "../actions/subscribers"

const initialState = {
    testers: [],
    subscribers: []
};


export default function users(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_SUBSCRIBERS_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_SUBSCRIBERS_SUCCESS:
            return {
                ...state,
                testers: action.testers,
                subscribers: action.subscribers,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
        case apiActions.EDIT_SUBSCRIBERS_SUCCESS:
            let testers = state.testers.slice(0);
            for (let i = 0; i < testers.length; i ++){
                if (testers[i].email == action.email){
                    testers[i][action.field] = action.value;
                    break;
                }
            }
            return {...state, testers: testers};
        default:
            return state
    }
}