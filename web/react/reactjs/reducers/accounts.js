import * as apiActions from "../actions/accounts"

const initialState = {
    isFetching: false,
    isAddingUser: false,
    items: [],
    requested_domains: [],
    domains: [],
    max_users: 0,
    max_codes: 0,
    transactions : [],
    batch_transactions : []
};


export default function users(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_ACCOUNTS_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_ACCOUNTS_SUCCESS:
            let orders = action.orders;
            let transactions = [];
            if (orders != undefined) {
                transactions = []
                orders.map((item) => {
                    transactions = transactions.concat(item.transactions)
                })
            }
            let batch_orders = action.batch_orders;
            let batch_transactions = [];
            if (batch_orders != undefined) {
                batch_transactions = []
                batch_orders.map((item) => {
                    batch_transactions = batch_transactions.concat(item.transactions)
                })
            }
            return {
                ...state,
                items: action.users,
                domains: action.domains,
                requested_domains: action.requested_domains,
                groups: action.groups,
                max_codes: action.max_codes != undefined ? action.max_codes : 0,
                max_users: action.max_users != undefined ? action.max_users : 100000,
                orders: action.orders,
                transactions: transactions,
                batch_transactions: batch_transactions,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
            break;
        case apiActions.BATCH_CODES_SUCCESS:
            batch_orders = action.batch_orders;
            batch_transactions = [];
            if (batch_orders != undefined) {
                batch_transactions = []
                batch_orders.map((item) => {
                    batch_transactions = batch_transactions.concat(item.transactions)
                })
            }
            return {...state, batch_transactions: batch_transactions}
        case apiActions.CREATE_DOMAINS_START:
            return {...state, isFetching: true};
        case apiActions.CREATE_DOMAINS_SUCCESS:
            return {
                ...state,
                requested_domains: action.requested_domains,
                isFetching: false,
                lastUpdated: action.receivedAt
            };
        case apiActions.CREATE_DOMAINS_FAIL:
            return {...state, isFetching: false};
        case apiActions.CREATE_ACCOUNTS_START:
            return {...state, isAddingUser: true};
        case apiActions.CREATE_ACCOUNTS_SUCCESS:
            return {
                ...state,
                items: action.users,
                isAddingUser: false,
                createUserMessage: '',
                lastUpdated: action.receivedAt
            };
        case apiActions.CREATE_ACCOUNTS_FAIL:
            return {
                ...state,
                createUserMessage: action.message
            };
        case apiActions.CREATE_ACCOUNTS_ALERT_CLEAR:
            return {
                ...state,
                createUserMessage: undefined
            };
        case apiActions.DELETE_ACCOUNTS_SUCCESS:
            return {
                ...state,
                items: action.users,
                lastUpdated: action.receivedAt
            }
        case apiActions.RESET_PASS_ACCOUNTS_SUCCESS:
            return state;
        case apiActions.ORDER_CODES_START:
            return {...state, isOrderingCode: true};
        case apiActions.ORDER_CODES_SUCCESS:
            return {
                ...state,
                orders: action.orders,
                isOrderingCode: false,
                createUserMessage: '',
                lastUpdated: action.receivedAt
            };
        case apiActions.ORDER_CODES_FAIL:
            return {
                ...state,
                createUserMessage: action.message
            };
        default:
            return state
    }
}