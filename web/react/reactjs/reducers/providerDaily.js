import * as apiActions from "../actions/providerDaily"

const initialState = {};

export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_PROVIDER_DAILY_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_PROVIDER_DAILY_SUCCESS:
            return {
                ...state,
                [action.index]: {
                    items: action.payload,
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_PROVIDER_DAILY_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}