import * as apiActions from "../actions/userMonthly"

const initialState = {};

export default function userMonthly(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_USER_MONTHLY_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_USER_MONTHLY_SUCCESS:
            return {
                ...state,
                [action.index]: {
                    items: action.payload,
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_USER_MONTHLY_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}