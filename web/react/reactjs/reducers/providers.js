import * as apiActions from "../actions/providers"

const initialState = {
    isFetching: false,
    items: undefined
};


export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_PROVIDERS_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_PROVIDERS_SUCCESS:
            return {...state, items: action.payload, isFetching: false, lastUpdated: action.receivedAt,
            app_version: process.env.APP_VERSION};
        default:
            return state
    }
}