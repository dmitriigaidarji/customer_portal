import * as apiActions from "../actions/coveragemap"

const initialState = {
    isFetching: false
};


export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_COVERAGEMAP_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_COVERAGEMAP_SUCCESS:
            return {...state,
                hotspots: action.payload,
                isFetching: false,
                lastUpdated: action.receivedAt,
                app_version: process.env.APP_VERSION
            };
        default:
            return state
    }
}