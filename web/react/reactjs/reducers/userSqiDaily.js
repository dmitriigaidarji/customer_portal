import * as apiActions from "../actions/userSqiDaily"

const initialState = {};

export default function sqi(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_USER_SQI_DAILY_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_USER_SQI_DAILY_SUCCESS:
            console.log(action)
            return {
                ...state,
                [action.index]: {
                    items: action.sqi,
                    hotspots: action.hotspots,
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_USER_SQI_DAILY_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}