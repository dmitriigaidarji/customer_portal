import * as apiActions from "../actions/sqiDaily"

const initialState = {};

export default function sqi(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_SQI_DAILY_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_SQI_DAILY_SUCCESS:
            return {
                ...state,
                [action.index]: {
                    items: action.payload,
                    hotspots: action.hotspots,
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_SQI_DAILY_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}