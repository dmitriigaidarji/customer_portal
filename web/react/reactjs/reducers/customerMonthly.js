import * as apiActions from "../actions/customerMontly"

const initialState = {};

export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_CUSTOMER_MONTHLY_START:
            return {...state, [action.index]: {
                isFetching: true
            }};
        case apiActions.FETCH_CUSTOMER_MONTHLY_SUCCESS:
            return {
                ...state,
                [action.index]: {
                    items: action.payload,
                    isFetching: false,
                    lastUpdated: action.receivedAt,
                    app_version: process.env.APP_VERSION
                }
            };
        case apiActions.FETCH_CUSTOMER_MONTHLY_FAIL:
            return {...state, [action.index]: {isFetching: false}};
        default:
            return state
    }
}