import * as apiActions from "../actions/referralRequests"

const initialState = {
    isFetching: false,
    items: undefined
};


export default function providers(state = initialState, action = {}) {
    switch (action.type) {
        case apiActions.FETCH_REFERRAL_REQUESTS_START:
            return {...state, isFetching: true};
        case apiActions.FETCH_REFERRAL_REQUESTS_SUCCESS:
            return {
                ...state, items: action.payload, isFetching: false, lastUpdated: action.receivedAt,
                app_version: process.env.APP_VERSION
            };
        case apiActions.FETCH_REFERRAL_REQUESTS_FAIL:
            return {
                ...state, items: action.payload, isFetching: false, lastUpdated: action.receivedAt,
                app_version: process.env.APP_VERSION
            };
        case apiActions.EDIT_REFERRAL_REQUESTS_SUCCESS:
            return {...state, items: action.payload, isFetching: false, lastUpdated: action.receivedAt,
                app_version: process.env.APP_VERSION}
        default:
            return state
    }
}