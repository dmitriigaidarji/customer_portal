import React from "react"

export default class PieChart extends React.Component {
    constructor(props) {
        super(props);
        this.uuid = props.uuid ? props.uuid : guid();
    }
    componentDidUpdate() {
        console.log(this.uuid)
        if (window['qweqwe'])
            window['qweqwe'].destroy();
        let config = {
            size: {
                canvasHeight: 150,
                canvasWidth: 200
            },
            labels: {
                inner: {
                    format: "percentage"
                },
                outer: {
                    format: "label-value2",
                    pieDistance: 5
                }
            },
            data: {
                content: this.props.data
            },
            tooltips: {
                enabled: true,
                type: "caption"
            }
        };
        if (this.props.footer != undefined)
            config['footer'] = {
                text: this.props.footer,
                location: "bottom-center",
                font:  '-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;',
                color: 'black',
                fontSize: 12.8
            };
        if (this.props.title != undefined)
            config['header'] = {
                title: {
                    text: this.props.title
                }
            };
        window['qweqweqwe'] = new d3pie('123123', config);
    }

    componentDidUnmount() {
        if (this.pie)
            this.pie.destroy()
    }

    render() {
        return (
            <div style={{margin: "0 auto"}}>
                <div id='123123' className="chartBlock"></div>
            </div>
        )
    }
}

