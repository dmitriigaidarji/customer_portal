import React from "react"
import {connect} from 'react-redux'
import {orderCode, clearCreateUserAlert} from '../../actions/accounts'
import { Alert } from 'reactstrap';

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class CreateCode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            codeType: 7,
            userEmail: '',
            userEmailState: 'normal'
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.id;

        this.setState({
            [name]: value.toLowerCase()
        });
    }

    handleSelectChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(){
        let {userEmail, codeType} = this.state;
        let {dispatch} = this.props;
        let userEmailState = 'normal';
        if (!validateEmail(userEmail))
            userEmailState = 'warning';
        if (userEmailState === 'normal'){
            dispatch(orderCode(codeType, userEmail))
        }else {

        }
        this.setState({
            userEmailState: userEmailState
        })
    }

    render() {
        let {alert, dispatch, codetypes} = this.props;
        let {userEmail, codeType, userEmailState} = this.state;
        return (
            <div style={{padding: '10px 0'}}>
                <form>
                    <legend>Order a Code</legend>
                    {alert != undefined &&
                    <Alert color={alert === '' ? 'success' : "danger"} isOpen={true} toggle={() => {
                        dispatch(clearCreateUserAlert())
                    }}>
                        {alert === '' ?
                            <span>Code has been delivered!</span>
                            :
                            <span>{alert}</span>
                        }
                    </Alert>
                    }
                    <div className="form-group">
                        <label htmlFor="codeType" className="form-control-label">Code entitlement</label>
                        <select className="form-control" id="codeType" value={codeType}
                               onChange={this.handleSelectChange}>
                            {codetypes.map((item, index) => <option key={index} value={item}>{item} days</option>)}
                        </select>
                        <small id="codeTypeHelp" className="form-text text-muted">Duration this code will be active after activation (first usage)
                        </small>
                    </div>
                    <div className={userEmailState === 'normal' ? "form-group" : "form-group has-warning"}>
                        <label htmlFor="userEmail" className="form-control-label">Email address</label>
                        <input type="email" className={userEmailState === 'normal' ? "form-control" : "form-control form-control-warning"} id="userEmail" aria-describedby="emailHelp"
                               placeholder="Enter email"
                               value={userEmail}
                               onChange={this.handleInputChange}/>
                        {userEmailState !== 'normal' &&
                            <div className="form-control-feedback">Please, provide a valid email.</div>
                        }
                        <small id="emailHelp" className="form-text text-muted">The user will receive account details to
                            this email
                        </small>
                    </div>
                    <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    alert: state.accounts.createUserMessage,
    codetypes: [7,14,30]
});

export default connect(
    mapStateToProps
)(CreateCode)
