import React from "react"
import {connect} from 'react-redux'
import {createUser, clearCreateUserAlert} from '../../actions/accounts'
import { Alert } from 'reactstrap';

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        let domainSelect = '';
        if (props.domains != undefined)
            domainSelect = props.domains[0];
        this.state = {
            userEmail: '',
            username: '',
            domainSelect: domainSelect,
            roleSelect: 'End User',
            userEmailState: 'normal',
            usernameState: 'normal',
            domainSelectState: 'normal',
            roleSelectState: 'normal',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillUpdate(nextProps){
        if (nextProps.domains != undefined && this.props.domains == undefined)
            this.setState({domainSelect: nextProps.domains[0]})
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.id;

        this.setState({
            [name]: value.toLowerCase()
        });
    }

    handleSelectChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(){
        let {userEmail, username, domainSelect, roleSelect} = this.state;
        let {dispatch} = this.props;
        let userEmailState = 'normal',
            usernameState = 'normal',
            domainSelectState = 'normal',
            roleSelectState = 'normal';
        if (!validateEmail(userEmail))
            userEmailState = 'warning';
        if (!validateEmail(username + '@test.com'))
            usernameState = 'warning';
        if (userEmailState == 'normal' &&
            usernameState == 'normal'){
            dispatch(createUser(userEmail, username, domainSelect, roleSelect))
        }else {

        }
        this.setState({
            usernameState: usernameState,
            userEmailState: userEmailState,
            domainSelectState: domainSelectState,
            roleSelectState: roleSelectState
        })
    }

    render() {
        let {domains, groups, alert, dispatch, mode} = this.props;
        if (domains == undefined)
            return <div style={{padding: '10px 0'}}>Loading customer data...</div>
        let {userEmail, username, domainSelect, roleSelect,
            userEmailState, usernameState, domainSelectState, roleSelectState} = this.state;
        let roles = ['End User'];
        if (mode !== undefined) {
            roles = [];
            groups.map((item) => {
                if (item !== 'End User')
                    roles.push(item)
            })
        }
        return (
            <div>
                <form>
                    <legend>Create New User</legend>
                    {alert != undefined &&
                    <Alert color={alert == '' ? 'success' : "danger"} isOpen={true} toggle={() => {
                        dispatch(clearCreateUserAlert())
                    }}>
                        {alert == '' ?
                            <span>User has been created!</span>
                            :
                            <span>{alert}</span>
                        }
                    </Alert>
                    }
                    <div className={userEmailState == 'normal' ? "form-group" : "form-group has-warning"}>
                        <label htmlFor="userEmail" className="form-control-label">Email address</label>
                        <input type="email" className={userEmailState == 'normal' ? "form-control" : "form-control form-control-warning"} id="userEmail" aria-describedby="emailHelp"
                               placeholder="Enter email"
                               value={userEmail}
                               onChange={this.handleInputChange}/>
                        {userEmailState != 'normal' &&
                            <div className="form-control-feedback">Please, provide a valid email.</div>
                        }
                        <small id="emailHelp" className="form-text text-muted">The user will receive account details to
                            this email
                        </small>
                    </div>
                    <div className={usernameState == 'normal' ? "form-group" : "form-group has-warning"}>
                        <label htmlFor="username" className="form-control-label">Username</label>
                        <input type="text" className={usernameState == 'normal' ? "form-control" : "form-control form-control-warning"} id="username" aria-describedby="usernameHelp"
                               placeholder="Enter username"
                               value={username}
                               onChange={this.handleInputChange}
                        />
                        {usernameState != 'normal' &&
                            <div className="form-control-feedback">This username is not valid.</div>
                        }
                        <small id="usernameHelp" className="form-text text-muted">User's unique log-in credentials
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="domainSelect" className="form-control-label">Domain</label>
                        <select className="form-control" id="domainSelect" value={domainSelect}
                               onChange={this.handleSelectChange}>
                            {domains.map((item, index) => <option key={index} value={item}>{item}</option>)}
                        </select>
                    </div>
                    {mode != undefined &&
                    <div className="form-group">
                        <label htmlFor="roleSelect" className="form-control-label">User Role</label>
                        <select className="form-control" id="roleSelect" value={roleSelect}
                                onChange={this.handleSelectChange}>
                            {roles.map((item, index) => <option key={index} value={item}>{item}</option>)}
                        </select>
                    </div>
                    }
                    <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    domains: state.accounts.domains,
    groups: state.accounts.groups,
    alert: state.accounts.createUserMessage
});

export default connect(
    mapStateToProps
)(CreateUser)
