import React from "react"
import {connect} from 'react-redux'
import {createDomain} from '../../actions/accounts'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class Domains extends React.Component {
    render() {
        return (
            <div>
                <RequestDomain {...this.props}/>
                <DomainList {...this.props}/>
            </div>
        )
    }
}
class DomainList extends React.Component{
    render(){
        let {requested_domains, domains, users} = this.props;
        console.log('users')
        console.log(users)
        let data = [];
        requested_domains.map((item)=>data.push({
            name: item.name,
            status: item.status,
            users: 0
        }));
        domains.map((domain) => {
            let usersCount = 0;
            users.map((user) => {
                if (domain === user.username.split('@')[1])
                    usersCount ++;
            })
            data.push({
                name: domain,
                status: 'Active',
                users: usersCount
            })
        })
        let columns = [
            {
                Header: 'Name',
                accessor: 'name'
            },
            {
                id: 'status',
                Header: 'Status',
                accessor: 'status'
            },
            {
                Header: 'Users',
                accessor: 'users'
            }
        ]
        return(<div style={{marginTop:'10px'}}>
            <h5>Domain List</h5>
            <ReactTable
                    className='-striped -highlight'
                    data={data}
                    columns={columns}
                    showPagination={true}
                    defaultPageSize={5}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] != undefined ?
                            String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                            true
                    }}
                    defaultSorted={[{
                        id: 'status',
                        desc: false
                    }]}
                />
        </div>)
    }
}
class RequestDomain extends React.Component {
    constructor(){
        super();
        this.state = {
            domainName: '',
            domainState: 'normal'
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(){
        let {domainName} = this.state;
        let {dispatch} = this.props;
        if (domainName.replace(/\./g,'').length < 3 || !validateEmail('a@'+domainName+'.com'))
            this.setState({domainState:'warn'})
        else {
            this.setState({
                domainState: 'normal',
                domainName: ''
            }, () => {
                dispatch(createDomain(domainName))
            })
        }
    }
    render() {
        let {domainName, domainState} = this.state;
        return (
            <form>
                <div className={domainState == 'normal' ? "form-group" : "form-group has-warning"}>
                    <label htmlFor="reqDomain" className="form-control-label">
                        <legend>Order new domain</legend></label>
                        <input type="email" className={domainState == 'normal' ? "form-control" : "form-control form-control-warning"} id="reqDomain" aria-describedby="domainHelp"
                               placeholder="Enter desired domain"
                               value={domainName}
                               onChange={(event)=>{
                               this.setState({domainName: event.target.value})
                           }}/>
                        {domainState != 'normal' &&
                            <div className="form-control-feedback">Please, provide a valid domain name.</div>
                        }
                        <small id="domainHelp" className="form-text text-muted">Domain name can contain letters and dots. Must also be at least 3 letters long.
                        </small>
                    </div>
                <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
            </form>
        )
    }
}

const mapStateToProps = (state) => ({
    domains: state.accounts.domains,
    requested_domains: state.accounts.requested_domains,
    users: state.accounts.items
});

export default connect(
    mapStateToProps
)(Domains)
