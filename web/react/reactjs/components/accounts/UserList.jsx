import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {connect} from 'react-redux'
import {deleteUser, resetPasswordUser} from '../../actions/accounts'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css';
import {
    Link
} from 'react-router-dom'
let styles = {
    filters: {
        fontSize: '90%'
    }
}
class UserList extends React.Component {
    constructor() {
        super();

        this.state = {
            showFilters: false
        }
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    render() {
        let {users, dispatch} = this.props;
        if (users == undefined)
            return <div>Loading...</div>
        let data = [];
        users.map((item)=>{
           if (item.role === 'End User')
               data.push(item)
        });
        let columns = [
            {
                id: 'date',
                Header: 'Created',
                accessor: d => d.created,
                Cell: d => moment(d.value * 1000).format('lll'),
                width: 140
            },
            {
                Header: 'Username',
                accessor: 'username',
                Cell: (props) => <Link
                            to={`/${process.env.PUBLIC_URL}/users/${props.value}/${moment().format('YYYY')}/${moment().format('MM')}/`}>{props.value}</Link>
            },
            {
                Header: 'Email',
                accessor: 'email'
            },
            {
                Header: <div>
                    <div>Password</div>
                    <div>Set</div>
                </div>,
                accessor: 'password_set',
                Cell: d => {
                    if (d.value)
                        return <div style={{textAlign: 'center', color: 'green'}}><i className={`fa fa-check`}
                                                                                     aria-hidden="true"></i></div>
                    else return <div style={{textAlign: 'center', color: 'red'}}><i className={`fa fa-times`}
                                                                                    aria-hidden="true"></i></div>
                },
                width: 60
            },
            {
                Header: 'Activated',
                accessor: 'activated',
                Cell: d => {
                    if (d.value)
                        return <div style={{textAlign: 'center'}}>{moment(d.value * 1000).format('YYYY/MM/DD')}</div>
                    else return <div></div>
                },
                width: 70
            }
        ];

        let pageSize = 10;
        let showPagination = true;
        return (<div>
                <legend><span>All Users <a onClick={this.toggleTableFilters} style={styles.filters}
                                     href="javascript:void(0);"><i className={`fa fa-filter`}
                                                                   aria-hidden="true"></i></a></span>
                    <button type="button" onClick={()=>this.props.setMode(1)} className="btn btn-success" style={{float:'right'}}>
                        <i className={`fa fa-plus`} aria-hidden="true" style={{marginRight:'8px'}}></i>Add User</button>
                </legend>
                {data.length > 0 &&
                <ReactTable
                    className='-striped -highlight'
                    data={data}
                    showPagination={showPagination}
                    columns={columns}
                    defaultPageSize={pageSize}
                    filterable={this.state.showFilters}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] != undefined ?
                            String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                            true
                    }}
                    defaultSorted={[{
                        id: 'date',
                        desc: true
                    }]}
                    SubComponent={(row, a, b, c) => {
                        return (
                            <div style={{padding: '20px'}}>
                                <a href="#">
                                    <button onClick={() => {
                                        dispatch(resetPasswordUser(row.original.username))
                                    }} style={{width: '40%', fontSize: '115%', float: 'left'}} type="submit"
                                            className="btn btn-primary">
                                        Send
                                        email with password reset link
                                    </button>
                                </a>
                                <a href="#">
                                    <button onClick={() => {
                                        dispatch(deleteUser(row.original.username))
                                    }} style={{width: '40%', fontSize: '115%', float: 'right'}} type="submit"
                                            className="btn btn-danger">
                                        Delete user account
                                    </button>
                                </a>
                            </div>
                        )
                    }}
                    getTrProps={(state, rowInfo, column) => {
                        let colour;
                        if (rowInfo != undefined) {
                            let {activated} = rowInfo.row;
                            if (activated != undefined)
                                colour = '#eafff3';
                            else colour = '#fff5f5';
                            if (colour != undefined)
                                return {
                                    style: {
                                        background: colour
                                    }
                                };
                        }
                        return {};
                    }}
                />
                }</div>
        )
    }
}

const mapStateToProps = (state) => ({
    users: state.accounts.items
});

export default connect(
    mapStateToProps
)(UserList)
