import React from "react"
import {connect} from 'react-redux'
import moment from 'moment'
import Gauge from '../Gauge'
import Radium from 'radium'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
let styles = {
    navcontainer: {
        position: 'fixed',
        top: '110px',
        margin: '0 auto',
        maxWidth: '1080px',
        backgroundColor: 'white',
        zIndex: 9,
        left: 0,
        right: 0,
        borderBottom: '1px solid rgb(243, 243, 243)',
        WebkitBoxShadow: '0px 2px 5px 0px rgba(0,0,0,0.25)',
        MozBoxShadow: '0px 2px 5px 0px rgba(0,0,0,0.25)',
        boxShadow: '0px 2px 5px 0px rgba(0,0,0,0.25)',
    },
    more: {
        fontSize: '50%',
        textAlign: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        cursor: 'pointer',
        ':hover': {
            color: '#d20909'
        }
    },
    moreicon: {},
    nav: {
        display: 'flex',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        whiteSpace: 'nowrap'
    },
    group: {
        display: 'inline-block',
        position: 'relative',
        // border: 'solid 1px rgba(0,0,0,0.05)',
        // borderRadius: '4px',
        // WebkitBoxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        // MozBoxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        // boxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        padding: '0 0 5px 0'
    },
    biggroupmargin: {
        margin: '5px 0 8px 2px',
    },
    container: {
        fontSize: '100%',
        textAlign: 'center',
        whiteSpace: 'nowrap',
        display: 'inline-block',
        verticalAlign: 'top',
        margin: '0 10px',
        cursor: 'pointer',
        ':hover': {
            color: '#d20909'
        }
    },
    icon: {
        fontSize: '150%'
    },
    value: {
        fontSize: '200%',
        margin: '-5px 0'
    },
    title: {
        fontWeight: '300',
        fontSize: '80%'
    }
}
class Stats extends React.Component {
    render() {
        let {users, domains, max_users, transactions, max_codes} = this.props;
        if (users == undefined)
            return <div></div>
        return (
            <Row style={{
                display: 'flex',
                flexWrap: 'nowrap',
                justifyContent: 'space-between',
            }}>
                <div style={{textAlign: 'center', padding: '10px', display: 'inline-block'}}
                     className="group_container">
                    <Stat title="Domain" value={domains.length} icon="cloud"/>
                    <Stat title="User" value={users.length} icon="users"/>
                </div>
                <ActiveIndicator users={users} title="Active users"/>
                <ActiveIndicator users={users} title="Active codes"/>
                <EntitlementsIndicator users={users} max={max_users} title="User Entitlements"/>
                <EntitlementsIndicator users={transactions} max={max_codes} title="Code Entitlements"/>
            </Row>
        )
    }
}

class ActiveIndicator extends React.Component {
    render() {
        let {users, title} = this.props;
        let num_active = 0;
        users.map((user) => {
            if (user.activated != undefined && moment(user.activated * 1000).isBefore(moment().add(30, 'day').endOf('day')))
                num_active++
        })
        return (
            <div style={{display: 'inline-block', textAlign: 'center', padding: '10px'}} className="group_container">
                <Gauge value={users.length === 0 ? 0 : num_active * 100 / users.length} title={title}/>
                <div style={{margin: '10px 0 0 0'}}>
                    <div>
                    <span style={{
                        fontSize: '110%', fontWeight: 'bold', border: '1px solid black',
                        borderRadius: '3px',
                        padding: '3px 7px'
                    }}>
                        <i className='fa fa-users' aria-hidden="true" style={{marginRight: '10px'}}/>
                        {users.length === 0 ? 0 : `${num_active} / ${users.length}`}
                    </span>
                    </div>
                </div>
            </div>)
    }
}

class EntitlementsIndicator extends React.Component {
    render() {
        let {users, max, title} = this.props;
        return (
            <div style={{display: 'inline-block', textAlign: 'center', padding: '10px'}} className="group_container">
                <Gauge value={max === 0 ? 0 : users.length * 100 / max} title={title} reversed/>
                <div style={{margin: '10px 0 0'}}>
                    <div>
                    <span style={{
                        fontSize: '110%', fontWeight: 'bold', border: '1px solid black',
                        borderRadius: '3px',
                        padding: '3px 7px'
                    }}>
                        <i className='fa fa-user-plus' aria-hidden="true" style={{marginRight: '10px'}}/>
                        {max === 0 ? 0 : `${users.length} / ${max}`}</span>
                    </div>
                </div>
            </div>)
    }
}


@Radium
class Stat extends React.Component {
    render() {
        let {title, value, icon, showTiny, pericon} = this.props;
        if (showTiny)
            return (<div style={styles.container}>
                <div style={styles.value}>{value}</div>
                <div style={styles.title}>{title}</div>
            </div>)
        return (
            <div style={styles.container}>
                {icon &&
                <div style={styles.icon}><i className={`fa fa-${icon}`} aria-hidden="true"/>
                    {pericon &&
                    <span> / <i className={`fa fa-${pericon}`} aria-hidden="true"></i></span>
                    }</div>
                }
                <div style={styles.value}>{value}</div>
                <div style={styles.title}>{parseInt(value) == 1 ? title : title + 's'}</div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    users: state.accounts.items,
    domains: state.accounts.domains,
    max_users: state.accounts.max_users,
    max_codes: state.accounts.max_codes,
    transactions: state.accounts.transactions.concat(state.accounts.batch_transactions),
});
export default connect(mapStateToProps)(Stats)