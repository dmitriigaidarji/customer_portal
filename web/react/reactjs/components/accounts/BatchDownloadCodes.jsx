import React from "react"
import BatchCodesList from './BatchCodesList'
import {batchCodes} from '../../actions/accounts'
import {connect} from 'react-redux'

class BatchDownloadCodes extends React.Component {
    constructor(){
        super()
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(){
        let entitlement = this.entitlementSelect.value;
        let amount = this.amountInput.value;
        let {dispatch} = this.props;
        dispatch(batchCodes(entitlement, amount))
    }
    render() {
        let codetypes = [7, 14, 30];
        return (
            <div>
                <form>
                    <legend>Download multiple codes</legend>
                    <div className="form-group">
                        <label htmlFor="numCodes" className="form-control-label">Number of codes to download</label>
                        <input type="number" className="form-control" id="numCodes" aria-describedby="codesHelp"
                               placeholder="Enter the amount" ref={(input) => this.amountInput = input}/>
                        <small id="codesHelp" className="form-text text-muted">You will receive the codes in a
                            downloadable format to share them the way you want
                        </small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="codeType" className="form-control-label">Code entitlement</label>
                        <select className="form-control" id="codeType" ref={(select) => this.entitlementSelect = select}>
                            {codetypes.map((item, index) => <option key={index} value={item}>{item} days</option>)}
                        </select>
                        <small id="codeTypeHelp" className="form-text text-muted">Duration this code will be active
                            after activation (first usage)
                        </small>
                    </div>
                    <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </form>
                <BatchCodesList />
            </div>
        )
    }
}

export default connect()(BatchDownloadCodes)
