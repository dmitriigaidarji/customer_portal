import React from "react"
import ReactQuill, {Quill}  from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import Api from '../../Api'
const Parchment = Quill.import('parchment')

class IndentAttributor extends Parchment.Attributor.Style {
    add(node, value) {
        if (value === 0) {
            this.remove(node)
            return true
        } else {
            return super.add(node, `${value}em`)
        }
    }
}

let IndentStyle = new IndentAttributor('indent', 'text-indent', {
    scope: Parchment.Scope.BLOCK,
    whitelist: ['1em', '2em', '3em', '4em', '5em', '6em', '7em', '8em', '9em']
})
// Quill.register(Quill.import('attributors/class/color'), true);
Quill.register(Quill.import('attributors/style/size'), true);
Quill.register(Quill.import('attributors/style/font'), true);
Quill.register(Quill.import('attributors/style/align'), true);
Quill.register(Quill.import('attributors/style/direction'), true);
Quill.register(Quill.import('attributors/style/align'), true);
Quill.register(IndentStyle, true)

export default class EmailTemplate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {text: '<p>Dear RoamVU Beta Subscriber,</p><p>Thank you for your interest in the Point Dume Public Beta of our international Wi-Fi roaming app, RoamVU! You have been selected to participate in our Beta and given free Wi-Fi access on your Apple device while traveling to Hong Kong or Japan. If you have any questions, feel free to contact us using the in-app Feedback Icon or send us an email to <a href="mailto:support@roamvu.com" target="_blank">support@roamvu.com</a>. We’re happy to hear from you.</p><p>We recommend that you follow these instructions to install and Sign-In to our RoamVU Application on your Home Network in the week prior to leaving on your journey. Each crypto-secured Sign-In is good for 1 month and entitles your device to connect at any of our Hotpots during that time. If you get Signed-Out, that’s OK! You can Sign-In again at any time using your RoamVU Account and Password that you set below:</p><p><strong>Your RoamVU App Sign-In:</strong></p><p>{{username}}</p><p><strong>Use the link below in any Internet Browser to set your RoamVU Sign-In Password.</strong></p><p><strong>Remember it! You’ll need this password later:</strong></p><p>{{url}}</p>'}
        this.handleChange = this.handleChange.bind(this)
        this.handleTestEmail = this.handleTestEmail.bind(this)
    }

    handleChange(value) {
        console.log(value)
        this.setState({text: value})
    }

    handleTestEmail() {
        console.log(this.testEmailInput.value)
        Api.post(process.env.BASE_API_URL + 'email/test/', {
            email: this.testEmailInput.value,
            text: this.state.text
        }).then(res => {
            if (res.status == 200)
                alert('email was sent')
            else alert('error sending email')
        })
    }

    render() {
        let modules = {
            toolbar: [
                [{'header': [1, 2, false]}],
                ['bold', 'italic', 'underline', 'strike'],
                [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                ['link', 'image'],
                [{'color': []}, {'background': []}],          // dropdown with defaults from theme
                [{'font': []}],
                [{'align': []}],
                ['clean'],
            ],
        }


        let formats = [
            'header',
            'bold', 'italic', 'underline', 'strike',
            'list', 'bullet', 'indent',
            'link', 'image', 'color', 'background', 'font', 'align', 'clean'
        ];
        return (<div>
                <ReactQuill value={this.state.text}
                            onChange={this.handleChange}
                            modules={modules}
                            formats={formats}
                            theme="snow"
                />
                <div style={{padding: '20px 0'}}>
                    <input type="email" className="form-control"
                           style={{width: '250px', display: 'inline-block', margin: '0 20px 0 0'}}
                           placeholder="Enter your email" ref={(input) => {
                        this.testEmailInput = input;
                    }}/>
                    <button type="button" className="btn btn-primary" onClick={this.handleTestEmail}>Test Template
                    </button>
                </div>
            </div>
        )
    }
}