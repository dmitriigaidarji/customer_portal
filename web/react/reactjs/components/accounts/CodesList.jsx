import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {connect} from 'react-redux'
import {resendCode} from '../../actions/accounts'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css';
import {
    Link
} from 'react-router-dom'
let styles = {
    filters: {
        fontSize: '90%',
        marginLeft: '15px'
    }
}
class CodesList extends React.Component {
    constructor() {
        super();

        this.state = {
            showFilters: false
        }
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    render() {
        let {transactions, dispatch} = this.props;
        if (transactions == undefined)
            return <div>Loading...</div>
        let columns = [
            {
                id: 'date',
                Header: 'Ordered',
                accessor: d => moment(d.created * 1000).format('YYYY/MM/DD HH:MM'),
                // Cell: d => {
                //     let values = d.value.split(' ');
                //     return <div>
                //         <div>{values[0]}</div>
                //         <div>{values[1]}</div>
                //     </div>
                // },
                width: 150
            },
            {
                Header: 'Code',
                accessor: 'code',
                width: 100,
                Cell: (props) => <Link
                            to={`/${process.env.PUBLIC_URL}/users/${props.value}@codes.roamvu.net/${moment().format('YYYY')}/${moment().format('MM')}/`}>{props.value}</Link>
            },
            {
                Header: 'Email',
                accessor: 'email'
            },
            {
                Header: 'Entitlement',
                accessor: 'entitlement',
                Cell: d => <div style={{textAlign: 'center'}}>{d.value} days</div>,
                width: 100
            },
            {
                Header: 'Activated',
                accessor: 'activated',
                Cell: d => {
                    if (d.value)
                        return <div style={{textAlign: 'center'}}>{moment(d.value * 1000).format('YYYY/MM/DD')}</div>
                    else return <div></div>
                },
                width: 100
            }
        ];

        let pageSize = transactions.length > 10 ? 10 : 5;
        let showPagination = true;
        return (<div style={{padding: '10px 0'}}>
            <legend><span>Issued Activation Codes <a onClick={this.toggleTableFilters} style={styles.filters}
                                     href="javascript:void(0);"><i className={`fa fa-filter`}
                                                                   aria-hidden="true"></i></a></span>
                    <button type="button" onClick={()=>this.props.setMode(5)} className="btn btn-success" style={{float:'right'}}>
                        <i className={`fa fa-plus`} aria-hidden="true" style={{marginRight:'8px'}}></i>Order codes</button>
                </legend>
            <ReactTable
                className='-striped -highlight'
                data={transactions}
                showPagination={showPagination}
                columns={columns}
                defaultPageSize={pageSize}
                filterable={this.state.showFilters}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id
                    return row[id] !== undefined ?
                        String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                        true
                }}
                defaultSorted={[{
                    id: 'date',
                    desc: true
                }]}
                SubComponent={(row, a, b, c) => {
                    return (
                        <div style={{padding: '20px'}}>
                            <a href="#">
                                <button onClick={() => {
                                    dispatch(resendCode(row.original.code))
                                }} type="submit" className="btn btn-primary">
                                    Resend code
                                </button>
                            </a>
                        </div>
                    )
                }}
            /></div>)
    }
}

const mapStateToProps = (state) => ({
    transactions: state.accounts.transactions
});

export default connect(
    mapStateToProps
)(CodesList)
