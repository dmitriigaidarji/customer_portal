import React from "react"
import Api from '../../Api'
import {connect} from 'react-redux'

class BatchUpload extends React.Component {
    constructor() {
        super();
        this.submitTemplate = this.submitTemplate.bind(this)

    }

    submitTemplate() {
        let file = this.fileInput.files[0];
        if (file != undefined) {
            let data = new FormData();
            data.append('file', file);
            let config = {
                onUploadProgress: function (progressEvent) {
                    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                }
            };
            Api.post(process.env.BASE_API_URL+ 'users/upload/', data, config)
                .then(function (res) {
                    console.log(res)
                })
                .catch(function (err) {

                });
        }else alert('No file was attached.')
    }

    render() {
        let {domains} = this.props;
        if (domains == undefined || domains.length == 0)
            return <div></div>
        return (
            <div>
                <legend>Import your users to the service</legend>
                Download the template for mass upload using this <a href="/static/media/users_template.csv">link</a>
                <p>The password field is optional for every user. If the field is left empty the user will be required
                    to follow a link received in the email to set his desired password. Otherwise, the user will receive
                    both username and password ready to use in his email.
                </p>
                <h5 style={{marginTop: '20px'}}>Example of template filling:</h5>
                <div style={{maxWidth: '550px', margin: '0px auto 20px auto'}}>
                    <table className="table" style={{textAlign: 'center'}}>
                        <thead className="thead-inverse">
                        <tr>
                            <th style={{textAlign: 'center'}}>username</th>
                            <th style={{textAlign: 'center'}}>domain</th>
                            <th style={{textAlign: 'center'}}>email</th>
                            <th style={{textAlign: 'center'}}>password</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>fredrick</td>
                            <td>{domains[0]}</td>
                            <td>fred@gmail.com</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>test</td>
                            <td>{domains.length > 1 ? domains[1] : domains[0]}</td>
                            <td>test@mail.com</td>
                            <td>a7sy2</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <form method="post" encType="multipart/form-data">
                    <div className="form-group">
                        <label htmlFor="uploadFile">File input</label>
                        <input type="file" className="form-control-file" id="uploadFile"
                               aria-describedby="fileHelp" ref={(input) => this.fileInput = input}/>
                        <small id="fileHelp" className="form-text text-muted">Upload the downloaded template
                            populated
                            by you here.
                        </small>
                    </div>
                    <button type="button" className="btn btn-primary" onClick={this.submitTemplate}>Submit</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    domains: state.accounts.domains
});

export default connect(
    mapStateToProps
)(BatchUpload)