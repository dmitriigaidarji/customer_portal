import React from "react"
import Api from '../../Api';
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import moment from 'moment'
export default class UsageDuration extends React.Component {
    constructor(props) {
        super(props)
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;

    }

    updateChart(data) {
        if (data != undefined) {
            let initData = [];
            let {match, history} = this.props;
            for (let property in data.duration.data) {
                if (data.duration.data.hasOwnProperty(property)) {
                    let date = moment.utc("{0}-{1}-{2}".format(
                        property.substring(0, 4),
                        property.substring(4, 6),
                        property.substring(6, 8)
                    ));
                    initData.push({
                        date: date,
                        datestr: property,
                        date_str: date.format('DD'),
                        duration: data.duration.data[property],
                        usage: 0
                    })
                }
            }
            for (let property in data.usage.data) {
                if (data.usage.data.hasOwnProperty(property)) {
                    let found = false;
                    for (let i = 0; i < initData.length; i++) {
                        if (initData[i].datestr == property) {
                            initData[i].usage = data.usage.data[property];
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        let date = moment.utc("{0}-{1}-{2}".format(
                            property.substring(0, 4),
                            property.substring(4, 6),
                            property.substring(6, 8)
                        ));
                        initData.push({
                            date: date,
                            datestr: property,
                            date_str: date.format('DD'),
                            duration: 0,
                            usage: data.usage.data[property],
                        })
                    }
                }
            }
            data = initData;

            data.sort(function (a, b) {
                return a.date - b.date;
            });
            let svg = d3.select("#durations");
            svg.attr('width', $("#durationsChartParent").width());
            svg.selectAll("*").remove();
            if (data.length > 0) {
                let margin = {top: 20, right: 100, bottom: 30, left: 100},
                    width = +svg.attr("width") - margin.left - margin.right,
                    height = +svg.attr("height") - margin.top - margin.bottom,
                    legendWidth = 19,
                    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                let x = d3.scaleBand()
                    .rangeRound([0, width])
                    .paddingInner(0.05)
                    .align(0.1);

                let y = d3.scaleLinear()
                    .rangeRound([height, 0]);

                let z = d3.scaleOrdinal()
                    .range(["#359cd6", "#73A66B", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

                let keys = ['duration'];

                let div = d3.select('#durationstooltip');

                function getDaysInMonth(date) {
                    date = date.startOf('month');
                    let month = date.month();
                    let days = [];
                    while (date.month() === month) {
                        days.push(moment(date));
                        date.add(1, 'days')
                    }
                    return days;
                }


                let monthdates = getDaysInMonth(moment(data[data.length - 1].date));
                x.domain(monthdates);

                y.domain([0, d3.max(data, function (d) {
                    return d.duration;
                })]).nice();
                z.domain(keys);

                g.append("g")
                    .selectAll("g")
                    .data(d3.stack().keys(keys)(data))
                    .enter().append("g")
                    .attr("fill", function (d) {
                        return z(d.key);
                    })
                    .selectAll("rect")
                    .data(function (d) {
                        return d;
                    })
                    .enter().append("rect")
                    .attr("x", function (d) {
                        return x(d.data.date);
                    })
                    .attr("y", function (d) {
                        return y(d[1]);
                    })
                    .attr("height", function (d) {
                        return y(d[0]) - y(d[1]);
                    })
                    .attr("width", x.bandwidth())
                    .on("mousemove", function (d) {
                        let coordinates = d3.mouse(this);
                        let pt = svg.node().createSVGPoint();
                        pt.x = coordinates[0];
                        pt.y = coordinates[1];
                        pt = pt.matrixTransform(this.getCTM());
                        let xPosition = pt.x - 70;
                        let yPosition = pt.y - 50;
                        div.style("display", 'block');
                        let title = 'Duration', value = present_clock(d.data.duration);
                        div.html(
                            '<div style="text-align: start;"><span style="font-weight: bold;">Duration:</span> ' + value + '</br>' +
                            '<span style="font-weight: bold;">Usage: ' + '</span> ' + present_data(d.data.usage) + '</div>'
                        )
                            .style("left", (xPosition + 25) + "px")
                            .style("top", (yPosition + 25) + "px");
                    })
                    .on("mouseout", function (d) {
                        div.style("display", 'none');
                    })
                    .on("click", (d) => {
                        history.push(`${match.url}${match.url.slice(-1) == '/' ? d.data.date_str : '/' + d.data.date_str}/`)
                    });

                g.append("g")
                    .attr("class", "axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x).ticks(d3.utcDay).tickFormat(d3.utcFormat("%d")));

                g.append("g")
                    .attr("class", "axis")
                    .call(d3.axisLeft(y).ticks().tickFormat((val) =>
                        present_clock(val).substring(0, 5)
                    ))
                    .append("text")
                    .attr("x", 2)
                    .attr("y", y(y.ticks().pop()) + 0.5)
                    .attr("dy", "0.32em")
                    .attr("fill", "#000")
                    .attr("font-weight", "bold")
                    .attr("text-anchor", "start")
                    .text("Hours");

                let legend = g.append("g")
                    .attr("font-family", "sans-serif")
                    .attr("font-size", 10)
                    .attr("text-anchor", "end")
                    .selectAll("g")
                    .data(keys.slice().reverse())
                    .enter().append("g")
                    .attr("transform", function (d, i) {
                        return "translate(0," + i * 20 + ")";
                    });

                legend.append("rect")
                    .attr("x", width - 19)
                    .attr("width", 19)
                    .attr("height", 19)
                    .attr("fill", z);

                legend.append("text")
                    .attr("x", width - 24)
                    .attr("y", 9.5)
                    .attr("dy", "0.32em")
                    .text(function (d) {
                        return d.capitalizeFirstLetter();
                    });
            }

        }
    }

    render() {
        let {stats} = this.props;
        if (stats == undefined || stats.user_count == 0 || stats.connections.count == 0)
            return (<div></div>)
        return (
            <Row>
                <Col md="12" id="durationsChartParent">
                    <h5>Usage Duration</h5>
                    <svg id="durations" width="1000" height="300"></svg>
                    <div id='durationstooltip' className="tooltip"></div>
                </Col>
            </Row>
        )
    }
}
