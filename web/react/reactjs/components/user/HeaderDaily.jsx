import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../../Api'
import $ from 'jquery'
import {connect} from 'react-redux'
import {selectCurrentDay, selectStartEndDate} from '../../actions/selects'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'
import {fetchUserMonthlyIfNeeded} from '../../actions/userMonthly'
import Home from '../header/Home'

class HeaderDaily extends React.Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);
        this.isDayBlocked = this.isDayBlocked.bind(this);
        this.allowedDays = [];
        if (props.active_dates != undefined)
            props.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentWillUpdate(nextProps) {
        if (nextProps.active_dates != undefined) {
            this.allowedDays = [];
            nextProps.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        }
    }

    isDayBlocked(date) {
        return !this.allowedDays.includes(date.format('YYYYMMDD'));
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date.startOf('day');
    }


    render() {
        let {props} = this;
        let {customer, dispatch, history, user, current_day, startDate, access_level} = props;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <div style={{marginRight: '10px'}}>
                        <Link to={`/${process.env.PUBLIC_URL}/users/${user.nai}/${startDate.format('YYYY')}/${startDate.format('MM')}`}>&lt;Monthly</Link>
                    </div>
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <NavLink href="#">{this.props.user.nai}</NavLink>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight:'10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <SingleDatePicker
                                date={this.props.current_day} // momentPropTypes.momentObj or null,
                                onDateChange={(date) => {
                                    dispatch(selectCurrentDay(date))
                                    if (access_level !== 'END_USER')
                                        history.push(`/${process.env.PUBLIC_URL}/users/${user.nai}/${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}/`);
                                    else  history.push(`/${process.env.PUBLIC_URL}/${current_day.format('YYYY')}/${current_day.format('MM')}/${date.format('DD')}/`);
                                }}
                                focused={this.state.focused} // PropTypes.bool
                                onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        if (access_level === 'END_USER') {
                                            history.push(`/${process.env.PUBLIC_URL}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user.nai));
                                        } else {
                                            history.push(`/${process.env.PUBLIC_URL}/users/${user.nai}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                            dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user.nai));
                                        }
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        if (access_level === 'END_USER') {
                                            history.push(`/${process.env.PUBLIC_URL}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user.nai));
                                        } else {
                                            history.push(`/${process.env.PUBLIC_URL}/users/${user.nai}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                            dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user.nai));
                                        }
                                    }
                                }}
                                numberOfMonths={1}
                                isDayBlocked={this.isDayBlocked}
                                displayFormat="YYYY/MM/DD"
                            />
                            {access_level === 'ADMIN' &&
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}><span style={{whiteSpace: 'nowrap'}}>Clear cache</span></NavLink>
                            </NavItem>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    access_level: state.selects.access_level,
    customer: state.selects.customer,
});
export default connect(mapStateToProps)(HeaderDaily)