import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../../Api'
import {connect} from 'react-redux'
import {fetchUserMonthlyIfNeeded} from '../../actions/userMonthly'
import {selectStartEndDate} from '../../actions/selects'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'
import Home from '../header/Home'
class HeaderMonthly extends React.Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);

        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date
    }


    render() {
        let {props} = this;
        let {customer, startDate, dispatch, history, access_level} = props;
        let {user_hash} = props.match.params;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    {/*{access_level !== 'END_USER' &&*/}
                    {/*<div style={{marginRight: '10px'}}>*/}
                        {/*<a href="javascript:void(0);" onClick={() => history.goBack()}>&lt;Back</a>*/}
                    {/*</div>*/}
                    {/*}*/}
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <NavLink href="#">{user_hash}</NavLink>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight: '10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <DateRangePicker
                                startDate={this.props.startDate} // momentPropTypes.momentObj or null,
                                endDate={this.props.endDate}
                                onDatesChange={({startDate, endDate}) => {
                                    dispatch(selectStartEndDate(
                                        startDate.startOf('day'),
                                        endDate.endOf('day')
                                    ));
                                }}
                                focusedInput={this.state.focusedInput} // PropTypes.bool
                                onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') !== oldDate) {
                                        if (access_level === 'END_USER') {
                                            history.push(`/${process.env.PUBLIC_URL}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user_hash));
                                        } else {
                                            history.push(`/${process.env.PUBLIC_URL}/users/${user_hash}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                            dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user_hash));
                                        }
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') !== oldDate) {
                                        if (access_level === 'END_USER') {
                                            history.push(`/${process.env.PUBLIC_URL}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user_hash));
                                        } else {
                                            history.push(`/${process.env.PUBLIC_URL}/users/${user_hash}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                            dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                            dispatch(fetchUserMonthlyIfNeeded(newStart, user_hash));
                                        }
                                    }
                                }}
                                numberOfMonths={1}
                                displayFormat="YYYY/MM/DD"
                            />
                            {access_level === 'ADMIN' &&
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}><span style={{whiteSpace: 'nowrap'}}>Clear cache</span></NavLink>
                            </NavItem>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    access_level: state.selects.access_level,
    customer: state.selects.customer,
});
export default connect(mapStateToProps)(HeaderMonthly)
