import React from "react"
import _ from "lodash";
import {GoogleMap, Marker, withGoogleMap, InfoWindow} from "react-google-maps";
import userMapIcon from '../../static/media/user_map.png'
import userMapFlag from '../../static/media/flag.png'
import MarkerClusterer from "react-google-maps/lib/addons/MarkerClusterer";
import {
    Button, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import TransactionContainer from '../../containers/TransactionContainer'

const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={props.zoom}
        defaultCenter={{lat: 35, lng: 140}}
        onClick={props.onMapClick}
        // scrollWheel={false}
        // defaultOptions={{
        //     scrollwheel: false,
        // }}
        onZoomChanged={props.onZoomChanged}
        onDragEnd={props.onDragEnd}
    >
        {props.hotspotmarkers.map(marker => (
            <Marker
                {...marker}
                onClick={() => props.onHotspotClick(marker)}
            >
                {marker.showInfo && (
                    <InfoWindow onCloseClick={() => props.onHotspotClose(marker)}>
                        <div>{marker.infoContent}</div>
                    </InfoWindow>
                )}
            </Marker>
        ))}
        <MarkerClusterer
            averageCenter
            enableRetinaIcons
            gridSize={10}
            minimumClusterSize={5}
            maxZoom={15}
        >
            {props.markers.map(marker => (
                <Marker
                    {...marker}
                    onClick={() => props.onMarkerClick(marker)}
                >
                    {marker.showInfo && (
                        <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
                            <div>{marker.infoContent}</div>
                        </InfoWindow>
                    )}
                </Marker>
            ))}
        </MarkerClusterer>
    </GoogleMap>
));
const style = {
    container: {
        marginTop: '15px'
    }
};
const defaultIcon = {
    url: userMapIcon,
    scaledSize: new google.maps.Size(16, 19)
};
const flagIcon = {
    url: userMapFlag,
    scaledSize: new google.maps.Size(20, 20)
};
export default class UsageMap extends React.Component {
    constructor() {
        super();
        this.state = {
            zoom: 11,
            markers: [],
            reloadMap: true,
            hotspotmarkers: [],
            always_hide_hotspots: false,
            show_hotspots: false,
            selUser: '',
            transactionModal: false,
            transaction_id: undefined
        };
        this.handleMapMounted = this.handleMapMounted.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
        this.calcMarkers = this.calcMarkers.bind(this);
        this.calcHotspots = this.calcHotspots.bind(this);
        this.handleHotspotClick = this.handleHotspotClick.bind(this);
        this.handleHotspotClose = this.handleHotspotClose.bind(this);
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.transactionToggle = this.transactionToggle.bind(this)
        this.setTransaction = this.setTransaction.bind(this)

    }

    transactionToggle() {
        this.setState({
            transactionModal: !this.state.transactionModal
        })
    }

    setTransaction(transaction_id) {
        this.setState({transaction_id: transaction_id, transactionModal: true})
    }

    handleMarkerClick(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleMarkerClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }

    handleHotspotClick(targetMarker) {
        this.setState({
            hotspotmarkers: this.state.hotspotmarkers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleHotspotClose(targetMarker) {
        this.setState({
            hotspotmarkers: this.state.hotspotmarkers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }

    setShowHotspots(bool, zoom) {
        if (this.state.show_hotspots != bool) {
            if (this.updateTimeout != undefined)
                window.clearTimeout(this.updateTimeout);
            this.updateTimeout = window.setTimeout(() => this.setState({
                show_hotspots: bool,
                zoom: zoom
            }), 1000)
        }
    }

    componentWillUnmount() {
        window.clearTimeout(this.updateTimeout);
        window.clearTimeout(this.updateDrag);
    }

    handleZoomChange() {
        if (!this.state.always_hide_hotspots) {
            let zoom = this._connmap.getZoom();
            if (zoom >= 13) {
                if (this.zoomHotspotsShow != undefined)
                    window.clearTimeout(this.zoomHotspotsShow);
                this.zoomHotspotsShow = window.setTimeout(() =>
                    this.setState({hotspotmarkers: this.calcHotspots(this.props.hotspots, true)}), 200);
                this.setShowHotspots(true, zoom)
            }
            else this.setShowHotspots(false, zoom)
        }
    }

    handleDrag() {
        let _this = this;
        if (this.state.show_hotspots) {
            if (this.updateDrag != undefined)
                window.clearTimeout(this.updateDrag);
            this.updateDrag = window.setTimeout(() => {
                _this.setState({
                    hotspotmarkers: _this.calcHotspots(_this.props.hotspots, _this.state.show_hotspots),
                })
            }, 1000)
        }
    }

    handleMapMounted(map) {
        this._connmap = map;
        let {stats} = this.props;
        let {selUser} = this.state;
        if (stats != undefined && stats.length > 0 && map != undefined) {
            const bounds = new google.maps.LatLngBounds();
            let shouldZoom = false;
            let latg, lngg;
            stats.map((item, index) => {
                if (item != undefined) {
                    if (selUser != '' && selUser != item[2])
                        return;
                    let lat = parseFloat(item[6][0]);
                    let lng = parseFloat(item[6][1]);
                    if (lat != 0 && lng != 0) {
                        shouldZoom = true;
                        latg = lat;
                        lngg = lng
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                }
            });
            if (shouldZoom && map != undefined) {
                bounds.extend(new google.maps.LatLng(latg - 0.0015, lngg - 0.0015));
                bounds.extend(new google.maps.LatLng(latg + 0.0015, lngg + 0.0015));
                map.fitBounds(bounds);
            }

        }
    }


    componentDidMount() {
        let {props} = this;
        if (props.stats != undefined) {
            this.setState({
                markers: this.calcMarkers(props.stats),
                zoom: 11
            });
        }
    }

    componentWillUpdate(nextProps, nextState) {
        let old_stats = this.props.stats, {stats, hotspots} = nextProps;
        let old_selUser = this.state.selUser, {selUser} = nextState;
        if (stats.length !== old_stats.length ||
            (stats.length !== 0 && stats[0][1] !== old_stats[0][1]))
            this.setState({
                markers: this.calcMarkers(stats),
                zoom: 11,
                reloadMap: true,
                selUser: ''
            })
        else if (selUser != old_selUser) {
            if (selUser == '')
                this.setState({
                    markers: this.calcMarkers(stats),
                    zoom: 11,
                    reloadMap: true
                })
            else this.setState({
                markers: this.calcMarkers(stats.filter((item) => item[2] === selUser)),
                zoom: 11,
                reloadMap: true
            })
        }
    }

    componentDidUpdate() {
        let _this = this;
        if (this.state.reloadMap)
            this.setState({reloadMap: false})
    }

    calcMarkers(stats) {
        let markers = [];
        let {date, access_level} = this.props
        if (stats != undefined && stats.length > 0) {
            const bounds = new google.maps.LatLngBounds();
            let shouldZoom = false;
            let latg, lngg;
            stats.map((item, index) => {
                if (item != undefined) {
                    let lat = parseFloat(item[6][0]);
                    let lng = parseFloat(item[6][1]);
                    if (lat != 0 && lng != 0) {
                        latg = lat;
                        lngg = lng;
                        shouldZoom = true;
                        markers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            showInfo: false,
                            infoContent: (<div>
                                <div><span className="markerTitle">ID:</span>
                                    {item[3] === 'Connection' ?
                                        <a href="javascript:void(0);" className="markerValue"
                                           onClick={() => this.setTransaction(item[1])}>{item[1]}</a>
                                        :
                                        <span className="markerValue">{item[1]}</span>
                                    }</div>
                                <div><span className="markerTitle">User:</span><span
                                    className="markerValue">
                                    {access_level !== 'END_USER' ?
                                        <Link
                                            to={`/${process.env.PUBLIC_URL}/users/${item[2]}/${date.format('YYYY')}/${date.format('MM')}/`}>{item[2]}</Link>
                                        :
                                        item[2]
                                    }
                                </span></div>
                                <div><span className="markerTitle">Type:</span><span
                                    className="markerValue">{item[3]}</span></div>
                                <div><span className="markerTitle">Provider:</span><span
                                    className="markerValue">
                                    {access_level === 'ADMIN' ?
                                        <Link
                                            to={`/${process.env.PUBLIC_URL}/providers/${item[4]}/${date.format('YYYY')}/${date.format('MM')}/`}>{item[4]}</Link>
                                        :
                                        item[4]
                                    }
                                    </span></div>
                                <div><span className="markerTitle">Country:</span><span
                                    className="markerValue">{item[5]}</span></div>
                                <div><span className="markerTitle">Position:</span><span
                                    className="markerValue">{'{0},{1}'.format(parseFloat(item[6][0]).toFixed(2),
                                    parseFloat(item[6][1]).toFixed(2))}</span></div>
                                <div><span className="markerTitle">Device:</span><span
                                    className="markerValue">{item[7]}</span></div>
                                <div><span className="markerTitle">Seconds:</span><span
                                    className="markerValue">{isNaN(parseInt(item[8])) ? 0 : parseInt(item[8])}</span>
                                </div>
                                <div><span className="markerTitle">Bytes:</span><span
                                    className="markerValue">{isNaN(parseInt(item[9])) ? 0 : parseInt(item[9])}</span>
                                </div>
                            </div>),
                            icon: defaultIcon,
                            key: index
                        });
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                }
            });

            if (this._connmap != undefined && shouldZoom) {
                bounds.extend(new google.maps.LatLng(latg - 0.0015, lngg - 0.0015));
                bounds.extend(new google.maps.LatLng(latg + 0.0015, lngg + 0.0015));
                this._connmap.fitBounds(bounds);
            }
        }
        return markers;
    }

    calcHotspots(hotspots, show_hotspots) {
        let hotspotmarkers = []
        if (show_hotspots && this._connmap != undefined && hotspots != undefined && hotspots.length > 0) {
            hotspots.map((item, index) => {
                let lat = parseFloat(item.location.x);
                let lng = parseFloat(item.location.y);
                if (lat != 0 && lng != 0) {
                    let mapbounds = this._connmap.getBounds();
                    if (mapbounds.contains(new google.maps.LatLng(lat, lng))) {
                        let key = 'h' + index;
                        hotspotmarkers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            showInfo: false,
                            infoContent: (<div>
                                <div><span className="markerTitle">ID:</span><span
                                    className="markerValue">{item._id.$oid}</span></div>
                                <div><span className="markerTitle">Country:</span><span
                                    className="markerValue">{item.country_name}</span></div>
                                <div><span className="markerTitle">City District:</span><span
                                    className="markerValue">{item.city_district}</span></div>
                                <div><span className="markerTitle">Region:</span><span
                                    className="markerValue">{item.pref_state_region}</span></div>
                                <div><span className="markerTitle">Venue:</span><span
                                    className="markerValue">{item.venue_name}</span></div>
                                <div><span className="markerTitle">Address:</span><span
                                    className="markerValue">{item.english_address}</span></div>
                                <div><span className="markerTitle">ZIP:</span><span
                                    className="markerValue">{item.zipcode}</span></div>
                                <div><span className="markerTitle">Latitude:</span><span
                                    className="markerValue">{item.latitude}</span></div>
                                <div><span className="markerTitle">Longitude:</span><span
                                    className="markerValue">{item.longitude}</span></div>
                                <div><span className="markerTitle">Customer:</span><span
                                    className="markerValue">{item.customer_id} {item.customer_name}</span></div>
                                <div><span className="markerTitle">SSID:</span><span
                                    className="markerValue">{item.ssid}</span></div>
                                <div><span className="markerTitle">Suffix:</span><span
                                    className="markerValue">{item.suffix}</span></div>
                                <div><span className="markerTitle">l2auth:</span><span
                                    className="markerValue">{item.l2auth}</span></div>
                                <div><span className="markerTitle">l3auth:</span><span
                                    className="markerValue">{item.l3auth}</span></div>
                                <div><span className="markerTitle">Modified:</span><span
                                    className="markerValue">{item.mod_date}</span></div>
                            </div>),
                            key: key,
                            defaultAnimation: null,
                            title: item.venue_name
                        });
                    }
                }
            });
        }
        return hotspotmarkers
    }

    render() {
        let {stats, mapheight} = this.props;
        let {markers, hotspotmarkers, reloadMap, selUser, always_hide_hotspots} = this.state;
        if (stats === undefined || reloadMap || stats.length === 0 || markers.length === 0)
            return (<div></div>)
        let uniq_users = [], uniq_countries = [], uniq_countries_data = {};
        stats.map((item) => {
            if (parseInt(item[6][0]) != 0 && parseInt(item[6][1]) != 0 && !uniq_users.includes(item[2]))
                uniq_users.push(item[2])
            if (selUser != '' && selUser != item[2])
                return;
            if (item[3] === 'Connection' && item[5] != undefined && item[5] != '') {
                if (uniq_countries_data.hasOwnProperty(item[5])) {
                    uniq_countries_data[item[5]] = {
                        data: isNaN(parseInt(item[9])) ? uniq_countries_data[item[5]].data :
                            uniq_countries_data[item[5]].data + parseInt(item[9]),
                        seconds: isNaN(parseInt(item[8])) ? uniq_countries_data[item[5]].seconds :
                            uniq_countries_data[item[5]].seconds + parseInt(item[8]),
                        connections: uniq_countries_data[item[5]].connections + 1,
                        users: uniq_countries_data[item[5]].users.includes(item[2]) ? uniq_countries_data[item[5]].users :
                            uniq_countries_data[item[5]].users.concat(item[2])
                    }
                } else {
                    uniq_countries_data[item[5]] = {
                        data: isNaN(parseInt(item[9])) ? 0 : parseInt(item[9]),
                        seconds: isNaN(parseInt(item[8])) ? 0 : parseInt(item[8]),
                        users: [item[2]],
                        connections: 1
                    }
                }
            }
        });
        for (let key in uniq_countries_data) {
            if (uniq_countries_data.hasOwnProperty(key))
                uniq_countries.push({
                    country: key,
                    data: uniq_countries_data[key].data,
                    seconds: uniq_countries_data[key].seconds,
                    connections: uniq_countries_data[key].connections,
                    users: uniq_countries_data[key].users.length
                })
        }
        uniq_countries.sort((first, second) => {
            return first.connections < second.connections
        })
        let emptyRows = [];
        for (let i = 13; i > uniq_countries.length; i--)
            emptyRows.push({})
        let mapWidth = 620;
        return (
            <div style={{margin: '20px 0 0 0', whiteSpace: 'nowrap'}}>
                <div style={(uniq_countries.length > 0) ? {width: `${mapWidth}px`, display: 'inline-block'} : {}}>
                    <div>
                        <span style={{fontWeight: 'bold', fontSize: '120%'}}>Usage Map</span>
                        {uniq_users.length > 1 &&
                        <select style={{margin: '0 10px 5px 20px'}} value={selUser}
                                onChange={(event) => {
                                    this.setState({selUser: event.target.value})
                                }} className="tableFilter custom-select">
                            <option value="">All Users</option>
                            {uniq_users.map((item, index) =>
                                <option value={item} key={index}>{item}</option>
                            )}
                        </select>
                        }
                        <a style={{float: 'right', marginTop: '5px'}} onClick={() => {
                            const bounds = new google.maps.LatLngBounds();
                            let shouldZoom = false;
                            let latg, lngg;
                            stats.map((item, index) => {
                                if (item != undefined) {
                                    if (selUser != '' && selUser != item[2])
                                        return;
                                    let lat = parseFloat(item[6][0]);
                                    let lng = parseFloat(item[6][1]);
                                    if (lat != 0 && lng != 0) {
                                        shouldZoom = true;
                                        latg = lat;
                                        lngg = lng
                                        bounds.extend(new google.maps.LatLng(lat, lng))
                                    }
                                }
                            });
                            if (shouldZoom && this._connmap != undefined) {
                                bounds.extend(new google.maps.LatLng(latg - 0.0015, lngg - 0.0015));
                                bounds.extend(new google.maps.LatLng(latg + 0.0015, lngg + 0.0015));
                                this._connmap.fitBounds(bounds);
                            }
                        }} href="javascript:void(0);">Reset zoom</a>
                        <a href="javascript:void(0);" style={{margin: '5px 10px 0 0', float: 'right'}} onClick={() => {
                            if (always_hide_hotspots)
                                this.setState({always_hide_hotspots: false}, this.handleZoomChange)
                            else this.setState({always_hide_hotspots: true, show_hotspots: false})
                        }}>
                            {always_hide_hotspots ?
                                <span>Show hotspots</span>
                                :
                                <span>Hide hotspots</span>
                            }
                        </a>
                    </div>
                    <div>
                        <GettingStartedGoogleMap
                            containerElement={
                                <div style={{height: `400px`, width: '100%'}}/>
                            }
                            mapElement={
                                <div style={{height: `400px`, width: '100%'}}/>
                            }
                            onMapLoad={this.handleMapMounted}
                            onMapClick={_.noop}
                            onDragEnd={this.handleDrag}
                            onZoomChanged={this.handleZoomChange}
                            hotspotmarkers={this.state.show_hotspots ? this.state.hotspotmarkers : []}
                            markers={markers}
                            zoom={this.state.zoom}
                            onMarkerRightClick={_.noop}
                            onMarkerClick={this.handleMarkerClick}
                            onMarkerClose={this.handleMarkerClose}
                            onHotspotClick={this.handleHotspotClick}
                            onHotspotClose={this.handleHotspotClose}
                        />
                    </div>
                </div>
                {uniq_countries.length > 0 &&
                <div style={{
                    display: 'inline-block',
                    verticalAlign: 'top',
                    margin: (uniq_users.length > 1) ? '42px 0 0 10px' : '28px 0 0 10px',
                    maxHeight: '400px',
                    overflow: 'scroll'
                }}>
                    <table className="table table-striped" style={{margin: 0}}>
                        <thead className="thead-inverse">
                        <tr>
                            <th>Country</th>
                            <th>Users</th>
                            <th>Connections</th>
                            <th>Data used</th>
                        </tr>
                        </thead>
                        <tbody>
                        {uniq_countries.map((item, index) => <tr key={index} style={{textAlign: 'center'}}>
                            <td>{item.country}</td>
                            <td>{item.users}</td>
                            <td>{item.connections}</td>
                            <td>{present_data(item.data)}</td>
                        </tr>)}
                        {emptyRows.map((item, index) => <tr key={'e' + index}>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
                }
                <Modal isOpen={this.state.transactionModal} toggle={this.transactionToggle} size="lg">
                    <ModalHeader toggle={this.transactionToggle}>Transaction {this.state.transaction_id}</ModalHeader>
                    <ModalBody>
                        <TransactionContainer
                            transaction_id={this.state.transaction_id}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.transactionToggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}
