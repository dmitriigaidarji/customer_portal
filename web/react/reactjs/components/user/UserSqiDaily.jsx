import React from "react"
import {connect} from 'react-redux'
import {fetchUserSqiDailyIfNeeded} from '../../actions/userSqiDaily'
import moment from 'moment'
import _ from "lodash";
import {GoogleMapLoader, GoogleMap, Marker, withGoogleMap, InfoWindow} from "react-google-maps";
import userMapIcon from '../../static/media/user_map.png'
import SqiTable from '../../components/sqi/SqiTable'

const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={16}
        defaultCenter={{lat: props.markers[0].position.lat, lng: props.markers[0].position.lng}}
        onClick={props.onMapClick}
    >
        {props.markers.map(marker => (
            <Marker
                {...marker}
                onRightClick={() => props.onMarkerRightClick(marker)}
            />
        ))}
    </GoogleMap>
));

const style = {
    container: {
        marginTop: '15px'
    }
};
const icon = {
    url: userMapIcon,
    scaledSize: new google.maps.Size(16, 19)
};
class UserSqiDaily extends React.Component {
    constructor() {
        super();
        this.updateMap = this.updateMap.bind(this);
        this.handleMapMounted = this.handleMapMounted.bind(this);
    }

    handleMapMounted(map) {
        this._sqimap = map;
        let {stats, hotspots} =this.props;
        if (stats != undefined && map != undefined) {
            const bounds = new google.maps.LatLngBounds();
            stats.map((item, index) => {
                let lat = parseFloat(item.lat);
                let lng = parseFloat(item.lng);
                if (lat != 0 && lng != 0) {
                    bounds.extend(new google.maps.LatLng(lat, lng))
                }
            });
            if (hotspots != undefined) {
                hotspots.map((item, index) => {
                    let lat = parseFloat(item.location.x);
                    let lng = parseFloat(item.location.y);
                    if (lat != 0 && lng != 0) {
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                });
            }
            map.fitBounds(bounds);
        }
    }

    componentDidMount() {
        let {dispatch, user, current_day, stats} = this.props;
        if (stats == undefined && user != undefined && current_day != undefined) {
            dispatch(fetchUserSqiDailyIfNeeded(current_day, user))
        }
        if (stats != undefined)
            this.updateMap(stats)
    }

    componentWillUpdate(nextProps, nextState) {
        let {dispatch, stats, hotspots} = this.props;
        dispatch(fetchUserSqiDailyIfNeeded(nextProps.current_day, nextProps.user))
        if (nextProps.stats != undefined)
            this.updateMap(nextProps.stats, nextProps.hotspots);
    }

    updateMap(stats, hotspots) {
        if (stats != undefined && this._sqimap != undefined) {
            const bounds = new google.maps.LatLngBounds();
            stats.map((item, index) => {
                let lat = parseFloat(item.lat);
                let lng = parseFloat(item.lng);
                if (lat != 0 && lng != 0) {
                    bounds.extend(new google.maps.LatLng(lat, lng))
                }
            });
            if (hotspots != undefined) {
                hotspots.map((item, index) => {
                    let lat = parseFloat(item.location.x);
                    let lng = parseFloat(item.location.y);
                    if (lat != 0 && lng != 0) {
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                });
            }
            this._sqimap.fitBounds(bounds);
        }
    }

    render() {
        let {stats, hotspots} = this.props;
        if (stats == undefined)
            return <div>Loading SQI..</div>
        let markers = [];
        if (stats != undefined) {
            const bounds = new google.maps.LatLngBounds();
            stats.map((item, index) => {
                let lat = parseFloat(item.lat);
                let lng = parseFloat(item.lng);
                if (lat != 0 && lng != 0) {
                    markers.push({
                        position: {
                            lat: lat,
                            lng: lng,
                        },
                        icon: icon,
                        key: index,
                        defaultAnimation: 2,
                        title: item.event
                    });
                    bounds.extend(new google.maps.LatLng(lat, lng))
                }
            });
            if (hotspots != undefined) {
                hotspots.map((item, index) => {
                    let lat = parseFloat(item.location.x);
                    let lng = parseFloat(item.location.y);
                    if (lat != 0 && lng != 0) {
                        let key = 'h' + index;
                        markers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            key: key,
                            defaultAnimation: 2,
                            title: item.venue_name
                        });
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                });
            }
            if (this._sqimap != undefined) {
                this._sqimap.fitBounds(bounds);
            }
        }
        return (
            <div style={style.container}>
                <h5>SQI Map</h5>
                {markers.length > 0 ?
                    <div>
                        <GettingStartedGoogleMap
                            containerElement={
                                <div style={{height: `350px`}}/>
                            }
                            mapElement={
                                <div style={{height: `100%`}}/>
                            }
                            onMapLoad={this.handleMapMounted}
                            onMapClick={_.noop}
                            markers={markers}
                            onMarkerRightClick={_.noop}
                            onTilesloaded={() => this.updateMap()}
                        />
                    </div>
                    :
                    <div>No data</div>
                }
                <SqiTable stats={stats} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    if (state.selects.user != undefined) {
        let stats = state.userSqiDaily[
        '' + state.selects.user.nai + '-' + moment.utc(state.selects.current_day).format('YYYYMMDD')
            ];
        let hotspots;
        if (stats != undefined) {
            hotspots = stats.hotspots;
            stats = stats.items;
        }
        return ({
            stats: stats,
            hotspots: hotspots,
            current_day: state.selects.current_day,
            user: state.selects.user
        });
    }
};

export default connect(
    mapStateToProps
)(UserSqiDaily)

