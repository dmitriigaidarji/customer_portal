import React from "react"
import LiquidFillGauge from 'react-liquid-gauge';

export default class Gauge extends React.Component {
    constructor(props) {
        super(props)
        // this.startColor = '#dc143c';
        // this.endColor = '#6495ed';
        this.startColor = '#dc143c';
        this.endColor = '#2cc625';
        this.updateDimensions = this.updateDimensions.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.value != undefined;
    }

    render() {
        let {reversed} = this.props;
        const radius = 30;
        const interpolate = reversed ? d3.interpolateRgb( this.endColor, this.startColor) : d3.interpolateRgb(this.startColor, this.endColor);
        const fillColor = interpolate(this.props.value / 100);
        const gradientStops = [
            {
                key: '0%',
                stopColor: d3.color(fillColor).darker(0.5).toString(),
                stopOpacity: 1,
                offset: '0%'
            },
            {
                key: '50%',
                stopColor: fillColor,
                stopOpacity: 0.75,
                offset: '50%'
            },
            {
                key: '100%',
                stopColor: d3.color(fillColor).brighter(0.5).toString(),
                stopOpacity: 0.5,
                offset: '100%'
            }
        ];
        return (
            <div className="gaugeContainer" style={{display: 'inline-block'}}>
                <LiquidFillGauge
                    style={{margin: '0 auto'}}
                    width={radius * 2}
                    height={radius * 2}
                    value={this.props.value}
                    percent="%"
                    textSize={1}
                    textOffsetX={0}
                    textOffsetY={0}
                    textRenderer={(props) => {
                        const value = Math.round(props.value);
                        const radius = Math.min(props.height / 2, props.width / 2);
                        const textPixels = (props.textSize * radius / 2);
                        const valueStyle = {
                            fontSize: textPixels
                        };
                        const percentStyle = {
                            fontSize: textPixels * 0.6
                        };

                        return (
                            <tspan>
                                <tspan className="value" style={valueStyle}>{value}</tspan>
                                <tspan style={percentStyle}>{props.percent}</tspan>
                            </tspan>
                        );
                    }}
                    riseAnimation
                    waveAnimation
                    waveFrequency={2}
                    waveAmplitude={1}
                    gradient
                    gradientStops={gradientStops}
                    circleStyle={{
                        fill: fillColor
                    }}
                    waveStyle={{
                        fill: fillColor
                    }}
                    textStyle={{
                        fill: d3.color('#444').toString(),
                        fontFamily: 'Arial'
                    }}
                    waveTextStyle={{
                        fill: d3.color('#fff').toString(),
                        fontFamily: 'Arial'
                    }}
                />
                <div className="gaugeTitle">{this.props.title}</div>
            </div>
        )
    }
}