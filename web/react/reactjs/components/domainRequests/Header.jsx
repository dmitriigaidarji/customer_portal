import React from "react"
import {
    Navbar, NavLink
}
    from 'reactstrap';
import Home from '../header/Home'

export default class Header extends React.Component {
    render() {
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <Home/>
                    <NavLink href="#">Domain Requests</NavLink>
                </Navbar>
            </div>
        )
    }
}
