import React from "react"

export default class CustomerInfo extends React.Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;
    }

    render() {
        if (!this.props.stats)
            return (
                <div>
                    <p>
                        Please select a customer using tools on the left.
                    </p>
                </div>
            );
        else if (this.props.stats.length == 0) return (
            <div>
                <p>
                    Customer has no data
                </p>
            </div>
        )
        else {
            let stat = this.props.stats[0]
            return (
                <div>
                    <h4>{ stat.id }: { stat.name }, { stat.country }</h4>
                </div>
            )
        }
    }
}