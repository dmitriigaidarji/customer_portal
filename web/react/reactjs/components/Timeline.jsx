import React from "react"
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
export default class Timeline extends React.Component {
    constructor(props) {
        super(props)
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {daily} = this.props;
        if (daily != undefined && daily.length > 0)
            this.updateChart(this.props.daily);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {daily} = this.props;
        if (daily != undefined && daily.length > 0)
            this.updateChart(this.props.daily);
    }

    //
    // shouldComponentUpdate(nextProps) {
    //     if (nextProps.daily != undefined)
    //         return true;
    //     return false;
    // }

    updateChart(data) {
        let {history, date} = this.props;
        if (data != undefined && data.length > 0) {
            function present_data(data) {
                if (data / 1073741824 > 1)
                    return '{0} Gb'.format((data / 1073741824).toFixed(2));
                else if (data / 1048576 > 1)
                    return '{0} Mb'.format((data / 1048576).toFixed(2));
                else if (data / 1024 > 1)
                    return '{0} Kb'.format((data / 1024).toFixed(2));
                return '{0} b'.format(data);
            }

            function present_clock(data) {
                let h = Math.floor(data / 3600);
                let m = Math.floor(data % 3600 / 60);
                let s = Math.floor(data % 3600 % 60);
                return '{0}:{1}:{2}'.format(
                    h > 9 ? h : '0' + h,
                    m > 9 ? m : '0' + m,
                    s > 9 ? s : '0' + s,
                )
            }

            let cleandata = [];
            data.forEach(function (d, i, object) {
                if (d.usage.secs != 0) {
                    d.startDate = new Date(parseInt(d.timestamp) * 1000);
                    d.endDate = new Date((parseInt(d.timestamp) + d.usage.secs) * 1000);
                    cleandata.push(d)
                }
            });
            data = cleandata;
            data.sort(function (a, b) {
                return a.endDate - b.endDate;
            });
            let maxDate = data[data.length - 1].endDate;
            data.sort(function (a, b) {
                return a.startDate - b.startDate;
            });
            let minDate = data[0].startDate;
            data.sort(function (a, b) {
                if (a.cui != b.cui) {
                    if (a.cui < b.cui) return -1;
                    if (a.cui > b.cui) return 1;
                    return 0;
                }
                else return a.startDate - b.startDate;
            });
            let maxRow = 0;
            let initialRow = 0;
            for (let i = 0; i < data.length; i++) {
                let d = data[i]
                let row = initialRow,
                    j = 0;
                if (i > 0 && d.cui != data[i - 1].cui) {
                    initialRow = maxRow + 1;
                    row = initialRow;
                }
                for (j = i - 1; j >= 0; j--) {
                    if (d.cui == data[j].cui && row == data[j].rectRow && d.startDate < data[j].endDate) {
                        row += 1
                    }
                }
                if (row > maxRow) maxRow = row;
                d.rectRow = row;
            }
            let cuis = [];
            for (let i = 0; i < data.length; i++) {
                if (!cuis.includes(data[i].cui))
                    cuis.push(data[i].cui);
            }
            let cuisheights = {};
            for (let i = 0; i < data.length; i++) {
                if (!cuisheights.hasOwnProperty(data[i].cui)) {
                    cuisheights[data[i].cui] = {}
                    cuisheights[data[i].cui]['start'] = cuisheights[data[i].cui]['end'] = data[i].rectRow
                }
                else if (cuisheights[data[i].cui]['start'] > data[i].rectRow)
                    cuisheights[data[i].cui]['start'] = data[i].rectRow
                else if (cuisheights[data[i].cui]['end'] < data[i].rectRow)
                    cuisheights[data[i].cui]['end'] = data[i].rectRow
            }
            let cuisheightsarr = []
            let realheight = 0;
            for (let key in cuisheights) {
                let d = cuisheights[key]
                cuisheightsarr.push({
                    'cui': key,
                    'start': d['start'],
                    'end': d['end']
                })
                realheight = Math.max(realheight, d['end'])
            }
            let svg = d3.select("#timelinechart");
            svg.attr('width', $('#timelineParent').width());
            svg.selectAll("*").remove();
            let margin = {top: 0, right: 0, bottom: 30, left: 150},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom,
                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            let x = d3.scaleUtc().domain([minDate, maxDate]).range([0, width]).clamp(true);
            let y = d3.scaleBand().domain(cuis).range([0, height - margin.top - margin.bottom]).padding(0.1);

            let xAxis = d3.axisBottom().scale(x)
                .tickSize(8).tickPadding(8);

            // let yAxis = d3.axisLeft().scale(y).tickSize(0);

            // g.selectAll(".chart")
            //     .data(data, function (d) {
            //         return d.startDate + d.cui + d.endDate;
            //     }).enter()
            //     .append("rect")
            //     .attr("rx", 5)
            //     .attr("ry", 5)
            //     .attr("class", function (d) {
            //         return "bar";
            //     })
            //     .attr("y", 0)
            //     .attr("transform", function (d) {
            //         return "translate(" + x(d.startDate) + "," + y(d.cui) + ")";
            //     })
            //     .attr("height", function (d) {
            //         return 70;
            //     })
            //     .attr("width", function (d) {
            //         return (x(d.endDate) - x(d.startDate));
            //     });

            // g.append("g")
            //     .attr("class", "x axis")
            //     .attr("transform", "translate(0, " + (height - margin.top - margin.bottom) + ")")
            //     .transition()
            //     .call(xAxis);

            // g.append("g").attr("class", "y axis").transition().call(yAxis);

            let barHeight = 20;
            let gap = 5;
            let topPadding = 75;
            let sidePadding = 75;

            let colorScale = d3.scaleLinear()
                .domain([0, cuis.length])
                .range(["#00B9FA", "#00ba73"])
                .interpolate(d3.interpolateHcl);

            let bigRects = g.append("g")
                .selectAll("rect")
                .data(cuisheightsarr)
                .enter()
                .append("rect")
                .attr("x", -margin.left)
                .attr("y", function (d, i) {
                    return d.start * barHeight + d.start * 2 * gap
                })
                .attr("width", function (d) {
                    return width + margin.left;
                })
                .attr("height", function (d) {
                    return (barHeight + gap * 2) * (d.end - d.start + 1)
                })
                .attr("stroke", "none")
                .attr("fill", function (d) {
                    for (let i = 0; i < cuis.length; i++) {
                        if (d.cui == cuis[i]) {
                            return d3.rgb(colorScale(i));
                        }
                    }
                })
                .attr("opacity", 0.2);

            let axisText = g.append("g")
                .selectAll("text")
                .data(cuisheightsarr)
                .enter()
                .append("text")
                .text(function (d) {
                    return d.cui == '' ? '(unknown)' : d.cui;
                })
                .attr("x", -margin.left)
                .attr("y", function (d, i) {
                    return d.start * barHeight + d.start * 2 * gap + (d.end - d.start + 1) * (barHeight + gap * 2) / 2 + 14 / 3
                })
                .attr("font-size", 11)
                .attr("text-anchor", "start")
                .attr("text-height", 14)
                .attr('cursor', 'pointer')
                .on('click',(item)=>{
                    history.push(`/${process.env.PUBLIC_URL}/users/${item.cui}/${date.format('YYYY')}/${date.format('MM')}/`)
                })
            // .attr("fill", function(d){
            //  for (let i = 0; i < cuis.length; i++) {
            //          if (d.cui == cuis[i]) {
            //              return d3.rgb(colorScale(i));
            //          }
            //      }
            // });

            let h = (realheight + 1) * barHeight + (realheight + 1) * 2 * gap
            g.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0, " + h + ")")
                .transition()
                .call(xAxis);


            let rectangles = g.append('g')
                .selectAll("rect")
                .data(data)
                .enter();


            let innerRects = rectangles.append("rect")
                .attr("rx", 3)
                .attr("ry", 3)
                .attr("x", function (d) {
                    return x(d.startDate);
                })
                .attr("y", function (d, i) {
                    return d.rectRow * barHeight + gap * ( 2 * d.rectRow + 1)
                })
                .attr("width", function (d) {
                    return (x(d.endDate) - x(d.startDate));
                })
                .attr("height", barHeight)
                .attr("stroke", "none")
                .attr("fill", function (d) {
                    for (let i = 0; i < cuis.length; i++) {
                        if (d.cui == cuis[i]) {
                            return d3.rgb(colorScale(i));
                        }
                    }
                })


            // let rectText = rectangles.append("text")
            //     .text(function (d) {
            //         return d.cui;
            //     })
            //     .attr("x", function (d) {
            //         return (x(d.endDate) - x(d.startDate)) / 2 + x(d.startDate) + sidePadding;
            //     })
            //     .attr("y", function (d, i) {
            //         return y(d.cui);
            //     })
            //     .attr("font-size", 11)
            //     .attr("text-anchor", "middle")
            //     .attr("text-height", barHeight)
            //     .attr("fill", "#0000");


            // rectText.on('mouseover', function (e) {
            //     let tag = "";
            //     tag = "Cui: " + d3.select(this).data()[0].cui + "<br/>" +
            //         "Duration: " + d3.select(this).data()[0].usage.secs + "<br/>" +
            //         "Starts: " + d3.select(this).data()[0].startDate + "<br/>" +
            //         "Ends: " + d3.select(this).data()[0].endDate;

            //     let output = document.getElementById("tag");

            //     let xp = this.x.animVal.value + margin.left + 15 + "px";
            //     let yp = this.y.animVal.value + margin.top + topPadding / 2 + 25 + "px";

            //     output.innerHTML = tag;
            //     output.style.top = yp;
            //     output.style.left = xp;
            //     output.style.display = "block";
            // }).on('mouseout', function () {
            //     let output = document.getElementById("tag");
            //     output.style.display = "none";
            // });

            innerRects.on('mouseover', function (e) {
                let tag = "";
                let d = d3.select(this).data()[0]
                let nas = d.nas_identifier == '' ? d.nas_ip : d.nas_identifier;
                let sh = d.startDate.getUTCHours();
                if (sh < 10) sh = '0' + sh;
                let sm = d.startDate.getUTCMinutes();
                if (sm < 10) sm = '0' + sm;
                let eh = d.endDate.getUTCHours();
                if (eh < 10) eh = '0' + eh;
                let em = d.endDate.getUTCMinutes();
                if (em < 10) em = '0' + em;
                tag = "<span class='timelineHoverTitle'>" + d.sessionid + " (" + nas + ")</span><br/><hr style='margin: 0px'/>" +
                    "<span class='timelineHoverTitle'>{0}:</span> {1}:{2} - {3}:{4}<br/>".format(d.cui, sh, sm, eh, em) +
                    "<span class='timelineHoverTitle'>Duration:</span> " + present_clock(d.usage.secs);
                let output = document.getElementById("tag");

                let xp = this.x.animVal.value + 140 + (x(d.endDate) - x(d.startDate)) / 2 + "px";
                let yp = this.y.animVal.value + margin.top + topPadding / 2 + 35 + "px";
                output.innerHTML = tag;
                output.style.top = yp;
                output.style.left = xp;
                output.style.display = "block";
            }).on('mouseout', function () {
                let output = document.getElementById("tag");
                output.style.display = "none";
            });

            svg.attr('height', h + 50);

        }
    }

    render() {
        if (this.props.daily == undefined)
            return (
                <Row>
                    <Col md="12">
                        <h5>Loading Timeline Chart...</h5>
                    </Col>
                </Row>
            );
        else if (this.props.daily.length == 0)
            return (
                <Row>
                    <Col md="12" id="timelineParent">
                        <h5>Timeline</h5>
                        <div>No data</div>
                    </Col>
                </Row>
            )
        return (
            <Row>
                <Col md="12" id="timelineParent">
                    <h5>Timeline</h5>
                    <div id="tag"></div>
                    <svg id="timelinechart" width="1000" height="300"></svg>
                </Col>
            </Row>
        )
    }
}
