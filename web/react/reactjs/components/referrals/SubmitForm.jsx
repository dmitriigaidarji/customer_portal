import React from "react"
import {Col, Button, Form, FormGroup, Label, Input, FormText} from 'reactstrap';
import {connect} from 'react-redux'
import {addReferral} from '../../actions/referrals'
class SubmitForm extends React.Component {
    constructor() {
        super()
        this.state = {
            name: '',
            email: ''
        }
        this.submitForm = this.submitForm.bind(this)
    }

    submitForm() {
        let {dispatch} = this.props;
        let {email, name} = this.state;
        console.log(email, name)
        if (email != '' && name != '')
            dispatch(addReferral(email, name))
        else alert('Wrong input format')
    }

    render() {
        let {name, email} = this.state;
        return (
            <div>
                <legend>Register New Referral</legend>
                <div>
                    <Form>
                        <FormGroup>
                            <Label for="fullName">Your Full Name</Label>
                            <Input type="text" name="fullname" id="fullName"
                                   value={name}
                                   onChange={(event) => {
                                       let {value} = event.target;
                                       this.setState({name: value})
                                   }}
                                   placeholder="Full Name"/>
                            <small className="form-text text-muted">This name will be presented in the email the
                                recipient gets
                            </small>
                        </FormGroup>
                        <FormGroup>
                            <Label for="customerEmail">Recipient's Email Address</Label>
                            <Input type="email" name="email" id="customerEmail" placeholder="Email"
                                   value={email}
                                   onChange={(event) => {
                                       let {value} = event.target;
                                       this.setState({email: value})
                                   }}
                            />
                        </FormGroup>
                        <Button type="button" onClick={this.submitForm}>Submit</Button>
                    </Form>
                </div>
            </div>
        )
    }
}

export default connect()(SubmitForm)