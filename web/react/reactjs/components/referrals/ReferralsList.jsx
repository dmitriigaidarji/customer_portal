import React from "react"
import {connect} from 'react-redux'
import moment from 'moment'
import {submitToReview} from '../../actions/referrals'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
let styles = {
    container: {
        minHeight: '434px'
    }
}
class ReferralsList extends React.Component {
    render() {
        let {referrals, dispatch} = this.props;
        if (referrals == undefined)
            return (
                <div>
                    <div style={styles.container}>
                        <legend>Responses</legend>
                        <div>Loading...</div>
                    </div>
                </div>
            )
        let columns = [{
            id: 'date',
            Header: 'Date',
            accessor: d => moment(d.created * 1000).format('lll'),
            width: 150
        },
            {
                id: 'email',
                Header: 'Email',
                accessor: 'email'
            },
            {
                id: 'name',
                Header: 'Company Name',
                accessor: 'company_name',
                width: 100
            },
            {
                id: 'status',
                Header: 'Status',
                accessor: 'status',
                width: 100
            },
            {
                id: 'response',
                Header: 'Response Date',
                accessor: d => d.response_date != undefined ? moment(d.response_date * 1000).format('lll') : d.response_date,
                width: 150
            },
            {
                id: 'note',
                Header: 'Note',
                Cell: d => {
                    let text = '';
                    switch (d.original.status){
                        case 'Sent':
                            text = 'An E-Mail has been sent to the requested address'
                            break;
                        case 'Invalid':
                            text = 'The E-Mail is Invalid.  Please verify and re-send.'
                            break;
                        case 'Responded':
                            text = 'This referral has been responded-to.  Please review.'
                            break;
                        case 'Reviewed':
                            text = 'This referral is ready to submit.'
                            break;
                        case 'Submitted':
                            text = 'This referral has been submitted.'
                            break;
                        case 'Completed':
                            text= 'This Company is available on your interface.'
                            break;
                        case 'Canceled':
                            text = 'No further action to be taken.'
                            break;
                    }
                    return <div style={{maxWidth:'150px', whiteSpace:'pre-wraps'}}>{text}</div>
                },
                width: 150
            },
            {
                id: 'action',
                Header: 'Action',
                Cell: item => {
                    let action = <div/>;
                    switch (item.status) {
                        case 'Sending':
                            // action = <button type="button" className="btn btn-primary">Resend</button>;
                            break;
                        case 'Responded':
                            action = <button type="button" className="btn btn-success" onClick={() => {
                                dispatch(submitToReview(item.id))
                            }}>Submit</button>;
                            break;
                    }
                    return action;
                },
                width: 100
            }
        ];
        return (
            <div>
                <div style={styles.container}>
                    <legend>Responses</legend>
                    {referrals.length == 0 ?
                        <div>You have no referrals right now</div>
                        :
                        <div>
                            <ReactTable
                                className='-striped -highlight'
                                data={referrals}
                                columns={columns}
                                filterable={false}
                                defaultPageSize={20}
                                showPagination={true}
                                defaultFilterMethod={(filter, row, column) => {
                                    const id = filter.pivotId || filter.id
                                    return row[id] != undefined ?
                                        String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                                        true
                                }}
                                defaultSorted={[{
                                    id: 'date',
                                    desc: true
                                }]}
                            />
                        </div>
                    }
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    referrals: state.referrals.items
});
export default connect(mapStateToProps)(ReferralsList)