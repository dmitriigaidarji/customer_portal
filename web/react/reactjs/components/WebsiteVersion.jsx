import React from "react"

export default class WebsiteVersion extends React.Component {
    render() {
        return (
            <div style={{color: 'grey', margin: '30px 0 0 50px', fontSize: '80%'}}>
                Website version: <span style={{color: 'green'}}>{process.env.APP_VERSION.toFixed(4)}</span>
            </div>
        )
    }
}