import React from "react"
import Android from '../static/media/android_device.png'
import iOS from '../static/media/ios_device.png'
export default class DeviceTiles extends React.Component {
    render() {
        let {data} = this.props;
        return (
            <div>
                {data.map((item, index) =>
                    <Tile data={item} key={index}/>
                )}
            </div>
        )
    }
}

let styles = {
    container: {
        fontSize: '70%',
        display: 'inline-block',
        margin: '5px',
        maxWidth: '200px'
    },
    cardTitle:{
        fontSize: '150%',
        fontWeight: 'bold'
    },
    cardBlock: {
        padding: '0.8rem'
    },
    title: {
        fontWeight: 'bold'
    },
    value: {
        marginLeft: '5px'
    },
    imgcontainer: {
        maxHeight: '100px',
        backgroundColor: 'grey',
        textAlign: 'center',
        display: 'none'
    },
    img: {
        maxHeight: '60px'
    }
}
class Tile extends React.Component {
    render() {
        let {data} = this.props;
        return (
            <div style={styles.container} className="card">
                <div style={styles.imgcontainer}>
                    <img style={styles.img} src={data.os == 'iOS' ? iOS : Android}
                         alt="Device image"/>
                </div>
                <div style={styles.cardBlock} className="card-block">
                    <h4 style={styles.cardTitle} className="card-title">{data.model}</h4>
                    <div className="card-text"><span style={styles.title}>Device ID:</span><span
                        style={styles.value}>{data.device_id}</span></div>
                    <div className="card-text"><span style={styles.title}>OS Ver.:</span><span
                        style={styles.value}>{data.os_ver}</span></div>
                    <div className="card-text"><span style={styles.title}>App Ver.:</span><span
                        style={styles.value}>{data.app_ver}</span></div>
                    {data.wifi_mac !== "" &&
                    <div className="card-text"><span style={styles.title}>WiFi MAC:</span><span
                        style={styles.value}>{data.wifi_mac}</span></div>
                    }
                </div>
            </div>
        )
    }
}
