import React from "react"
import {
    Container, Row, Col, Button,
} from 'reactstrap';
export default class DailyTotals extends React.Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;
    }

    render() {
        let {stats} = this.props;
        if (stats == undefined)
            return (
                <div>

                </div>
            )
        else
            return (
                <Row>
                    <Col sm="3">
                        <Stat title="Unique users:" value={stats.uniq_users}/>
                        <Stat title="Total Data:" value={present_data(stats.bytes)}/>
                    </Col>
                    <Col sm="3">
                        <Stat title="Domains:" value={stats.domains}/>
                        <Stat title="Total Time:" value={present_clock(stats.seconds)}/>
                    </Col>
                    <Col sm="3">
                        <Stat title="Sessions:" value={stats.stop}/>
                        <Stat title="Avg. data:" value={present_data(stats.bytes/stats.uniq_users) + '/User'}/>
                    </Col>
                    <Col sm="3">
                        <Stat title="Sources:" value={stats.sources}/>
                        <Stat title="Avg. Time:" value={present_clock(stats.seconds/stats.uniq_users) + ' /User'}/>
                    </Col>
                </Row>
            )
    }
}

function present_data(data) {
    if (data / 1073741824 > 1)
        return '{0} Gb'.format((data / 1073741824).toFixed(2));
    else if (data / 1048576 > 1)
        return '{0} Mb'.format((data / 1048576).toFixed(2));
    else if (data / 1024 > 1)
        return '{0} Kb'.format((data / 1024).toFixed(2));
    return '{0} b'.format(data);
}

function present_clock(data) {
    let h = Math.floor(data / 3600);
    let m = Math.floor(data % 3600 / 60);
    let s = Math.floor(data % 3600 % 60);
    return '{0}:{1}:{2}'.format(
        h > 9 ? h : '0' + h,
        m > 9 ? m : '0' + m,
        s > 9 ? s : '0' + s,
    )
}

class Stat extends React.Component {
    render() {
        return (
            <Row className="dailyStatsContainer">
                <Col sm="5" className="dailyStatsTitle">{this.props.title}</Col>
                <Col sm="7" className="dailyStatsValue">
                    {this.props.value.constructor == Array
                        ?
                        this.props.value.map((val, index) => {
                            return <div key={index}>{val}</div>
                        })
                        :
                        this.props.value
                    }
                </Col>
            </Row>
        )
    }
}