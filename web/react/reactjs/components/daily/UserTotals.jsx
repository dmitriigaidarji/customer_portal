import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import {
    Link
} from 'react-router-dom'
import {
    Row, Col
} from 'reactstrap';

let styles = {
    block: {
      marginTop: '15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '110%'
    },
    filters: {
        fontSize: '90%',
        fontWeight: 400,
        marginLeft: '15px'
    },
    alright: {
        textAlign: 'right',
        paddingRight: 0
    }
};
export default class UserTotals extends React.Component {
    constructor() {
        super();
        this.state = {
            showFilters: false
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    render() {
        let {stats, date} = this.props;
        let users = [];
        if (stats == undefined)
            return (
                <div>
                    <h5>Loading User Totals...</h5>
                </div>
            );
        if (stats.length == 0)
            return (
                <div>No data</div>
            );
        if (stats != undefined) {
            function getUser(stat) {
                for (let i = 0; i < users.length; i++)
                    if (users[i].cui == stat.cui)
                        return users[i]
                return {
                    cui: stat.cui,
                    last_seen: parseInt(stat.timestamp),
                    nas_identifier: stat.nas_identifier == '' ? stat.nas_ip : stat.nas_identifier,
                    username: stat.username,
                    username_short: stat.username.split('@')[0].length == 64 ? stat.username.replace('@' + stat.realm, '') : stat.username,
                    charge_user: stat.cui,
                    customer: stat.charge_customer,
                    count: 0,
                    time: 0,
                    bytes_in: 0,
                    bytes_out: 0,
                    giga_in: 0,
                    giga_out: 0
                }
            }

            function setArrayVal(obj, key, value) {
                for (let j = 0; j < obj[key].length; j++) {
                    if (obj[key][j] == value)
                        return;
                }
                obj[key].push(value)
            }

            function setUser(user) {
                for (let i = 0; i < users.length; i++)
                    if (users[i].cui == user.cui) {
                        users[i] = user;
                        return;
                    }
                users.push(user);
            }


            for (let i = 0; i < stats.length; i++) {
                let user = getUser(stats[i]);
                user.count = user.count + 1
                user.time = user.time + stats[i].usage.secs;
                user.bytes_in = user.bytes_in + stats[i].usage.bytes_in;
                user.bytes_out = user.bytes_out + stats[i].usage.bytes_out;
                user.giga_in = user.giga_in + stats[i].usage.giga_in;
                user.giga_out = user.giga_out + stats[i].usage.giga_out;
                let timestamp = parseInt(stats[i].timestamp)
                if (timestamp > user.last_seen) {
                    user.last_seen = timestamp;
                    user.nas_identifier = stats[i].nas_identifier == '' ? stats[i].nas_ip : stats[i].nas_identifier
                }
                setUser(user)
            }
        }
        function present_data(data) {
            if (data / 1073741824 > 1)
                return '{0} Gb'.format((data / 1073741824).toFixed(2));
            else if (data / 1048576 > 1)
                return '{0} Mb'.format((data / 1048576).toFixed(2));
            else if (data / 1024 > 1)
                return '{0} Kb'.format((data / 1024).toFixed(2));
            return '{0} b'.format(data);
        }

        function present_clock(data) {
            let h = Math.floor(data / 3600);
            let m = Math.floor(data % 3600 / 60);
            let s = Math.floor(data % 3600 % 60);
            return '{0}:{1}:{2}'.format(
                h > 9 ? h : '0' + h,
                m > 9 ? m : '0' + m,
                s > 9 ? s : '0' + s,
            )
        }

        let columns = [
            {
                id: 'lastSeen',
                Header: 'Last Seen (GMT)',
                accessor: d => (new Date(d.last_seen * 1000)).toISOString().replace('T', ' ').substring(0, 19),
                width: 100
            },
            {
                Header: 'NAS Identifier',
                accessor: 'nas_identifier',
                width: 120
            },
            {
                Header: 'Username / Token',
                accessor: 'username_short',
                width: 340,
                Cell: (props) => {
                    let {cui} = props.original;
                    if (cui == '')
                        return props.value;
                    return <Link
                        to={`/${process.env.PUBLIC_URL}/users/${props.original.cui}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                }

            },
            {
                id: 'cui',
                Header: 'Charge User',
                accessor: user => user.cui == '' ? '(unknown)' : user.cui,
                Cell: (props) => {
                    if (props.value == '(unknown)')
                        return props.value;
                    return <Link to={`/${process.env.PUBLIC_URL}/users/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                }
            },
            {
                Header: 'Customer',
                accessor: 'customer',
                Cell: props => <Link
                                    to={`/${process.env.PUBLIC_URL}/customers/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>,
                width: 60
            },
            {
                Header: 'Count',
                accessor: 'count',
                width: 60
            },
            {
                id: 'totalTime',
                Header: 'Total Time',
                accessor: d => present_clock(d.time),
                Cell: (props) => <div style={{textAlign:'center'}}>{props.value}</div>,
                width: 80
            },
            {
                id: 'dataInOut',
                Header: 'Data In / Out',
                accessor: user => user.bytes_in + user.bytes_out + user.giga_in * 2147483648 + user.giga_out * 2147483648,
                Cell: props => {
                    let res = present_data(props.original.bytes_in + props.original.bytes_out + props.original.giga_in * 2147483648 + props.original.giga_out * 2147483648)
                    let split = res.split(' ');
                    return <Row>
                        <Col sm="7" style={styles.alright}>
                            {split[0]}
                        </Col>
                        <Col sm="5">
                            {split[1]}
                        </Col>
                    </Row>
                },
                width: 100
            }
        ];
        let pageSize = users.length > 10 ? 10 : users.length;
        let showPagination = pageSize < users.length;
        return (
            <div>
                <div style={styles.block}>
                    <span style={styles.title}>User Totals
                        <a onClick={this.toggleTableFilters} style={styles.filters} href="javascript:void(0);">Toggle filters</a>
                    </span>
                </div>
                <ReactTable
                    className='-striped -highlight'
                    data={users}
                    showPagination={showPagination}
                    columns={columns}
                    defaultPageSize={pageSize}
                    filterable={this.state.showFilters}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] !== undefined ? String(row[id]).includes(filter.value) : true
                    }}
                    defaultSorted={[{
                        id: 'totalTime',
                        desc: true
                    }]}
                />
            </div>
        )
    }
}