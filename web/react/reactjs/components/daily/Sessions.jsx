import React from "react"
import {
    Link
} from 'react-router-dom'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import TransactionContainer from '../../containers/TransactionContainer'
import {
    Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup
} from 'reactstrap';
let styles = {
    block: {
        marginTop: '15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '110%'
    },
    filters: {
        fontSize: '90%',
        fontWeight: 400,
        marginLeft: '15px'
    }
};
export default class Sessions extends React.Component {
    constructor() {
        super();
        this.state = {
            sorting: [{id: 'transTime', desc: true}],
            showFilters: false,
            transactionModal: false,
            transaction_id: undefined
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
        this.sortChange = this.sortChange.bind(this)
        this.setTransaction = this.setTransaction.bind(this)
        this.transactionToggle = this.transactionToggle.bind(this)
    }

    sortChange(column, shift) {
        let sort = {id: column.id};
        if (this.state.sorting.length && this.state.sorting[0].id == column.id)
            this.state.sorting[0].asc ? sort.desc = true : sort.asc = true
        else
            sort.asc = true;
        this.setState({
            sorting: [sort]
        })
    }

    transactionToggle() {
        this.setState({
            transactionModal: !this.state.transactionModal
        })
    }

    setTransaction(transaction_id) {
        this.setState({transaction_id: transaction_id, transactionModal: true})
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    render() {
        let {stats, date} = this.props;
        if (stats == undefined)
            return (
                <div>
                    <h5>Loading User Sessions...</h5>
                </div>
            );
        if (stats.length == 0)
            return (
                <div>No data</div>
            );
        let columns = [
            {
                id: 'transTime',
                Header: 'Transaction Time',
                accessor: d => (new Date(parseInt(d.timestamp) * 1000)).toISOString().replace('T', ' ').substring(0, 19),
                width: 100
            },
            {
                Header: 'Transaction ID',
                accessor: 'transaction_id',
                width: 180,
                Cell: (props) => <a href="javascript:void(0);"
                                      onClick={() => this.setTransaction(props.value)}>{props.value}</a>
            },
            {
                id: 'nasIdentifier',
                Header: 'Nas Identifier',
                accessor: stat => stat.nas_identifier == '' ? stat.nas_ip : stat.nas_identifier.replace(/-/g, ':'),
                width: 80
            },
            {
                Header: 'Session ID',
                accessor: 'sessionid',
                width: 70
            },
            {
                Header: 'Called Station',
                accessor: 'called_sid',
                width: 140
            },
            {
                id: 'callingStation',
                Header: 'Calling Station',
                accessor: stat => stat.calling_sid.replace(/-/g, ':'),
                width: 100
            },
            {
                id: 'cui',
                Header: 'Charge User',
                accessor: stat => stat.cui == '' ? '(unknown)' : stat.cui,
                Cell: (props) => {
                    if (props.value == '(unknown)')
                        return props.value;
                    return <Link
                        to={`/${process.env.PUBLIC_URL}/users/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                }
            },
            {
                Header: 'Customer',
                accessor: 'charge_customer',
                Cell: props => <Link
                    to={`/${process.env.PUBLIC_URL}/customers/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>,
                width: 60
            },
            {
                Header: 'Seconds',
                accessor: 'usage.secs',
                width: 60
            },
            {
                id: 'bytes',
                Header: 'Bytes',
                accessor: stat => stat.usage.bytes_in + stat.usage.bytes_out + stat.usage.giga_in * 2147483648 + stat.usage.giga_out * 2147483648,
                width: 60
            },
            {
                Header: 'Close Event',
                accessor: 'term_cause',
                width: 70
            }];
        let pageSize = stats.length > 20 ? 20 : stats.length;
        let showPagination = pageSize < stats.length;
        return (
            <div>
                <div style={styles.block}>
                    <span style={styles.title}>Sessions
                        <a onClick={this.toggleTableFilters} style={styles.filters} href="javascript:void(0);">Toggle filters</a>
                    </span>
                </div>
                <ReactTable
                    className='-striped -highlight'
                    data={stats}
                    showPagination={showPagination}
                    columns={columns}
                    defaultPageSize={pageSize}
                    filterable={this.state.showFilters}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] !== undefined ? String(row[id]).includes(filter.value) : true
                    }}
                    defaultSorted={this.state.sorting}
                    // onSortedChange={this.sortChange}
                />
                <Modal isOpen={this.state.transactionModal} toggle={this.transactionToggle} size="lg">
                    <ModalHeader toggle={this.transactionToggle}>Transaction {this.state.transaction_id}</ModalHeader>
                    <ModalBody>
                        <TransactionContainer
                            transaction_id={this.state.transaction_id}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.transactionToggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}