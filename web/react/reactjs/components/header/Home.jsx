import React from "react"
import {
    Link
} from 'react-router-dom'
export default class Home extends React.Component {
    render() {
        return (
            <Link className="navbar-brand" to={`/${process.env.PUBLIC_URL}/`}>
                <i className="fa fa-home" aria-hidden="true"></i> Home
            </Link>
        )
    }
}