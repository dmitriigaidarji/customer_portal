import React from "react"
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import '../../static/css/sunburst.css';
export default class Sunburst extends React.Component {
    constructor(props) {
        super(props)
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.stats != undefined)
            return true;
        return false;
    }

    parseData(data) {
        let root = {
            name: 'root',
            children: [
                {
                    name: 'iPhone',
                    children: []
                },
                {
                    name: 'android',
                    children: []
                }
            ]
        };

// Mapping of step names to colors.
        let colors = {
            "iPhone": 1,
            "android": 1
        };
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            for (let j = 0; j < item.devices.length; j++) {
                let device = item.devices[j];
                if (device.platform.device_model == 'iPhone') {
                    colors[device.version] = 2
                    colors[device.platform.ios_version] = 3
                    let found = false;
                    for (let k = 0; k < root.children[0].children.length; k++) {
                        if (root.children[0].children[k].name == device.platform.ios_version) {
                            found = true;
                            let vfound = false;
                            for (let l = 0; l < root.children[0].children[k].children.length; l++) {
                                if (root.children[0].children[k].children[l].name == device.version) {
                                    vfound = true;
                                    root.children[0].children[k].children[l].size++;
                                    break;
                                }
                            }
                            if (!vfound) {
                                root.children[0].children[k].children.push({
                                    name: device.version,
                                    size: 1
                                })
                            }
                            break;
                        }
                    }
                    if (!found) {
                        root.children[0].children.push(
                            {
                                name: device.platform.ios_version,
                                children: [
                                    {
                                        name: device.version,
                                        size: 1
                                    }
                                ]
                            }
                        )
                    }
                } else {

                }
            }
        }
        return [root, colors]
    }

    updateChart(data) {
        // Dimensions of sunburst.
        // let width = $("#chart").width();
        let width = 355;
        $('#explanation').css('left', width / 2 - 70);
        let height = 350;
        let radius = Math.min(width, height) / 2;

// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
        let b = {
            w: 75, h: 30, s: 3, t: 10
        };

        let parsed = this.parseData(data)
// Mapping of step names to colors.
        let colors = parsed[1]
        let colorarr = d3.scaleOrdinal(d3.schemeCategory20);


// Total size of all segments; we set this later, after loading the data.
        let totalSize = 0;

        let vis = d3.select("#sunburstChart");
        vis.selectAll("*").remove();

        vis = d3.select("#sunburstChart")
            .attr("width", width)
            .attr("height", height)
            .append("svg:g")
            .attr("id", "container")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        let partition = d3.partition()
            .size([2 * Math.PI, radius * radius]);

        let arc = d3.arc()
            .startAngle(function (d) {
                return d.x0;
            })
            .endAngle(function (d) {
                return d.x1;
            })
            .innerRadius(function (d) {
                return Math.sqrt(d.y0);
            })
            .outerRadius(function (d) {
                return Math.sqrt(d.y1);
            });
        createVisualization(parsed[0]);

// Main function to draw and set up the visualization, once we have the data.
        function createVisualization(json) {

            // Basic setup of page elements.
            initializeBreadcrumbTrail();
            drawLegend();
            d3.select("#togglelegend").on("click", toggleLegend);

            // Bounding circle underneath the sunburst, to make it easier to detect
            // when the mouse leaves the parent g.
            vis.append("svg:circle")
                .attr("r", radius)
                .style("opacity", 0);

            // Turn the data into a d3 hierarchy and calculate the sums.
            let root = d3.hierarchy(json)
                .sum(function (d) {
                    return d.size;
                })
                .sort(function (a, b) {
                    return b.value - a.value;
                });

            // For efficiency, filter nodes to keep only those large enough to see.
            let nodes = partition(root).descendants()
                .filter(function (d) {
                    return (d.x1 - d.x0 > 0.005); // 0.005 radians = 0.29 degrees
                });

            let grp = vis.data([json]).selectAll('path')
                .data(nodes)
                .enter().append("svg:g")
            grp.append('path')
                .attr("display", function (d) {
                    return d.depth ? null : "none";
                })
                .attr("d", arc)
                .attr("id", function (d, index) {
                    return 'arc' + index;
                })
                .attr("fill-rule", "evenodd")
                .style("fill", function (d) {
                    return colorarr(d.data.name)
                })
                .style("opacity", 1)
                .on("mouseover", mouseover)
            grp
                .append('text')
                .style("text-anchor", "middle")
                .attr("x", function (d) {
                    //     // if (d.depth == 1) return 1020;
                    //     // if (0.5 < d.x1 - d.x0 && d.x1 - d.x0 < 0.7)
                    //     //     return (d.x1 - d.x0) * 180 / Math.PI / 2;
                    let multiplier = 180 / Math.PI;
                    if (d.depth == 3)
                        multiplier = 90;
                    else if (d.depth == 2)
                        multiplier = 70;
                    else if (d.depth == 1)
                        if (d.parent.children.length == 1) multiplier = 168;
                        else{
                            let count = 0;
                            for (let i = 0; i < d.parent.children.length; i ++) {
                                let cchild = d.parent.children[i]
                                if (cchild.children != undefined) count++;
                            }
                            if (count < 2) multiplier = 168;
                        }
                    return (d.x1 - d.x0) * multiplier;
                })
                .attr("dy", (d) => {
                    if (d.depth == 3)
                        return 15;
                    if (d.depth == 2)
                        return 18;
                    return 23;
                })
                .append('textPath')
                .attr("stroke", "black")
                .attr("xlink:href", function (d, index) {
                    return "#arc" + index;
                })
                .text(function (d) {
                    if (d.depth == 0)
                        return '';
                    else {
                        if (d.x1 - d.x0 < 0.4) return d.value;
                        return d.data.name + " : " + d.value;
                    }
                });

            // grp.append('text')
            //     .style("text-anchor","middle")
            //     .append('textPath')
            //     .attr("stroke", "black")
            //     .attr("xlink:href", function (d, index) {
            //         return "#arc" + index;
            //     })
            //     .attr("startOffset", "10%")
            //     .text(function (d) {
            //         return d.value;
            //         if (d.depth == 0)
            //             return '';
            //         else {
            //             if (d.x1 - d.x0 < 0.5) return d.value;
            //             return d.data.name + " : " + d.value;
            //         }
            //     });
            // vis.data([json]).selectAll('text')
            //     .data(nodes)
            //     .enter()
            //     .append('text')
            //     // .attr("dx", function (d) {
            //     //     // if (d.depth == 1) return 1020;
            //     //     // if (0.5 < d.x1 - d.x0 && d.x1 - d.x0 < 0.7)
            //     //     //     return (d.x1 - d.x0) * 180 / Math.PI / 2;
            //     //     return (d.x1 - d.x0) * 180 / Math.PI;
            //     // })
            //     // .attr("dy", 18)
            //     // .attr('fff', function (d) {
            //     //     return d.x1 * 180 / Math.PI;
            //     // })
            //     // .style("text-anchor","end")
            //     .attr("startOffset", "50%")
            //     .append("textPath")
            //     .attr("stroke", "black")
            //
            //
            //     .attr("xlink:href", function (d, index) {
            //         return "#arc" + index;
            //     })
            //     .style("text-anchor","middle")
            //     .text(function (d) {
            //         return d.value;
            //         if (d.depth == 0)
            //             return '';
            //         else {
            //             if (d.x1 - d.x0 < 0.5) return d.value;
            //             return d.data.name + " : " + d.value;
            //         }
            //     });

            // Add the mouseleave handler to the bounding circle.
            d3.select("#container").on("mouseleave", mouseleave);

            // Get total size of the tree = value of root node from partition.
            totalSize = grp.datum().value;
        };

// Fade all but the current sequence, and show it in the breadcrumb trail.
        function mouseover(d) {
            console.log(d);
            console.log(d.x1 - d.x0)
            let percentage = (100 * d.value / totalSize).toPrecision(3);
            let percentageString = percentage + "%";
            if (percentage < 0.1) {
                percentageString = "< 0.1%";
            }

            d3.select("#percentage")
                .text(d.value);

            let desc = '';
            if (d.depth == 1) {
                desc = 'people use {0} device'.format(d.data.name)
            } else if (d.depth == 2) {
                desc = 'people use "{0}" system version'.format(d.data.name)
            } else if (d.depth == 3) {
                desc = 'people use "{0}" app version'.format(d.data.name)
            }
            d3.select("#percentagedesc")
                .text(desc);

            d3.select("#explanation")
                .style("visibility", "");

            let sequenceArray = d.ancestors().reverse();
            sequenceArray.shift(); // remove root node from the array
            updateBreadcrumbs(sequenceArray, percentageString);

            // Fade all the segments.
            vis.selectAll("path")
                .style("opacity", 0.3);

            // Then highlight only those that are an ancestor of the current segment.
            vis.selectAll("path")
                .filter(function (node) {
                    return (sequenceArray.indexOf(node) >= 0);
                })
                .style("opacity", 1);
        }

// Restore everything to full opacity when moving off the visualization.
        function mouseleave(d) {

            // Hide the breadcrumb trail
            d3.select("#trail")
                .style("visibility", "hidden");

            // Deactivate all segments during transition.
            vis.selectAll("path").on("mouseover", null);

            // Transition each segment to full opacity and then reactivate it.
            vis.selectAll("path")
                .transition()
                .duration(100)
                .style("opacity", 1)
                .on("end", function () {
                    d3.select(this).on("mouseover", mouseover);
                });

            // d3.select("#explanation")
            //     .style("visibility", "hidden");
        }

        function initializeBreadcrumbTrail() {
            // Add the svg area.
            let trail = d3.select("#sunburstChartSequence");
            trail.selectAll("*").remove();
            trail
                .attr("width", width)
                .attr("height", 50)
                .attr("id", "trail");
            // Add the label at the end, for the percentage.
            trail.append("svg:text")
                .attr("id", "endlabel")
                .style("fill", "#000");
        }

// Generate a string that describes the points of a breadcrumb polygon.
        function breadcrumbPoints(d, i) {
            let points = [];
            points.push("0,0");
            points.push(b.w + ",0");
            points.push(b.w + b.t + "," + (b.h / 2));
            points.push(b.w + "," + b.h);
            points.push("0," + b.h);
            if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
                points.push(b.t + "," + (b.h / 2));
            }
            return points.join(" ");
        }

// Update the breadcrumb trail to show the current sequence and percentage.
        function updateBreadcrumbs(nodeArray, percentageString) {

            // Data join; key function combines name and depth (= position in sequence).
            let trail = d3.select("#trail")
                .selectAll("g")
                .data(nodeArray, function (d) {
                    return d.data.name + d.depth;
                });

            // Remove exiting nodes.
            trail.exit().remove();

            // Add breadcrumb and label for entering nodes.
            let entering = trail.enter().append("svg:g");

            entering.append("svg:polygon")
                .attr("points", breadcrumbPoints)
                .style("fill", function (d) {
                    return colorarr(d.data.name);
                });

            entering.append("svg:text")
                .attr("x", (b.w + b.t) / 2)
                .attr("y", b.h / 2)
                .attr("dy", "0.35em")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.data.name;
                });

            // Merge enter and update selections; set position for all nodes.
            entering.merge(trail).attr("transform", function (d, i) {
                return "translate(" + i * (b.w + b.s) + ", 0)";
            });

            // Now move and update the percentage at the end.
            d3.select("#trail").select("#endlabel")
                .attr("x", (nodeArray.length + 0.5) * (b.w + b.s))
                .attr("y", b.h / 2)
                .attr("dy", "0.35em")
                .attr("text-anchor", "middle")
                .text(percentageString);

            // Make the breadcrumb trail visible, if it's hidden.
            d3.select("#trail")
                .style("visibility", "");

        }

        function drawLegend() {

            // Dimensions of legend item: width, height, spacing, radius of rounded rect.
            let li = {
                w: 75, h: 15, s: 3, r: 3
            };

            let legend = d3.select("#legendChart");
            legend.selectAll("*").remove();
            legend
                .attr("width", li.w)
                .attr("height", d3.keys(colors).length * (li.h + li.s));

            let g = legend.selectAll("g")
                .data(d3.entries(colors))
                .enter().append("svg:g")
                // .filter(function (d) {
                //     console.log(d)
                //     return d.depth < 2
                // })
                .attr("transform", function (d, i) {
                    return "translate(0," + i * (li.h + li.s) + ")";
                });

            g.append("svg:rect")
                .attr("rx", li.r)
                .attr("ry", li.r)
                .attr("width", li.w)
                .attr("height", li.h)
                .style("fill", function (d) {
                    return colorarr(d.key);
                });

            g.append("svg:text")
                .attr("x", li.w / 2)
                .attr("y", li.h / 2)
                .attr("dy", "0.35em")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.key;
                });
        }

        function toggleLegend() {
            let legend = d3.select("#legend");
            if (legend.style("visibility") == "hidden") {
                legend.style("visibility", "");
            } else {
                legend.style("visibility", "hidden");
            }
        }

    }

    render() {
        let {stats} = this.props;
        if (stats == undefined)
            return (<div></div>);
        return (
            <Row>
                <Col sm="5" id="main">
                    <h5>Users Info</h5>
                    <div id="sequence">
                        <svg id="sunburstChartSequence"/>
                    </div>
                    <div id="chart">
                        <div id="explanation">
                            <span id="percentage"></span><br/>
                            <span id="percentagedesc"></span>
                        </div>
                        <svg id="sunburstChart"/>
                    </div>
                </Col>
                <Col sm="6" id="sidebar"><input type="checkbox" id="togglelegend"/> Legend<br/>
                    <div id="legend" style={{visibility: 'hidden'}}>
                        <svg id="legendChart"/>
                    </div>
                </Col>
            </Row>
        )
    }
}