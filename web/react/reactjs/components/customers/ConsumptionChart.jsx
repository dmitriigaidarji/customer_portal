import React from "react"
import $ from 'jquery'
import {
    Link
} from 'react-router-dom'
export default class ConsumptionChart extends React.Component {
    constructor(props) {
        super(props)
        this.updateChart = this.updateChart.bind(this)
        this.shouldRender = false;
    }


    componentDidMount() {
        let {stats} = this.props;
        if (stats != undefined && this.shouldRender && stats.length > 0)
            this.updateChart(this.props.stats);
    }

    componentDidUpdate() {
        let {stats} = this.props;
        let _this = this;
        if (stats != undefined && this.shouldRender && stats.length > 0) {
            if (this.updateConsumptionChart != undefined)
                window.clearTimeout(this.updateConsumptionChart);
            this.updateConsumptionChart = window.setTimeout(() => {
                _this.updateChart(this.props.stats);
            }, 300)
        }
    }

    shouldComponentUpdate(nextProps) {
        console.log(nextProps)
        return nextProps.stats != undefined && nextProps.startDate != undefined
        // &&
        // (this.props.stats == undefined ||
        //     this.props.startDate.format('MM') !== nextProps.startDate.format('MM') ||
        //     nextProps.stats.length != this.props.stats.length ||
        //     (nextProps.stats.length > 0 && nextProps.stats[0].usage_mb != this.props.stats[0].usage_mb)
        // )
    }

    updateChart(data) {
        let _this = this;
        console.log(data)
        let {full_width, startDate, history} = this.props;
        if (data != undefined && data.length > 0) {
            let filteredData = [];
            let showInGb = false;

            for (let i = 0; i < data.length; i++) {
                data[i].usage_mb = data[i].usage_current.total_bytes / 1048576;
                if (data[i].usage_mb > 1) filteredData.push(data[i])
                if (data[i].usage_mb > 1000) showInGb = true;
            }
            console.log(filteredData)
            data = filteredData
            data.sort(function (a, b) {
                return b.usage_mb - a.usage_mb;
            });
            data = data.slice(0, 10);
            data.sort(function (a, b) {
                return a.usage_mb - b.usage_mb;
            });
            let svg = d3.select("#consumptionChart");
            if (data.length == 0){
                svg.selectAll("*").remove();
                svg.attr('height', 0);
            }else {
                if (data.length == 1)
                    svg.attr('height', 100);
                else if (data.length == 2)
                    svg.attr('height', 120);
                else if (data.length == 3)
                    svg.attr('height', 200);
                else svg.attr('height', 300);
                svg.attr('width', $("#consumptionChartParent").width());
                svg.selectAll("*").remove();
                let margin = {top: 5, right: 10, bottom: 30, left: 200},
                    width = +svg.attr("width") - margin.left - margin.right,//+svg.attr("width") - margin.left - margin.right,
                    height = +svg.attr("height") - margin.top - margin.bottom,
                    legendWidth = 19,
                    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                console.log('02')
                let div = d3.select('#consumptiontooltip');

                let x = d3.scaleLinear().range([0, width - 10]);
                let y = d3.scaleBand().range([height, 0]);
                let a = 1;
                console.log('02')
                x.domain([0, d3.max(data, function (d) {
                    return d.usage_mb;
                })]);
                let fontSize = '80%'
                console.log('1')
                y.domain(data.map(function (d) {
                    if (d.nai.length > 25) fontSize = '60%';
                    if (d.nai.length >= 35) fontSize = '40%'
                    return d.nai;
                })).padding(0.1);
                g.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x).tickSizeInner([-height]).tickFormat((i) => {
                        if (showInGb)
                            return (i / 1024).toFixed(1)
                        return i
                    }))
                    .append("text")
                    .attr("x", x(x.ticks().pop()) - 5)
                    .attr("y", 18)
                    .attr("dy", "0.32em")
                    .attr("fill", "#000")
                    .attr("font-weight", "bold")
                    .attr("text-anchor", "start")
                    .text(showInGb ? "Gb" : 'Mb');

                g.append("g")
                    .attr("class", "yaxis")
                    .call(d3.axisLeft(y))
                    .style("font-size", fontSize)
                d3.select('#consumptionChart .yaxis')
                    .selectAll('.tick')
                    .attr('cursor', 'pointer')
                    .on('click', (item) => {
                        history.push(`/${process.env.PUBLIC_URL}/users/${item}/${startDate.format('YYYY')}/${startDate.format('MM')}/`)
                    });
                g.selectAll(".bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("fill", "#359cd6")
                    .attr("class", "bar")
                    .attr("x", 0)
                    .on("mousemove", function (d) {
                        let coordinates = d3.mouse(this);
                        let pt = svg.node().createSVGPoint();
                        pt.x = coordinates[0];
                        pt.y = coordinates[1];
                        pt = pt.matrixTransform(this.getCTM());
                        let xPosition = pt.x - 120;
                        let yPosition = pt.y - 50;
                        div.style("display", 'block');
                        div.html(
                            '<table style="text-align: start"><tbody><tr><td><span style="font-weight: bold;">User: </span></td><td>' + d.nai + '</td></tr>' +
                            '<tr><td><span style="font-weight: bold;">Total: </span></td><td>' + present_data(d.usage_current.total_bytes) + '</td></tr></tbody></table>'
                        )
                            .style("left", (xPosition + 40) + "px")
                            .style("top", (yPosition + 20) + "px");
                    })
                    .on("mouseout", function (d) {
                        div.style("display", 'none');
                    })
                    .transition()
                    .duration(200)
                    .delay(function (d, i) {
                        return i * 50;
                    })
                    .attr("height", y.bandwidth())
                    .attr("y", function (d) {
                        return y(d.nai);
                    })
                    .attr("width", function (d) {
                        return x(d.usage_mb);
                    })
            }

            // x.domain(data.map(function (d) {
            //     return d.nai;
            // }));
            // y.domain([0, d3.max(data, function (d) {
            //     return d.usage_mb;
            // })]).nice();

            // g.append("g")
            //     .attr("class", "axis")
            //     .attr("transform", "translate(0," + height + ")")
            //     .call(d3.axisBottom(x));
            //
            // g.append("g")
            //     .attr("class", "axis")
            //     .call(d3.axisLeft(y).ticks(null, "s"))
            //     .append("text")
            //     .attr("transform", "rotate(-90)")
            //     .attr("y", 6)
            //     .attr("dy", ".71em")
            //     .style("text-anchor", "end")
            //     .text("Usage");

            // g.selectAll(".bar")
            //     .data(data)
            //     .enter().append("rect")
            //     .attr("class", "bar")
            //     .attr("x", function (d) {
            //         return x(d.nai);
            //     })
            //     .attr("width", x.bandwidth())
            //     .attr("y", function (d) {
            //         return y(d.usage_mb);
            //     })
            //     .attr("height", function (d) {
            //         return height - y(d.usage_mb);
            //     })

        }
    }

    render() {
        let {stats} = this.props;
        console.log(stats)
        this.shouldRender = false;
        if (stats == undefined || stats.length == 0)
            return (<div></div>)
        this.shouldRender = true;
        return (
            <div id="consumptionChartParent" style={{position: 'relative'}}>
                <div style={{fontWeight: 'bold', fontSize: '85%'}}>Top Users by Data Consumption (Min: 1mb)</div>
                <svg id="consumptionChart" width="500" height="300"></svg>
                <div id='consumptiontooltip' className="tooltip"></div>
            </div>
        )
    }
}
