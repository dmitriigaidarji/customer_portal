import React from "react"
import Api from '../../Api';
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import moment from 'moment'
export default class ConnectionsChart extends React.Component {
    constructor(props) {
        super(props);
        this.uuid = props.uuid ? props.uuid : 'c' + guid();
        this.modes = ['data used', 'connections', 'duration'];
        let mode = this.modes[0]
        if (props.mode != undefined)
            mode = this.modes[parseInt(props.mode)];
        this.state = {
            mode: mode
        };
        this.updateChart = this.updateChart.bind(this)
    }


    componentDidMount() {
        let {stats} = this.props;
        let _this = this;
        if (stats != undefined && (stats.activations.count != 0 || stats.connections.count != 0)) {
            if (this.updateConnectionsChart != undefined)
                window.clearTimeout(this.updateConnectionsChart);
            this.updateConnectionsChart = window.setTimeout(() => {
                _this.updateChart(this.props.stats);
            }, 500)
        }
    }

    componentDidUpdate() {
        let {stats} = this.props;
        let _this = this;
        if (stats != undefined && (stats.activations.count != 0 || stats.connections.count != 0)) {
            if (this.updateConnectionsChart != undefined)
                window.clearTimeout(this.updateConnectionsChart);
            this.updateConnectionsChart = window.setTimeout(() => {
                _this.updateChart(this.props.stats);
            }, 200)
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.stats != undefined &&
            (this.props.stats == undefined ||
                this.props.startDate.format('MM') !== nextProps.startDate.format('MM') ||
                nextState.mode !== this.state.mode ||
                nextProps.stats.activations.count != this.props.stats.activations.count ||
                nextProps.stats.connections.count != this.props.stats.connections.count
            )
    }

    updateChart(data) {
        let _this = this;
        let {mode} = this.state;
        if (data != undefined) {
            let initData = [], minDate, maxDate;
            let {match, history} = this.props;
            for (let property in data.activations.data) {
                if (data.activations.data.hasOwnProperty(property)) {
                    let date = moment.utc("{0}-{1}-{2}".format(
                        property.substring(0, 4),
                        property.substring(4, 6),
                        property.substring(6, 8)
                    ));
                    initData.push({
                        date: date,
                        datestr: property,
                        date_str: date.format('DD'),
                        activations: data.activations.data[property],
                        connections: 0,
                        usage: 0,
                        duration: 0,
                        total: data.activations.data[property]
                    })
                }
            }
            for (let property in data.connections.data) {
                if (data.connections.data.hasOwnProperty(property)) {
                    let found = false;
                    for (let i = 0; i < initData.length; i++) {
                        if (initData[i].datestr == property) {
                            initData[i].connections = data.connections.data[property];
                            initData[i].total = data.connections.data[property] + initData[i].activations;
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        let date = moment.utc("{0}-{1}-{2}".format(
                            property.substring(0, 4),
                            property.substring(4, 6),
                            property.substring(6, 8)
                        ));
                        initData.push({
                            date: date,
                            datestr: property,
                            date_str: date.format('DD'),
                            activations: 0,
                            usage: 0,
                            duration: 0,
                            connections: data.connections.data[property],
                            total: data.connections.data[property]
                        })
                    }
                }
            }
            for (let property in data.usage.data) {
                if (data.usage.data.hasOwnProperty(property)) {
                    let found = false;
                    for (let i = 0; i < initData.length; i++) {
                        if (initData[i].datestr == property) {
                            initData[i].usage = data.usage.data[property];
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        let date = moment.utc("{0}-{1}-{2}".format(
                            property.substring(0, 4),
                            property.substring(4, 6),
                            property.substring(6, 8)
                        ));
                        initData.push({
                            date: date,
                            datestr: property,
                            date_str: date.format('DD'),
                            duration: 0,
                            activations: 0,
                            connections: 0,
                            usage: data.usage.data[property],
                        })
                    }
                }
            }
            for (let property in data.duration.data) {
                if (data.duration.data.hasOwnProperty(property)) {
                    let found = false;
                    for (let i = 0; i < initData.length; i++) {
                        if (initData[i].datestr == property) {
                            initData[i].duration = data.duration.data[property];
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        let date = moment.utc("{0}-{1}-{2}".format(
                            property.substring(0, 4),
                            property.substring(4, 6),
                            property.substring(6, 8)
                        ));
                        initData.push({
                            date: date,
                            datestr: property,
                            date_str: date.format('DD'),
                            duration: data.duration.data[property],
                            activations: 0,
                            connections: 0,
                            usage: 0,
                        })
                    }
                }
            }
            if (initData.length > 0) {
                data = initData;
                data.sort(function (a, b) {
                    return a.date - b.date;
                });
                let svg = d3.select("#" + this.uuid);
                svg.attr('width', $("#" + this.uuid + 'parent').width());
                svg.selectAll("*").remove();
                let margin = {top: 20, right: 10, bottom: 30, left: 60},
                    width =  +svg.attr("width") - margin.left - margin.right, //460
                    height = +svg.attr("height") - margin.top - margin.bottom,
                    legendWidth = 19,
                    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                let x = d3.scaleBand()
                    .rangeRound([0, width])
                    .paddingInner(0.05)
                    .align(0.1);

                let y = d3.scaleLinear()
                    .rangeRound([height, 0]);

                let z = d3.scaleOrdinal()
                    .range(["#359cd6", "#00ba73", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

                let keys;
                if (mode === _this.modes[0])
                    keys = ['usage']
                else if (mode === _this.modes[1])
                    keys = ['connections', 'activations'];
                else if (mode === _this.modes[2])
                    keys = ['duration'];

                let div = d3.select('#' + this.uuid + 'tooltip');

                function getDaysInMonth(date) {
                    date = date.startOf('month');
                    let month = date.month();
                    let days = [];
                    while (date.month() === month) {
                        days.push(moment(date));
                        date.add(1, 'days')
                    }
                    return days;
                }

                let monthdates = getDaysInMonth(moment(data[data.length - 1].date));
                x.domain(monthdates);

                y.domain([0, d3.max(data, function (d) {
                    if (mode === _this.modes[0])
                        return d.usage;
                    else if (mode === _this.modes[1])
                        return d.total;
                    else if (mode === _this.modes[2])
                        return d.duration;
                    return d.total;
                })]).nice();
                z.domain(keys);

                g.append("g")
                    .selectAll("g")
                    .data(d3.stack().keys(keys)(data))
                    .enter().append("g")
                    .attr("fill", function (d) {
                        return z(d.key);
                    })
                    .selectAll("rect")
                    .data(function (d) {
                        return d;
                    })
                    .enter().append("rect")
                    .attr('cursor', 'pointer')
                    .on("mousemove", function (d) {
                        let coordinates = d3.mouse(this);
                        let pt = svg.node().createSVGPoint();
                        pt.x = coordinates[0];
                        pt.y = coordinates[1];
                        pt = pt.matrixTransform(this.getCTM());
                        let xPosition = pt.x - 100;
                        let yPosition = pt.y - 40;
                        div.style("display", 'block');
                        div.html(
                            '<div style="text-align: start;"><span style="font-weight: bold;">Activations:</span> ' + d.data.activations + '</br>' +
                            '<span style="font-weight: bold;">Connections: </span> ' + d.data.connections +
                            '</br><span style="font-weight: bold;">Usage: </span> ' + present_data(d.data.usage) +
                            '</br><span style="font-weight: bold;">Duration: </span> ' + present_clock(d.data.duration) + '</div>'
                        )
                            .style("left", (xPosition + 40) + "px")
                            .style("top", (yPosition - 50) + "px")
                            .style('min-width', '118px')
                    })
                    .on("mouseout", function (d) {
                        div.style("display", 'none');
                    })
                    .on("click", (d) => {
                        history.push(`${match.url}${match.url.slice(-1) == '/' ? d.data.date_str : '/' + d.data.date_str}/`)
                    })
                    .attr("x", function (d) {
                        return x(d.data.date);
                    })
                    .attr("y", function (d, i) {
                        return height;
                    })
                    .attr("height", 0)
                    .transition()
                    .duration(200)
                    .delay(function (d, i) {
                        return i * 20;
                    })
                    .attr("y", function (d) {
                        return y(d[1]);
                    })
                    .attr("height", function (d) {
                        return y(d[0]) - y(d[1]);
                    })
                    .attr("width", x.bandwidth())


                g.append("g")
                    .attr("id", this.uuid+"connectionChartXAxis")
                    .attr("class", "axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x).ticks(d3.utcDay, 1).tickFormat(d3.utcFormat("%d")));

                d3.select('#' + this.uuid+"connectionChartXAxis")
                    .selectAll('.tick')
                    .attr('cursor', 'pointer')
                    .on('click', (date) => {
                        history.push(`${match.url}${match.url.slice(-1) == '/' ? date.format('DD') : '/' + date.format('DD')}/`)
                    });

                let countLabel = "Number of connections";
                if (mode === _this.modes[0])
                    countLabel = 'Data usage'
                else if (mode === _this.modes[2])
                    countLabel = 'Hours elapsed'
                g.append("g")
                    .attr("class", "axis")
                    .call(mode === _this.modes[1] ?
                        d3.axisLeft(y).ticks(null, "s")
                        :
                        d3.axisLeft(y).ticks().tickFormat((val) =>
                            mode === _this.modes[0] ?
                                '{0} Gb'.format((val / 1073741824).toFixed(1))
                                :
                                present_clock(val).substring(0, 5)
                        )
                    )
                    .append("text")
                    .attr("x", -120)
                    .attr("y", y(y.ticks().pop()) - 50)
                    .attr("dy", "0.32em")
                    .attr("fill", "#000")
                    .attr('font-size', '130%')
                    .attr("text-anchor", "start")
                    .text(countLabel)
                    .style("text-anchor", "middle")
                    .attr("transform", "rotate(-90)" );

                // let legend = g.append("g")
                //     .attr("font-family", "sans-serif")
                //     .attr("font-size", 10)
                //     .attr("text-anchor", "end")
                //     .selectAll("g")
                //     .data(keys.slice().reverse())
                //     .enter().append("g")
                //     .attr("transform", function (d, i) {
                //         return "translate(0," + i * 20 + ")";
                //     });
                //
                // legend.append("rect")
                //     .attr("x", width + 55)
                //     .attr("width", 19)
                //     .attr("height", 19)
                //     .attr("fill", z);
                //
                // legend.append("text")
                //     .attr("x", width + 50)
                //     .attr("y", 9.5)
                //     .attr("dy", "0.32em")
                //     .text(function (d) {
                //         return d.capitalizeFirstLetter();
                //     });
            }
        }
    }

    render() {
        let {stats} = this.props;
        return (
            <div id={this.uuid + 'parent'} style={{position: 'relative'}}>
                <svg id={this.uuid} width="500" height="300"></svg>
                <div id={this.uuid + 'tooltip'} className="tooltip"></div>
                <div style={{
                    position: 'absolute',
                    right: '100px',
                    bottom: '0',
                    fontStyle: 'italic',
                    fontSize: '0.8em'
                }}>* Click on a day or a bar to view daily details
                </div>
            </div>
        )
    }
}
