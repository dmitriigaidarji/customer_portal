import React from "react"
import ConnectionsChart from './ConnectionsChart'
import ConsumptionChart from './ConsumptionChart'
import {
    Container, Row, Col
} from 'reactstrap';
export default class MonthlyChartsContainer extends React.Component {
    render() {
        let {stats, startDate, match, history, current_stats} = this.props;
        if (stats == undefined || (stats.activations.count == 0 && stats.connections.count == 0))
            return (<div></div>);
        return (
            <Row>
                <Col md="12">
                    <div><span style={{fontWeight: 'bold', fontSize: '110%'}}>Data Usage, Connections and Activations</span>
                    <div style={{float:'right'}}>
                        {
                            stats.connections.count + stats.activations.count == 0 ?
                                stats.activations.count :
                                "{0} ({1}%)".format(stats.activations.count, (stats.activations.count / (stats.connections.count + stats.activations.count) * 100).toFixed(0))
                        } Activations
                        <span style={{marginLeft: '40px'}}>{stats.connections.count} Connections</span>
                    </div></div>
                </Col>
                <Col md="6">
                    <ConnectionsChart stats={stats} startDate={startDate}
                                      match={match} history={history} mode={0}/>
                </Col>
                <Col md="6">
                    <ConnectionsChart stats={stats} startDate={startDate}
                                      match={match} history={history} mode={1}/>
                </Col>
                <Col md="6">
                    <ConnectionsChart stats={stats} startDate={startDate}
                                      match={match} history={history} mode={2}/>
                </Col>
                <Col md="6">
                    <ConsumptionChart stats={current_stats} startDate={startDate}
                                      match={match} history={history} />
                </Col>
            </Row>
        )
    }
}