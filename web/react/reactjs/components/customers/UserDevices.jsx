import React from "react"
import Donut from '../Donut'
import {
    Container, Row, Col
} from 'reactstrap';
import DeviceTiles from '../DeviceTiles'

export default class UserDevices extends React.Component {
    render() {
        let {stats, iosreleases, iosuserdevices, iosversions,
        androidreleases, androidversions, androiduserdevices} = this.props;
        if (stats == undefined) return <div></div>
        return (
            <Row style={{marginTop: '30px'}}>
                {stats.devices.data.ios.count != 0 &&
                <Col sm={stats.devices.data.android.count == 0 ? 12 : 6}>
                    <div>{stats.devices.data.ios.count} iOS device
                        {stats.devices.data.ios.count > 1 && 's'}
                    </div>
                    { iosreleases.length > 0 &&
                    <Donut data={iosreleases} title="RoamVU Releases"/>
                    }
                    { iosversions.length > 0 &&
                    <Donut data={iosversions} title="iOS Versions"/>
                    }
                    {iosuserdevices != undefined &&
                    <DeviceTiles data={iosuserdevices}/>
                    }
                </Col>
                }
                {stats.devices.data.android.count != 0 &&
                <Col sm={stats.devices.data.ios.count == 0 ? 12 : 6}>
                    <div>{stats.devices.data.android.count} Android device
                        {stats.devices.data.android.count > 1 && 's'}
                    </div>
                    { androidreleases.length > 0 &&
                    <Donut data={androidreleases} title="RoamVU Releases"/>
                    }
                    { androidversions.length > 0 &&
                    <Donut data={androidversions} title="Android Versions"/>
                    }
                    {androiduserdevices != undefined &&
                    <DeviceTiles data={androiduserdevices}/>
                    }
                </Col>
                }
            </Row>
        )
    }
}