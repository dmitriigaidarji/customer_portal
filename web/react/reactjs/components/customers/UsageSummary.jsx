import React from "react"
import {
    Link
} from 'react-router-dom'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {
    Row, Col
} from 'reactstrap';

let styles = {
    title: {
        fontWeight: 'bold',
        fontSize: '110%'
    },
    filters: {
        fontSize: '90%',
        marginLeft: '15px'
    },
    right: {
        float: 'right',
        fontSize: '85%'
    },
    alright: {
        textAlign: 'right',
        paddingRight: 0
    },
    nopadding: {
        padding: '0 0 0 5px'
    }
};

export default class UsageSummary extends React.Component {
    constructor(){
        super();
        this.state = {
            showFilters: false
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
    }
    toggleTableFilters(){
        this.setState({showFilters: !this.state.showFilters})
    }
    render() {
        let {stats, month, date, customer_id} = this.props;
        if (stats == undefined || stats.length === 0)
            return (
                <div></div>
            );
        let columns = [
            // {
            //     Header: props => <input type="checkbox" checked={true}/>,
            //     Cell: props => <div style={{textAlign: 'center'}} onClick={(e) => {
            //         let d = e.target.querySelector('input')
            //         d.checked = !d.checked;
            //     }}><input type="checkbox" onClick={(e)=>{
            //         e.stopPropagation();
            //     }}/></div>,
            //     width: 30,
            //     sortable: false,
            //     hideFilter: true
            // },
            {
                Header: 'User NAI/ActivationID',
                accessor: 'nai',
                Cell: props => <Link to={`/${process.env.PUBLIC_URL}/users/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>,
            }, {
                id: 'userHashId',
                Header: 'User HashID',
                accessor: d => d.hash_id.split('@')[0],
                Cell: props => <Link to={`/${process.env.PUBLIC_URL}/users/${props.row.nai}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>,
                width: 350
            }, {
                id: 'devicesNumber', // Required because our accessor is not a string
                Header: 'Devices',
                accessor: d => d.devices_current.length,
                width: 60
            }, {
                id: 'signIn',
                Header: 'Sign-In',
                accessor: d => d.activations_current.length,
                width: 60
            }, {
                id: 'connect',
                Header: 'Connect',
                accessor: d => d.user_tx_current.length,
                width: 60
            }, {
                Header: 'Total Data',
                accessor: 'usage_current.total_bytes',
                Cell: props => {
                    let res = present_data(props.value)
                    let split = res.split(' ');
                    return <Row>
                        <Col sm="7" style={styles.alright}>
                            {split[0]}
                        </Col>
                        <Col sm="5" style={styles.nopadding}>
                            {split[1]}
                        </Col>
                    </Row>
                },
                width: 80
            }, {
                Header: 'Total Time',
                accessor: 'usage_current.total_seconds',
                Cell: props => <div style={{textAlign: 'center'}}>{present_clock(props.value)}</div>,
                width: 70
            }, {
                id: 'consume',
                Header: 'Consumes',
                accessor: d => d.usage_current.total_seconds == 0 ? 0 : d.usage_current.total_bytes / d.usage_current.total_seconds,
                Cell: props => {
                    let interval, avg_rate;
                    if (props.row['usage_current.total_seconds'] > 3600) {
                        interval = 'Hr';
                        avg_rate = present_data((props.row['usage_current.total_bytes'] / (props.row['usage_current.total_seconds'] / 3600)).toFixed(2))
                    } else if (props.row['usage_current.total_seconds'] > 60) {
                        interval = 'Min';
                        avg_rate = present_data((props.row['usage_current.total_bytes'] / (props.row['usage_current.total_seconds'] / 60)).toFixed(2))
                    } else if (props.row['usage_current.total_seconds'] > 0) {
                        interval = 'Sec';
                        avg_rate = present_data((props.row['usage_current.total_bytes'] / props.row['usage_current.total_seconds']).toFixed(2))
                    } else {
                        interval = 'Sec';
                        avg_rate = '0 B'
                    }
                    let res = '{0}/{1}'.format(avg_rate, interval)
                    let split = res.split(' ');
                    return <Row>
                        <Col sm="6" style={styles.alright}>
                            {split[0]}
                        </Col>
                        <Col sm="6" style={styles.nopadding}>
                            {split[1]}
                        </Col>
                    </Row>
                },
                width: 100
            }];
        let pageSize = 10;
        let showPagination = true;
        console.log(month)
        console.log(stats != undefined? stats[0].devices_current.length:'')
        return (
            <div>
                <div style={{marginTop:'15px'}}>
                    <span style={styles.title}>
                        {month != undefined &&
                        <span>{month} </span>
                        }Usage Summary (Total: {stats.length}):<a onClick={this.toggleTableFilters} style={styles.filters} href="javascript:void(0);">Toggle filters</a></span>
                    <span style={styles.right}><a
                        href={`${process.env.BASE_API_URL}customer/csv/usage/${customer_id}/${date.format('YYYYMM')}/`}
                        download>Download Summary in CSV</a></span>
                </div>
                <ReactTable
                    className='-striped -highlight'
                    data={stats}
                    showPagination={showPagination}
                    columns={columns}
                    defaultPageSize={pageSize}
                    filterable={this.state.showFilters}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] !== undefined ? String(row[id]).includes(filter.value) : true
                    }}
                    defaultSorted={[{
                        id: 'usage_current.total_bytes',
                        desc: true
                    }]}
                />
            </div>
        )
    }
}


function present_data(data) {
    if (data / 1073741824 > 1)
        return '{0} Gb'.format((data / 1073741824).toFixed(2));
    else if (data / 1048576 > 1)
        return '{0} Mb'.format((data / 1048576).toFixed(2));
    else if (data / 1024 > 1)
        return '{0} Kb'.format((data / 1024).toFixed(2));
    return '{0} b'.format(data);
}

function present_clock(data) {
    let h = Math.floor(data / 3600);
    let m = Math.floor(data % 3600 / 60);
    let s = Math.floor(data % 3600 % 60);
    return '{0}:{1}:{2}'.format(
        h > 9 ? h : '0' + h,
        m > 9 ? m : '0' + m,
        s > 9 ? s : '0' + s,
    )
}
