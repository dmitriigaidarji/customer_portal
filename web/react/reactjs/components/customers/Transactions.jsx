import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import _ from "lodash";
import {
    Link
} from 'react-router-dom'
import {
    Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup
} from 'reactstrap';
import {connect} from 'react-redux'
import {withGoogleMap, GoogleMap, Marker, OverlayView, InfoWindow} from "react-google-maps";
import withScriptjs from "react-google-maps/lib/async/withScriptjs";
import TransactionContainer from '../../containers/TransactionContainer'
import Api from '../../Api'
import userMapIcon from '../../static/media/user_map.png'
const AsyncGettingStartedExampleGoogleMap = withScriptjs(
    withGoogleMap(
        props => (
            <GoogleMap
                ref={props.onMapLoad}
                defaultZoom={16}
                defaultCenter={{lat: props.markers[0].position.lat, lng: props.markers[0].position.lng}}
                onClick={props.onMapClick}
            >
                {props.markers.map(marker => (
                    <Marker
                        {...marker}
                        onRightClick={() => props.onMarkerRightClick(marker)}
                    />
                ))}
            </GoogleMap>
        )
    )
);
const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={16}
        defaultCenter={{lat: props.markers[0].position.lat, lng: props.markers[0].position.lng}}
        onClick={props.onMapClick}
    >
        {props.markers.map(marker => (
            <Marker
                {...marker}
                onClick={() => props.onMarkerClick(marker)}
            >
                {marker.showInfo && (
                    <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
                        <div>{marker.infoContent}</div>
                    </InfoWindow>
                )}
            </Marker>
        ))}
    </GoogleMap>
));
let styles = {
    block: {
        marginTop: '20px'
    },
    filters: {
        fontSize: '90%',
        marginLeft: '15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '110%',
    },
    right: {
        float: 'right',
        fontSize: '85%'
    },
    geospan: {
        color: 'rgb(1, 76, 140)',
        cursor: 'pointer'
    }
};
class Transactions extends React.Component {
    constructor() {
        super();
        this.state = {
            sorting: [{id: 'date', desc: true}],
            showFilters: false,
            modal: false,
            markers: [{
                position: {
                    lat: 25.0112183,
                    lng: 121.52067570000001,
                },
                key: `key`,
                defaultAnimation: 2,
            }],
            transactionModal: false,
            transaction_id: undefined
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
        this.mapToggle = this.mapToggle.bind(this)
        this.transactionToggle = this.transactionToggle.bind(this)
        this.setMarkers = this.setMarkers.bind(this)
        this.setTransaction = this.setTransaction.bind(this)
        this.sortChange = this.sortChange.bind(this)
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
    }

    handleMarkerClick(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleMarkerClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }

    sortChange(column, shift) {
        let sort = {id: column.id};
        if (this.state.sorting.length && this.state.sorting[0].id == column.id)
            this.state.sorting[0].asc ? sort.desc = true : sort.asc = true
        else
            sort.asc = true;
        this.setState({
            sorting: [sort]
        })
    }

    transactionToggle() {
        this.setState({
            transactionModal: !this.state.transactionModal
        })
    }

    mapToggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    setMarkers(markers) {
        this.setState({markers: markers, modal: true})
    }

    setTransaction(transaction_id) {
        this.setState({transaction_id: transaction_id, transactionModal: true})
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    render() {
        let {stats, month, customer_id, date, showCSV, hideProvider, access_level} = this.props;
        if (stats == undefined)
            return (
                <div></div>
            );
        if (stats.length === 0)
            return (
                <div>No data</div>
            );
        let columns = [
            {
                id: 'date',
                Header: 'Date',
                accessor: d => d[0].format('YYYY-MM-DD HH:mm:ss'),
                width: 100
            },
            {
                id: 'transactionId',
                Header: 'Transaction ID',
                accessor: d => d[1],
                width: 190,
                Cell: (props) => {
                    if (props.original[3] === 'Activation')
                        return props.value;
                    return <a href="javascript:void(0);"
                              onClick={() => this.setTransaction(props.value)}>{props.value}</a>
                }
            },
            {
                id: 'userIdentity',
                Header: 'User Identity',
                accessor: d => d[2],
                Cell: (props) => {
                    if (access_level !== 'END_USER')
                        return <Link
                            to={`/${process.env.PUBLIC_URL}/users/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                    else return props.value;
                }
            },
            {
                id: 'txType',
                Header: 'Tx Type',
                accessor: d => d[3],
                width: 80
            },
            {
                id: 'provider',
                Header: 'Provider',
                accessor: d => d[4],
                Cell: props => {
                    if (access_level !== 'ADMIN') return props.value;
                    return <Link
                        to={`/${process.env.PUBLIC_URL}/providers/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                },
                width: 60
            },
            {
                id: 'country',
                Header: 'Country',
                accessor: d => d[5],
                width: 50
            },
            {
                id: 'geo',
                Header: 'Geoposition',
                accessor: d => {
                    return '{0},{1}'.format(parseFloat(d[6][0]).toFixed(2),
                        parseFloat(d[6][1]).toFixed(2))
                },
                Cell: props => {
                    if (props.value != '0.00,0.00')
                        return <div style={styles.geospan} onClick={() => {
                            let _this = this;
                            let icon = {
                                url: userMapIcon, // url
                                scaledSize: new google.maps.Size(32, 38) // scaled size
                                //origin: new google.maps.Point(0, 0), // origin
                                //anchor: new google.maps.Point(0, 0) // anchor
                            };
                            let item = props.original;
                            let markers = [{
                                position: {
                                    lat: parseFloat(props.original[6][0]),
                                    lng: parseFloat(props.original[6][1]),
                                },
                                icon: icon,
                                showInfo: false,
                                infoContent: (<div>
                                    <div><span className="markerTitle">ID:</span><span
                                        className="marekValue">{item[1]}</span></div>
                                    <div><span className="markerTitle">User:</span><span
                                        className="marekValue">{item[2]}</span></div>
                                    <div><span className="markerTitle">Type:</span><span
                                        className="marekValue">{item[3]}</span></div>
                                    <div><span className="markerTitle">Provider:</span><span
                                        className="marekValue">{item[4]}</span></div>
                                    <div><span className="markerTitle">Country:</span><span
                                        className="marekValue">{item[5]}</span></div>
                                    <div><span className="markerTitle">Position:</span><span
                                        className="marekValue">{'{0},{1}'.format(parseFloat(item[6][0]).toFixed(2),
                                        parseFloat(item[6][1]).toFixed(2))}</span></div>
                                    <div><span className="markerTitle">Device:</span><span
                                        className="marekValue">{item[7]}</span></div>
                                    <div><span className="markerTitle">Seconds:</span><span
                                        className="marekValue">{isNaN(parseInt(item[8])) ? 0 : parseInt(item[8])}</span>
                                    </div>
                                    <div><span className="markerTitle">Bytes:</span><span
                                        className="marekValue">{isNaN(parseInt(item[9])) ? 0 : parseInt(item[9])}</span>
                                    </div>
                                </div>),
                                key: `key`,
                                defaultAnimation: 2,
                            }];
                            Api.get(process.env.BASE_API_URL + 'poi/', {
                                params: {
                                    lat: parseFloat(props.original[6][0]),
                                    lng: parseFloat(props.original[6][1])
                                }
                            }).then(res => {
                                if (res.status == 200) {
                                    let array = res.data.results;
                                    if (array.length > 0) {
                                        for (let i = 0; i < array.length; i++) {
                                            let item = array[i];
                                            markers.push({
                                                position: {
                                                    lat: item.location.x,
                                                    lng: item.location.y,
                                                },
                                                showInfo: false,
                                                infoContent: (<div>
                                                    <div><span className="markerTitle">ID:</span><span
                                                        className="marekValue">{item._id.$oid}</span></div>
                                                    <div><span className="markerTitle">Country:</span><span
                                                        className="marekValue">{item.country_name}</span></div>
                                                    <div><span className="markerTitle">City District:</span><span
                                                        className="marekValue">{item.city_district}</span></div>
                                                    <div><span className="markerTitle">Region:</span><span
                                                        className="marekValue">{item.pref_state_region}</span></div>
                                                    <div><span className="markerTitle">Venue:</span><span
                                                        className="marekValue">{item.venue_name}</span></div>
                                                    <div><span className="markerTitle">Address:</span><span
                                                        className="marekValue">{item.english_address}</span></div>
                                                    <div><span className="markerTitle">ZIP:</span><span
                                                        className="marekValue">{item.zipcode}</span></div>
                                                    <div><span className="markerTitle">Latitude:</span><span
                                                        className="marekValue">{item.latitude}</span></div>
                                                    <div><span className="markerTitle">Longitude:</span><span
                                                        className="marekValue">{item.longitude}</span></div>
                                                    <div><span className="markerTitle">Customer:</span><span
                                                        className="marekValue">{item.customer_id} {item.customer_name}</span>
                                                    </div>
                                                    <div><span className="markerTitle">SSID:</span><span
                                                        className="marekValue">{item.ssid}</span></div>
                                                    <div><span className="markerTitle">Suffix:</span><span
                                                        className="marekValue">{item.suffix}</span></div>
                                                    <div><span className="markerTitle">l2auth:</span><span
                                                        className="marekValue">{item.l2auth}</span></div>
                                                    <div><span className="markerTitle">l3auth:</span><span
                                                        className="marekValue">{item.l3auth}</span></div>
                                                    <div><span className="markerTitle">Modified:</span><span
                                                        className="marekValue">{item.mod_date}</span></div>
                                                </div>),
                                                key: i,
                                                defaultAnimation: 2,
                                                title: item.venue_name
                                            })
                                        }
                                        _this.setMarkers(markers)
                                    }
                                }
                            });
                            this.setMarkers(markers)
                        }}>{props.value}</div>;
                    else return props.value
                },
                width: 76
            },
            {
                id: 'device',
                Header: 'Device',
                accessor: d => d[7],
                width: 100
            },
            {
                id: 'seconds',
                Header: 'Seconds',
                accessor: d => isNaN(parseInt(d[8])) ? 0 : parseInt(d[8]),
                Cell: props => props.value,
                width: 60
            },
            {
                id: 'bytes',
                Header: 'Bytes',
                accessor: d => isNaN(parseInt(d[9])) ? 0 : parseInt(d[9]),
                Cell: props => props.value,
                width: 70
            }];
        let pageSize = stats.length > 20 ? 20 : 10;
        let showPagination = true;
        return (
            <div>
                <div>
                    <div style={styles.block}>
                    <span style={styles.title}>All
                        {month != undefined &&
                        <span> {month}</span>
                        } Transactions (Total: {stats.length}):<a
                            onClick={this.toggleTableFilters} style={styles.filters} href="javascript:void(0);">Toggle filters</a></span>
                        { showCSV != undefined &&
                        <span style={styles.right}><a
                            href={`${process.env.BASE_API_URL}customer/csv/transactions/${customer_id}/${date.format('YYYYMM')}/`}
                            download>Download Summary in CSV</a></span>
                        }
                    </div>
                    <ReactTable
                        className='-striped -highlight'
                        data={stats}
                        showPagination={showPagination}
                        columns={columns}
                        defaultPageSize={pageSize}
                        filterable={this.state.showFilters}
                        defaultFilterMethod={(filter, row, column) => {
                            const id = filter.pivotId || filter.id
                            return row[id] !== undefined ?
                                String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) :
                                true
                        }}
                        defaultSorted={this.state.sorting}
                        // sorted={this.state.sorting}
                        // onSortedChange={this.sortChange}
                    />
                </div>
                <Modal isOpen={this.state.modal} toggle={this.mapToggle} size="lg">
                    <ModalHeader toggle={this.mapToggle}>Location</ModalHeader>
                    <ModalBody>
                        <GettingStartedGoogleMap
                            containerElement={
                                <div style={{height: `350px`}}/>
                            }
                            mapElement={
                                <div style={{height: `100%`}}/>
                            }
                            onMapLoad={_.noop}
                            onMapClick={_.noop}
                            markers={this.state.markers}
                            onMarkerRightClick={_.noop}
                            onMarkerClick={this.handleMarkerClick}
                            onMarkerClose={this.handleMarkerClose}
                        />

                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.mapToggle}>Close</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.transactionModal} toggle={this.transactionToggle} size="lg">
                    <ModalHeader toggle={this.transactionToggle}>Transaction {this.state.transaction_id}</ModalHeader>
                    <ModalBody>
                        <TransactionContainer
                            transaction_id={this.state.transaction_id}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.transactionToggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    access_level: state.selects.access_level
});
export default connect(mapStateToProps)(Transactions)
/*<AsyncGettingStartedExampleGoogleMap
 googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyD3iIKy7TloAGaFrkhDOV77DJF3naC7ZfA"
 loadingElement={
 <div style={{height: `100%`}}>
 </div>
 }
 containerElement={
 <div style={{height: `350px`}}/>
 }
 mapElement={
 <div style={{height: `100%`}}/>
 }
 onMapLoad={_.noop}
 onMapClick={_.noop}
 markers={this.state.markers}
 onMarkerRightClick={_.noop}
 />*/