import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../../Api'
import $ from 'jquery'
import {connect} from 'react-redux'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'
import {selectStartEndDate, selectCustomer} from '../../actions/selects'
import Home from '../header/Home'
class HeaderMonthly extends React.Component {
    constructor() {
        super()
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);

        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date
    }

    render() {
        let {props} = this;
        let {customers, customer, startDate, endDate, dispatch, history, access_level} = props;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <Nav navbar>
                    <NavDropdown isOpen={this.state.providerDropdownOpen} toggle={this.toggleProviderDropdown}>
                        { customer ?
                            <DropdownToggle nav caret>
                                <span style={{whiteSpace:'nowrap'}}>{customer.id }: {customer.name }, { customer.country }</span>
                            </DropdownToggle>
                            :
                            <DropdownToggle nav caret>Customer</DropdownToggle>
                        }
                        <DropdownMenu>
                            {
                                customers ?
                                    customers.map((prov) => {
                                        return <DropdownItem key={prov.id}
                                                             onClick={() => {
                                                                 dispatch(selectCustomer(
                                                                     prov
                                                                 ));
                                                                 if (customer.id != prov.id) {
                                                                     history.push(`/${process.env.PUBLIC_URL}/customers/${prov.id}/${startDate.format('YYYY')}/${startDate.format('MM')}/`);
                                                                     dispatch(fetchCustomerMonthlyIfNeeded(startDate, prov));
                                                                 }
                                                             }}>
                                            {prov.id} - {prov.name}</DropdownItem>
                                    })
                                    :
                                    <DropdownItem>Loading...</DropdownItem>
                            }
                        </DropdownMenu>
                    </NavDropdown>
                    </Nav>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight: '10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <DateRangePicker
                                startDate={startDate}
                                endDate={endDate}
                                onDatesChange={({startDate, endDate}) => {
                                    dispatch(selectStartEndDate(
                                        startDate.startOf('day'),
                                        endDate.endOf('day')
                                    ));
                                }}
                                focusedInput={this.state.focusedInput} // PropTypes.bool
                                onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                numberOfMonths={1}
                                displayFormat="YYYY/MM/DD"
                            />
                            {access_level === 'ADMIN' &&
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}><span style={{whiteSpace: 'nowrap'}}>Clear cache</span></NavLink>
                            </NavItem>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    customer: state.selects.customer,
    customers: state.customers.items,
    access_level: state.selects.access_level
});
export default connect(mapStateToProps)(HeaderMonthly)
