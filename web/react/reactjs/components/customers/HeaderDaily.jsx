import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../../Api'
import Home from '../header/Home'
import {connect} from 'react-redux'
import {selectCurrentDay, selectStartEndDate, selectCustomer} from '../../actions/selects'
import {fetchCustomerMonthlyIfNeeded} from '../../actions/customerMontly'

class HeaderDaily extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);
        this.isDayBlocked = this.isDayBlocked.bind(this);
        this.allowedDays = [];
        if (props.active_dates != undefined)
            props.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentWillUpdate(nextProps) {
        if (nextProps.active_dates != undefined) {
            this.allowedDays = [];
            nextProps.active_dates.map((date) => {
                this.allowedDays.push(date.format('YYYYMMDD'))
            });
        }
    }

    isDayBlocked(date) {
        return !this.allowedDays.includes(date.format('YYYYMMDD'));
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date.startOf('day');
    }


    render() {
        let {props} = this;
        let {customers, customer, current_day, startDate, endDate, dispatch, history, access_level} = props;
        let backurl = `/${process.env.PUBLIC_URL}/customers/${customer.id}/${current_day.format('YYYY')}/${current_day.format('MM')}/`;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <div style={{marginRight: '5px'}}><Link to={backurl}>&lt;Monthly</Link></div>
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <Nav navbar>
                        <NavDropdown isOpen={this.state.providerDropdownOpen} toggle={this.toggleProviderDropdown}
                                     className="elddi eldd">
                            { customer ?
                                <DropdownToggle nav caret>
                                    <span
                                        style={{whiteSpace: 'nowrap'}}>{customer.id }: {customer.name }, { customer.country }</span>
                                </DropdownToggle>
                                :
                                <DropdownToggle nav caret>Customer</DropdownToggle>
                            }
                            <DropdownMenu>
                                {
                                    customers ?
                                        customers.map((prov) => {
                                            return <DropdownItem key={prov.id}
                                                                 onClick={() => {
                                                                     dispatch(selectCustomer(
                                                                         prov
                                                                     ));
                                                                     if (customer.id != prov.id) {
                                                                         history.push(`/${process.env.PUBLIC_URL}/customers/${prov.id}/${startDate.format('YYYY')}/${startDate.format('MM')}/${current_day.format('DD')}/`);
                                                                         dispatch(fetchCustomerMonthlyIfNeeded(startDate, prov));
                                                                     }
                                                                 }}>
                                                {prov.id} - {prov.name}</DropdownItem>
                                        })
                                        :
                                        <DropdownItem>Loading...</DropdownItem>
                                }
                            </DropdownMenu>
                        </NavDropdown>
                    </Nav>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight: '10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <SingleDatePicker
                                date={current_day} // momentPropTypes.momentObj or null,
                                onDateChange={(date) => {
                                    history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}/`);
                                    dispatch(selectCurrentDay(date))
                                }}
                                focused={this.state.focused} // PropTypes.bool
                                onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/customers/${customer.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${current_day.format('DD')}/`);
                                        dispatch(fetchCustomerMonthlyIfNeeded(newStart, customer));
                                    }
                                }}
                                numberOfMonths={1}
                                isDayBlocked={this.isDayBlocked}
                                displayFormat="YYYY/MM/DD"
                            />
                            {access_level === 'ADMIN' &&
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}>Clear cache</NavLink>
                            </NavItem>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    access_level: state.selects.access_level
});

export default connect(
    mapStateToProps
)(HeaderDaily)
