import React from "react"
import {Table} from 'reactstrap';

let style = {
    inline: {
        display: 'inline-block',
        marginLeft: '20px'
    },
    title: {
        fontWeight: 'bold'
    }
}
export default class TableData extends React.Component {
    render() {
        let {data} = this.props;
        if (data == undefined || data.user_count == 0)
            return (<div></div>);
        return (
            <div style={style.inline}>
                <Table bordered>
                    <tbody>
                    <Cell title="iOS" value={data.devices.data.ios.count} color="azure"/>
                    <Cell title="Android" value={data.devices.data.android.count} color="azure"/>
                    <Cell title="Activations" value=
                        {
                            data.connections.count + data.activations.count == 0 ?
                            data.activations.count :
                            "{0} ({1}%)".format(data.activations.count, (data.activations.count / (data.connections.count + data.activations.count) * 100).toFixed(0))
                        }
                          color="lightgoldenrodyellow"
                    />
                    <Cell title="Connections" value={data.connections.count} color="lightgoldenrodyellow"/>
                    </tbody>
                </Table>
            </div>
        )
    }
}
class Cell extends React.Component {
    render() {
        let {title, value, color} = this.props;
        return (
            <tr style={{backgroundColor: color}}>
                <td style={style.title}>{title}</td>
                <td>{value}</td>
            </tr>
        )
    }
}