import React from "react"
import Api from '../../Api';
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
import moment from 'moment'
export default class UsageDaily extends React.Component {
    constructor(props) {
        super(props)
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {stats} = this.props;
        if (stats != undefined)
            this.updateChart(this.props.stats);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;
    }

    updateChart(data) {
        let _this = this;
        if (data != undefined) {
            // data = data.slice(-1)
            let svg = d3.select("#usagechart");
            svg.attr('width', $("#usageDailyContainer").width());
            svg.selectAll("*").remove();
            if (data.length > 0) {
                data.forEach((item) => {
                    item.values.sort(function (a, b) {
                        return a.date - b.date;
                    });
                });
                // console.log(data)
                // data[0].values = data[0].values.slice(0,3)
                // data[0].values.forEach((item) => {
                //     console.log(item.date.format('HH:MM'), item.seconds)
                // })
                let margin = {top: 20, right: 160, bottom: 30, left: 40},
                    width = +svg.attr("width") - margin.left - margin.right,
                    height = +svg.attr("height") - margin.top - margin.bottom,
                    legendWidth = 19,
                    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                let x = d3.scaleTime().range([0, width]),
                    y = d3.scaleLinear().range([height, 0]),
                    z = d3.scaleOrdinal(d3.schemeCategory20);

                let line = d3.line()
                // .curve(d3.curveBasis)
                    .x(function (d) {
                        return x(d.date);
                    })
                    .y(function (d) {
                        return y(d.seconds);
                    });
                let div = d3.select('#usagecharttooltip');

                x.domain([moment(this.props.date).startOf('day'), moment(this.props.date).endOf('day')]);

                y.domain([
                    d3.min(data, function (c) {
                        return d3.min(c.values, function (d) {
                            return d.seconds;
                        });
                    }),
                    d3.max(data, function (c) {
                        return d3.max(c.values, function (d) {
                            return d.seconds;
                        });
                    })
                ]);

                z.domain(data.map(function (c) {
                    return c.id;
                }));

                g.append("g")
                    .attr("class", "axis axis--x")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x).ticks(d3.utcHour).tickFormat(d3.utcFormat("%H:%M")));

                g.append("g")
                    .attr("class", "axis axis--y")
                    .call(d3.axisLeft(y).ticks().tickFormat((val) =>
                        present_clock(val).substring(0, 5)
                    ).tickSizeInner([-width]))
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", "0.71em")
                    .attr("fill", "#000")
                    .text("Hours");

                // g.append("g")
                //     .attr("class", "axis axis--y-inner")
                //     .call(d3.axisLeft(y).tickSizeInner([-width]))
                //     .attr("y", 6)
                //     .attr("dy", "0.71em")
                //     .attr("fill", "#000");

                let city = g.selectAll(".city")
                    .data(data)
                    .enter().append("g")
                    .attr("class", "city");

                city.append("path")
                    .attr("class", "line")
                    .attr("d", function (d) {
                        return line(d.values);
                    })
                    .style("stroke", function (d) {
                        return z(d.id);
                    });

                city.selectAll("dot")
                    .data(function (d) {
                        if (d.values.length != 1)
                            return d.values;
                        else {
                            return [{
                                date: d.values[0].date,
                                seconds: d.values[0].seconds,
                                bytes: d.values[0].bytes,
                                nai: d.values[0].nai,
                                single: true
                            }]
                        }
                    })
                    .enter().append("circle")
                // .attr("transform", "translate(" + x.bandwidth() * 2.7 + ", 0)")
                    .attr("stroke", (d) => {
                        return d.single ? z(d.nai) : 'transparent'
                    })
                    .attr("fill", (d) => {
                        return d.single ? z(d.nai) : 'transparent'
                    })
                    // .attr('class','transperant')
                    .attr("r", 2.5)
                    .attr("cx", function (d) {
                        return x(d.date);
                    })
                    .attr("cy", function (d) {
                        return y(d.seconds)
                    })
                    .on("mouseover", function (d,b,e) {
                        let coordinates = d3.mouse(this);
                        let pt = svg.node().createSVGPoint();
                        pt.x = coordinates[0];
                        pt.y = coordinates[1];
                        pt = pt.matrixTransform(this.getCTM());
                        let xPosition = pt.x - 70;
                        let yPosition = pt.y - 50;
                        div.style("opacity", .9);
                        div.html(
                            '<div style="text-align: start;"><span style="font-weight: bold;">Duration:</span> ' + present_clock(d.seconds) + '</br>' +
                            '<span style="font-weight: bold;">Usage: ' + '</span> ' + present_data(d.bytes) + '</br>' +
                            '<span style="font-weight: bold;">Date: ' + '</span> ' + d.date.format('ddd HH:MM') + '</br>' +
                            '<span style="font-weight: bold;">User: ' + '</span> ' + d.nai + '</div>'
                        )
                            .style("left", (xPosition + 10) + "px")
                            .style("top", (yPosition + 10) + "px");
                        city.selectAll("path")
                            .style("opacity", 0.3);
                        if (e != undefined)
                            e[0].parentNode.querySelector('path').style.opacity = 1
                    })
                    .on("mouseout", function () {
                        div.style("opacity", 0);
                        city.selectAll("path")
                            .style("opacity", 1);
                    });

                let legend = g.append("g")
                    .attr("font-family", "sans-serif")
                    .attr("font-size", 10)
                    .attr("text-anchor", "end")
                    .selectAll("g")
                    .data(data)
                    .enter().append("g")
                    .attr("transform", function (d, i) {
                        return "translate(0," + i * 20 + ")";
                    });

                legend.append("rect")
                    .attr("x", width - 19 + margin.right)
                    .attr("width", 19)
                    .attr("height", 19)
                    .attr("fill", function (d) {
                        return z(d.id)
                    });

                legend.append("text")
                    .attr("x", width - 24 + margin.right)
                    .attr("y", 9.5)
                    .attr("dy", "0.32em")
                    .text(function (d) {
                        return d.id;
                    });

                // city.append("text")
                //     .datum(function (d) {
                //         console.log(d)
                //         return {id: d.id, value: d.values[d.values.length - 1]};
                //     })
                //     .attr("transform", function (d) {
                //         return "translate(" + x(d.value.date) + "," + y(d.value.seconds) + ")";
                //     })
                //     .attr("x", 3)
                //     .attr("dy", "0.35em")
                //     .style("font", "10px sans-serif")
                //     .text(function (d) {
                //         return d.id;
                //     });

                {/*let x = d3.scaleTime()*/
                }
                {/*.rangeRound([0, width]);*/
                }

                {/*let y = d3.scaleLinear()*/
                }
                {/*.rangeRound([height, 0]);*/
                }

                // let area = d3.area()
                //     .x(function (d) {
                //         return x(d.date);
                //     })
                //     .y1(function (d) {
                //         return y(d.seconds);
                //     });
                // // x.domain(d3.extent(data, function (d) {
                {/*//     return d.date;*/
                }
                {/*// }));*/
                }

                //
                // x.domain([moment(data[0].date).startOf('day'), moment(data[0].date).endOf('day')]);
                //
                // y.domain([0, d3.max(data, function (d) {
                //     return d.seconds;
                // })]);
                // area.y0(y(0));
                // g.append("path")
                //     .datum(data)
                //     .attr("fill", "steelblue")
                //     .attr("d", area);
                //
                // g.append("g")
                //     .attr("transform", "translate(0," + height + ")")
                //     .call(d3.axisBottom(x).ticks(d3.utcHour).tickFormat(d3.utcFormat("%H:%M")));
                //
                // g.append("g")
                //     .call(d3.axisLeft(y).ticks().tickFormat((val) =>
                //         present_clock(val).substring(0, 5)
                //     ))
                //     .append("text")
                //     .attr("fill", "#000")
                //     .attr("transform", "rotate(-90)")
                //     .attr("y", 6)
                //     .attr("dy", "0.71em")
                //     .attr("text-anchor", "end")
                //     .text("Time (hours)");
            }
        }
    }

    render() {
        let {stats} = this.props;
        if (stats == undefined || stats.length == 0)
            return (<div></div>);
        return (
            <Row>
                <Col md="12" id="usageDailyContainer">
                    <h5>Usage daily</h5>
                    <svg id="usagechart" width="1000" height="300"></svg>
                    <div id='usagecharttooltip' className="tooltip"></div>
                </Col>
            </Row>
        )
    }
}
