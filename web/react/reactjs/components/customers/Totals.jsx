import React from "react"
import Radium from 'radium'
import Donut from '../Donut'
import {
    Container, Row, Col
} from 'reactstrap';
import DeviceTiles from '../DeviceTiles'

@Radium
export default class DailyTotals extends React.Component {
    constructor() {
        super();
        this.modes = ['', 'devices', 'data'];
        this.state = {
            mode: this.modes[0],
            showTiny: false,
        };
        this.updateDimensions = this.updateDimensions.bind(this)
    }

    updateDimensions() {
        let {showTiny} = this.state;
        if (document.body.scrollTop < 50 && showTiny)
            this.setState({showTiny: false})
        else if (document.body.scrollTop >= 50 && !showTiny)
            this.setState({showTiny: true})
    }

    componentDidMount() {
        window.addEventListener("scroll", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.updateDimensions);
    }

    render() {
        let {
            stats, userLevel
        } = this.props;
        let {mode, showTiny} = this.state
        if (stats == undefined)
            return (
                <div>
                    Loading stats..
                </div>
            );
        let moneySaved = (stats.totals.tbytes * 30 / 1073741824).toFixed(0);
        return (
            <div style={styles.navcontainer}>
                <div style={(showTiny) ? styles.nav : [styles.nav, styles.biggroupmargin]}>
                    <div style={styles.group}
                         onClick={() => this.setState({mode: mode == this.modes[1] ? '' : this.modes[1]})}>
                        {userLevel !== true &&
                        <Stat title="Users" value={stats.user_count} icon="users" showTiny={showTiny}/>
                        }
                        <Stat title="Devices" value={stats.devices.count} icon="tablet" showTiny={showTiny}/>
                    </div>
                    <div style={styles.group}>
                        <Stat title="Total Data" value={stats.totals.tdata} icon="wifi" showTiny={showTiny}/>
                        {userLevel === true ?
                            <Stat title="Average Data per Connection" value={stats.totals.avgdata} icon="wifi"
                                  pericon="plug"
                                  showTiny={showTiny}/>
                            :
                            <Stat title="Average Data per User" value={stats.totals.avgdata} icon="wifi" pericon="user"
                                  showTiny={showTiny}/>
                        }
                    </div>
                    <div style={styles.group}
                        onClick={() => this.setState({mode: mode == this.modes[2] ? '' : this.modes[2]})}>
                        <Stat title="Saved vs. Mobile Network"
                              value={moneySaved}
                              icon="money" sign="usd" showTiny={showTiny} style={{color:"#0d580d"}}/>
                    </div>
                    <div style={styles.group}>
                        <Stat title="Total Time" value={stats.totals.ttime} icon="clock-o" showTiny={showTiny}/>
                        {userLevel === true ?
                            <Stat title="Average Time per Connection" value={stats.totals.avgtime} icon="clock-o"
                                  pericon="plug" showTiny={showTiny}/>
                            :
                            <Stat title="Average Time per User" value={stats.totals.avgtime} icon="clock-o"
                                  pericon="user" showTiny={showTiny}/>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

let styles = {
    navcontainer: {
        position: 'fixed',
        top: '90px',
        margin: '0 auto',
        maxWidth: '1080px',
        backgroundColor: 'white',
        zIndex: 9,
        left: 0,
        right: 0,
        WebkitBoxShadow: '0px 1px 3px 0px rgba(0,0,0,0.15)',
        MozBoxShadow: '0px 1px 3px 0px rgba(0,0,0,0.15)',
        boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.15)',
    },
    more: {
        fontSize: '50%',
        textAlign: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        cursor: 'pointer',
        ':hover': {
            color: '#d20909'
        }
    },
    moreicon: {},
    nav: {
        display: 'flex',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        whiteSpace: 'nowrap'
    },
    group: {
        display: 'inline-block',
        position: 'relative',
        // border: 'solid 1px rgba(0,0,0,0.05)',
        // borderRadius: '4px',
        // WebkitBoxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        // MozBoxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        // boxShadow: '4px 4px 5px 0px rgba(0,0,0,0.25)',
        padding: '0 0 5px 0'
    },
    biggroupmargin: {
        margin: '5px 0 8px 2px',
    },
    container: {
        fontSize: '100%',
        textAlign: 'center',
        whiteSpace: 'nowrap',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: '0 10px',
        cursor: 'pointer',
        ':hover': {
            color: '#d20909'
        }
    },
    icon: {
        fontSize: '200%',
        margin: '-5px 0 -12px 0'
    },
    value: {
        fontSize: '200%',
        margin: '-5px 0'
    },
    title: {
        fontWeight: '300',
        fontSize: '80%'
    }
}

@Radium
class Stat extends React.Component {
    render() {
        let {title, value, icon, showTiny, pericon, sign, style} = this.props;
        if (showTiny)
            return (<div style={(style == undefined) ? styles.container : [styles.container, style]}>
                <div style={styles.value}>{value}
                    {sign &&
                    <i style={{fontSize:'80%',
                        verticalAlign: 'middle',
                        marginTop: '-5px'
                    }} className={`fa fa-${sign}`} aria-hidden="true"></i>
                    }
                </div>
                <div style={styles.title}>{title}</div>
            </div>)
        return (
            <div style={(style == undefined) ? styles.container : [styles.container, style]}>
                {icon &&
                <div style={styles.icon}><i className={`fa fa-${icon}`} aria-hidden="true"></i>
                    {pericon &&
                    <span> / <i className={`fa fa-${pericon}`} aria-hidden="true"></i></span>
                    }</div>
                }
                <div style={styles.value}>{value}
                    {sign &&
                    <i style={{fontSize:'80%',
                        verticalAlign: 'middle',
                        marginTop: '-5px'
                    }} className={`fa fa-${sign}`} aria-hidden="true"></i>
                    }
                </div>
                <div style={styles.title}>{title}</div>
            </div>
        )
    }
}