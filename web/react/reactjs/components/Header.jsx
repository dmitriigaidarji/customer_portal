import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import Home from './header/Home'

import 'airbnb-js-shims/target/es2015.js'
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../Api'
import $ from 'jquery'
import {connect} from 'react-redux'
import {fetchProviderMonthlyIfNeeded} from '../actions/providerMontly'
import {selectStartEndDate, selectProvider} from '../actions/selects'
class Header extends React.Component {
    constructor() {
        super();
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);

        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focusedInput: undefined,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date
    }

    render() {
        let {providers, provider, startDate, endDate, history, dispatch} = this.props;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <Nav navbar>
                        <NavDropdown isOpen={this.state.providerDropdownOpen} toggle={this.toggleProviderDropdown}
                                     className="elddi eldd">
                            { provider ?
                                <DropdownToggle nav caret>
                                    <span style={{whiteSpace:'nowrap'}}>{provider.id }: {provider.name }, { provider.country }</span>
                                </DropdownToggle>
                                :
                                <DropdownToggle nav caret>Provider</DropdownToggle>
                            }
                            <DropdownMenu>
                                {
                                    providers ?
                                        providers.map((prov) => {
                                            return <DropdownItem key={prov.id}
                                                                 onClick={() => {
                                                                     dispatch(selectProvider(
                                                                         prov
                                                                     ));
                                                                     if (provider.id != prov.id) {
                                                                         history.push(`/${process.env.PUBLIC_URL}/providers/${prov.id}/${startDate.format('YYYY')}/${startDate.format('MM')}/`);
                                                                         dispatch(fetchProviderMonthlyIfNeeded(startDate, prov));
                                                                     }
                                                                 }}>
                                                {prov.id} - {prov.name}</DropdownItem>
                                        })
                                        :
                                        <DropdownItem>Loading...</DropdownItem>
                                }
                            </DropdownMenu>
                        </NavDropdown>
                    </Nav>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight: '10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <DateRangePicker
                                startDate={startDate} // momentPropTypes.momentObj or null,
                                endDate={endDate} // momentPropTypes.momentObj or null,
                                onDatesChange={({startDate, endDate}) => {
                                    dispatch(selectStartEndDate(
                                        startDate.startOf('day'),
                                        endDate.endOf('day')
                                    ));
                                }} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                                numberOfMonths={1}
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/providers/${provider.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchProviderMonthlyIfNeeded(newStart, provider));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/providers/${provider.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/`);
                                        dispatch(fetchProviderMonthlyIfNeeded(newStart, provider));
                                    }
                                }}
                                displayFormat="YYYY/MM/DD"
                            />
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}><span style={{whiteSpace: 'nowrap'}}>Clear cache</span></NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    startDate: moment.utc(state.selects.start_date),
    endDate: moment.utc(state.selects.end_date),
    provider: state.selects.provider,
    providers: state.providers.items
});
export default connect(mapStateToProps)(Header)
