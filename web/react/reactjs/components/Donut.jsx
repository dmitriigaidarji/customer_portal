import React from "react"
import Api from '../Api';
import $ from 'jquery'
import {
    Container, Row, Col, Button,
} from 'reactstrap';
let style = {
    donut: {
        display: 'inline-block',
        verticalAlign: 'top'
    }
}
export default class Donut extends React.Component {
    constructor(props) {
        super(props);
        this.uuid = props.uuid ? props.uuid : 'c' + guid();
        this.updateDimensions = this.updateDimensions.bind(this)
        this.updateChart = this.updateChart.bind(this)
    }

    updateDimensions() {
        this.forceUpdate();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        let {data} = this.props;
        if (data != undefined && data.length > 0)
            this.updateChart(data);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    componentDidUpdate() {
        let {data} = this.props;
        if (data != undefined && data.length > 0)
            this.updateChart(data);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.data != undefined;
    }

    updateChart(data) {
        let _this = this;
        if (data != undefined) {
            let colorarr = d3.scaleOrdinal(d3.schemeCategory20);
            let totalCount = 0;		//calcuting total manually
            data.forEach((d) => {
                totalCount += d.count;
            });
            if (_this.props.sort == 'value')
                data.sort(function (a, b) {
                    return b.count - a.count;
                });
            else data.sort(function (a, b) {
                if (b.name < a.name)
                    return -1;
                else if (b.name > a.name)
                    return 1
                else return 0;
            });
            let legendRectSize = 10;
            let legendSpacing = 4;
            let width = 230,
                height = Math.max(180, data.length * (legendRectSize + legendSpacing)),
                radius = 30;

            let arc = d3.arc()
                .outerRadius(radius + 25)
                .innerRadius(radius);

            let pie = d3.pie()
                .sort(null)
                .value(function (d) {
                    return d.count;
                });

            let div = d3.select('#' + this.uuid + 'tooltip');
            let svg = d3.select('#' + this.uuid).attr("width", width)
                .attr("height", height);
            svg.selectAll("*").remove();
            let marginleft = radius + 35 + 30;

            let gg = svg.append("g")
                .append("g")
                .attr("transform", "translate(" + (marginleft - 30) + "," + (marginleft - 30) + ")");

            let loffset = 0;
            let legend = gg.selectAll('.legend')
                .data(data)
                .enter()
                .append('g')
                .attr('class', 'legend')
                .attr('transform', function (d, i) {
                    let height = legendRectSize + legendSpacing;
                    let offset = height * data.length / 2;
                    let horz = -2 * legendRectSize;
                    let vert = i * height - offset;
                    if (vert < -60 && loffset == 0) loffset = -vert - 60;
                    return 'translate(' + (horz + 100) + ',' + (vert + loffset) + ')';
                });

            legend.append('rect')
                .attr('width', legendRectSize)
                .attr('height', legendRectSize)
                .style('fill', function (d) {
                    if (d.color != undefined)
                        return d.color;
                    return colorarr(d.name)
                })

            legend.append('text')
                .attr('x', legendRectSize + legendSpacing)
                .attr('y', legendRectSize - legendSpacing + 2.5)
                .attr('font-size', '0.6em')
                .text(function (d) {
                    return d.name + ' - ' + d.count;
                });


            let g = gg.selectAll(".arc")
                .data(pie(data))
                .enter().append("g");

            g.append("path")
                .attr("d", arc)
                .style("fill", function (d, i) {
                    if (d.data.color != undefined)
                        return d.data.color;
                    return colorarr(d.data.name)
                })
                .on("mousemove", function (d) {
                    let coordinates = d3.mouse(this);
                    let pt = svg.node().createSVGPoint();
                    pt.x = coordinates[0];
                    pt.y = coordinates[1];
                    pt = pt.matrixTransform(this.getCTM());
                    let xPosition = pt.x - 70;
                    let yPosition = pt.y - 50;
                    div.style("opacity", .9);
                    div.html(
                        '<span style="font-weight: bold;">' + d.data.name + '</span> - ' + d.data.count + '</br>' +
                        '<span style="font-weight: bold;">Percentage:</span> ' + (d.data.count / totalCount * 100).toFixed(2) + '%'
                    )
                        .style("left", (xPosition + 50) + "px")
                        .style("top", (yPosition + 70) + "px");
                })
                .on("mouseout", function (d) {
                    div.style("opacity", 0);
                });

            svg.append("text")
                .attr('x', marginleft - 30)
                .attr("text-anchor", "middle")
                .attr('font-size', '1.4em')
                .attr('y', marginleft - 22)
                .text((this.props.value != undefined) ? this.props.value : data.length);

            svg.append("text")
                .attr('x', marginleft - 30)
                .attr('font-size', '14px')
                .attr("text-anchor", "middle")
                .attr('y', marginleft * 1.5)
                .text(this.props.title);
        }
    }

    render() {
        let {data} = this.props;
        if (data == undefined || data.length == 0)
            return (
                <div></div>
            )
        return (
            <div style={style.donut}>
                <div style={{position: 'relative'}}>
                    <svg id={this.uuid}></svg>
                    <div id={this.uuid + 'tooltip'} className="tooltip"></div>
                </div>
            </div>
        )
    }
}
