import moment from 'moment'

export default {
  'Last year'  : {
    startDate     : (now) => {
      return moment(now).subtract(1,'years').endOf('year').startOf('month').format("YYYY-MM-DD");
    },
    endDate       : (now) => {
      return moment(now).subtract(1,'years').endOf('year').endOf('month').format("YYYY-MM-DD");
    }
  },
  'Last month'  : {
    startDate     : (now) => {
      return moment(now).subtract(1,'months').startOf('month').format("YYYY-MM-DD");
    },
    endDate       : (now) => {
      return moment(now).subtract(1,'months').endOf('month').format("YYYY-MM-DD");
    }
  },
  'This month'  : {
    startDate     : (now) => {
      return moment().format("YYYY-MM-01");
    },
    endDate       : (now) => {
      return now;
    }
  }
}