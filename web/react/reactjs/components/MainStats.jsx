import React from "react"
import {
    Container, Row, Col, Button,
} from 'reactstrap';

export default class MainStats extends React.Component {
    render() {
        let {stats} = this.props;
        if (stats == undefined)
            return (
                <div></div>
            );
        else
            return (
                <div style={styles.nav}>
                    <div style={styles.content}>
                        <div style={styles.group}>
                            <StatsContainer
                                title="Request"
                                value={ stats.request }
                            />
                            <StatsContainer
                                title="Accept"
                                value={ stats.accept }
                            />
                            <StatsContainer
                                title="Reject"
                                value={ stats.reject }
                            />
                            <StatsContainer
                                title="Deny"
                                value={ stats.deny }
                            />
                        </div>
                        <div style={styles.group}>
                            <StatsContainer
                                title="Start"
                                value={ stats.start }
                            />
                            <StatsContainer
                                title="Update"
                                value={ stats.update }
                            />
                            <StatsContainer
                                title="Stop"
                                value={ stats.stop }
                            />
                            <StatsContainer
                                title="Open"
                                value={ stats.open }
                            />
                        </div>
                        <div style={styles.group}>
                            <StatsContainer
                                title="Token"
                                value={ stats.token }
                            />
                            <StatsContainer
                                title="Anon"
                                value={ stats.anonymous }
                            />
                            <StatsContainer
                                title="Resolved"
                                value={ stats.cui_resolved }
                            />
                            <StatsContainer
                                title="Unresolved"
                                value={ stats.cui_unresolved }
                            />
                        </div>
                    </div>
                </div>
            )
    }
};

let styles = {
    nav: {
        position: 'fixed',
        top: '90px',
        left: 0,
        width: '100%',
        zIndex: 9,
        backgroundColor: 'white',
        borderBottom: '1px solid rgb(243, 243, 243)'
    },
    group: {
        margin: '0 30px',
        display: 'inline-block'
    },
    container: {
        fontSize: '100%',
        textAlign: 'center',
        whiteSpace: 'nowrap',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: '0 10px'
    },
    value: {
        fontSize: '100%'
    },
    title: {
        fontWeight: 'bold',
    },
    content:{
        maxWidth: '1080px',
        margin: '0 auto'
    }
}


class StatsContainer extends React.Component {
    render() {
        return (
           <div style={styles.container}>
                <div style={styles.title}>{this.props.title}</div>
                <div style={styles.value}>{this.props.value}</div>
            </div>
        )
    }
}