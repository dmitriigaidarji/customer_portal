import React from "react"
import {
    Container, Row, Col, Button,
} from 'reactstrap';
export default class TotalStats extends React.Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.stats != undefined;
    }

    render() {
        let {stats} = this.props;
        if (stats == undefined)
            return (
                <div>

                </div>
            )
        else
            return (
                <Row>
                    <Col md={{size: 8, offset: 2}} lg={{size: 6, offset: 3}} xs="12">
                        <Row>
                            <Stat title="Total Data" value={present_data(stats.bytes)}/>
                            { stats.avg_bytes != undefined ?
                                <Stat title="Avg. data" value={'{0}/d'.format((present_data(stats.avg_bytes)))}/>
                                :
                                <Stat title="Total data" value={present_data(stats.bytes)}/>
                            }
                            <Stat title="Time used" value={present_clock(stats.seconds)}/>
                            { stats.avg_users != undefined ?
                                <Stat title="Unique users" value={[stats.avg_users.toFixed(0), ' per day']}/>
                                :
                                <Stat title="Unique users" value={stats.uniq_users}/>
                            }
                        </Row>
                    </Col>
                </Row>
            )
    }
}

function present_data(data) {
    if (data / 1073741824 > 1)
        return '{0} Gb'.format((data / 1073741824).toFixed(2));
    else if (data / 1048576 > 1)
        return '{0} Mb'.format((data / 1048576).toFixed(2));
    else if (data / 1024 > 1)
        return '{0} Kb'.format((data / 1024).toFixed(2));
    return '{0} b'.format(data);
}

function present_clock(data){
    let h = Math.floor(data / 3600);
    let m = Math.floor(data % 3600 / 60);
    let s = Math.floor(data % 3600 % 60);
    return '{0}:{1}:{2}'.format(
        h > 9 ? h : '0' + h,
        m > 9 ? m : '0' + m,
        s > 9 ? s : '0' + s,
    )
}

class Stat extends React.Component {
    render() {
        return (
            //<Col md="2" lg="2">
            <Col md="3" className="statsContainer">
                <div className="statsTitle">{this.props.title}</div>
                <div className="statsValue">{this.props.value}</div>
            </Col>
            // </Col>
        )
    }
}