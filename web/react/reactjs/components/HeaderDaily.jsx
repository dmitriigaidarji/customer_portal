import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import Api from '../Api'
import $ from 'jquery'
import {connect} from 'react-redux'
import {fetchProviderDailyIfNeeded} from '../actions/providerDaily'
import {selectCurrentDay, selectStartEndDate, selectProvider} from '../actions/selects'
import {fetchProviderMonthlyIfNeeded} from '../actions/providerMontly'
import Home from './header/Home'

class HeaderDaily extends React.Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);
        this.isDayBlocked = this.isDayBlocked.bind(this);
        this.allowedDays = [];
        if (props.source_stats != undefined)
            props.source_stats.map((stat) => {
                this.allowedDays.push(moment.utc(parseInt(stat.date.$date)).format('YYYYMMDD'))
            });
        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentWillUpdate(nextProps) {
        if (nextProps.source_stats != undefined) {
            this.allowedDays = [];
            nextProps.source_stats.map((stat) => {
                this.allowedDays.push(moment.utc(parseInt(stat.date.$date)).format('YYYYMMDD'))
            });
        }
    }

    isDayBlocked(date) {
        return !this.allowedDays.includes(date.format('YYYYMMDD'));
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date.startOf('day');
    }
    render() {
        let {props} = this;
        let {providers, provider, startDate, dispatch, history} = props;
        let backurl = `/${process.env.PUBLIC_URL}/providers/${provider.id}/${startDate.format('YYYY')}/${startDate.format('MM')}/`;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <div style={{marginRight: '10px'}}><Link to={backurl}>&lt;Monthly</Link></div>
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home />
                    <Nav navbar>
                        <NavDropdown isOpen={this.state.providerDropdownOpen} toggle={this.toggleProviderDropdown}>
                                { provider ?
                                    <DropdownToggle nav caret>
                                        <span style={{whiteSpace:'nowrap'}}>{provider.id }: {provider.name }, { provider.country }</span>
                                    </DropdownToggle>
                                    :
                                    <DropdownToggle nav caret>Provider</DropdownToggle>
                                }
                                <DropdownMenu>
                                    {
                                        providers ?
                                            providers.map((prov) => {
                                                return <DropdownItem key={prov.id}
                                                                     onClick={() => {
                                                                         dispatch(selectProvider(
                                                                             prov
                                                                         ));
                                                                         if (provider.id != prov.id) {
                                                                             history.push(`/${process.env.PUBLIC_URL}/providers/${prov.id}/${startDate.format('YYYY')}/${startDate.format('MM')}/${props.match.params.day}/`);
                                                                             dispatch(fetchProviderMonthlyIfNeeded(startDate, prov));
                                                                             dispatch(fetchProviderDailyIfNeeded(startDate, prov));
                                                                         }
                                                                     }}>
                                                    {prov.id} - {prov.name}</DropdownItem>
                                            })
                                            :
                                            <DropdownItem>Loading...</DropdownItem>
                                    }
                                </DropdownMenu>
                            </NavDropdown>
                    </Nav>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto datePickerNav">
                            <NavItem>
                                <NavLink>
                                    <i className="fa fa-calendar" aria-hidden="true" style={{marginRight:'10px'}}></i>
                                    {startDate.format('MMMM')}
                                </NavLink>
                            </NavItem>
                            <SingleDatePicker
                                date={this.props.current_day} // momentPropTypes.momentObj or null,
                                onDateChange={(date) => {
                                    dispatch(selectCurrentDay(date))
                                }} // PropTypes.func.isRequired,
                                focused={this.state.focused} // PropTypes.bool
                                onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                                isOutsideRange={this.isOutsideRange}
                                onPrevMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).subtract(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/providers/${provider.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${props.match.params.day}/`);
                                        dispatch(fetchProviderMonthlyIfNeeded(newStart, provider));
                                        dispatch(fetchProviderDailyIfNeeded(newStart, provider));
                                    }
                                }}
                                onNextMonthClick={() => {
                                    let oldDate = startDate.format('YYYYMM');
                                    let newStart = moment(startDate).add(1, 'months').startOf('month').startOf('day');
                                    let newEnd = moment(newStart).endOf('month').endOf('day');
                                    dispatch(selectStartEndDate(
                                        newStart,
                                        newEnd
                                    ));
                                    if (newStart.format('YYYYMM') != oldDate) {
                                        history.push(`/${process.env.PUBLIC_URL}/providers/${provider.id}/${newStart.format('YYYY')}/${newStart.format('MM')}/${props.match.params.day}/`);
                                        dispatch(fetchProviderMonthlyIfNeeded(newStart, provider));
                                        dispatch(fetchProviderDailyIfNeeded(newStart, provider));
                                    }
                                }}
                                numberOfMonths={1}
                                isDayBlocked={this.isDayBlocked}
                                displayFormat="YYYY/MM/DD"
                            />
                            <NavItem>
                                <NavLink href="#" onClick={() => {
                                    Api.post(process.env.BASE_API_URL + 'clearcache/').then((result) => {
                                        console.log(result);
                                    });
                                }}><span style={{whiteSpace:'nowrap'}}>Clear cache</span></NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    providers: state.providers.items,
});

export default connect(mapStateToProps)(HeaderDaily)

