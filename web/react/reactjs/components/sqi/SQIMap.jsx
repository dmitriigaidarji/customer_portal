import React from "react"
import moment from 'moment'
import {
    Link
} from 'react-router-dom'
import {connect} from 'react-redux'
import _ from "lodash";
import {GoogleMapLoader, GoogleMap, Marker, withGoogleMap, InfoWindow} from "react-google-maps";
import userMapIcon from '../../static/media/user_map.png'
import userMapFlag from '../../static/media/flag.png'
import MarkerClusterer from "react-google-maps/lib/addons/MarkerClusterer";

const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={props.zoom}
        defaultCenter={{lat: 35, lng: 140}}
        onClick={props.onMapClick}
        onZoomChanged={props.onZoomChanged}
        onDragEnd={props.onDragEnd}
    >
        {props.hotspotmarkers.map(marker => (
            <Marker
                {...marker}
                onClick={() => props.onHotspotClick(marker)}
            >
                {marker.showInfo && (
                    <InfoWindow onCloseClick={() => props.onHotspotClose(marker)}>
                        <div>{marker.infoContent}</div>
                    </InfoWindow>
                )}
            </Marker>
        ))}
        <MarkerClusterer
            averageCenter
            enableRetinaIcons
            gridSize={10}
            minimumClusterSize={5}
            maxZoom={15}
        >
            {props.markers.map(marker => (
                <Marker
                    {...marker}
                    onClick={() => props.onMarkerClick(marker)}
                >
                    {marker.showInfo && (
                        <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
                            <div>{marker.infoContent}</div>
                        </InfoWindow>
                    )}
                </Marker>
            ))}
        </MarkerClusterer>
    </GoogleMap>
));
const style = {
    container: {
        marginTop: '15px'
    }
};
const defaultIcon = {
    url: userMapIcon,
    scaledSize: new google.maps.Size(16, 19)
};
const flagIcon = {
    url: userMapFlag,
    scaledSize: new google.maps.Size(20, 20)
};
class SQIContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            always_hide_hotspots: false,
            show_hotspots: false,
            zoom: 11,
            markers: [],
            hotspotmarkers: [],
            newData: true
        };
        this.resetZoom = true;
        this.handleMapMounted = this.handleMapMounted.bind(this);
        this.setShowHotspots = this.setShowHotspots.bind(this);
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.mapPanTo = this.mapPanTo.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
        this.handleHotspotClick = this.handleHotspotClick.bind(this);
        this.handleHotspotClose = this.handleHotspotClose.bind(this);
        this.calcMarkers = this.calcMarkers.bind(this);
        this.calcHotspots = this.calcHotspots.bind(this);
    }

    handleMarkerClick(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleMarkerClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }

    handleHotspotClick(targetMarker) {
        this.setState({
            hotspotmarkers: this.state.hotspotmarkers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: true,
                    };
                }
                return marker;
            }),
        });
    }

    handleHotspotClose(targetMarker) {
        this.setState({
            hotspotmarkers: this.state.hotspotmarkers.map(marker => {
                if (marker === targetMarker) {
                    return {
                        ...marker,
                        showInfo: false,
                    };
                }
                return marker;
            }),
        });
    }

    setShowHotspots(bool, zoom) {
        if (this.state.show_hotspots != bool) {
            if (this.updateTimeout != undefined)
                window.clearTimeout(this.updateTimeout);
            this.updateTimeout = window.setTimeout(() => this.setState({
                show_hotspots: bool,
                zoom: zoom
            }), 1000)
        }
    }

    componentWillUnmount() {
        window.clearTimeout(this.updateTimeout);
        window.clearTimeout(this.updateDrag);
    }

    handleZoomChange() {
        if (!this.state.always_hide_hotspots) {
            let zoom = this._sqimap.getZoom();
            if (zoom >= 13) {
                if (this.zoomHotspotsShow != undefined)
                    window.clearTimeout(this.zoomHotspotsShow);
                this.zoomHotspotsShow = window.setTimeout(() =>
                    this.setState({hotspotmarkers: this.calcHotspots(this.props.hotspots, true)}), 200);
                this.setShowHotspots(true, zoom)
            }
            else this.setShowHotspots(false, zoom)
        }
    }

    handleDrag() {
        let _this = this;
        if (this.state.show_hotspots) {
            if (this.updateDrag != undefined)
                window.clearTimeout(this.updateDrag);
            this.updateDrag = window.setTimeout(() => {
                _this.setState({
                    hotspotmarkers: _this.calcHotspots(_this.props.hotspots, _this.state.show_hotspots),
                })
            }, 1000)
        }
    }


    handleMapMounted(map) {
        this._sqimap = map;
        let {stats, hotspots} = this.props;
        if (stats != undefined && stats.length > 0 && map != undefined) {
            const bounds = new google.maps.LatLngBounds();
            let shouldZoom = false;
            stats.map((item, index) => {
                if (item != undefined) {
                    shouldZoom = true;
                    let lat = parseFloat(item.lat);
                    let lng = parseFloat(item.lng);
                    if (lat != 0 && lng != 0) {
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                }
            });
            this.resetZoom = false;
            if (shouldZoom)
                map.fitBounds(bounds);
        }
    }

    mapPanTo(point) {
        if (this._sqimap != undefined) {
            const bounds = new google.maps.LatLngBounds();
            bounds.extend(point);
            bounds.extend(new google.maps.LatLng(point.lat() - 0.0015, point.lng() - 0.0015));
            bounds.extend(new google.maps.LatLng(point.lat() + 0.0015, point.lng() + 0.0015));
            this._sqimap.fitBounds(bounds);
        }
    }

    componentDidMount() {
        let {props} = this;
        if (this.state.newData == true && props.stats != undefined) {
            console.log('calculating markers');
            this.resetZoom = true;
            this.setState({
                newData: false,
                markers: this.calcMarkers(props.stats),
                hotspotmarkers: [],
                zoom: 11,
                show_hotspots: false
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let {props, state} = this;
        // if (this.state.newData == true && props.stats != undefined) {
        //     this.resetZoom = true;
        //     this.setState({
        //         newData: false,
        //         markers: this.calcMarkers(props.stats),
        //         hotspotmarkers: this.calcHotspots(props.hotspots, state.show_hotspots),
        //         zoom: 11,
        //         show_hotspots: false
        //     });
        // } else if (state.show_hotspots != prevState.show_hotspots) {
        //     this.setState({
        //         hotspotmarkers: this.calcHotspots(props.hotspots, state.show_hotspots),
        //     })
        // } else {
        //     let bool = false;
        //     if (props.sort_indexes.length != prevProps.sort_indexes.length)
        //         bool = true;
        //     if (!bool) {
        //         for (let i = 0; i < props.sort_indexes.length; i++)
        //             if (props.sort_indexes[i] != prevProps.sort_indexes[i])
        //                 bool = true;
        //     }
        //     if (bool) {
        //         this.setState({
        //             markers: this.calcMarkers(props.stats),
        //             hotspotmarkers: this.calcHotspots(props.hotspots, state.show_hotspots),
        //         })
        //     }
        // }
        if (props.selectedCoordinate != undefined && props.selectedCoordinate != prevProps.selectedCoordinate) {
            this.mapPanTo(props.selectedCoordinate);
            this.setState({
                hotspotmarkers: this.calcHotspots(props.hotspots, true)
            })
        }
    }

    calcMarkers(stats) {
        let markers = [];
        if (stats != undefined && stats.length > 0) {
            const bounds = new google.maps.LatLngBounds();
            let shouldZoom = false;
            stats.map((item, index) => {
                if (item != undefined) {
                    shouldZoom = true;
                    let lat = parseFloat(item.lat);
                    let lng = parseFloat(item.lng);
                    if (lat != 0 && lng != 0) {
                        let icon = defaultIcon;
                        if (item.event == 'WISPr Authentication Failed')
                            icon = flagIcon;
                        markers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            showInfo: false,
                            infoContent: (<div>
                                <div><span className="markerTitle">Device ID:</span><span
                                    className="markerValue">{item.device_id}</span></div>
                                <div><span className="markerTitle">Duration:</span><span
                                    className="markerValue">{item.duration}</span></div>
                                <div><span className="markerTitle">Event:</span><span
                                    className="markerValue">{item.event}</span></div>
                                <div><span className="markerTitle">Latitude:</span><span
                                    className="markerValue">{item.lat}</span></div>
                                <div><span className="markerTitle">Longitude:</span><span
                                    className="markerValue">{item.lng}</span></div>
                                {item.platform &&
                                <div><span className="markerTitle">Platform:</span><span
                                    className="markerValue">{item.platform.device_model} {item.platform.ios_version}</span>
                                </div>
                                }
                                <div><span className="markerTitle">User:</span><span
                                    className="markerValue">
                                    <Link to={`/${process.env.PUBLIC_URL}/users/${item.userid}/${moment().format('YYYY')}/${moment().format('MM')}/`}>{item.userid}</Link>
                                    </span></div>
                                <div><span className="markerTitle">App version:</span><span
                                    className="markerValue">{item.version}</span></div>
                            </div>),
                            icon: icon,
                            key: index,
                            defaultAnimation: 2,
                            title: item.event
                        });
                        bounds.extend(new google.maps.LatLng(lat, lng))
                    }
                }
            });
            if (this._sqimap != undefined && this.resetZoom && shouldZoom) {
                this.resetZoom = false;
                this._sqimap.fitBounds(bounds);
            }
        }
        return markers;
    }

    calcHotspots(hotspots, show_hotspots) {
        let hotspotmarkers = []
        console.log('trying to calculate hotspots');
        console.log(show_hotspots, this._sqimap, hotspots)
        if (show_hotspots && this._sqimap != undefined && hotspots != undefined && hotspots.length > 0) {
            console.log('calculating hotspots');
            hotspots.map((item, index) => {
                let lat = parseFloat(item.location.x);
                let lng = parseFloat(item.location.y);
                if (lat != 0 && lng != 0) {
                    let mapbounds = this._sqimap.getBounds();
                    if (mapbounds.contains(new google.maps.LatLng(lat, lng))) {
                        let key = 'h' + index;
                        hotspotmarkers.push({
                            position: {
                                lat: lat,
                                lng: lng,
                            },
                            showInfo: false,
                            infoContent: (<div>
                                <div><span className="markerTitle">ID:</span><span
                                    className="markerValue">{item._id.$oid}</span></div>
                                <div><span className="markerTitle">Country:</span><span
                                    className="markerValue">{item.country_name}</span></div>
                                <div><span className="markerTitle">City District:</span><span
                                    className="markerValue">{item.city_district}</span></div>
                                <div><span className="markerTitle">Region:</span><span
                                    className="markerValue">{item.pref_state_region}</span></div>
                                <div><span className="markerTitle">Venue:</span><span
                                    className="markerValue">{item.venue_name}</span></div>
                                <div><span className="markerTitle">Address:</span><span
                                    className="markerValue">{item.english_address}</span></div>
                                <div><span className="markerTitle">ZIP:</span><span
                                    className="markerValue">{item.zipcode}</span></div>
                                <div><span className="markerTitle">Latitude:</span><span
                                    className="markerValue">{item.latitude}</span></div>
                                <div><span className="markerTitle">Longitude:</span><span
                                    className="markerValue">{item.longitude}</span></div>
                                <div><span className="markerTitle">Customer:</span><span
                                    className="markerValue">{item.customer_id} {item.customer_name}</span></div>
                                <div><span className="markerTitle">SSID:</span><span
                                    className="markerValue">{item.ssid}</span></div>
                                <div><span className="markerTitle">Suffix:</span><span
                                    className="markerValue">{item.suffix}</span></div>
                                <div><span className="markerTitle">l2auth:</span><span
                                    className="markerValue">{item.l2auth}</span></div>
                                <div><span className="markerTitle">l3auth:</span><span
                                    className="markerValue">{item.l3auth}</span></div>
                                <div><span className="markerTitle">Modified:</span><span
                                    className="markerValue">{item.mod_date}</span></div>
                            </div>),
                            key: key,
                            defaultAnimation: null,
                            title: item.venue_name
                        });
                    }
                }
            });
        }
        return hotspotmarkers
    }

    render() {
        let {current_day, stats, mapheight} = this.props;
        let {always_hide_hotspots} = this.state;
        if (stats === undefined)
            return (<div>Loading SQI data for day {current_day.format('MMMM Do YYYY')}...</div>)
        if (stats.length === 0)
            return (<div>No SQI data for day {current_day.format('MMMM Do YYYY')}</div>)
        return (
            <div>
                <a href="javascript:void(0);" style={{marginRight: '10px'}} onClick={() => {
                    if (always_hide_hotspots)
                        this.setState({always_hide_hotspots: false}, this.handleZoomChange)
                    else this.setState({always_hide_hotspots: true, show_hotspots: false})
                }}>
                    {always_hide_hotspots ?
                        <span>Show hotspots</span>
                        :
                        <span>Hide hotspots</span>
                    }
                </a>
                <span>{this.state.show_hotspots ? "Hotspots visible" : "Hotspots invisible"}
                    <a style={{float: 'right'}} onClick={() => {
                        let _this = this;
                        const bounds = new google.maps.LatLngBounds();
                        stats.map((item) => {
                            if (item !== undefined) {
                                let lat = parseFloat(item.lat);
                                let lng = parseFloat(item.lng);
                                if (lat !== 0 && lng !== 0) {
                                    bounds.extend(new google.maps.LatLng(lat, lng))
                                    _this._sqimap.fitBounds(bounds);
                                }
                            }
                        })
                    }} href="javascript:void(0);">Reset zoom</a>
                </span>
                <div>
                    <GettingStartedGoogleMap
                        containerElement={
                            <div style={{height: `${mapheight}px`}}/>
                        }
                        mapElement={
                            <div style={{height: `${mapheight}px`}}/>
                        }
                        onMapLoad={this.handleMapMounted}
                        onMapClick={_.noop}
                        onDragEnd={this.handleDrag}
                        onZoomChanged={this.handleZoomChange}
                        hotspotmarkers={this.state.show_hotspots ? this.state.hotspotmarkers : []}
                        markers={this.state.markers}
                        zoom={this.state.zoom}
                        onMarkerRightClick={_.noop}
                        onMarkerClick={this.handleMarkerClick}
                        onMarkerClose={this.handleMarkerClose}
                        onHotspotClick={this.handleHotspotClick}
                        onHotspotClose={this.handleHotspotClose}
                    />
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    let current_day = state.selects.current_day;
    let stats;
    let hotspots;
    let filteredStats;
    if (current_day != undefined) {
        current_day = moment.utc(current_day)
        stats = state.sqiDaily[current_day.format('YYYYMMDD')];
        if (stats != undefined) {
            hotspots = stats.hotspots;
            filteredStats = [];
            if (stats.items != undefined && state.sqiSorted.indexes.length > 0) {
                if (state.sqiSorted.indexes.length == stats.items.length)
                    filteredStats = stats.items;
                else state.sqiSorted.indexes.map((value) => {
                    filteredStats.push(stats.items[value]);
                });
            }
        }
    }
    return {
        current_day: current_day,
        stats: filteredStats,
        hotspots: hotspots,
        selectedCoordinate: state.sqiSorted.selectedCoordinate,
        sort_indexes: state.sqiSorted.indexes
    }
};
export default connect(
    mapStateToProps
)(SQIContainer)
