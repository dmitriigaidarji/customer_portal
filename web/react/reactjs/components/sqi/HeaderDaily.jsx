import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import logo from "../../static/media/rv_logo.png"

import 'airbnb-js-shims/target/es2015.js'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
import {connect} from 'react-redux'
import {selectCurrentDay} from '../../actions/selects'
import {fetchSqiDailyIfNeeded} from '../../actions/sqiDaily'
import Home from '../header/Home'

class HeaderDaily extends React.Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.toggleProviderDropdown = this.toggleProviderDropdown.bind(this);
        this.toggleLinksDropdown = this.toggleLinksDropdown.bind(this);
        this.state = {
            isOpen: false,
            providerDropdownOpen: false,
            linksDropdownOpen: false,
            month: undefined,
            focused: false,
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleProviderDropdown() {
        this.setState({
            providerDropdownOpen: !this.state.providerDropdownOpen
        });
    }

    toggleLinksDropdown() {
        this.setState({
            linksDropdownOpen: !this.state.linksDropdownOpen
        });
    }

    isOutsideRange(date) {
        return moment() < date.startOf('day');
    }

    render() {
        let {props} = this;
        let {dispatch, history, current_day} = props;
        return (
            <div>
                <Navbar color="faded" light toggleable fixed="top" className="consoleNav">
                    <NavbarToggler right onClick={this.toggle}/>
                    <Home/>
                    <NavLink href="#">SQI</NavLink>
                    <Nav navbar>
                        <NavItem>
                            <NavLink>
                                <i className="fa fa-calendar" aria-hidden="true" style={{marginRight: '10px'}}></i>
                                {current_day.format('MMMM')}
                            </NavLink>
                        </NavItem>
                        <SingleDatePicker
                            date={this.props.current_day} // momentPropTypes.momentObj or null,
                            onDateChange={(date) => {
                                dispatch(selectCurrentDay(date));
                                dispatch(fetchSqiDailyIfNeeded(date));
                                history.push(`/${process.env.PUBLIC_URL}/sqi/${date.format('YYYYMMDD')}/`);
                            }}
                            focused={this.state.focused} // PropTypes.bool
                            onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                            isOutsideRange={this.isOutsideRange}
                            numberOfMonths={1}
                            isDayBlocked={this.isDayBlocked}
                            displayFormat="YYYY/MM/DD"
                        />
                        <NavItem>
                            <NavLink>
                                <i className="fa fa-arrow-left" aria-hidden="true"
                                   style={{marginLeft: '10px', cursor: 'pointer'}} onClick={() => {
                                    let new_day = moment(current_day).subtract(1, "days")
                                    dispatch(selectCurrentDay(new_day));
                                    dispatch(fetchSqiDailyIfNeeded(new_day));
                                    history.push(`/${process.env.PUBLIC_URL}/sqi/${new_day.format('YYYYMMDD')}/`);
                                }}/>
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink>
                                <i className="fa fa-arrow-right" aria-hidden="true" style={{cursor: 'pointer'}} onClick={() => {
                                    let new_day = moment(current_day).add(1, "days")
                                    dispatch(selectCurrentDay(new_day));
                                    dispatch(fetchSqiDailyIfNeeded(new_day));
                                    history.push(`/${process.env.PUBLIC_URL}/sqi/${new_day.format('YYYYMMDD')}/`);
                                }}/>
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        )
    }
}

// const mapStateToProps = (state) => ({
//         current_day: moment.utc(current_)
// });

export default connect()(HeaderDaily)