import React from "react"
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {connect} from 'react-redux'
import {selectSortedData, panToCoordinate} from '../../actions/sqiSorted'
import moment from 'moment'
import {
    Link
} from 'react-router-dom'
let styles = {
    block: {
        marginTop: '15px'
    },
    title: {
        fontWeight: 'bold',
        fontSize: '110%'
    },
    filters: {
        fontSize: '90%',
        fontWeight: 400,
        marginLeft: '15px'
    }
};
class SqiTable extends React.Component {
    constructor(props) {
        super(props);
        let {dispatch} = props;
        this.state = {
            sorting: [{id: 'sqiTime', desc: true}],
            showFilters: false,
            uniq_users: [],
            uniq_versions: [],
            uniq_events: [],
            uniq_devices: [],
            uniq_ssids: [],
            selUser: '',
            selVersion: '',
            selEvent: '',
            selDevice: '',
            selSsid: '',
            resetTable: false,
            filteredData: undefined
        };
        this.toggleTableFilters = this.toggleTableFilters.bind(this)
        this.sortChange = this.sortChange.bind(this);
        this.setSortedIndexes = this.setSortedIndexes.bind(this);
        this.filterSelectChange = this.filterSelectChange.bind(this);
        this.setFilters = this.setFilters.bind(this);
        this.resetFilters = this.resetFilters.bind(this);
        let date = props.current_day == undefined ? moment() : props.current_day
        this.columns = [
            {
                id: 'sqiTime',
                Header: 'Time',
                accessor: d => {
                    if (('' + d.sqitime).length <= 10)
                        return moment.utc(parseInt(d.sqitime) * 1000).format('HH:mm')
                    else return moment.utc(parseInt(d.sqitime)).format('HH:mm')
                },
                width: 40
            },
            {
                id: 'cui',
                Header: 'Charge User',
                accessor: stat => stat.userid == '' ? '(unknown)' : stat.userid,
                Cell: (props) => {
                    if (props.value == '(unknown)')
                        return props.value;
                    return <Link
                        to={`/${process.env.PUBLIC_URL}/users/${props.value}/${date.format('YYYY')}/${date.format('MM')}/`}>{props.value}</Link>
                }
            },
            {
                id: 'version',
                Header: 'Version',
                accessor: 'version',
                width: 60
            },
            {
                id: 'sqievent',
                Header: 'SQI event',
                accessor: 'event',
                width: 100
            },
            {
                id: 'device_id',
                Header: 'Device ID',
                accessor: 'device_id',
                width: 60
            },
            {
                id: 'device_model',
                Header: 'Device',
                accessor: 'platform.device_model',
                width: 60
            },
            {
                id: 'os_ver',
                Header: 'OS Ver',
                Cell: d => d.original.platform == undefined ? '' :
                    d.original.platform.ios_version != undefined ?
                        d.original.platform.ios_version :
                        d.original.platform.android_version,
                width: 60
            },
            {
                id: 'ssid',
                Header: 'SSID',
                accessor: 'hotspot.ssid',
                width: 100
            },
            {
                id: 'bssid',
                Header: 'BSSID@RSSI',
                accessor: d => {
                    if (d.hotspot == undefined)
                        return '(unknown)';
                    return d.hotspot.bssid + '@' + d.hotspot.rssi
                },
                width: 70
            },
            {
                id: 'geo',
                Header: 'Geoposition',
                accessor: d => parseFloat(d.lat).toFixed(2) + ',' + parseFloat(d.lng).toFixed(2),
                Cell: d => {
                    if (d.value == '0.00,0.00')
                        return d.value;
                    return <a href="javascript:void(0);" onClick={() => {
                        dispatch(panToCoordinate(new google.maps.LatLng(d.original.lat, d.original.lng)));
                        {/*window.scrollTo(0,0);*/
                        }
                    }}>{d.value}</a>
                },
                width: 70
            },
            {
                Header: 'Secs',
                accessor: 'duration',
                width: 50
            }];
    }

    sortChange(column, shift) {
        let sort = {id: column.id};
        if (this.state.sorting.length && this.state.sorting[0].id == column.id)
            this.state.sorting[0].asc ? sort.desc = true : sort.asc = true
        else
            sort.asc = true;
        this.setState({
            sorting: [sort]
        })
    }

    toggleTableFilters() {
        this.setState({showFilters: !this.state.showFilters})
    }

    setSortedIndexes(indexes) {
        if (this.updateIndexes != undefined)
            window.clearTimeout(this.updateIndexes);
        this.updateIndexes = window.setTimeout(() => this.props.dispatch(selectSortedData(indexes)), 1000)
    }

    filterSelectChange(column, e) {
        console.log(column, e, e.target.value)
        let col = this.columns[column];
        this.refs.reactTable.filterColumn(col, e.target.value)
        // let accessor = col.accessor
        // if (col.id == 'cui')
        //     accessor = 'userid';
        // let {stats} = this.props;
        // let {statsIndexes} = this;
        // stats.map((item, index) => {
        //     let val = _get(item, accessor)
        //     if (val != undefined){
        //         if (!statsIndexes.includes(val))
        //             statsIndexes.push(val)
        //     }
        // })
    }

    componentDidMount() {
        let {stats} = this.props;
        if (stats != undefined) {
            this.setFilters(stats);
            // this.resetFilters()
        }
    }

    componentWillUpdate(nextProps) {
        let {stats} = nextProps;
        if (stats != undefined)
            if (this.props.stats == undefined || !nextProps.current_day.isSame(this.props.current_day)) {
                this.setFilters(stats);
                // this.resetFilters();
            }
    }

    componentDidUpdate() {
        if (this.state.resetTable) this.setState({resetTable: false});
    }

    resetFilters() {
        this.setState({
            selUser: '',
            selVersion: '',
            selEvent: '',
            selDevice: '',
            selSsid: '',
            resetTable: true
        })
    }

    setFilters(stats) {
        let uniq_users = [],
            uniq_versions = [],
            uniq_events = [],
            uniq_devices = [],
            uniq_ssids = [];

        stats.map((stat) => {
            if (stat.userid !== '' && stat.userid !== undefined)
                if (!uniq_users.includes(stat.userid))
                    uniq_users.push(stat.userid)
            if (stat.version !== '' && stat.version !== undefined)
                if (!uniq_versions.includes(stat.version))
                    uniq_versions.push(stat.version)
            if (stat.event !== '' && stat.event !== undefined)
                if (!uniq_events.includes(stat.event))
                    uniq_events.push(stat.event)
            if (stat.platform != undefined && stat.platform.device_model !== '' && stat.platform.device_model !== undefined)
                if (!uniq_devices.includes(stat.platform.device_model))
                    uniq_devices.push(stat.platform.device_model)
            if (stat.hotspot != undefined && stat.hotspot.ssid !== '' && stat.hotspot.ssid !== undefined)
                if (!uniq_ssids.includes(stat.hotspot.ssid))
                    uniq_ssids.push(stat.hotspot.ssid)
        });

        this.setState({
            uniq_users: uniq_users,
            uniq_versions: uniq_versions,
            uniq_events: uniq_events,
            uniq_devices: uniq_devices,
            uniq_ssids: uniq_ssids,
            selUser: '',
            selVersion: '',
            selEvent: '',
            selDevice: '',
            selSsid: '',
            resetTable: true
        })
    }

    render() {
        let {columns} = this;
        let {stats, date, dispatch} = this.props;
        if (stats == undefined || stats.length == 0)
            return <div>No data</div>
        let {
            uniq_users,
            uniq_versions,
            uniq_events,
            uniq_devices,
            uniq_ssids,
            selUser,
            selVersion,
            selEvent,
            selDevice,
            selSsid,
            resetTable
        } = this.state;
        if (resetTable)
            return <div></div>
        let pageSize = stats.length > 50 ? 50 : 20;
        let showPagination = true;
        return (
            <div>
                <div style={styles.block}>
                    <span style={styles.title}>SQI Table (Total: {stats.length})
                        <a onClick={this.toggleTableFilters} style={styles.filters} href="javascript:void(0);">Toggle filters</a>
                    </span>
                </div>
                <div style={{whiteSpace: 'normal'}}>
                    <select style={{width: '180px', margin: '0 10px 5px 0px'}} value={selUser}
                            onChange={(event) => {
                                this.filterSelectChange(1, event)
                                this.setState({selUser: event.target.value})
                            }} className="tableFilter">
                        <option value="">All Users</option>
                        {uniq_users.map((item, index) =>
                            <option value={item} key={index}>{item}</option>
                        )}
                    </select>
                    <select style={{maxWidth: '80px', margin: '0 10px 5px 0'}} value={selVersion}
                            onChange={(event) => {
                                this.filterSelectChange(2, event)
                                this.setState({selVersion: event.target.value})
                            }} className="tableFilter">
                        <option value="">All Versions</option>
                        {uniq_versions.map((item, index) =>
                            <option value={item} key={index}>{item}</option>
                        )}
                    </select>
                    <select style={{maxWidth: '100px', margin: '0 10px 5px 0'}} value={selEvent}
                            onChange={(event) => {
                                this.filterSelectChange(3, event)
                                this.setState({selEvent: event.target.value})
                            }} className="tableFilter">
                        <option value="">All Events</option>
                        {uniq_events.map((item, index) =>
                            <option value={item} key={index}>{item}</option>
                        )}
                    </select>
                    <select style={{maxWidth: '130px', margin: '0 10px 5px 0'}} value={selDevice}
                            onChange={(event) => {
                                this.filterSelectChange(5, event)
                                this.setState({selDevice: event.target.value})
                            }} className="tableFilter">
                        <option value="">All Devices</option>
                        {uniq_devices.map((item, index) =>
                            <option value={item} key={index}>{item}</option>
                        )}
                    </select>
                    <select style={{maxWidth: '130px', margin: '0 10px 5px 0'}} value={selSsid}
                            onChange={(event) => {
                                this.filterSelectChange(7, event)
                                this.setState({selSsid: event.target.value})
                            }} className="tableFilter">
                        <option value="">All SSIDs</option>
                        {uniq_ssids.map((item, index) =>
                            <option value={item} key={index}>{item}</option>
                        )}
                    </select>
                    <a href="javascript:void(0);" style={styles.filters} onClick={this.resetFilters}>Reset filters</a>
                </div>
                <ReactTable
                    ref="reactTable"
                    className='-striped -highlight'
                    data={stats}
                    showPagination={showPagination}
                    columns={columns}
                    defaultPageSize={pageSize}
                    filterable={this.state.showFilters}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        return row[id] !== undefined ? String(row[id]).toLowerCase() === filter.value.toLowerCase() : false
                    }}
                    defaultSorted={this.state.sorting}
                    // onSortedChange={this.sortChange}
                >
                    {(state, makeTable, instance) => {
                        let indexes = [];
                        state.sortedData.map((item) => {
                            indexes.push(item._index)
                        });
                        this.setSortedIndexes(indexes);
                        return makeTable()
                    }}
                </ReactTable>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let current_day = state.selects.current_day;
    let stats;
    if (current_day != undefined) {
        current_day = moment.utc(current_day)
        stats = state.sqiDaily[current_day.format('YYYYMMDD')];
        if (stats != undefined) {
            stats = stats.items;
        }
    }
    return {
        stats: stats,
        current_day: current_day
    }
};
export default connect(mapStateToProps)(SqiTable)
