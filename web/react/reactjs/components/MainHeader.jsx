import React from "react"
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavDropdown,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
}
    from 'reactstrap';
import {
    Link
} from 'react-router-dom'
import Radium from 'radium'
import {connect} from 'react-redux'
import {selectAccessLevel} from '../actions/selects'

let styles = {
    navlink: {
        color: '#e4e4e4',
        ':hover': {
            color: 'white'
        }
    }
}

@Radium
class MainHeader extends React.Component {
    constructor() {
        super()
        this.toggle = this.toggle.bind(this);
        this.toggleUserDropdown = this.toggleUserDropdown.bind(this);

        this.state = {
            isOpen: false,
            userDropDown: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleUserDropdown() {
        this.setState({
            userDropDown: !this.state.userDropDown
        });
    }

    componentDidMount() {
        let {dispatch, level} = this.props;
        dispatch(selectAccessLevel(level))
    }

    render() {
        let {access_level} = this.props;
        return (
            <div>
                <Navbar toggleable fixed="top" className='topNavigationBar'>
                    <NavbarToggler right onClick={this.toggle}/>
                    <NavbarBrand href="javascript:void(0);" style={{color:'white'}}>Roaming Portal</NavbarBrand>
                    <Nav navbar>
                        <Link to={`/${process.env.PUBLIC_URL}/`} className="nav-link mainheaderLink">Dashboard</Link>
                        {access_level !== 'END_USER' &&
                        <Link to={`/${process.env.PUBLIC_URL}/accounts/`}
                              className="nav-link mainheaderLink">Accounts</Link>
                        }
                        {access_level === 'ADMIN' &&
                        <Link to={`/${process.env.PUBLIC_URL}/subscribers/`} className="nav-link mainheaderLink">Subscribers</Link>
                        }
                        {CONST_CAN_HAVE_REFERRALS &&
                        <Link to={`/${process.env.PUBLIC_URL}/referrals/`} className="nav-link mainheaderLink">Referrals</Link>
                        }
                        <Link to={`/${process.env.PUBLIC_URL}/map`} className="nav-link mainheaderLink">Coverage
                            Map</Link>
                    </Nav>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav navbar className="ml-auto">
                            <NavDropdown isOpen={this.state.userDropDown} toggle={this.toggleUserDropdown}>
                                <DropdownToggle nav caret style={{color:'white'}}>{CONST_USERNAME}</DropdownToggle>
                                <DropdownMenu style={{color: 'black'}}>
                                    <DropdownItem>
                                        <NavLink href={CONST_LOG_OUT_URL}>Sign Out</NavLink>
                                    </DropdownItem>
                                </DropdownMenu>
                            </NavDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    access_level: state.selects.access_level
});
export default connect(mapStateToProps)(MainHeader)
