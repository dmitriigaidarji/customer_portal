import React from "react"
import {render} from "react-dom"
import {StyleRoot} from 'radium';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'
import MainController from "./customeraccess/containers/MainController"
import CoverageMapContainer from './containers/CoverageMapContainer'
import AccountsController from './containers/AccountsContainer'
import MainHeader from './components/MainHeader'

import './static/css/console.css';
import './static/css/accounts.css'
import {
    createStore,
    compose,
    applyMiddleware,
    combineReducers,
} from "redux"
import {Provider} from "react-redux"
import thunk from "redux-thunk"
import {persistStore, autoRehydrate} from 'redux-persist'
import localForage from 'localforage'
import {composeWithDevTools} from 'redux-devtools-extension'

// import 'bootstrap/dist/css/bootstrap.css';


import * as reducers from "./reducers"

let reducer = combineReducers(reducers);
const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk)),
    autoRehydrate()
);
class CustomerApp extends React.Component {
    constructor() {
        super();
        this.state = {rehydrated: false}
    }

    componentWillMount() {
        persistStore(store, {
            storage: localForage,
            blacklist: ['sqiDaily', 'userSqiDaily', 'coveragemap', 'sqiSorted', 'selects']
        }, () => {
            this.setState({rehydrated: true})
        })
    }
        render() {
        if (!this.state.rehydrated) {
            return <div>Loading...</div>
        }
            return (
                <Provider store={store}>
                    <StyleRoot>
                        <Router>
                        <div>
                            <MainHeader level="CUSTOMER"/>
                            <Switch>
                                <Route path={`/${process.env.PUBLIC_URL}/accounts`} name="accounts"
                                       component={AccountsController}/>
                                <Route path={`/${process.env.PUBLIC_URL}/map`} name="map"
                                       component={CoverageMapContainer}/>
                                <Route path='/' name="main" component={MainController}/>
                            </Switch>
                        </div>
                        </Router>
                    </StyleRoot>
                </Provider>
            )
        }
    }
render(<CustomerApp/>, document.getElementById('App1'))
