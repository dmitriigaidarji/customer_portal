import hashlib
import os

from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives, send_mail
from django.template import Context
from django.template import loader
from django.template.loader import get_template
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

import config
from email.mime.image import MIMEImage

import logging

from userarea import constants
from web.models import Referral
from django.urls import reverse

logger = logging.getLogger('web')

def getValueForEntitlement(entitlement):
    entitlement = int(entitlement)
    for pair in config.ACTIVATION_CODES_PRICES:
        if pair[0] == entitlement:
            return pair[1]
    raise IndexError('Entitlement of {} days not found in config'.format(entitlement))


def emailToken(email):
    hash_object = hashlib.sha256("{}{}".format(email, settings.SECRET_KEY))
    return hash_object.hexdigest()


def send_admin_registration_email(user):
    c = {
        'email': user.email,
        'domain': constants.FRAMEWORK_URL,
        'site_name': 'RoamVU',
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'user': user,
        'token': default_token_generator.make_token(user),
        'protocol': 'https',
    }
    subject_template_name = 'registration/password_reset_subject.txt'
    email_template_name = 'registration/password_reset_email.html'
    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)
    send_mail(subject, email, config.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)


def send_end_user_registration_email(user, bcc='blair.bullock@point-dume.com'):
    c = {
        'site_name': 'RoamVU',
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'user': user,
        'token': default_token_generator.make_token(user),
        'domain': constants.FRAMEWORK_URL,
        'protocol': 'https'
    }
    subject_template_name = 'email/beta_user_subject.txt'
    # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
    email_template_name = 'email/beta_user_email.html'
    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
    subject = loader.render_to_string(subject_template_name, c)
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)
    # msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [user.email])
    if bcc is not None:
        msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [user.email],
                                     bcc=[bcc], cc=['support@roamvu.com'])
    else:
        msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [user.email],
                                     cc=['support@roamvu.com'])
    msg.attach_alternative(email, "text/html")
    # msg.content_subtype = 'html'  # Main content is text/html
    msg.mixed_subtype = 'related'  # This is critical, otherwise images will be displayed as attachments!

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_1.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_1.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_2.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_2.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_3.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_3.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_4.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_4.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_5.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_5.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_6.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_6.png>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/beta_portrait/beta_portrait_7.png', 'rb').read())
    image.add_header('Content-ID', '<beta_portrait_7.png>')
    msg.attach(image)

    msg.send()
    # send_mail(subject, email, config.DEFAULT_FROM_EMAIL, [user.email], html_message=email, fail_silently=False)

    if bcc is not None:
        logger.info('Sent email to {}. Bcc: {}'.format(user.email, bcc))
    else:
        logger.info('Sent email to {}. No bcc included'.format(user.email))


def send_custom_end_user_registration_email(emailaddress, bodytext):
    c = {
        'site_name': 'RoamVU',
        'bodytext': bodytext,
        'domain': constants.FRAMEWORK_URL,
        'protocol': 'https'
    }
    subject_template_name = 'email/beta_user_subject.txt'
    # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
    email_template_name = 'email/custom_user_email.html'
    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
    subject = loader.render_to_string(subject_template_name, c)
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)

    msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [emailaddress])
    msg.attach_alternative(email, "text/html")
    msg.mixed_subtype = 'related'  # This is critical, otherwise images will be displayed as attachments!
    msg.send()

    logger.info('Sent email to {}. No bcc included'.format(emailaddress))



def old_send_end_user_registration_email(user, bcc='blair.bullock@point-dume.com'):
    c = {
        'site_name': 'RoamVU',
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'user': user,
        'token': default_token_generator.make_token(user),
        'domain': constants.FRAMEWORK_URL,
        'protocol': 'https'
    }
    subject_template_name = 'email/beta_user_subject.txt'
    # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
    email_template_name = 'email/beta_user_email.html'
    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
    subject = loader.render_to_string(subject_template_name, c)
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)
    if bcc is not None:
        msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [user.email],
                                     bcc=[bcc], cc=['support@roamvu.com'])
    else:
        msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [user.email],
                                     cc=['support@roamvu.com'])
    msg.attach_alternative(email, "text/html")
    # msg.content_subtype = 'html'  # Main content is text/html
    msg.mixed_subtype = 'related'  # This is critical, otherwise images will be displayed as attachments!

    image = MIMEImage(open('static/media/email/steps/step1.jpg', 'rb').read())
    image.add_header('Content-ID', '<step1.jpg>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/steps/step2.jpg', 'rb').read())
    image.add_header('Content-ID', '<step2.jpg>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/steps/step3.jpg', 'rb').read())
    image.add_header('Content-ID', '<step3.jpg>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/steps/step4.jpg', 'rb').read())
    image.add_header('Content-ID', '<step4.jpg>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/steps/step5a.jpg', 'rb').read())
    image.add_header('Content-ID', '<step5a.jpg>')
    msg.attach(image)

    image = MIMEImage(open('static/media/email/steps/step5b.jpg', 'rb').read())
    image.add_header('Content-ID', '<step5b.jpg>')
    msg.attach(image)

    msg.send()
    # send_mail(subject, email, config.DEFAULT_FROM_EMAIL, [user.email], html_message=email, fail_silently=False)

    if bcc is not None:
        logger.info('Sent email to {}. Bcc: {}'.format(user.email, bcc))
    else:
        logger.info('Sent email to {}. No bcc included'.format(user.email))


def send_referral_email(referral_id):
    referral = Referral.objects.get(pk=referral_id)
    c = {
        'parent_name': referral.name,
        'parent_email': referral.orderer.email,
        'register_link': 'https://{}{}'.format(constants.FRAMEWORK_URL,
                                               reverse('referrals', kwargs={'token': referral.token}))
    }
    subject_template_name = 'email/beta_user_subject.txt'
    email_template_name = 'email/referral_email.html'
    subject = loader.render_to_string(subject_template_name, c)
    subject = ''.join(subject.splitlines())
    email = loader.render_to_string(email_template_name, c)

    msg = EmailMultiAlternatives(subject, email, config.DEFAULT_FROM_EMAIL, [referral.email])
    msg.attach_alternative(email, "text/html")
    msg.send()

    logger.info('Sent email to referral {}. No bcc included'.format(referral.email))
    referral.status = Referral.STATUS_SENT
    referral.save()
