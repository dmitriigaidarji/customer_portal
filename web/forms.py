from django import forms
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

import config
from web.models import User, Domain


class CodesForm(forms.Form):
    prices = []
    for price in config.ACTIVATION_CODES_PRICES:
        prices.append((price[0], "{} credits for {} days".format(price[1], price[0])))

    code_entitlement = forms.ChoiceField(label="Entitlement", choices=prices,
                                         widget=forms.Select(attrs={'class': 'form-control'}))

    email = forms.EmailField(label='User email', max_length=100, required=False,
                             widget=forms.EmailInput(
                                 attrs={'class': 'codesemail form-control', 'placeholder': 'Email Address'}))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = forms.CharField(validators=[phone_regex], label='User Phone', max_length=30, required=False,
                            widget=forms.TextInput(
                                attrs={'class': 'codesphone form-control', 'placeholder': 'Phone Number: +9...'}))

    def clean(self):
        cleaned_data = super(CodesForm, self).clean()
        phone = cleaned_data.get('phone')
        email = cleaned_data.get('email')
        if (not phone or phone == '') and (not email or email == ''):
            error = ValidationError(_('Fill one of the fields'), code='empty')
            self.add_error('phone', error)
            self.add_error('email', error)


class CheckoutForm(forms.Form):
    confirm_hash = forms.CharField(
        max_length=50, required=True
    )


class RegistrationForm(forms.ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        if user:
            qs = user.customer.domain_set.all()
            self.fields['domain'].queryset = qs

    username = forms.CharField(max_length=30, required=True)
    domain = forms.ModelChoiceField(queryset=Domain.objects.all(), required=True)

    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   required=True, label='Role')

    class Meta:
        model = User
        fields = ['email', ]


class UserUpdateForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput(), required=False)
    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   required=True, label='Role')
    email = forms.EmailField(label='User email', max_length=100, disabled=True)

    class Meta:
        model = User
        fields = ['username', 'email', ]


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        'password_short': ("The password should be at least 6 characters long.")
    }
    new_password1 = forms.CharField(label=("Set your password"),
                                    widget=forms.PasswordInput(attrs={'placeholder': 'Minimum 6 characters long'}))
    new_password2 = forms.CharField(label=("Password confirmation"),
                                    widget=forms.PasswordInput(attrs={'placeholder': 'Minimum 6 characters long'}))

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if len(password1) < 6:
                raise forms.ValidationError(
                    self.error_messages['password_short'],
                    code='password_short'
                )
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2


class ReferralForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    business_name = forms.CharField(max_length=50, required=True,
                                    label='Business Name',
                                    widget=forms.TextInput(
                                        attrs={
                                            'class': 'form-control',
                                            'placeholder': 'Your Business Name',
                                            'data-container': 'body',
                                            'data-toggle': 'popover',
                                            'data-placement': 'top',
                                            'data-content': 'Please provide your complete company Name'
                                        }))
    domain = forms.CharField(max_length=50, required=True,
                             label='Domain Name',
                             widget=forms.TextInput(
                                 attrs={
                                     'class': 'form-control',
                                     'placeholder': 'myco.com',
                                     'data-container': 'body',
                                     'data-toggle': 'popover',
                                     'data-placement': 'top',
                                     'data-content': 'When roaming Internationally with RoamVU, what Domain should your users use?'
                                 }))


    admin_name = forms.CharField(max_length=30, required=True, label='Administrator Name',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class': 'form-control',
                                         'placeholder': 'Service Administrator Name',
                                         'data-container': 'body',
                                         'data-toggle': 'popover',
                                         'data-placement': 'top',
                                         'data-content': 'Please provide the name of your RoamVU.com Roaming Portal Administrator'
                                     }))

    admin_title = forms.CharField(max_length=30, required=True, label='Administrator Title',
                                 widget=forms.TextInput(
                                     attrs={
                                         'class': 'form-control',
                                         'placeholder': 'Chief Executor',
                                         'data-container': 'body',
                                         'data-toggle': 'popover',
                                         'data-placement': 'top',
                                         'data-content': 'Please provide the title of the person\'s position in the company'
                                     }))

    # admin_email = forms.EmailField(max_length=70, required=True,
    #                                label='Email',
    #                                widget=forms.EmailInput(
    #                                    attrs={
    #                                        'class': 'form-control',
    #                                        'placeholder': 'bob@example.com',
    #                                        'data-container': 'body',
    #                                        'data-toggle': 'popover',
    #                                        'data-placement': 'top',
    #                                        'data-content': 'Please provide the Email Address of your RoamVU.com Roaming Portal Administrator'
    #                                    }))
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = forms.CharField(validators=[phone_regex], label='Telephone', max_length=30, required=False,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control', 'placeholder': '+1-(555)-555-5555','data-container': 'body',
                                           'data-toggle': 'popover',
                                           'data-placement': 'top',
                                           'data-content': 'Please provide the Phone Number of your RoamVU.com Roaming Portal Administrator'
                                       }))

    country = forms.CharField(max_length=30, required=True, label='Country',
                              widget=forms.TextInput(attrs={
                                  'class': 'form-control',
                                  'placeholder': 'Country Name'
                              }))
    state = forms.CharField(max_length=30, required=True,
                            label='State',
                            widget=forms.TextInput(attrs={
                                'class': 'form-control',
                                'placeholder': 'State Name'
                            }))
    city = forms.CharField(max_length=30, required=True,
                           label='City',
                           widget=forms.TextInput(attrs={
                               'class': 'form-control',
                               'placeholder': 'City Name'
                           }))
    address = forms.CharField(max_length=50, required=True, label='Address',
                              widget=forms.TextInput(attrs={
                                  'class': 'form-control',
                                  'placeholder': 'Street and House Number',
                                  'data-container': 'body',
                                  'data-toggle': 'popover',
                                  'data-placement': 'top',
                                  'data-content': 'Please provide your Shipping Address'
                              }))
