#!/usr/bin/python2
from pymongo import MongoClient as Connection
from pymongo.errors import ConnectionFailure
from userarea import constants
# mongo database connections
def db_connect(dbhost, port):
    try:
        c = Connection(str(dbhost), port)
        status = 'success'
    except ConnectionFailure, e:
        status = 'fail'
        return status
    return status

#customerid = '700001'
#startday = datetime(2017, 1, 1)
#endday = datetime(2017, 2, 1)
#query = tdbQuery()
#query.pstats(customerid, startday, endday)
class tdbQuery():
    '''organizes the .querystring and .headers for different request types\nUse .available to view available methods'''
    def __init__(self):
        self.available = ['pstat', 'tx', 'attr']
        self.type = None
        self.collection = None
        self.query = None
    def pstats(self, startdate, enddate, customerid = 'all'):
        '''Accepts start and end datetimes and optional pdmaster provider id'''
        self.type = 'pstat'
        self.customerid = customerid
        self.collection = 'pstatdata'
        if self.customerid != 'all':
            self.query = {
                'provider_id':customerid,
                'date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
        else:
            self.query = {
                'date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
    def tx_closed(self, startdate, enddate, customerid = 'all'):
        '''Closed Sessions by start and end datetimes and optional provider/customer id'''
        self.type = 'tx'
        self.customerid = customerid
        self.collection = 'transactions'
        if self.customerid != 'all':
            self.query = {
                'provider_id':customerid,
                'status':'Session Closed',
                'transaction_date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
        else:
            self.query = {
                'status':'Session Closed',
                'transaction_date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
    def tx_open(self, startdate, enddate, customerid = 'all'):
        '''Open Sessions by start and end datetimes and optional provider/customer id'''
        self.type = 'tx'
        self.customerid = customerid
        self.collection = 'transactions'
        if self.customerid != 'all':
            self.query = {
                'provider_id':customerid,
                'status':'Session Started',
                'transaction_date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
        else:
            self.query = {
                'status':'Session Started',
                'transaction_date': {
                    '$gte' :startdate,
                    '$lt' :enddate}
                }
    def tx_user(self, startdate, enddate, cui):
        '''All User Sessions by start and end datetimes and CUI''' 
        self.type = 'tx'
        self.cui = cui
        self.collection = 'transactions'
        self.query = {
            'cui':cui,
            'transaction_date': {
                '$gte' :startdate,
                '$lt' :enddate}
            }
    def tx_query(self, query):
        '''Transactions by unstructured Mongo Query String'''
        self.type = 'tx'
        self.query = query
        self.collection = 'transactions'
    def attr_company(self, charge_period, customerid = 'all'):
        '''Montly charge period Attribution Summary Records for a roaming customer'''
        self.type = 'attr'
        self.customerid = customerid
        self.collection = 'attributions'
        if self.customerid != 'all':
            self.query = {
                'customer_id': customerid,
                'charge_period': charge_period
                }
        else:
            self.query = {
                'charge_period': charge_period
                }
    def attr_user(self, charge_period, nai):
        '''Montly charge period Attribution Summary Records for an individual NAI'''
        self.type = 'attr'
        self.collection = 'attributions'
        self.query = {
            'nai':nai,
            'charge_period': charge_period
            }
    def attr_hash(self, charge_period, hash_id):
        '''Montly charge period Attribution Summary Records for an individual HashID'''
        self.type = 'attr'
        self.collection = 'attributions'
        self.query = {
            'hash_id': hash_id,
            'charge_period': charge_period
            }
    #querystring = {'devices.device_id': 'EC13F826-93E4-467C-AAB3-104B083B2BA7'}
    #query = tdb.tdbQuery()
    #query.attr_query(querystring)
    #atrecords = tdb.tdbConnect(query)
    #atrecords.transdb()
    #atrecords.execute()
    #len(atrecords.documents)
    def attr_query(self, query):
        '''Attributions by unstructured Mongo Query String'''
        self.type = 'attr'
        self.collection = 'attributions'
        self.query = query

class tdbConnect(object):
    '''Inherits a DbQuery Object, assigns a Database, and executes the query'''
    def __init__(self, object):
        if isinstance(object, tdbQuery) is False:
            raise ValueError('DbData requires a DbQuery object')
        self.query = object.query
        self.type = object.type
        self.collection = object.collection
        self.dbhost = None
        self.port = None
        self.documents = []
    def transdb(self):
        '''Attaches the pdmaster database details to the inherited query object'''
        supported = ['pstat', 'tx', 'attr']
        if self.type not in supported:
            raise IOError('database does not support this query type')
        self.dbhost = constants.TRANSACTION_DB_HOST
        self.db = constants.TRANSACTION_DB
        self.port = constants.TRANSACTION_DB_PORT
        self.dbuser = constants.TRANSACTION_USER
        self.dbpassword = constants.TRANSACTION_PASS
    def execute(self):
        '''Runs the attached DbQuery object for the previously specified DB method and returns a list as data'''
        if self.dbhost == None:
            raise ValueError('no database method has been specified')
        self.status = db_connect(self.dbhost, self.port)
        if self.status == 'success':
            c = Connection(str(self.dbhost), self.port)
            db = c[self.db]
            try:
                assert db.client == c
            except AssertionError, e:
                pass
            if db.client == c:
                data = db[self.collection]
                records = data.find(self.query)
                for record in records:
                    self.documents.append(record)
                del self.db
                del self.dbuser
                del self.dbpassword
                del self.port
        
#######################################################
