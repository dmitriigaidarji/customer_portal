# from django.utils.translation import ugettext_lazy as _
from userarea import constants

ACTIVATION_CODE_DOMAIN = 'codes.roamvu.net'
ACTIVATION_CODES_SERVER = constants.ACTIVATION_CODES_SERVER
ACTIVATION_CODES_COUNT = 100
ACTIVATION_CODES_THRESHOLD = int(ACTIVATION_CODES_COUNT * 0.2)
ACTIVATION_CODES_CACHE_KEY = 'getting_new_codes'

APP_FULL_NAME = 'Customer Portal'


RADIUS_SERVER = constants.RADIUS_SERVER
RADIUS_SECRET = constants.RADIUS_SECRET
RADIUS_DICTIONARY = 'dictionary'
RADIUS_NAS_IDENTIFIER = APP_FULL_NAME


DEFAULT_FROM_EMAIL = 'support@roamvu.com'


SQI_LOCATION = '/opt/sqisync/'

# Prices in USD
PRICE_ENT_07 = 5
PRICE_ENT_10 = 10
PRICE_ENT_30 = 15

# (Entitlement, Price in USD)
ACTIVATION_CODES_PRICES = [
    (7, 5),
    (14, 10),
    (30, 15)
]