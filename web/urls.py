from django.conf.urls import url, include
import views
urlpatterns = [
    #url(r'^accounts/$', views.UserView.as_view(), name='users'),
    #url(r'^subscribers/$', views.SubscribersView.as_view(), name='subscribers'),
    # url(r'^map/$', views.MapView.as_view(), name='map'),
    url(r'^confirm/$', views.Payment.as_view(), name='payment'),
    url(r'^resend/$', views.Resend.as_view(), name='resend'),
    url(r'^delete/(?P<id>\d+)/$', views.UserDelete.as_view(), name='delete-user'),
    url(r'^resetpassword/(?P<id>\d+)/$', views.UserReset.as_view(), name='reset-user'),
    #url(r'^codes/', views.Codes.as_view(), name='codes'),
    url(r'api/', include('web.urlsapi')),
    url(r'^', views.Index.as_view(), name='main')
]
