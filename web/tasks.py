from __future__ import absolute_import, unicode_literals

import requests
from celery.utils.log import get_task_logger
from celery.task import periodic_task, task

from web import config

logger = get_task_logger(__name__)

"""
# Notify Codes Server about activating a code
#
"""


@task(bind=True, default_retry_delay=10, max_retries=5,
      options={'queue': 'default'}, queue='default')
def code_was_activated(self, code):
    r = requests.post(
        'http://{}/activate/'.format(config.ACTIVATION_CODES_SERVER), data={'code': code})
    if r.status_code == 200:
        logger.warning('Codes server was notified of code {} activation'.format(code))
    else:
        logger.error('Codes Server returned error code {} for code with id {} activation'.format(r.status_code, code))
        self.retry()

"""
# Notify Codes Server about issuing an activation code
#
"""


@task(bind=True, default_retry_delay=10, max_retries=5,
      options={'queue': 'default'}, queue='default')
def issue_activation_code(self, code_id):
    r = requests.post(
        'http://{}/codes/'.format(config.ACTIVATION_CODES_SERVER), data={'code_id': code_id})
    if r.status_code == 200:
        logger.warning('Code was issued {}'.format(code_id))
    else:
        logger.error('Codes Server returned error code {} for issuing code with id {}'.format(r.status_code, code_id))
        self.retry()


