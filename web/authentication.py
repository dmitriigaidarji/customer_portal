from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password, make_password
import pyrad.packet
from django.contrib.auth.models import Permission
from pyrad.client import Client, Timeout
from pyrad.dictionary import Dictionary
import config
import logging

logger = logging.getLogger('web')

# class LocalAuth(ModelBackend):
#     def __init__(self):
#         self.name = 'LOCAL-Authentication'
#         pass
#
#     def authenticate(self, username=None, password=None):
#         logger.info('{}. Trying to authenticate {}'.format(self.name, username))
#         try:
#             user = get_user_model().objects.get(username=username)
#         except get_user_model().DoesNotExist:
#             return None
#             # try:
#             #     user = get_user_model().objects.get(email=username)
#             # except get_user_model().DoesNotExist:
#             #     return None
#         if check_password(password, user.password):
#             return user
#         return None
#
#     def get_user(self, user_id):
#         try:
#             return get_user_model().objects.get(pk=user_id)
#         except get_user_model().DoesNotExist:
#             return None
#     def _get_user_permissions(self, user_obj):
#         return user_obj.user_permissions.all()
#
#     def _get_group_permissions(self, user_obj):
#         user_groups_field = get_user_model()._meta.get_field('groups')
#         user_groups_query = 'group__%s' % user_groups_field.related_query_name()
#         return Permission.objects.filter(**{user_groups_query: user_obj})
#
#     def _get_permissions(self, user_obj, obj, from_name):
#         """
#         Return the permissions of `user_obj` from `from_name`. `from_name` can
#         be either "group" or "user" to return permissions from
#         `_get_group_permissions` or `_get_user_permissions` respectively.
#         """
#         if not user_obj.is_active or user_obj.is_anonymous or obj is not None:
#             return set()
#
#         perm_cache_name = '_%s_perm_cache' % from_name
#         if not hasattr(user_obj, perm_cache_name):
#             if user_obj.is_superuser:
#                 perms = Permission.objects.all()
#             else:
#                 perms = getattr(self, '_get_%s_permissions' % from_name)(user_obj)
#             perms = perms.values_list('content_type__app_label', 'codename').order_by()
#             setattr(user_obj, perm_cache_name, set("%s.%s" % (ct, name) for ct, name in perms))
#         return getattr(user_obj, perm_cache_name)
#
#     def get_user_permissions(self, user_obj, obj=None):
#         """
#         Return a set of permission strings the user `user_obj` has from their
#         `user_permissions`.
#         """
#         return self._get_permissions(user_obj, obj, 'user')
#
#     def get_group_permissions(self, user_obj, obj=None):
#         """
#         Return a set of permission strings the user `user_obj` has from the
#         groups they belong.
#         """
#         return self._get_permissions(user_obj, obj, 'group')
#
#     def get_all_permissions(self, user_obj, obj=None):
#         if not user_obj.is_active or user_obj.is_anonymous or obj is not None:
#             return set()
#         if not hasattr(user_obj, '_perm_cache'):
#             user_obj._perm_cache = self.get_user_permissions(user_obj)
#             user_obj._perm_cache.update(self.get_group_permissions(user_obj))
#         return user_obj._perm_cache
#
#     def has_perm(self, user_obj, perm, obj=None):
#         if not user_obj.is_active:
#             return False
#         return perm in self.get_all_permissions(user_obj, obj)


class RadiusBackend(ModelBackend):
    def __init__(self):
        self.name = 'RADIUS-Authentication'
        pass

    def authenticate(self, username=None, password=None, **kwargs):
        logger.info('{}. Trying to authenticate {}'.format(self.name, username))
        srv = Client(server=config.RADIUS_SERVER,
                     secret=config.RADIUS_SECRET,
                     dict=Dictionary(config.RADIUS_DICTIONARY))

        req = srv.CreateAuthPacket(code=pyrad.packet.AccessRequest)
        req["User-Name"] = username
        req["User-Password"] = req.PwCrypt(password)
        req["NAS-Identifier"] = config.RADIUS_NAS_IDENTIFIER
        req["NAS-IP-Address"] = settings.ALLOWED_HOSTS[0]

        try:
            reply = srv.SendPacket(req)
        except:
            logger.warn('{}. Failed to login "{}" - Radius timeout error!'
                         .format(self.name, username))
            return None

        if reply.code == pyrad.packet.AccessAccept:
            try:
                user = get_user_model().objects.get(username=username)
            except get_user_model().DoesNotExist:
                user = get_user_model().objects.create_user(
                    username=username,
                    email=username,
                    password=None
                )
                user.set_unusable_password()
                user.save()
            logger.info('{}. Successfully authenticated "{}"'
                         .format(self.name, username))
            return user
        else:
            logger.warn('{}. Failed to login "{}" - Wrong password!'
                         .format(self.name, username))
            return None

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            logger.warn('{} failed. User does not exist!'.format(self.name))
            return None
