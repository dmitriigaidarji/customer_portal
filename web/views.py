import datetime
import json

from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import Permission
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.db import transaction
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from django.views.generic import FormView
from django.views.generic import UpdateView

from landing.models import Subscriber
from web import strings, config
from web.functions import getValueForEntitlement, emailToken, send_admin_registration_email, \
    send_end_user_registration_email
from web.models import Order, Transaction, ActivationCode, User, Radcheck, Referral
from web.tasks import issue_activation_code
from .forms import CodesForm, CheckoutForm, RegistrationForm, UserUpdateForm, SetPasswordForm, ReferralForm
from django.forms import formset_factory
from django.core import serializers
from django.template.loader import get_template
from django.template import Context

import logging

logger = logging.getLogger('web')


def get_user_permissions(user):
    if user.is_superuser:
        return Permission.objects.all()
    return user.user_permissions.all() | Permission.objects.filter(group__user=user)


class Payment(View):
    def get(self, request):
        return redirect('codes')

    def post(self, request):
        form = CheckoutForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                order = Order.objects.get(confirm_hash=form.cleaned_data['confirm_hash'])
                order.is_paid = True
                order.save()
                send_codes_for_order(order.id)
        return redirect('codes')


class Index(View):
    def get(self, request):
        level = 'end-user'
        customer_id = None
        if request.user.is_superuser:
            level = 'admin'
        elif request.user.groups.filter(name='Customer Administrator').exists():
            level = 'customer-administrator'

        if request.user is not None and request.user.customer is not None:
            customer_id = request.user.customer.customer_id

        can_have_referrals = False
        if level != 'end-user' and \
                        request.user.customer.referrals == True and \
                        request.user.customer.parent_id is None:
            can_have_referrals = True

        return render(request, 'web/dashboard/index.html',
                      {
                          'access': level,
                          'customer_id': customer_id,
                          'can_have_referrals': can_have_referrals
                      })


class Codes(View):
    def get(self, request):
        # TODO check for permissions
        # if request.user.has_perm('web.user.buy_codes'):
        if Permission.objects.get(codename='buy_codes') in get_user_permissions(request.user):
            orders_arr = []
            if Order.objects.filter(customer=request.user.customer).exists():
                orders = Order.objects.filter(customer=request.user.customer)
                for order in orders:
                    order_dict = {'value': order.value, 'is_paid': order.is_paid, 'transactions': []}
                    for transaction in order.transaction_set.all():
                        order_dict['transactions'].append({
                            'email': transaction.email,
                            'phone': transaction.phone,
                            'code': transaction.activation_code.code,
                            'entitlement': transaction.activation_code.entitlement,
                            'date': transaction.order_date.strftime('%m/%d/%Y'),
                            'price': float(transaction.price),
                            'status': transaction.delivery_status
                        })
                    orders_arr.append(order_dict)

            SendCodesFormSet = formset_factory(CodesForm)
            return render(request, 'web/codes/codes.html',
                          {'formset': SendCodesFormSet, 'orders': orders_arr})
        return redirect('main')

    def post(self, request):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user):
            # if request.user.has_perm('web.user.create_user'):
            SendCodesFormSet = formset_factory(CodesForm)
            formset = SendCodesFormSet(request.POST)
            if formset.is_valid():
                order = None
                with transaction.atomic():
                    order_value = 0
                    for entry in formset.cleaned_data:
                        order_value += getValueForEntitlement(entry['code_entitlement'])
                    order = Order.objects.create(
                        orderer=request.user,
                        customer=request.user.customer,
                        value=order_value
                    )
                    order_dict = {'value': order.value, 'is_paid': order.is_paid, 'transactions': []}
                    for entry in formset.cleaned_data:
                        act_code = ActivationCode.objects.select_for_update().filter(
                            entitlement=int(entry['code_entitlement']),
                            issue_date=None
                        ).first()
                        phone = entry['phone']
                        if phone == '':
                            phone = None
                        email = entry['email']
                        if email == '':
                            email = None
                        Transaction.objects.create(
                            order=order,
                            activation_code=act_code,
                            phone=phone,
                            email=email,
                            price=getValueForEntitlement(entry['code_entitlement'])
                        )
                        # transactions.append({''})
                        act_code.issue_date = datetime.date.today()
                        act_code.save()
                return render(request, 'web/codes/checkout.html',
                              {
                                  'form': CheckoutForm(initial={'confirm_hash': order.confirm_hash}),
                                  'order_id': order.id
                                  # 'transactions':
                              }
                              )
            return render(request, 'web/codes/codes.html', {'formset': formset})
        return redirect('main')


class Resend(View):
    def post(self, request):
        code = request.POST.get('code', None)
        if code:
            act_code = ActivationCode.objects.get(code=code)
            trans = act_code.transaction
            if trans.order.is_paid:
                deliver_code(code, act_code.pin, trans.email, trans.phone)
                trans.delivery_status = '2'
                trans.save()
                return JsonResponse({'message': 'success'})
        return JsonResponse({'message': 'fail'}, status=400)


class UnmatchedRedirect(View):
    def get(self, request):
        return redirect('landing')


def send_codes_for_order(order_id):
    order = Order.objects.get(pk=order_id)
    with transaction.atomic():
        for trans in order.transaction_set.all():
            try:
                act = trans.activation_code
                deliver_code(act.code, act.pin, trans.email)
                trans.delivery_status = '2'
                trans.save()
                issue_activation_code.delay(act.id)
            except Exception as exc:
                logger.error(str(exc))


def deliver_code(code, pin, email=None, phone=None):
    send_mail('Your RoamVu Activation Code!',
              'Code: {}\nPin: {}'.format(code, pin),
              'support@roamvu.com',
              [email], fail_silently=False)


class SubscribersView(View):
    def get(self, request):
        if request.user.is_superuser:
            testers = Subscriber.objects.filter(quiz__going_to_HK_or_JP_date__in=('0', '1'))
            others = Subscriber.objects.exclude(quiz__going_to_HK_or_JP_date__in=('0', '1'))
            return render(request, 'web/users/subscribers.html',
                          {
                              'testers': json.loads(
                                  serializers.serialize('json', testers)
                              ),
                              'subscribers': json.loads(
                                  serializers.serialize('json', others)
                              )
                          })
        return redirect('main')


class MapView(View):
    def get(self, request):
        return render(request, 'web/map/index.html')


class EndUserView(View):
    def get(self, request):
        # if request.user.has_perm('web.user.create_user'):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user):
            context = {'form': RegistrationForm(user=request.user), 'users': request.user.customer.user_set.all()}
            return render(request, 'web/users/users.html', context)
        return redirect('main')

    def post(self, request):
        form = RegistrationForm(None, request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.set_unusable_password()
            obj.username = "{}@{}".format(form.cleaned_data['username'], form.cleaned_data['domain'])
            obj.customer = request.user.customer
            obj.save()
            obj.groups.add(form.cleaned_data['group'])
            obj.username = '{}@{}'.format(form.cleaned_data['username'], form.cleaned_data['domain'])
            obj.save()

            # send register email
            send_end_user_registration_email(obj)
            return render(request, 'web/users/users.html',
                          {'form': RegistrationForm(user=request.user), 'users': request.user.customer.user_set.all()}
                          )
        return render(request, 'web/users/users.html', {'form': form})


class UserView(View):
    def get(self, request):
        # if request.user.has_perm('web.user.create_user'):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user):
            context = {'form': RegistrationForm(user=request.user), 'users': request.user.customer.user_set.all()}
            return render(request, 'web/users/users.html', context)
        return redirect('main')

    def post(self, request):
        # if request.user.has_perm('web.user.create_user'):
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user):
            form = RegistrationForm(None, request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.set_unusable_password()
                obj.username = "{}@{}".format(form.cleaned_data['username'], form.cleaned_data['domain'])
                obj.customer = request.user.customer
                obj.save()
                obj.groups.add(form.cleaned_data['group'])
                obj.username = '{}@{}'.format(form.cleaned_data['username'], form.cleaned_data['domain'])
                obj.save()

                # send register email
                send_end_user_registration_email(obj)
                return render(request, 'web/users/users.html',
                              {'form': RegistrationForm(user=request.user),
                               'users': request.user.customer.user_set.all()}
                              )
            return render(request, 'web/users/users.html', {'form': form})
        return redirect('main')


class UserDelete(View):
    def post(self, request, id):
        user_id = int(id)
        if Permission.objects.get(codename='create_user') in get_user_permissions(request.user) \
                and 0 < user_id != request.user.id:
            user = User.objects.get(pk=user_id)
            if user.groups.filter(name='End User').exists():
                delete_user_from_rad(user)
            user.delete()
            return redirect('users')
        return redirect('main')


class UserReset(View):
    def post(self, request, id):
        user_id = int(id)
        if Permission.objects.get(codename='create_user') in get_user_permissions(
                request.user) and 0 < user_id != request.user.id:
            user = User.objects.get(pk=user_id)
            send_end_user_registration_email(user, bcc=None)
            return redirect('users')
        return redirect('main')


class UserUpdateView(UpdateView):
    model = User
    template_name = 'web/users/edit.html'

    def get_initial(self):
        initial = super(UserUpdateView, self).get_initial()
        try:
            current_group = self.object.groups.get()
        except:
            # exception can occur if the edited user has no groups
            # or has more than one group
            pass
        else:
            initial['group'] = current_group.pk
            initial['email'] = self.object.email
            initial['id'] = self.kwargs['id']
        return initial

    def get_object(self, queryset=None):
        obj = User.objects.get(id=self.kwargs['id'])
        return obj

    def get_form_class(self):
        return UserUpdateForm

    def form_valid(self, form):
        self.object.groups.clear()
        self.object.groups.add(form.cleaned_data['group'])
        return super(UserUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('users')


class ReferralConfirm(View):
    template_name = "referrals/index.html"
    form_class = ReferralForm

    def submit_referral(self, token, data):
        referral = Referral.objects.get(token=token)
        referral.business_name = data['business_name']
        referral.domain = data['domain']
        referral.admin_name = data['admin_name']
        referral.admin_title = data['admin_title']
        referral.phone = data['phone']
        referral.country = data['country']
        referral.state = data['state']
        referral.city = data['city']
        referral.address = data['address']
        referral.status = Referral.STATUS_FILLED
        referral.response_date = datetime.datetime.now()
        referral.save()

    def get(self, request, token):
        referral = Referral.objects.get(token=token)
        return render(request, self.template_name, {'form': self.form_class})

    def post(self, request, token):
        assert token is not None
        form = self.form_class(request.POST)
        if form.is_valid():
            self.submit_referral(token, form.cleaned_data)
            return render(request, 'referrals/thanks.html')
        return render(request, self.template_name, {'form': form})

class PasswordResetConfirmView(FormView):
    template_name = "registration/password_set.html"
    success_url = '/thanks/'
    form_class = SetPasswordForm

    def get(self, request, uidb64=None, token=None, *arg, **kwargs):
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            return render(request, self.template_name, {'form': self.form_class})
        elif user is not None:
            send_end_user_registration_email(user, bcc=None)
            messages.error(request,
                           'The reset password link is no longer valid. A new reset password email was sent to your email {}'.format(
                               user.email))
        else:
            messages.error(request, 'The reset password link is no longer valid.')
        return render(request, self.template_name, {'form': self.form_class})

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                if user.groups.filter(name='End User').exists():
                    add_user_to_rad(user, new_password)
                user.is_active = True
                user.save()
                self.success_url = '{}?username={}'.format(self.success_url, user.username)
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        elif user is not None:
            send_end_user_registration_email(user, bcc=None)
            messages.error(request,
                           'The reset password link is no longer valid. A new reset password email was sent to your email {}'.format(
                               user.email))
            return self.form_invalid(form)
        else:
            messages.error(request, 'The reset password link is no longer valid.')
            return self.form_invalid(form)


def add_user_to_rad(user, password):
    if Radcheck.objects.filter(
            username=user.username,
            attribute=strings.RADIUS_ATTRIBUTE_PASSWORD
    ).exists():
        rad = Radcheck.objects.get(
            username=user.username,
            attribute=strings.RADIUS_ATTRIBUTE_PASSWORD
        )
        rad.value = password
        rad.save()
    else:
        Radcheck.objects.create(
            username=user.username,
            attribute=strings.RADIUS_ATTRIBUTE_PASSWORD,
            op=strings.RADIUS_ATTRIBUTE_OP_EQUAL,
            value=password
        )


def delete_user_from_rad(user):
    if Radcheck.objects.filter(
            username=user.username,
            attribute=strings.RADIUS_ATTRIBUTE_PASSWORD
    ).exists():
        Radcheck.objects.filter(
            username=user.username,
        ).delete()


class CheckIfAlive(View):
    def get(self, request):
        return HttpResponse('I am alive!')

    def post(self, request):
        return HttpResponse('I am alive!')


@method_decorator(csrf_exempt, name='dispatch')
class ApplicationEvents(View):
    def post(self, request):
        logger.info(request.body)
        event = request.POST.get('event')
        if event == 'ACCOUNT_WAS_ACTIVATED':
            try:
                date = datetime.datetime.strptime(request.POST.get('date'), '%Y%m%d')
            except:
                return JsonResponse({'data': request.body, 'message': 'Wrong date format'}, status=400)
            try:
                username = request.POST.get('username')
                username_split = username.split('@')
                if username_split[1] == config.ACTIVATION_CODE_DOMAIN:
                    act_code = ActivationCode.objects.get(code=username_split[0])
                    act_code.activated = date
                    act_code.save()
                else:
                    user = User.objects.get(username=request.POST.get('username'))
                    user.activated = date
                    user.save()
            except:
                return JsonResponse({'data': request.body, 'message': 'No such user'}, status=404)
            return JsonResponse({'data': request.body, 'message': 'User updated'})
        return JsonResponse({'data': request.body, 'message': 'Unknown event'}, status=400)


