from __future__ import unicode_literals

import random
import string

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models

from web import strings
import uuid as uuid

class ProvisionCountry(models.Model):
    code = models.IntegerField(primary_key=True)
    country_name = models.CharField(max_length=40)
    iso_code = models.CharField(max_length=2)
    active_pop = models.CharField(default='0', max_length=2)
    center_lat = models.FloatField(null=True)
    center_lng = models.FloatField(null=True)
    class Meta:
        app_label = 'provision'
        db_table = 'countries'


# Provisioning Table
class ProvisionCustomer(models.Model):
    customer_id = models.IntegerField(primary_key=True)
    customer_name = models.CharField(max_length=64)
    country = models.ForeignKey(ProvisionCountry, related_name='+', db_column='country')
    service = models.CharField(max_length=10)
    parent_id = models.IntegerField(null=True)
    status = models.CharField(max_length=16)
    portal_admin = models.CharField(max_length=32, null=True)
    admin_email = models.CharField(max_length=32, null=True)
    email = models.CharField(max_length=32, null=True)
    referrals = models.BooleanField(default=False)
    on_portal = models.BooleanField(default=False)
    max_users = models.IntegerField(null=True)
    max_codes = models.IntegerField(null=True)
    create_date = models.CharField(max_length=32)
    mod_date = models.CharField(max_length=32)

    class Meta:
        app_label = 'provision'
        db_table = 'customers'


class ProvisionRealm(models.Model):
    customer_id = models.IntegerField()
    realm = models.CharField(max_length=128, primary_key=True)
    allow_subdomain = models.CharField(max_length=1)
    create_date = models.CharField(max_length=32)
    mod_date = models.CharField(max_length=32)

    class Meta:
        app_label = 'provision'
        db_table = 'realms'



class Radcheck(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=128)
    attribute = models.CharField(max_length=64)
    op = models.CharField(max_length=2, default=strings.RADIUS_ATTRIBUTE_OP_EQUAL)
    value = models.CharField(max_length=253)

    class Meta:
        app_label = 'radius'
        db_table = 'radcheck'

class Country(models.Model):
    code = models.IntegerField(primary_key=True)
    country_name = models.CharField(max_length=40)
    iso_code = models.CharField(max_length=2)
    active_pop = models.CharField(default='0',max_length=2)
    center_lat = models.FloatField(null=True)
    center_lng = models.FloatField(null=True)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.country_name

class Customer(models.Model):
    customer_id = models.IntegerField()
    is_active = models.BooleanField(default=False)

    customer_name = models.CharField(max_length=100, unique=True)
    country = models.ForeignKey(Country, related_name='+', db_column='country', null=True)
    service = models.CharField(max_length=10, null=True)
    parent_id = models.IntegerField(null=True)
    status = models.CharField(max_length=16, null=True)
    portal_admin = models.CharField(max_length=32, null=True)
    admin_email = models.CharField(max_length=32, null=True)
    email = models.CharField(max_length=32, null=True)
    referrals = models.BooleanField(default=False)
    on_portal = models.BooleanField(default=False)
    max_users = models.IntegerField(null=True)
    max_codes = models.IntegerField(null=True)
    create_date = models.CharField(max_length=32)
    mod_date = models.CharField(max_length=32)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def source_model(self):
        return ProvisionCustomer.objects.get(customer_id=self.customer_id)

    def __str__(self):
        return self.customer_name

class Domain(models.Model):
    name = models.CharField(max_length=128, unique=True)
    allow_subdomain = models.CharField(max_length=1, null=True)

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class UserManager(BaseUserManager):
    def create_user(self, username, email, password, **kwargs):
        user = self.model(
            username=username,
            email=self.normalize_email(email),
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **kwargs):
        user = self.model(
            username=username,
            email=email,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    username = models.EmailField(unique=True, max_length=100)
    email = models.EmailField(max_length=100)

    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)

    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    token = models.UUIDField(default=uuid.uuid4, editable=False)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    activated = models.DateTimeField(null=True)

    objects = UserManager()

    def get_username(self):
        return self.username

    def get_full_name(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        return self.get_username()

    def get_short_name(self):
        if self.first_name:
            return self.first_name
        else:
            return self.get_username()

    class Meta:
        ordering = ('id',)
        permissions = (
            ("buy_codes", "Can order new activation codes"),
            ("view_customer_transactions", "Can view company transactions"),
            ("create_user", "Can create new company users"),
            ("invoice", "Manage payments"),
            ("statistics", "Access to statistics of all company users"),
            ("network_status", "Network status")
        )

# class Domain(models.Model):
#     name = models.CharField(max_length=100, unique=True)
#     customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
#
#     def source_model(self):
#         return ProvisionRealm.objects.get(customer_id=self.customer_id)
#
#     def __str__(self):
#         return self.name


class DomainRequest(models.Model):
    name = models.CharField(max_length=100, unique=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    STATUS_PENDING = 'PENDING'
    STATUS_ACCEPTED = 'ACCEPTED'
    STATUS_DENIED = 'DENIED'
    STATUS_ACTIVE = 'ACTIVE'
    STATUS_CHOICES = (
        (STATUS_PENDING, 'Pending'),
        (STATUS_ACCEPTED, 'Accepted'),
        (STATUS_DENIED, 'Denied'),
        (STATUS_ACTIVE, 'Active')
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=STATUS_PENDING)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name




def random_order_confirm_hash():
    return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(50))


class Order(models.Model):
    orderer = models.ForeignKey(User, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=8, decimal_places=2)
    is_paid = models.BooleanField(default=False)
    confirm_hash = models.CharField(
        max_length=50,
        default=random_order_confirm_hash
    )

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class ActivationCode(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    code = models.CharField(max_length=20)
    pin = models.CharField(max_length=10)
    entitlement = models.IntegerField(default=7)
    reserve_date = models.DateField(null=True)
    reserve_entitlement = models.IntegerField(null=True)
    issue_date = models.DateField(null=True)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    activated = models.DateTimeField(null=True)
    class Meta:
        unique_together = ("code", "pin")


class Transaction(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    activation_code = models.OneToOneField(ActivationCode, on_delete=models.CASCADE)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(validators=[phone_regex], blank=True, max_length=20, null=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    order_date = models.DateTimeField(auto_now=True)
    DELIVERY_STATUS_CHOICES = (
        ('0', 'Not sent'),
        ('1', 'Pending'),
        ('2', 'Sent'),
        ('3', 'Failed'),
        ('3', 'Activated'),
    )
    delivery_status = models.CharField(max_length=2, choices=DELIVERY_STATUS_CHOICES, default='0')
    price = models.DecimalField(max_digits=8, decimal_places=2)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class Referral(models.Model):
    parent = models.ForeignKey(Customer, on_delete=models.CASCADE)
    child = models.OneToOneField(Customer, on_delete=models.CASCADE, null=True, related_name='+')
    orderer = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)

    STATUS_SENDING = 'Sending'
    STATUS_INVALID = 'Invalid'
    STATUS_SENT = 'Sent'
    STATUS_REVIEWED = 'Reviewed'
    STATUS_FILLED = 'Responded'
    STATUS_SUBMITTED = 'Submitted'
    STATUS_COMPETED = 'Completed'
    STATUS_REJECTED = 'Rejected'
    STATUS_CANCELED = 'Canceled'
    STATUS_CHOICES = (
        (STATUS_SENDING, 'Sending'),
        (STATUS_INVALID, 'Invalid'),
        (STATUS_SENT, 'Sent'),
        (STATUS_REVIEWED, 'Reviewed'),
        (STATUS_FILLED, 'Responded'),
        (STATUS_SUBMITTED, 'Submitted'),
        (STATUS_COMPETED, 'Completed'),
        (STATUS_REJECTED, 'Rejected'),
        (STATUS_CANCELED, 'Canceled')
    )
    status = models.CharField(max_length=12, choices=STATUS_CHOICES, default=STATUS_SENDING)

    token = models.UUIDField(default=uuid.uuid4)

    business_name = models.CharField(max_length=50, null=True)
    domain = models.CharField(max_length=50, null=True)
    admin_name = models.CharField(max_length=50, null=True)
    admin_title = models.CharField(max_length=50, null=True)
    phone = models.CharField(max_length=50, null=True)
    country = models.CharField(max_length=50, null=True)
    state = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=50, null=True)

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    submit_date = models.DateTimeField(null=True)
    response_date = models.DateTimeField(null=True)