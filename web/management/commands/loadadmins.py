from django.contrib.auth.models import Group
from django.core.cache import cache
from django.core.management import BaseCommand
from django.core.management import CommandError
from django.db import transaction

from web.functions import send_admin_registration_email
from web.models import ProvisionCustomer, ProvisionRealm, ProvisionCountry, \
    Customer, Country, Domain, User


class Command(BaseCommand):
    def sync_countries(self):
        countries = ProvisionCountry.objects.all()
        for country in countries:
            if Country.objects.filter(code=country.code).exists():
                existent = Country.objects.get(code=country.code)
                existent.country_name = country.country_name
                existent.iso_code = country.iso_code
                existent.active_pop = country.active_pop
                existent.center_lat = country.center_lat
                existent.center_lng = country.center_lng
                existent.save()
            else:
                Country.objects.create(
                    code=country.code,
                    country_name=country.country_name,
                    iso_code=country.iso_code,
                    active_pop=country.active_pop,
                    center_lat=country.center_lat,
                    center_lng=country.center_lng
                )

    def handle(self, *args, **options):
        with transaction.atomic():
            self.sync_countries()
            all_customers = ProvisionCustomer.objects.filter(status__in=['Production', 'In-Test'], on_portal=True)
            provider_customers = all_customers.filter(service__in=['Two-way', 'Provider'])
            for new_customer in provider_customers:
                if not Customer.objects.filter(customer_id=new_customer.customer_id).exists():
                    Customer.objects.create(
                        customer_id=new_customer.customer_id,
                        customer_name=new_customer.customer_name,
                        country=Country.objects.get(code=new_customer.country.code),
                        service=new_customer.service,
                        parent_id=new_customer.parent_id,
                        status=new_customer.status,
                        portal_admin=new_customer.portal_admin,
                        admin_email=new_customer.admin_email,
                        email=new_customer.email,
                        create_date=new_customer.create_date,
                        mod_date=new_customer.mod_date,
                        referrals=new_customer.referrals,
                        on_portal=new_customer.on_portal,
                        max_users=new_customer.max_users,
                        max_codes=new_customer.max_codes,
                        is_active=True
                    )
                else:
                    customer = Customer.objects.get(customer_id=new_customer.customer_id)
                    customer.customer_name = new_customer.customer_name
                    customer.country = Country.objects.get(code=new_customer.country.code)
                    customer.service = new_customer.service
                    customer.parent_id = new_customer.parent_id
                    customer.status = new_customer.status
                    customer.portal_admin = new_customer.portal_admin
                    customer.admin_email = new_customer.admin_email
                    customer.email = new_customer.email
                    customer.create_date = new_customer.create_date
                    customer.mod_date = new_customer.mod_date
                    customer.referrals = new_customer.referrals
                    customer.on_portal = new_customer.on_portal
                    customer.max_users = new_customer.max_users
                    customer.max_codes = new_customer.max_codes
                    customer.is_active = True
                    customer.save()
            provision_customers = all_customers \
                .filter(service__in=['Two-way', 'Roamer'])
            Customer.objects.all().update(is_active=False)
            for new_customer in provision_customers:
                if not Customer.objects.filter(customer_id=new_customer.customer_id).exists():
                    customer = Customer.objects.create(
                        customer_id=new_customer.customer_id,
                        customer_name=new_customer.customer_name,
                        country=Country.objects.get(code=new_customer.country.code),
                        service=new_customer.service,
                        parent_id=new_customer.parent_id,
                        status=new_customer.status,
                        portal_admin=new_customer.portal_admin,
                        admin_email=new_customer.admin_email,
                        email=new_customer.email,
                        create_date=new_customer.create_date,
                        mod_date=new_customer.mod_date,
                        referrals=new_customer.referrals,
                        on_portal=new_customer.on_portal,
                        max_users=new_customer.max_users,
                        max_codes=new_customer.max_codes,
                        is_active=True
                    )
                    realms = ProvisionRealm.objects.filter(customer_id=new_customer.customer_id)
                    for realm in realms:
                        dom = Domain.objects.create(
                            customer=customer,
                            name=realm.realm,
                            allow_subdomain=realm.allow_subdomain,
                        )
                        if new_customer.admin_email is not None:
                            create_admin_user_for_realm(dom)
                else:
                    customer = Customer.objects.get(customer_id=new_customer.customer_id)
                    customer.customer_name = new_customer.customer_name
                    customer.country = Country.objects.get(code=new_customer.country.code)
                    customer.service = new_customer.service
                    customer.parent_id = new_customer.parent_id
                    customer.status = new_customer.status
                    customer.portal_admin = new_customer.portal_admin
                    customer.admin_email = new_customer.admin_email
                    customer.email = new_customer.email
                    customer.create_date = new_customer.create_date
                    customer.mod_date = new_customer.mod_date
                    customer.referrals = new_customer.referrals
                    customer.on_portal = new_customer.on_portal
                    customer.max_users = new_customer.max_users
                    customer.max_codes = new_customer.max_codes
                    customer.is_active = True
                    realms = ProvisionRealm.objects.filter(customer_id=new_customer.customer_id)
                    for realm in realms:
                        if not Domain.objects.filter(customer=customer, name=realm.realm).exists():
                            dom = Domain.objects.create(
                                customer=customer,
                                name=realm.realm,
                                allow_subdomain=realm.allow_subdomain,
                            )
                            if new_customer.admin_email is not None:
                                create_admin_user_for_realm(dom)
                        else:
                            dom = Domain.objects.get(customer=customer, name=realm.realm)
                            dom.allow_subdomain = realm.allow_subdomain
                            dom.save()
                            if new_customer.admin_email is not None and not User.objects.filter(
                                    username='admin@{}'.format(dom.name)).exists():
                                create_admin_user_for_realm(dom)
                    customer.save()
            cache.delete_pattern('console*')


def create_admin_user_for_realm(domain):
    # if not User.objects.filter(email=domain.customer.admin_email).exists():
    user = User.objects.create(
        username='admin@{}'.format(domain.name),
        email=domain.customer.admin_email,
        customer=domain.customer
    )
    user.groups.add(Group.objects.get(name='Customer Administrator'))
    send_admin_registration_email(user)
