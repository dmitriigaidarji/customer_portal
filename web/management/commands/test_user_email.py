from django.core.management import BaseCommand
from web.models import User
from web.functions import send_end_user_registration_email


class Command(BaseCommand):
    def handle(self, *args, **options):
        send_end_user_registration_email(User.objects.get(email='dmitriigaidarji@gmail.com'))
