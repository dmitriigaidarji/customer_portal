from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from web.models import User
from django.core.management import BaseCommand




class Command(BaseCommand):
    help = 'Creates initial user groups'

    def handle(self, *args, **options):
        new_group, created = Group.objects.get_or_create(name='Customer Administrator')
        new_group.permissions.add(Permission.objects.get(codename='buy_codes'),
                                  Permission.objects.get(codename='create_user'),
                                  Permission.objects.get(codename='view_customer_transactions'))
        new_group.save()

        new_group, created = Group.objects.get_or_create(name='Operations')
        new_group.permissions.add(Permission.objects.get(codename='buy_codes'),
                                  Permission.objects.get(codename='create_user'),
                                  Permission.objects.get(codename='view_customer_transactions'))
        new_group.save()

        new_group, created = Group.objects.get_or_create(name='Billing User')
        new_group.permissions.add(Permission.objects.get(codename='invoice'))
        new_group.save()

        new_group, created = Group.objects.get_or_create(name='Reporting User')
        new_group.permissions.add(Permission.objects.get(codename='statistics'))
        new_group.save()

        new_group, created = Group.objects.get_or_create(name='Support User')
        new_group.permissions.add(Permission.objects.get(codename='network_status'))
        new_group.save()

        new_group, created = Group.objects.get_or_create(name='End User')
        new_group.save()


