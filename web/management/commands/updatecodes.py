import datetime

from web import config
from django.core.management import BaseCommand, CommandError

import requests

from web.models import ActivationCode
import logging

logger = logging.getLogger("web")


class Command(BaseCommand):
    help = 'Get and extend activation codes'

    def handle(self, *args, **options):
        update()


def update():
    # remove outdated codes
    existing_codes = ActivationCode.objects.filter(issue_date=None)
    if len(existing_codes) > 0:
        for code in existing_codes:
            if code.reserve_date < datetime.date.today() - datetime.timedelta(days=code.reserve_entitlement):
                code.delete()

    amount_to_request = config.ACTIVATION_CODES_COUNT * 3 - ActivationCode.objects.filter(issue_date=None).count()
    if amount_to_request > 0:
        existing_codes = ActivationCode.objects.filter(issue_date=None)
        if len(existing_codes) > 0:
            ids = []
            for code in existing_codes:
                ids.append(code.pk)
            r = requests.get('http://{}/codes/?count={}&ids={}&identifier={}'.format(
                config.ACTIVATION_CODES_SERVER,
                amount_to_request,
                ','.join(str(code_id) for code_id in ids),
                config.APP_FULL_NAME)
            )
        else:
            r = requests.get('http://{}/codes/?count={}&identifier={}'.format(
                config.ACTIVATION_CODES_SERVER,
                config.ACTIVATION_CODES_COUNT,
                config.APP_FULL_NAME))
        if r.status_code == 200:
            entries = []
            try:
                codes_array = r.json()['activation_codes']
                for code in codes_array:
                    try:
                        entries.append(ActivationCode(
                            id=int(code['id']),
                            code=code['code'],
                            pin=code['pin'],
                            entitlement=code['entitlement'],
                            reserve_date=datetime.datetime.utcfromtimestamp(float(code['reserved_date'])),
                            reserve_entitlement=code['reserved_entitlement']
                        ))
                    except:
                        raise CommandError('Error parsing activation code json')
            except:
                raise CommandError('Error parsing response object')
            ActivationCode.objects.bulk_create(entries)
            # self.stdout.write('Extended {} codes. Added {} new activation codes'.format(
            #     len(existing_codes), len(entries)))
            logger.info('Extended {} codes. Added {} new activation codes'.format(
                len(existing_codes), len(entries)))
        else:
            logger.error('Received "{}" status code with text: "{}"'.format(r.status_code, r.text))
            raise CommandError('Received "{}" status code with text: "{}"'.format(r.status_code, r.text))
    else:
        existing_codes = ActivationCode.objects.filter(issue_date=None)
        if len(existing_codes) > 0:
            ids = []
            for code in existing_codes:
                ids.append(code.pk)
            r = requests.get('http://{}/codes/?ids={}&identifier={}'.format(
                config.ACTIVATION_CODES_SERVER,
                ','.join(str(code_id) for code_id in ids),
                config.APP_FULL_NAME)
            )
            if r.status_code == 200:
                logger.info('Extended {} codes. Requested 0 new ones.'.format(len(existing_codes)))
            else:
                logger.error('Received "{}" status code with text: "{}"'.format(r.status_code, r.text))
                raise CommandError('Received "{}" status code with text: "{}"'.format(r.status_code, r.text))
